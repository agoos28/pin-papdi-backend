<?php // @version $Id: default.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
$u =& JURI::getInstance();
$b =& JURI::base();
$c = JURI::current();
$tcount = modLoginHelper::getTransactionCount();
$rcount = modLoginHelper::getRentedProducts();

?>
<?php if ($type == 'logout') : ?>
<li class="nav-item dropdown">
	<a href="#" data-toggle="dropdown" class="profile-dropdown-toggle nav-link dropdown-toggle d-flex align-items-center padding-sm-10"
		 aria-expanded="false">
		<span class="p-r-5"><?php echo $user->name; ?></span>
		<span class="thumbnail-wrapper d32 circular inline">
						<img src="https://www.tutorialrepublic.com/examples/images/avatar/2.jpg" class="avatar" alt="Avatar">
					</span>
		<b class="caret"></b>
	</a>
	<ul class="dropdown-menu no-padding">
		<li>
			<a class="list-group-item <?php if ($c == $b . 'transactions') {
				echo 'current';
			} ?>" href="<?php echo $b; ?>transactions" title="<?php echo JText::_('TRANSACTION_HISTORY'); ?>"> <span
					class="badge"><?php echo $tcount; ?></span> <span
					class="fa fa fa-angle-right"></span> <?php echo JText::_('TRANSACTION_HISTORY'); ?></a>
		</li>
		<li>
			<a class="list-group-item <?php if ($c == $b . 'my-rented-items') {
				echo 'current';
			} ?>" href="<?php echo $b; ?>my-rented-items" title="My Rented Items"> <span
					class="badge"><?php echo $rcount; ?></span> <span class="fa fa fa-angle-right"></span> My Rented Items</a>
		</li>
		<li>
			<a class="list-group-item <?php if ($c == $b . 'my-account') {
				echo 'current';
			} ?>" href="<?php echo $b; ?>my-account" title="<?php echo JText::_('MY_ACCOUNT'); ?>"><span
					class="fa fa fa-angle-right"></span> <?php echo JText::_('MY_ACCOUNT'); ?></a>
		</li>

		<?php if ($user->usertype == 'Super Administrator') { ?>
			<li>
				<a class="list-group-item" target="_blank" href="<?php echo $b; ?>administration"
					 title="<?php echo JText::_('ADMIN_PAGE'); ?>"><span
						class="fa fa fa-angle-right"></span> <?php echo JText::_('ADMIN_PAGE'); ?></a>
			</li>
		<?php } else { ?>
			<li>
				<a class="list-group-item" href="<?php echo $b; ?>contact" title="<?php echo JText::_('HELP'); ?>"><span
						class="fa fa fa-angle-right"></span> <?php echo JText::_('HELP'); ?></a>
			</li>
		<?php } ?>
		<li class="bg-master-lighter">
			<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" name="login"
						class="form-login fancy">
				<input type="hidden" name="option" value="com_user"/>
				<input type="hidden" name="task" value="logout"/>
				<input type="hidden" name="return" value="<?php echo base64_encode('index.php'); ?>"/>
				<button type="submit" class="clearfix">
					<span class="pull-left">Logout</span>
					<span class="pull-right"><i class="pg-power"></i></span>
				</button>
			</form>
		</li>
	</ul>
</li>
	<?php else : ?>
<li class="nav-item dropdown">
	<a href="#" data-toggle="dropdown" class="profile-dropdown-toggle nav-link dropdown-toggle d-flex align-items-center padding-sm-10"
		 aria-expanded="false">
		<span class="p-r-5">Login</span>
		<span class="thumbnail-wrapper d32 circular inline">
						<img src="https://www.tutorialrepublic.com/examples/images/avatar/2.jpg" class="avatar" alt="Avatar">
					</span>
		<b class="caret"></b>
	</a>
	<div class="dropdown-menu">
			<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" name="login"
						class="form-login fancy">
				<div class="panel-heading">
					<h4 class="panel-title"><?php echo JText::_('ACCOUNT'); ?></h4>
				</div>
				<div class="panel-body">
					<p>Have an account? please sign in.</p>
					<div class="form-group">
						<input type="text" name="username" id="mod_login_username" class="form-control"
									 placeholder="<?php echo JText::_('EMAIL'); ?>"/>
					</div>
					<div class="form-group">
						<input type="password" id="mod_login_password" name="passwd" class="form-control"
									 placeholder="<?php echo JText::_('PASSWORD'); ?>"/>
					</div>
					<div class="row">
						<div class=" col-xs-7">
							<label class="checkbox-inline">
								<input type="checkbox" class="chkbox icheckbox_minimal"/>
								Remember</label>
						</div>
						<div class=" col-xs-5 text-right"><a class="btn btn-link" style="text-transform: none;"
																								 href="<?php echo $b; ?>forgot-pass"
																								 title="Reset password"><?php echo JText::_('FORGOT_LOGIN'); ?></a>
						</div>
					</div>
					<input type="hidden" name="option" value="com_user"/>
					<input type="hidden" name="task" value="login"/>
					<input type="hidden" name="return" value="<?php echo base64_encode($u->toString()); ?>"/>
					<?php echo JHTML::_('form.token'); ?> </div>
				<div class="panel-footer">
					<div class="row">
						<div class="col-md-12 text-right m-b-10">
							<input type="submit" name="Submit" class="btn btn-block btn-primary" value="Sign In"/>
						</div>
						<div class="col-md-12 m-b-5"><a class="btn btn-block btn-social btn-facebook fb-login"><i
									class="fa fa-facebook"></i> Sign in with Facebook</a></div>
						<div class="col-md-12 m-b-5">
							<button type="button" id="g-login"
											data-theme="dark"
											class="g-signin btn btn-block btn-social btn-google-plus" title=""><i
									class="fa fa-google-plus"></i> Sign in with Google
							</button>
						</div>
						<div class="col-md-12 text-right">
							<a class="btn btn-link" style="text-transform: none;" href="<?php echo $b; ?>user/register"
								 title="Create new account using form.">Create New Account <span class="fa fa-angle-right "
																																								 aria-hidden="true"></span></a>
						</div>
					</div>
				</div>
			</form>
		</div>
</li>
	<?php endif; ?>
