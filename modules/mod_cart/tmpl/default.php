<?php
/**
 * @package    modtemplate
 * @author     agoos28
 * @copyright  bebas
 * @license    bebas
 **/

//no direct access
defined('_JEXEC') or die('Restricted access');

require_once(dirname(dirname(__FILE__)) . "/helper.php");

$session =& JFactory::getSession();
$cart = getCart();
$count = 0;
$total = 0;

if ($cart) {
	foreach ($cart as $item) {
		$count++;
		$total += $item->price;
	}
	$cart = array_reverse($cart);
}
//$session->set('cart', '');
//echo'<pre>';print_r($cart);echo'</pre>';die();

?>

<li class="nav-item dropdown cartmenu">
	<a class="nav-link dropdown-toggle padding-sm-10" id="cart_togggle">
		<span class="fa fa-shopping-basket fa-lg"></span>
		<span class="badge buble qcount"><?php echo $count; ?></span>
	</a>
	<div class="dropdown-menu cart-container pull-right bg-white position-absolute hide p-t-0">
		<div class="list-view-group-container ">
			<!-- BEGIN List Group Header!-->
			<div class="list-view-group-header text-uppercase">
				<?php echo JText::_('SHOPPING_CART'); ?>
			</div>
		</div>
		<div class="relativebg-master-lightest" style="height: 340px; position: relative; background: #fcfcfc;">
			<div class="list-view-wrapper scrollable">
				<div data-init-list-view="ioslist" class="shoppingcart list-view boreded no-top-border">
					<!-- BEGIN List Group !-->
					<div class="cart_row">
						<!-- BEGIN List Group Header!-->
						<?php if ($cart) {
							?>
							<?php foreach ($cart as $item) {
								$item->option = json_decode($item->option);
								$event = getEvent($item->event_id);
								?>
								<div
									id="<?php echo $item->id; ?>"
									data-email="<?php echo $item->email; ?>"
									data-price="<?php echo $item->price; ?>"
									data-event_id="<?php echo $item->event_id; ?>"
									data-as="<?php echo $item->as; ?>"
									class="cartItem row no-margin p-t-15 p-b-15 border-bottom"
								>
									<div class="col-sm-7 col-xs-12">
										<div class="">
											<h6 class="m-b-5 m-t-0"><?php echo $item->name ?></h6>
											<div class="size">
												<a href="<?php echo getArticleLink($event->content_id); ?>"><?php echo $event->title; ?><br/>
													<?php echo $item->title; ?></a>
											</div>
										</div>
									</div>
									<div class="col-sm-4 col-10">
										<div class="price">Rp. <span
												class="subtotal"><?php echo number_format(($item->price), 0, '', '.') ?></span>
										</div>
									</div>
									<div class="col-sm-1 col-2 p-l-0"><a href="#" class="del_btn del"
																											 data-id="<?php echo $item->id ?>"><i
												class="fa fa-trash-o"></i></a></div>
								</div>
							<?php } ?>
						<?php } else { ?>
							<h5 class="emptyCart text-center padding-20">Keranjang Kosong</h5>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer pull-bottom full-width padding-20 border-top">
			<div class="row no-gutters align-content-between">
				<div class="col-sm-6 total"> TOTAL : Rp. <span
						class="tot"><?php echo number_format($total, 0, '', '.'); ?></span></div>
				<div class="col-sm-6 text-right"><a href="<?php echo JURI::base() ?>cart"
																						class="btn btn-purple"><?php echo JText::_('CHECKOUT'); ?></a>
				</div>
			</div>
		</div>
	</div>
</li>
