<?php
/**
 * @package    JJ Module Generator
 * @author    JoomJunk
 * @copyright  Copyright (C) 2011 - 2012 JoomJunk. All Rights Reserved
 * @license    http://www.gnu.org/licenses/gpl-3.0.html
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_cckjseblod' . DS . 'tables');

function getCart()
{
	// $session =& JFactory::getSession();
	// $cartItems = $session->get('cart');
	$user = JFactory::getUser();
	$cartTable = JTable::getInstance('cart');
	$cart = $cartTable->load($user->id);
	if ($cart) {
		return json_decode($cart);
	} else {
		return 0;
	}
}


function getEvent($id)
{
	$db = JFactory::getDBO();
	$query = 'SELECT a.*, b.title AS title, b.introtext ' .
		' FROM #__event AS a' .
		' LEFT JOIN #__content AS b ON b.id = a.content_id' .
		' WHERE a.id = ' . (int)$id;
	$db->setQuery($query);
	return $db->loadObject();
}


function getArticleLink($cid)
{
	require_once(JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');
	jimport('joomla.application.component.helper');
	$db = JFactory::getDBO();
	$query = "SELECT id, sectionid, catid FROM #__content WHERE id = " . $cid;
	$db->setQuery($query);
	$result = $db->loadObject();
	return JRoute::_(ContentHelperRoute::getArticleRoute($result->id, $result->catid, $result->sectionid));
}

?>