<?php
/**
 * Created by IntelliJ IDEA.
 * User: agoos28
 * Date: 11/03/20
 * Time: 03.49
 */

defined('_JEXEC') or die('Restricted access');


function storeEvent($content, $isNew)
{
	$cck = (object)CCK::CONTENT_getValues($content->introtext);
	if(!$cck->ev_title){
		return;
	}
	// echo '<pre>'; print_r($cck); echo '</pre>';
	JTable::addIncludePath( JPATH_ADMINISTRATOR.DS.'components'.DS.'com_eventorg'.DS.'tables' );
	$row =& JTable::getInstance('event');

	//print_r($row);

	$row->content_id = $content->id;
	$row->title = $cck->ev_title;
	$row->seat = $cck->ev_seat;
	$row->location = $cck->ev_location;
	$row->start_date = $cck->ev_start_date . ':00';
	$row->end_date = $cck->ev_end_date . ':00';
	$row->store();
	// echo '<pre>'; print_r($cck); echo '</pre>'; die();
	return;
}

function storeTimeline($content, $isNew)
{
	$cck = (object)CCK::CONTENT_getValues($content->introtext);
	// echo '<pre>'; print_r($cck); echo '</pre>';
	$row =& JTable::getInstance('event_timeline');

	$id = $row->check($content->event_id);

	if ($id) {
		$row->id = $id;
	}
	$row->content_id = $content->id;
	$row->event_id = $content->event_id;
	$row->name = $cck->name;
	$row->start_time = $cck->ev_start_time . ':00';
	$row->end_time = $cck->ev_end_date . ':00';
	$row->store();
	// echo '<pre>'; print_r($row); echo '</pre>'; die();
	return;
}