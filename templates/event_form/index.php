<?php

/**
 * @version      1.9.0
 * @author        http://www.seblod.com
 * @copyright    Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license      GNU General Public License version 2 or later; see _LICENSE.php
 * @package      Event Form Template (Custom) - jSeblod CCK ( Content Construction Kit )
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod = clone $this;
?>

<?php
/**
 * Init Style Parameters
 **/
include(dirname(__FILE__) . '/helper.php');
include(dirname(__FILE__) . '/params.php');

$data = $event_model->getDataFromContentId($this->content->id);

?>
<pre>
<?php // print_r($data); ?>
  </pre>
<?php echo $jSeblod->default_action->form; ?>

<style>
	.wf-editor-tabs{
		float: right !important;
	}
	.editor-label{
		transform: translate(0px, 30px);
		font-size: 14px;
	}
</style>

<div class="row gutter-15">
	<div class="col-sm-12">
		<div class="row gutter-15">
			<div class="col-sm-4 col-sm-push-8">
				<div class="panel panel-blue m-b-15 no-overflow">
					<div class="panel-body no-padding">
						<div class="clearfix">
							<div class="col-xs-4 text-center padding-15">
								<div class="">
									<span class="text-bold block text-super-large">90</span>
									<span class="text-strong">Register</span>
								</div>
							</div>
							<div class="col-xs-4 text-center padding-15 partition-orange">
								<div class="">
									<span class="text-bold block text-super-large">2</span>
									<span class="text-strong">Pending</span>
								</div>
							</div>
							<div class="col-xs-4 text-center padding-15 partition-green">
								<span class="text-bold block text-super-large">8</span>
								<span class="text-strong">Paid</span>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-white">
					<div class="panel-heading border-light">
						<h4 class="panel-title">Event <span class="text-bold">Listing</span></h4>
					</div>
					<div class="panel-body">
						<div class="row gutter-15">
							<div class="col-xs-6">
								<div class="form-group">
									<label class="control-label">
										Status <span class="symbol required"></span>
									</label>
									<div class="form-group">
										<?php echo $jSeblod->state->form; ?>
									</div>
								</div>
							</div>
							<div class="col-xs-6">
								<div class="form-group">
									<label class="control-label">
										Available Seats <span class="symbol required"></span>
									</label>
									<?php echo $jSeblod->ev_seat->form; ?>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">
										Event Start <span class="symbol required"></span>
									</label>
									<div class="input-append date input-group datetime" id="datetimepicker"
											 data-date-format="yyyy-mm-dd hh:ii">
										<span class="add-on input-group-addon"><i class="icon-th"><i
													class="fa fa-calendar"></i></i></span>
										<?php echo $jSeblod->ev_start_date->form; ?>
									</div>
								</div>
							</div>
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">
										Event End <span class="symbol required"></span>
									</label>
									<div class="input-append date input-group datetime" id="datetimepicker"
											 data-date-format="yyyy-mm-dd hh:ii">
                                        <span class="add-on input-group-addon"><i class="icon-th"><i
																							class="fa fa-calendar"></i></i></span>
										<?php echo $jSeblod->ev_end_date->form; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-8 col-sm-pull-4">
				<div class="panel panel-white">
					<div class="panel-heading border-light">
						<h4 class="panel-title">Event <span class="text-bold">Listing</span></h4>
					</div>
					<div class="panel-body">
						<div class="row gutter-15">
							<div class="col-sm-8">
								<div class="form-group">
									<label class="control-label">
										Event Name <span class="symbol required"></span>
									</label>
									<?php echo $jSeblod->ev_title->form; ?>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label class="control-label">
										Event Category <span class="symbol required"></span>
									</label>
									<?php echo $jSeblod->sectionid->form; ?>

									<?php echo str_replace('id="catid"', 'id="parent_id"', $jSeblod->catid->form); ?>

								</div>
							</div>
							<div class="col-sm-8">
								<div class="form-group">
									<label class="control-label">
										<?php echo $jSeblod->ev_short_description->label; ?> <span
											class="symbol required"></span>
									</label>
									<?php echo $jSeblod->ev_short_description->form; ?>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label class="control-label">
										<?php echo $jSeblod->ev_cover->label; ?> <span class="symbol required"></span>
									</label>
									<?php echo $jSeblod->ev_cover->form; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				<!-- start: PANLEL TABS -->
				<div class="panel panel-tabs m-t-50">
					<div class="panel-body">
						<div class="tabbable">
							<ul class="nav nav-tabs m-b-0" style="padding-left: 1px;">
								<li class="active">
									<a data-toggle="tab" href="#panel_tab_1">
										<strong>Sambutan</strong>
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#panel_tab_2">
										<strong>Detail</strong>
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#panel_tab_3">
										<strong>Lokasi</strong>
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#panel_tab_4">
										<strong>Registration Policy</strong>
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#panel_tab_example2">
										<strong>Event Rundown</strong>
									</a>
								</li>
								<li class="">
									<a data-toggle="tab" href="#panel_tab_example3">
										<strong>Package & Pricing</strong>
									</a>
								</li>
							</ul>
							<div class="tab-content">
								<div id="panel_tab_1" class="tab-pane active fade in">
									<div class="row gutter-15">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label editor-label"><?php echo $jSeblod->ev_info->label; ?></label>
												<?php echo $jSeblod->ev_info->form; ?>
											</div>
										</div>
									</div>
								</div>
								<div id="panel_tab_2" class="tab-pane fade in">
									<div class="row gutter-15">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="control-label editor-label"><?php echo $jSeblod->ev_description->label; ?></label>
												<?php echo $jSeblod->ev_description->form; ?>
											</div>
										</div>
									</div>
								</div>
								<div id="panel_tab_3" class="tab-pane fade in">
									<div class="row gutter-15">
										<div class="col-sm-12">
											<div class="form-group">
												<?php echo $jSeblod->ev_location->form; ?>
											</div>
										</div>
									</div>
								</div>
								<div id="panel_tab_4" class="tab-pane fade">
									<div class="form-group">
										<label class="control-label editor-label"><?php echo $jSeblod->ev_registration_policy->label; ?></label>
										<?php echo $jSeblod->ev_registration_policy->form; ?>
									</div>
								</div>
								<div id="panel_tab_example2" class="tab-pane fade">
									<div class="row">
										<div class="col-xs-6">
											<div class="partition-white">
												<div class="panel-heading border-light">
													<div class="pull-right">
														<?php
														$scheduleUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=schedule&id=' . $data->event->id . '&format=raw');
														?>
														<a class="ajax-modal btn btn-grey" href="<?php echo $scheduleUri; ?>">
															<i class="fa fa-plus"></i> Add Schedule</a>
													</div>
													<h4 class="panel-title">Event <span class="text-bold">Timeline</span>
													</h4>
													<span>Event activity rundown</span>
												</div>
												<div class="panel-body">
													<div class="panel-scroll padding-15" style="height: 70vh;">
														<div id="schedule-list" data-url="<?php echo $scheduleUri; ?>">
															<?php // echo renderScheduleItem($data->schedule); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="partition-white">
												<div class="panel-heading border-light">
													<div class="pull-right">
														<?php
														$activityUrl = JRoute::_('index.php?option=com_eventorg&view=manage&layout=activity&id=' . $data->event->id . '&format=raw');
														?>
														<a class="ajax-modal btn btn-grey" href="<?php echo $activityUrl; ?>">
															<i class="fa fa-plus"></i> Add Activity</a>
													</div>
													<h4 class="panel-title">Event <span class="text-bold">Activity</span>
													</h4>
													<span>Unscheduled events</span>
												</div>
												<div class="panel-body padding-15 list-group" data-id="0" style="height: 70vh;">
													<div class="dd" id="unlinkedActivities" data-url="<?php echo $activityUrl; ?>&fragment=list">
														<ol class="dd-list">
															<li class="dd-item dd3-item" data-id="14">
																<div class="dd-handle dd3-handle"></div>
																<div class="dd3-content">
																	Item 14
																</div>
															</li>
															<li class="dd-item dd3-item" data-id="15">
																<div class="dd-handle dd3-handle"></div>
																<div class="dd3-content">
																	Item 15
																</div>
															</li>
														</ol>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div id="panel_tab_example3" class="tab-pane fade">
									<div class="row">
										<div class="col-sm-6 col-sm-push-6">
											<div class="panel-heading border-light">
												<div class="pull-right">
													<?php
													$groupUrl = JRoute::_('index.php?option=com_eventorg&view=manage&layout=packagegroup&eventId=' . $data->event->id . '&format=raw');
													?>
													<a class="ajax-modal btn btn-grey" href="<?php echo $groupUrl; ?>">
														<i class="fa fa-plus"></i> Add Group</a>
												</div>
												<h4 class="panel-title">Package <span class="text-bold">Group</span>
												</h4>
												<span>Event package Groups</span>
											</div>
											<div class="panel-body">
												<div class="padding-15" style="height: 70vh;">
													<div id="package-groups" data-url="<?php echo $groupUrl; ?>">
														<?php // echo renderScheduleItem($data->schedule); ?>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-6 col-sm-pull-6">
											<div class="panel-heading border-light">
												<div class="pull-right">
													<?php
													$packageUrl = JRoute::_('index.php?option=com_eventorg&view=manage&layout=package&eventId=' . $data->event->id . '&format=raw');
													?>
													<a class="ajax-modal btn btn-grey" href="<?php echo $packageUrl; ?>">
														<i class="fa fa-plus"></i> Add Package</a>
												</div>
												<h4 class="panel-title">Event <span class="text-bold">Package</span>
												</h4>
												<span>Event package pricing and options</span>
											</div>
											<div class="panel-body">
												<div class="padding-15" style="height: 70vh;">
													<div id="package-list" data-url="<?php echo $packageUrl; ?>">
														<?php // echo renderScheduleItem($data->schedule); ?>
													</div>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- end: PANLEL TABS -->
				<div class="text-right m-b-50">
					<?php echo $jSeblod->my_readmore->form; ?>
					<button class="color-button btn btn-default" type="button" onclick="history.go(-1);"
									name="button_back">Cancel
					</button>&nbsp;
					<button class="color-button btn btn-green" type="submit" name="apply" value="1">Apply</button>&nbsp;
					<button class="color-button btn btn-primary" type="submit" name="button_submit">Save It !</button>
				</div>
			</div>

		</div>
	</div>
	<div class="col-md-12">

	</div>
</div>


<script>
	var baseUrl = '<?php echo JURI::base(); ?>';
	var componentUrl = '<?php echo JRoute::_('index.php?option=com_eventorg'); ?>';
	var eventId = '<?php echo $data->event->id; ?>';

	function getSchedule() {
		var url = $('#schedule-list').data('url');
		$('#schedule-list').load(url, function () {
			initNestable()
		})
	}

	function deleteSchedule(id) {
		var postData = {
			id: id,
			task: 'remove',
			type: 'timeline',
			'<?php echo JUtility::getToken(); ?>': 1,
		};
		$.post(componentUrl, postData, function (data) {
			getSchedule();
			getActivities();
			toastr.success('Deleted');
		})
	}

	function deletePackage(id) {
		var postData = {
			task: 'remove',
			type: 'package',
			'<?php echo JUtility::getToken(); ?>': 1,
			id: id
		};
		$.post(componentUrl, postData, function (data) {
			getPackage()
			toastr.success('Deleted');
		})
	}

	function getActivities() {
		var url = $('#unlinkedActivities').data('url');
		$('#unlinkedActivities').load(url, function () {
			initNestable()
		})
	}

	function deleteActivity(id) {
		var postData = {
			task: 'remove',
			type: 'activity',
			'<?php echo JUtility::getToken(); ?>': 1,
			id: id
		};
		$.post(componentUrl, postData, function (data) {
			getSchedule();
			getActivities();
			toastr.success('Deleted');
		})
	}

	function updateActivities(data) {
		var postData = {
			task: 'update_activities',
			type: 'activity',
			'<?php echo JUtility::getToken(); ?>': 1,
			items: data
		};
		$.post(componentUrl, postData, function (data) {
			console.log(data);
			getSchedule();
			getActivities();
			toastr.success('Saved');
		})
	}


	function getPackage() {
		var url = $('#package-list').data('url');
		$('#package-list').load(url, function () {
			//initNestable()
		})
	}

	function deletePackage(id) {
		var postData = {
			task: 'remove',
			type: 'package',
			'<?php echo JUtility::getToken(); ?>': 1,
			id: id
		};
		$.post(componentUrl, postData, function (data) {
			getPackage();
			toastr.success('Deleted');
		})
	}

	function getPackageGroups() {
		var url = $('#package-groups').data('url');
		$('#package-groups').load(url, function () {
			//initNestable()
		})
	}

	function deletePackageGroup(id) {
		var postData = {
			task: 'remove',
			type: 'packagegroup',
			'<?php echo JUtility::getToken(); ?>': 1,
			id: id
		};
		$.post(componentUrl, postData, function (data) {
			getPackageGroups();
			toastr.success('Deleted');
		})
	}

	function initNestable() {
		$('.dd').nestable({
			onEnd: function (item) {
				var data = [];
				item.map(function (e) {
					data.push({
						id: e.id,
						timeline_id: e.parentid,
						ordering: e.index
					});
				});
				updateActivities(data)
			}
		});
	}


	$('document').ready(function () {
		var body = $('body');

		$.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
			'<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
			'<div class="progress progress-striped active">' +
			'<div class="progress-bar" style="width: 100%;"></div>' +
			'</div>' +
			'</div>';
		$.fn.modalmanager.defaults.resize = true;

		body.on('click', '.ajax-modal', function (e) {
			e.preventDefault();
			var modal = $('#ajax-modal');
			modal.addClass('modal-lg')
			var url = $(this).attr('href');
			$('body').modalmanager('loading');
			setTimeout(function () {
				modal.load(url + '&form=1&format=raw&eventId=' + eventId, '', function () {
					modal.modal();
				});
			}, 1000);
		});
		body.on('click', '.delete-schedule', function (e) {
			var th = $(this);
			e.preventDefault();
			bootbox.confirm("Delete schedule?", function (ok) {
				if (ok) {
					deleteSchedule(th.data('id'));
				}
			});
		});
		body.on('click', '.delete-activity', function (e) {
			var th = $(this);
			e.preventDefault();
			bootbox.confirm("Delete activity", function (ok) {
				if (ok) {
					deleteActivity(th.data('id'));
				}
			});
		});
		body.on('click', '.delete-package', function (e) {
			var th = $(this);
			e.preventDefault();
			bootbox.confirm("Delete package", function (ok) {
				if (ok) {
					deletePackage(th.data('id'));
				}
			});
		});
		body.on('click', '.delete-package-group', function (e) {
			var th = $(this);
			e.preventDefault();
			bootbox.confirm("Delete package group", function (ok) {
				if (ok) {
					deletePackageGroup(th.data('id'));
				}
			});
		});
		toastr.options = {
			"closeButton": true,
			"positionClass": "toast-bottom-right",
			"onclick": null,
			"showDuration": "1000",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		}
	});
	$(window).load(function () {
		var newDate = new Date();
		var ev_start_date = $('#ev_start_date').val();
		var startDate = ev_start_date ? new Date(ev_start_date) : newDate
		$("input[name='ev_seat']").TouchSpin({
			verticalbuttons: true
		});
		$('#sectionid').trigger('change');
		$('#default_action').validate();
		$('.datetime').datetimepicker({
			autoclose: true,
			initialDate: newDate,
			startDate: startDate
		});
		getSchedule();
		getActivities();
		getPackageGroups();
		getPackage();
	})
</script>

<div id="ajax-modal" class="modal extended-modal fade no-display" tabindex="-1"></div>