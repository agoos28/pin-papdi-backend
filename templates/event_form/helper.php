<?php
/**
 * @version            1.9.0
 * @author        http://www.seblod.com
 * @copyright        Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license            GNU General Public License version 2 or later; see _LICENSE.php
 * @package            SEBLOD 1.x (CCK for Joomla!)
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');

JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_eventorg' . DS . 'models');
$event_model =& JModel::getInstance('manage', 'EventOrgModel');


// Init Client
$client =& JApplicationHelper::getClientInfo($mainframe->getClientId());

// Init Path
$path = ($client->id) ? @$this->rooturl . '/templates/' . $this->template : $this->baseurl . '/templates/' . $this->template;


?>
