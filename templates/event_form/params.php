<?php

/**
 * @version 			1.9.0
 * @author       	http://www.seblod.com
 * @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license 			GNU General Public License version 2 or later; see _LICENSE.php
 * @package			Event Form Template (Custom) - jSeblod CCK ( Content Construction Kit )
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');
?>

<?php
/**
 * Init Style Parameters
 **/

// Init Client
$client        = &JApplicationHelper::getClientInfo($mainframe->getClientId());

// Init Path
$path        =  ($client->id) ? @$this->rooturl . '/templates/' . $this->template : $this->baseurl . '/templates/' . $this->template;
?>

<style>
  .tabbable>.nav-tabs {
    margin-right: 15px;
    margin-top: -47px;
  }

  .tab-content .nav-tabs {
    margin: 0;
  }

  .tabbable>.nav-tabs>li {
    background: #e8e8e8;
    margin-bottom: -3px;
    text-align: center;
    margin: 0px 2px;
    border-top-left-radius: 5px;
    overflow: hidden;
    border-top-right-radius: 5px;
  }

  .fileupload-new.thumbnail {
    width: auto;
    margin-bottom: 10px;
  }

  .fileupload-preview.thumbnail {
    max-width: auto;
    margin-bottom: 5px;
  }
</style>