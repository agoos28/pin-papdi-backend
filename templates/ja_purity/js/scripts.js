jQuery.noConflict();
jQuery.extend({
    stringify  : function stringify(obj) {         
        if ("JSON" in window) {
            return JSON.stringify(obj);
        }

        var t = typeof (obj);
        if (t != "object" || obj === null) {
            // simple data type
            if (t == "string") obj = '"' + obj + '"';

            return String(obj);
        } else {
            // recurse array or object
            var n, v, json = [], arr = (obj && obj.constructor == Array);

            for (n in obj) {
                v = obj[n];
                t = typeof(v);
                if (obj.hasOwnProperty(n)) {
                    if (t == "string") {
                        v = '"' + v + '"';
                    } else if (t == "object" && v !== null){
                        v = jQuery.stringify(v);
                    }

                    json.push((arr ? "" : '"' + n + '":') + String(v));
                }
            }

            return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
        }
    }
});
function GROUP_Remove(elem){
	jQuery(elem).parent().parent().remove()
}
function addGroupitem(el, maxitem){

	var elem = jQuery('#'+el)
		
	var list = elem.parent()
	var listname = list.attr('id')
	
	var num = eval("groupmax_"+listname);
		eval("groupmax_"+listname+"++");
		
	var newelem = elem.clone()
	
	
	newelem = elem.get(0).outerHTML
	newelem = newelem.replace(/\[\d+\]\[/g,"\["+num+"\]\[")
	newelem = newelem.replace(/-\d+-/g,"-"+num+"-")
	newelem = newelem.replace(/-\d+_/g,"-"+num+"_")
	newelem = newelem.replace(/item-\d/g,"item-"+num+"")
	
	newelemid = jQuery(newelem).attr('id') + '-lp_item_location'

	list.append(newelem)
	list.find('.xitem:last').find('.inputbox').val('')
	list.find('.xitem:last').find('.preview').empty()
	list.find('.xitem:last').find('h4').text('Select Image')
	
	new Sortables($(listname), {
			'handles': $(listname).getElements('img.button-drag'),
			snap: 1
		});	
	
}
function addMarker(position){
	jQuery('#gmap').gmap3({ 
		clear: {
			name: 'marker',
			last: true
		},
		marker:{
			latLng: position,
			options:{
				draggable: true
			},
			events: {
				dragend: function(marker, event, context){
					updateForm(marker.position)
				}
			},
			callback: function(){
				updateForm(position)
			}
		}
	})
	//updatePoly()
}
function updateForm(position){
	var pos = {'H': position.lat(), 'L':position.lng()}
	jQuery('input[name="lc_map"]').val(jQuery.stringify(pos))
}
jQuery(document).ready(function(){
	
	jQuery('.newitemgroup').click(function(e){
		e.preventDefault()
		var el = jQuery(this).data('id')
		var maxitem = jQuery(this).data('max')
		addGroupitem(el, maxitem)
	})
	
	if(jQuery('form#product_category').length){
		//jQuery('#section').val(2)
	}
	if(jQuery('form#gallery_category').length){
		//jQuery('#section').val(3)
	}
	jQuery('input[name="current_url"]').val(document.referrer)
	
	setTimeout(function(){
		jQuery('#system-message').slideUp()
	}, 2000)
	
	jQuery('button[name="button_back"]').appendTo('#button_submit_container')
	jQuery('.collection-group-repeatable').dragsort({ 
		dragSelector: 'a', 
		dragBetween: true,
		placeHolderTemplate: "<li class='collection-group-repeatable'><div class='collection-group-wrap'></div></li>" 
	});
	if(!jQuery('.collection-group-form:first').find('img').length){
		jQuery('li.collection-group-repeatable:first').remove()
	}
	jQuery('body').on('change','#imagex li.new input[type="file"]',function(){
		var th = jQuery(this)
		var files = this.files
		var reader = new FileReader();
		var newimg
		reader.onload = function(event){
			newimg = event.target.result;
			th.parent().find('a').css('background','url('+newimg+')')	
		}
		reader.readAsDataURL(files[0]);
	})
	jQuery('.addimg').click(function(e){
		var th = jQuery(this)
		var lastid = 0
		var value = th.val()
		jQuery('li.collection-group-repeatable').each(function(index, element) {
            var ths = jQuery(this).find('.collection-group-form')
			
			jQuery(this).data('listidx', index)
			ths.find('div:first').attr('id', 'post_img-'+index+'_container')
			
			ths.find('input[type="hidden"]')
			.attr('name','imagex['+index+'][post_img_hidden]')
			.attr('id','imagex['+index+'][post_img_hidden]')
			
			ths.find('input[type="file"]')
			.attr('name','imagex['+index+'][post_img]')
			.attr('id','imagex['+index+'][post_img]')
			.attr('onchange','$("imagex['+index+'][post_img]_delete").checked=true;')
			//.val(value)
			
			lastid = index + 1
        });
		
			var tmpl = '<li class="collection-group-repeatable new" data-listidx="'+lastid+'">'+
					  '<div class="collection-group-wrap">'+
						'<div class="collection-group-form">'+
						 '<div id="post_img-'+lastid+'_container">'+
						 '<a data-cursor="move" style="cursor: move;" href="javascript:void(0);" onclick=""><img style="padding-bottom: 1px; width: 100%" alt="" src=""></a>'+
							'<input type="file" onchange="$("imagex['+lastid+'][post_img]_delete").checked=true;" size="32" name="imagex['+lastid+'][post_img]" id="imagex['+lastid+'][post_img]" class="inputbox file">'+
							'</span></div>'+
							'<div class="asd" id="image_title-'+lastid+'_container"><input class="inputbox text" id="imagex-'+lastid+'-image_title" name="imagex['+lastid+'][image_title]" maxlength="50" size="32" value="" type="text"></div>'+
						'</div>'+
						'<div class="collection-group-button"><img alt="Del" src="'+baseUrl+'media/jseblod/_icons/del-default.gif" onclick="CCK_GROUP_Remove(this);" class="button-del"></div>'+
					  '</div>'+
					'</li>';
		jQuery('#imagex').append(tmpl)		
		e.preventDefault()
	})
	jQuery('body').on('click', 'a.editshipment', function(e){
		e.preventDefault()
		var th = jQuery(this)
		var pathname = window.location.pathname;
		var parent = th.parent().parent()
		var id = th.attr('id')
		if(th.text() == 'Saving'){
			return false
		}
		if(th.text() == 'Edit'){
			var regular = parent.find('.regular').text()
			var express = parent.find('.express').text()
			var cod = parent.find('.cod').text()
			parent.find('.regular').html('<input style="width: 50px; text-align: center;" type="text" name="regular" value="'+regular+'"></input>')
			parent.find('.express').html('<input style="width: 50px; text-align: center;" type="text" name="express" value="'+express+'"></input>')
			parent.find('.cod').html('<input style="width: 50px; text-align: center;" type="text" name="cod" value="'+cod+'"></input>')
			th.text('Save')
		}else{
			th.text('Saving')
			var regular = parent.find('input[name="regular"]').val()  //['update = '+id+', reg = '+regular+', exp = '+express ]
			var express = parent.find('input[name="express"]').val()
			var cod = parent.find('input[name="cod"]').val()
			jQuery.post(pathname, {'update':id, 'reg':regular, 'exp':express, 'cod': cod }, function(data){
				if(data == 'ok'){
					parent.find('.regular').html(regular)
					parent.find('.express').html(express)
					parent.find('.cod').html(cod)
					th.text('Edit')
				}else{
					th.text('Failed')
					setTimeout(function(){
						th.text('Save')
					}, 2000)
				}
			})
		}
	})
})


  