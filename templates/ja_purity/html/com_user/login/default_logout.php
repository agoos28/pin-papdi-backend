<?php // @version $Id: default_logout.php 8944 2007-09-18 03:04:14Z friesengeist $
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form action="index.php" method="post" name="login" id="login" class="logout_form<?php echo $this->params->get( 'pageclass_sfx' ); ?>">
	
	<h1 class="componentheading<?php echo $this->params->get( 'pageclass_sfx' ); ?>">
		Administration
	</h1>
	

	<?php if ( $this->params->get( 'description_logout' ) || isset( $this->image ) ) : ?>
	<div class="contentdescription<?php echo $this->params->get( 'pageclass_sfx' ); ?>">
		<?php if (isset ($this->image)) :
			echo $this->image;
		endif;
		if ( $this->params->get( 'description_logout' ) ) : ?>
		<p>
			<?php echo $this->params->get('description_logout_text'); ?><br />
            For help and support contact <a href="mailto:agus_riyanto_28@yahoo.com">agus_riyanto_28@yahoo.com</a><br />
            Satu collective. 
		</p>
		<?php endif; ?>
	</div>
	<?php endif; ?>

	<p><input type="submit" name="Submit" class="button" value="<?php echo JText::_( 'Logout' ); ?>" /></p>
	<input type="hidden" name="option" value="com_user" />
	<input type="hidden" name="task" value="logout" />
	<input type="hidden" name="return" value="<?php echo $this->return; ?>" />
</form>
