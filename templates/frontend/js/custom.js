function addCommas(nStr) {
	nStr += '';
	x = nStr.split(',');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}

function dellCart(id) {
	if (!id) {
		return;
	}

	var postdata = {
		id: id,
		useajax: 'true',
		task: 'dellCart'
	};

	var elem = $('.cart_row').find('#' + id);

	$.post(baseUrl + 'cart', postdata, function (response) {
		elem.each(function () {
			var th = $(this)
			th.slideUp(function () {
				th.remove();
				updatecart();
			})
		})
	})
}

function updatecart() {
	var total = 0;
	var count = 0;
	setTimeout(function () {
		$('.shoppingcart').find('.cart_row .cartItem').each(function () {
			count++;
			var th = $(this);
			total += parseInt(th.attr('data-price'));
		});
		$('.qcount').text(count);
		$('.total').find('.tot').text(addCommas(total))
		$('.alltot').text(addCommas(total))
		if(!count){
			$('.cart_row').html('<h5 class="emptyCart text-center padding-20">Keranjang Kosong</h5>')
		}else{
			$('.cart_row').find('.emptyCart').slideUp()
		}
	}, 200)
}

$(document).ready(function () {
	'use strict';
	var body = $('body');
	var slider = new Swiper('#hero', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		slidesPerView: 1,
		parallax: true,
		speed: 1000,
	});
	var testimonials_slider = new Swiper('#testimonials_slider', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		parallax: true,
		speed: 1000
	});
	$('[data-pages="search"]').search({
		searchField: '#overlay-search',
		closeButton: '.overlay-close',
		suggestions: '#overlay-suggestions',
		brand: '.brand',
		onSearchSubmit: function (searchString) {
			console.log("Search for: " + searchString);
		},
		onKeyEnter: function (searchString) {
			console.log("Live search for: " + searchString);
			var searchField = $('#overlay-search');
			var searchResults = $('.search-results');
			clearTimeout($.data(this, 'timer'));
			searchResults.fadeOut("fast");
			var wait = setTimeout(function () {
				searchResults.find('.result-name').each(function () {
					if (searchField.val().length != 0) {
						$(this).html(searchField.val());
						searchResults.fadeIn("fast");
					}
				});
			}, 500);
			$(this).data('timer', wait);
		}
	});
	$('.scrollable').scrollbar();
	var cart = $('.cart-container');
	body.not($('#cart_togggle')).click(function (e) {
		if (!$(e.target).parents('.cartmenu').length) {
			if (!cart.hasClass('hide')) {
				cart.addClass('hide')
			}
		}

	});
	$('#cart_togggle').click(function (e) {
		e.preventDefault();
		if (cart.hasClass('hide')) {
			cart.removeClass('hide')
		} else {
			cart.addClass('hide')
		}
	});

	body.on('click', '.del_btn', function (e) {
		var th = $(this);
		var id = th.attr('data-id');
		dellCart(id);
	})
});