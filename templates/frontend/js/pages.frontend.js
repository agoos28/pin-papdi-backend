(function ($) {
  'use strict';
  var Pages = function () {
    this.VERSION = '';
    this.AUTHOR = '';
    this.SUPPORT = '';
    this.pageScrollElement = 'html, body';
    this.$body = $('body');
    this.setUserOS();
    this.setUserAgent();
  }
  Pages.prototype.setUserOS = function () {
    var OSName = '';
    if (navigator.appVersion.indexOf('Win') != - 1) OSName = 'windows';
    if (navigator.appVersion.indexOf('Mac') != - 1) OSName = 'mac';
    if (navigator.appVersion.indexOf('X11') != - 1) OSName = 'unix';
    if (navigator.appVersion.indexOf('Linux') != - 1) OSName = 'linux';
    this.$body.addClass(OSName);
  }
  Pages.prototype.setUserAgent = function () {
    console.log(navigator.userAgent)
    if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
      this.$body.addClass('mobile');
      console.log('mobile')
    } else {
      this.$body.addClass('desktop');
      console.log('desktop')
      if (navigator.userAgent.match(/MSIE 9.0/)) {
        this.$body.addClass('ie9');
      }
    }
  }
  Pages.prototype.isVisibleXs = function () {
    (!$('#pg-visible-xs').length) && this.$body.append('<div id="pg-visible-xs" class="visible-xs" />');
    return $('#pg-visible-xs').is(':visible');
  }
  Pages.prototype.isVisibleSm = function () {
    (!$('#pg-visible-sm').length) && this.$body.append('<div id="pg-visible-sm" class="visible-sm" />');
    return $('#pg-visible-sm').is(':visible');
  }
  Pages.prototype.isVisibleMd = function () {
    (!$('#pg-visible-md').length) && this.$body.append('<div id="pg-visible-md" class="visible-md" />');
    return $('#pg-visible-md').is(':visible');
  }
  Pages.prototype.isVisibleLg = function () {
    (!$('#pg-visible-lg').length) && this.$body.append('<div id="pg-visible-lg" class="visible-lg" />');
    return $('#pg-visible-lg').is(':visible');
  }
  Pages.prototype.getUserAgent = function () {
    return $('body').hasClass('mobile') ? 'mobile' : 'desktop';
  }
  Pages.prototype.setFullScreen = function (element) {
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;
    if (requestMethod) {
      requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== 'undefined') {
      var wscript = new ActiveXObject('WScript.Shell');
      if (wscript !== null) {
        wscript.SendKeys('{F11}');
      }
    }
  }
  Pages.prototype.getColor = function (color, opacity) {
    opacity = parseFloat(opacity) || 1;
    var elem = $('.pg-colors').length ? $('.pg-colors')  : $('<div class="pg-colors"></div>').appendTo('body');
    var colorElem = elem.find('[data-color="' + color + '"]').length ? elem.find('[data-color="' + color + '"]')  : $('<div class="bg-' + color + '" data-color="' + color + '"></div>').appendTo(elem);
    var color = colorElem.css('background-color');
    var rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    var rgba = 'rgba(' + rgb[1] + ', ' + rgb[2] + ', ' + rgb[3] + ', ' + opacity + ')';
    return rgba;
  }
	/** @function initSidebar
	 * @description Initialize side bar to open and close
	 * @param {(Element|JQuery)} [context] - A DOM Element, Document, or jQuery to use as context.
	 * @requires ui/sidebar.js
	 */
	Pages.prototype.initSidebar = function(context) {
		$('[data-pages="sidebar"]', context).each(function() {
			var $sidebar = $(this)
			$sidebar.sidebar($sidebar.data())
		})
	}
  Pages.prototype.setBackgroundImage = function () {
    $('[data-pages-bg-image]').each(function () {
      var _elem = $(this)
      var defaults = {
        pagesBgImage: '',
        lazyLoad: 'true',
        progressType: '',
        progressColor: '',
        bgOverlay: '',
        bgOverlayClass: '',
        overlayOpacity: 0,
      }
      var data = _elem.data();
      $.extend(defaults, data);
      var url = defaults.pagesBgImage;
      var color = defaults.bgOverlay;
      var opacity = defaults.overlayOpacity;
      var overlay = $('<div class="bg-overlay"></div>');
      overlay.addClass(defaults.bgOverlayClass);
      overlay.css({
        'background-color': color,
        'opacity': 1
      });
      _elem.append(overlay);
      var img = new Image();
      img.src = url;
      img.onload = function () {
        _elem.css({
          'background-image': 'url(' + url + ')'
        });
        _elem.children('.bg-overlay').css({
          'opacity': opacity
        });
      }
    })
  }
  Pages.prototype.initRevealFooter = function () {
    var _elem = $('[data-pages="reveal-footer"]');
    setHeight();
    function setHeight() {
      var h = _elem.outerHeight();
      _elem.prev().css({
        'margin-bottom': h
      })
    }
    $(window).resize(function () {
      setHeight();
    })
  }
  Pages.prototype.initFormGroupDefault = function () {
    $('.form-group.form-group-default').click(function () {
      $(this).find('input').focus();
    });
    
  }
  Pages.prototype.initTextRotator = function () {
    var defaults = {
      animation: 'flipUp',
      separator: ',',
      speed: 2000
    }
    $('[data-pages-init="text-rotate"]').each(function () {
      defaults = $(this).data();
      if (!$.fn.textrotator) return;
      $(this).textrotator(defaults);
    });
  }
  Pages.prototype.initAnimatables = function () {
    if (!$.fn.appear) return;
    $('[data-pages-animate="number"]').appear();
    $('[data-pages-animate="progressbar"]').appear();
    $('[data-pages-animate="number"]').on('appear', function () {
      $(this).animateNumbers($(this).attr('data-value'), true, parseInt($(this).attr('data-animation-duration')));
    });
    $('[data-pages-animate="progressbar"]').on('appear', function () {
      $(this).css('width', $(this).attr('data-percentage'));
    });
  }
  Pages.prototype.initAutoImageScroller = function () {
    $('[data-pages="auto-scroll"]').each(function () {
      var y = 0;
      var interval;
      var Screen = $(this).children('.screen');
      var img = Screen.children().children('img');
      var scroll = function () {
        var screenHeight = Screen.height();
        var swipeDistance = screenHeight / 2;
        y -= swipeDistance;
        img.css({
          'transform': 'translateY(' + y + 'px)'
        });
        if (y <= - img.height() + screenHeight + screenHeight / 2) {
          y = 0;
          clearTimeout(interval);
          setTimeout(function () {
            interval = setInterval(scroll, 1000);
          }, 1000);
        }
      }
      interval = setInterval(scroll, 1000);
    })
  }
  Pages.prototype.initUnveilPlugin = function () {
    $.fn.unveil && $('img').unveil();
  }


	/** @function initSlidingTabs
	 * @description Initialize Bootstrap Custom Sliding Tabs
	 * @param {(Element|JQuery)} [context] - A DOM Element, Document, or jQuery to use as context.
	 * @requires bootstrap.js
	 */
	Pages.prototype.initSlidingTabs = function(context) {
		// TODO: move this to a separate file
		$('a[data-toggle="tab"]', context).on('show.bs.tab', function(e) {
			e = $(e.relatedTarget || e.target).parent().find('a[data-toggle=tab]');
			e = $(e.target).parent().find('a[data-toggle=tab]');

			var hrefPrev = e.attr('href');

			var hrefCurrent = e.attr('href');

			if (!$(hrefCurrent).is('.slide-left, .slide-right')) return;
			$(hrefCurrent).addClass('sliding');

			setTimeout(function() {
				$(hrefCurrent).removeClass('sliding');
			}, 100);
		});
	}
	/** @function reponsiveTabs
	 * @description Responsive handlers for Bootstrap Tabs
	 */
	Pages.prototype.reponsiveTabs = function() {
		//Dropdown FX
		$('[data-init-reponsive-tabs="dropdownfx"]').each(function() {
			var drop = $(this);
			drop.addClass("d-none d-sm-block");
			var content = '<select class="cs-select cs-skin-slide full-width" data-init-plugin="cs-select">'
			for(var i = 1; i <= drop.children("li").length; i++){
				var li = drop.children("li:nth-child("+i+")");
				var selected ="";
				if(li.hasClass("active")){
					selected="selected";
				}
				content +='<option value="'+ li.children('a').attr('href')+'" '+selected+'>';
				content += li.children('a').text();
				content += '</option>';
			}
			content +='</select>'
			drop.after(content);
			var select = drop.next()[0];
			$(select).on('change', function (e) {
				var optionSelected = $("option:selected", this);
				var valueSelected = this.value;
				drop.find('a[href="'+valueSelected+'"]').tab('show')
			})
			$(select).wrap('<div class="nav-tab-dropdown cs-wrapper full-width p-t-10  d-block d-sm-none"></div>');
			new SelectFx(select);
		});

		$('[data-toggle="tab"]').on('show.bs.tab', function(e) {
			console.log()
			var th = $(this);
			th.parent().parent().find('li').not(th.parent()).removeClass('active');
			th.parent().addClass('active');
		})

		//Tab to Accordian
		//$.fn.tabCollapse && $('[data-init-reponsive-tabs="collapse"]').tabCollapse();
	}

  Pages.prototype.init = function () {
    this.setBackgroundImage();
		this.initSidebar();
    this.initFormGroupDefault();
    this.initUnveilPlugin();
    this.initAnimatables();
    this.initAutoImageScroller();
    this.initTextRotator();
    this.initRevealFooter();
    this.initSlidingTabs();
		this.reponsiveTabs();
  }
  $.Pages = new Pages();
  $.Pages.Constructor = Pages;
}) (window.jQuery);
(function ($) {
  'use strict';
  var Header = function (element, options) {
    this.$body = $('body');
    this.$element = $(element);
    this.options = $.extend(true, {
    }, $.fn.header.defaults, options);
    if (this.$element.attr('data-pages-header') == 'autoresize')
    this.options.autoresize = true
    if (this.$element.attr('data-pages-header') != null)
    this.options.minimizedClass = this.options.minimizedClass + ' ' + this.$element.attr('data-pages-resize-class');
    this.initAffix();
  }
  Header.prototype.initAffix = function () {
    if (this.$element.attr('data-pages-autofixed') == 'true') {
      this.$element.affix({
        offset: {
          top: this.$element.offset().top,
        }
      });
    }
  };
  Header.prototype.updateAffix = function () {
    if (this.$element.attr('data-pages-autofixed') == 'true') {
      console.log(this.$element.offset().top)
      this.$element.removeData('affix').removeClass('affix affix-top affix-bottom');
      this.$element.affix({
        offset: this.$element.offset().top
      })
    }
  };
  Header.prototype.addMinimized = function () {
    if (this.options.autoresize && !this.$element.hasClass('affix-top'))
    if (!this.$element.hasClass(this.options.minimizedClass))
    this.$element.addClass(this.options.minimizedClass);
  };
  Header.prototype.removeMinized = function () {
    if (this.options.autoresize || this.$element.hasClass('affix-top'))
    this.$element.removeClass(this.options.minimizedClass);
  };
  function Plugin(option) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('pg.header');
      var options = typeof option == 'object' && option;
      if (!data) $this.data('pg.header', (data = new Header(this, options)));
      if (typeof option == 'string') data[option]();
    })
  }
  var old = $.fn.header
  $.fn.header = Plugin
  $.fn.header.Constructor = Header
  $.fn.header.defaults = {
    duration: 350,
    autoresize: false,
    minimizedClass: 'minimized'
  }
  $.fn.header.noConflict = function () {
    $.fn.header = old;
    return this;
  }
  $(document).ready(function () {
    $('.menu > li > a').on('click', function (e) {
      if ($(this).parent().hasClass('mega')) {
        if ($(this).parent().hasClass('open')) {
          $(this).parents('.container').removeClass('clip-mega-menu');
        } else {
          $(this).parents('.container').addClass('clip-mega-menu');
        }
      } else {
        $(this).parents('.container').removeClass('clip-mega-menu');
      }
      $(this).parent().toggleClass('open')
      $('.open').not($(this).parent()).removeClass('open');
    });
    $('.desktop .menu > li > nav').on('mouseleave', function (e) {
      //$('.menu > li').removeClass('open');
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest(".open").length === 0) {
          $('.open').removeClass('open');
      }
    });
  })
  $(window).on('load', function () {
    $('[data-pages="header"]').each(function () {
      var $header = $(this)
      $header.header($header.data())
    })
  });
  $('[data-pages="header-toggle"]').on('click touchstart', function (e) {
    e.preventDefault();
    var el = $(this)
    var header = el.attr('data-pages-element');
    $('body').toggleClass('menu-opened');
    $('[data-pages="header-toggle"]').toggleClass('on');
  });
  $(window).on('resize', function () {
    $('[data-pages="header"]').header('updateAffix');
  })
  $(window).on('scroll', function () {
    var ScrollTop = parseInt($(window).scrollTop());
    if (ScrollTop > 1) {
      $('[data-pages="header"]').header('addMinimized');
    } else {
      if (ScrollTop < 10) {
        $('[data-pages="header"]').header('removeMinized');
      }
    }
  });
}) (window.jQuery);
/* ============================================================
  * Pages Sidebar
  * ============================================================ */

(function($) {
	'use strict';
	// SIDEBAR CLASS DEFINITION
	// ======================

	var Sidebar = function(element, options) {
		this.$element = $(element);
		this.$body = $('body');
		this.options = $.extend(true, {}, $.fn.sidebar.defaults, options);

		this.bezierEasing = [.05, .74, .27, .99];
		this.cssAnimation = true;
		this.css3d = true;

		this.sideBarWidth = 280;
		this.sideBarWidthCondensed = 280 - 70;



		this.$sidebarMenu = this.$element.find('.sidebar-menu > ul');
		this.$pageContainer = $(this.options.pageContainer);


		if (!this.$sidebarMenu.length) return;

		// apply perfectScrollbar plugin only for desktops
		($.Pages.getUserAgent() == 'desktop') && this.$sidebarMenu.scrollbar({
			ignoreOverlay: false
		});


		if (!Modernizr.csstransitions)
			this.cssAnimation = false;
		if (!Modernizr.csstransforms3d)
			this.css3d = false;

		// Bind events
		// Toggle sub menus
		// In Angular Binding is done using a pg-sidebar directive
		(typeof angular === 'undefined') && $(document).on('click', '.sidebar-menu a', function(e) {

			if ($(this).parent().children('.sub-menu') === false) {
				return;
			}
			var el = $(this);
			var parent = $(this).parent().parent();
			var li = $(this).parent();
			var sub = $(this).parent().children('.sub-menu');

			if(li.hasClass("open active")){
				el.children('.arrow').removeClass("open active");
				sub.slideUp(200, function() {
					li.removeClass("open active");
				});

			}else{
				parent.children('li.open').children('.sub-menu').slideUp(200);
				parent.children('li.open').children('a').children('.arrow').removeClass('open active');
				parent.children('li.open').removeClass("open active");
				el.children('.arrow').addClass("open active");
				sub.slideDown(200, function() {
					li.addClass("open active");

				});
			}
			//e.preventDefault();
		});

		// Toggle sidebar
		$('.sidebar-slide-toggle').on('click touchend', function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
			var el = $(this).attr('data-pages-toggle');
			if (el != null) {
				$(el).toggleClass('show');
			}
		});

		var _this = this;

		function sidebarMouseEnter(e) {
			var _sideBarWidthCondensed = _this.$body.hasClass("rtl") ? -_this.sideBarWidthCondensed : _this.sideBarWidthCondensed;

			var menuOpenCSS = (_this.css3d === true ? 'translate3d(' + _sideBarWidthCondensed + 'px, 0,0)' : 'translate(' + _sideBarWidthCondensed + 'px, 0)');

			console.log($.Pages.isVisibleSm(), 'sssssssssssssssssssssss');

			if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) {
				//return false
			}
			if ($('.close-sidebar').data('clicked')) {
				return;
			}
			if (_this.$body.hasClass('menu-pin'))
				return;
			if (_this.cssAnimation) {
				_this.$element.css({
					'transform': menuOpenCSS
				});
				_this.$body.addClass('sidebar-visible');
			} else {
				_this.$element.stop().animate({
					left: '0px'
				}, 400, $.bez(_this.bezierEasing), function() {
					_this.$body.addClass('sidebar-visible');
				});
			}
		}

		function sidebarMouseLeave(e) {
			var menuClosedCSS = (_this.css3d === true ? 'translate3d(0, 0,0)' : 'translate(0, 0)');

			if ($.Pages.isVisibleSm() || $.Pages.isVisibleXs()) {
				//return false
			}
			if (typeof e != 'undefined') {
				var target = $(e.target);
				if (target.parent('.page-sidebar').length) {
					//return;
				}
			}
			if (_this.$body.hasClass('menu-pin'))
				//return;

			if ($('.sidebar-overlay-slide').hasClass('show')) {
				$('.sidebar-overlay-slide').removeClass('show')
				$("[data-pages-toggle]").removeClass('active')

			}

			if (_this.cssAnimation) {
				_this.$element.css({
					'transform': menuClosedCSS
				});
				_this.$body.removeClass('sidebar-visible');
			} else {

				_this.$element.stop().animate({
					left: '-' + _this.sideBarWidthCondensed + 'px'
				}, 400, $.bez(_this.bezierEasing), function() {

					_this.$body.removeClass('sidebar-visible')
					setTimeout(function() {
						$('.close-sidebar').data({
							clicked: false
						});
					}, 100);
				});
			}
		}


		this.$element.bind('mouseenter', sidebarMouseEnter);
		this.$pageContainer.bind('mouseover', sidebarMouseLeave);

	}


	// Toggle sidebar for mobile view
	Sidebar.prototype.toggleSidebar = function(toggle) {
		var timer;
		var bodyColor = $('body').css('background-color');
		$('.page-container').css('background-color', bodyColor);
		if (this.$body.hasClass('sidebar-open')) {
			this.$body.removeClass('sidebar-open');
			timer = setTimeout(function() {
				this.$element.removeClass('visible');
			}.bind(this), 400);
		} else {
			clearTimeout(timer);
			this.$element.addClass('visible');
			setTimeout(function() {
				this.$body.addClass('sidebar-open');
			}.bind(this), 10);
			setTimeout(function(){
				// remove background color
				$('.page-container').css({'background-color': ''});
			},1000);

		}

	}

	Sidebar.prototype.togglePinSidebar = function(toggle) {
		if (toggle == 'hide') {
			this.$body.removeClass('menu-pin');
		} else if (toggle == 'show') {
			this.$body.addClass('menu-pin');
		} else {
			this.$body.toggleClass('menu-pin');
		}

	}


	// SIDEBAR PLUGIN DEFINITION
	// =======================
	function Plugin(option) {
		return this.each(function() {
			var $this = $(this);
			var data = $this.data('pg.sidebar');
			var options = typeof option == 'object' && option;

			if (!data) $this.data('pg.sidebar', (data = new Sidebar(this, options)));
			if (typeof option == 'string') data[option]();
		})
	}

	var old = $.fn.sidebar;

	$.fn.sidebar = Plugin;
	$.fn.sidebar.Constructor = Sidebar;


	$.fn.sidebar.defaults = {
		pageContainer: '.page-container'
	}

	// SIDEBAR PROGRESS NO CONFLICT
	// ====================

	$.fn.sidebar.noConflict = function() {
		$.fn.sidebar = old;
		return this;
	}

	// SIDEBAR PROGRESS DATA API
	//===================

	$(document).on('click.pg.sidebar.data-api', '[data-toggle-pin="sidebar"]', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $target = $('[data-pages="sidebar"]');
		$target.data('pg.sidebar').togglePinSidebar();
		return false;
	})
	$(document).on('click.pg.sidebar.data-api touchstart', '[data-toggle="sidebar"]', function(e) {
		e.preventDefault();
		var $this = $(this);
		var $target = $('[data-pages="sidebar"]');
		$target.data('pg.sidebar').toggleSidebar();
		return false
	})

})(window.jQuery);


(function ($) {
  'use strict';
  var Parallax = function (element, options) {
    this.$element = $(element);
    this.$body = $('body');
    this.options = $.extend(true, {
    }, $.fn.parallax.defaults, options);
    this.$coverPhoto = this.$element.find('.cover-photo');
    this.$content = this.$element.find('.inner');
    if (this.$coverPhoto.find('> img').length) {
      var img = this.$coverPhoto.find('> img');
      this.$coverPhoto.css('background-image', 'url(' + img.attr('src') + ')');
      img.remove();
    }
    this.translateBgImage();
  }
  Parallax.VERSION = '1.0.0';
  Parallax.prototype.animate = function (translate) {
    var scrollPos;
    var pagecoverHeight = this.$element.height();
    var opacityKeyFrame = pagecoverHeight * 50 / 100;
    var direction = 'translateX';
    scrollPos = $(window).scrollTop();
    if (this.$body.hasClass('mobile')) {
      scrollPos = - (translate);
    }
    direction = 'translateY';
    this.$coverPhoto.css({
      'transform': direction + '(' + scrollPos * this.options.speed.coverPhoto + 'px)'
    });
    this.$content.css({
      'transform': direction + '(' + scrollPos * this.options.speed.content + 'px)',
    });
    this.translateBgImage();
  }
  Parallax.prototype.translateBgImage = function () {
    var scrollPos = $(window).scrollTop();
    var pagecoverHeight = this.$element.height();
    if (this.$element.attr('data-pages-bg-image')) {
      var relativePos = this.$element.offset().top - scrollPos;
      if (relativePos > - pagecoverHeight && relativePos <= $(window).height()) {
        var displacePerc = 100 - ($(window).height() - relativePos) / ($(window).height() + pagecoverHeight) * 100;
        this.$element.css({
          'background-position': 'center ' + displacePerc + '%'
        });
      }
    }
  }
  function Plugin(option) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('pg.parallax');
      var options = typeof option == 'object' && option;
      if (!data) $this.data('pg.parallax', (data = new Parallax(this, options)));
      if (typeof option == 'string') data[option]();
    })
  }
  var old = $.fn.parallax
  $.fn.parallax = Plugin
  $.fn.parallax.Constructor = Parallax
  $.fn.parallax.defaults = {
    speed: {
      coverPhoto: 0.3,
      content: 0.17
    }
  }
  $.fn.parallax.noConflict = function () {
    $.fn.parallax = old;
    return this;
  }
  $(window).on('load', function () {
    $('[data-pages="parallax"]').each(function () {
      var $parallax = $(this)
      $parallax.parallax($parallax.data())
    })
  });
  $(window).on('scroll', function () {
    $('[data-pages="parallax"]').parallax('animate');
  });
}) (window.jQuery);
(function ($) {
  'use strict';
  var Search = function (element, options) {
    this.$element = $(element);
    this.options = $.extend(true, {
    }, $.fn.search.defaults, options);
    this.init();
  }
  Search.VERSION = '1.0.0';
  Search.prototype.init = function () {
    var _this = this;
    this.pressedKeys = [
    ];
    this.ignoredKeys = [
    ];
    this.$searchField = this.$element.find(this.options.searchField);
    this.$closeButton = this.$element.find(this.options.closeButton);
    this.$suggestions = this.$element.find(this.options.suggestions);
    this.$brand = this.$element.find(this.options.brand);
    this.$searchField.on('keyup', function (e) {
      _this.$suggestions && _this.$suggestions.html($(this).val());
    });
    this.$searchField.on('keyup', function (e) {
      _this.options.onKeyEnter && _this.options.onKeyEnter(_this.$searchField.val());
      if (e.keyCode == 13) {
        e.preventDefault();
        _this.options.onSearchSubmit && _this.options.onSearchSubmit(_this.$searchField.val());
      }
      if ($('body').hasClass('overlay-disabled')) {
        return 0;
      }
    });
    this.$closeButton.on('click', function () {
      _this.toggleOverlay('hide');
    });
    this.$element.on('click', function (e) {
      if ($(e.target).data('pages') == 'search') {
        _this.toggleOverlay('hide');
      }
    });
    $(document).on('keypress.pg.search', function (e) {
      _this.keypress(e);
    });
    $(document).on('keyup', function (e) {
      if (_this.$element.is(':visible') && e.keyCode == 27) {
        _this.toggleOverlay('hide');
      }
    });
  }
  Search.prototype.keypress = function (e) {
    e = e || event;
    var nodeName = e.target.nodeName;
    if ($('body').hasClass('overlay-disabled') || $(e.target).hasClass('js-input') || nodeName == 'INPUT' || nodeName == 'TEXTAREA') {
      return;
    }
    if (e.which !== 0 && e.charCode !== 0 && !e.ctrlKey && !e.metaKey && !e.altKey && e.keyCode != 27) {
      this.toggleOverlay('show', String.fromCharCode(e.keyCode | e.charCode));
    }
  }
  Search.prototype.toggleOverlay = function (action, key) {
    var _this = this;
    if (action == 'show') {
      this.$element.removeClass('hide');
      this.$element.fadeIn('fast');
      if (!this.$searchField.is(':focus')) {
        this.$searchField.val(key);
        setTimeout(function () {
          this.$searchField.focus();
          var tmpStr = this.$searchField.val();
          this.$searchField.val('');
          this.$searchField.val(tmpStr);
        }.bind(this), 100);
      }
      this.$element.removeClass('closed');
      this.$brand.toggleClass('invisible');
      $(document).off('keypress.pg.search');
    } else {
      this.$element.fadeOut('fast').addClass('closed');
      this.$searchField.val('').blur();
      setTimeout(function () {
        if ((this.$element).is(':visible')) {
          this.$brand.toggleClass('invisible');
        }
        $(document).on('keypress.pg.search', function (e) {
          _this.keypress(e);
        });
      }.bind(this), 100);
    }
  };
  function Plugin(option) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('pg.search');
      var options = typeof option == 'object' && option;
      if (!data) {
        $this.data('pg.search', (data = new Search(this, options)));
      }
      if (typeof option == 'string') data[option]();
    })
  }
  var old = $.fn.search
  $.fn.search = Plugin
  $.fn.search.Constructor = Search
  $.fn.search.defaults = {
    searchField: '[data-search="searchField"]',
    closeButton: '[data-search="closeButton"]',
    suggestions: '[data-search="suggestions"]',
    brand: '[data-search="brand"]'
  }
  $.fn.search.noConflict = function () {
    $.fn.search = old;
    return this;
  }
  $(document).on('click.pg.search.data-api', '[data-toggle="search"]', function (e) {
    var $this = $(this);
    var $target = $('[data-pages="search"]');
    if ($this.is('a')) e.preventDefault();
    $target.data('pg.search').toggleOverlay('show');
  })
}) (window.jQuery);
(function ($) {
  'use strict';
  var Float = function (element, options) {
    this.$element = $(element);
    this.options = $.extend(true, {
    }, $.fn.pgFloat.defaults, options);
    var _this = this;
    var _prevY;
    function update() {
      var element = _this.$element;
      var w = $(window).scrollTop();
      var translateY = (w - element.offset().top) * _this.options.speed;
      var delay = _this.options.delay / 1000;
      var curve = _this.options.curve;
      var maxTopTranslate = _this.options.maxTopTranslate;
      var maxBottomTranslate = _this.options.maxBottomTranslate;
      if (maxTopTranslate == 0) {
        if (element.offset().top + element.outerHeight() < w) return;
      }
      if (maxBottomTranslate == 0) {
        if (element.offset().top > w + $(window).height()) return;
      }
      if (_prevY < translateY) {
        if (maxTopTranslate != 0 && Math.abs(translateY) > maxTopTranslate) return;
      } else {
        if (maxBottomTranslate != 0 && Math.abs(translateY) > maxBottomTranslate) return;
      }
      element.css({
        'transition': 'transform ' + delay + 's ' + curve,
        'transform': 'translateY(' + translateY + 'px)',
      });
      _prevY = translateY;
    }
    $(window).bind('scroll', function () {
      update()
    });
    $(window).bind('load', function () {
      update()
    });
  }
  Float.VERSION = '1.0.0';
  function Plugin(option) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('pgFloat');
      var options = typeof option == 'object' && option;
      if (!data) $this.data('pgFloat', (data = new Float(this, options)));
      if (typeof option == 'string') data[option]();
    })
  }
  var old = $.fn.pgFloat;
  $.fn.pgFloat = Plugin;
  $.fn.pgFloat.Constructor = Float;
  $.fn.pgFloat.defaults = {
    topMargin: 0,
    bottomMargin: 0,
    speed: 0.1,
    delay: 1000,
    curve: 'ease'
  }
  $.fn.pgFloat.noConflict = function () {
    $.fn.pgFloat = old;
    return this;
  }
  $(window).on('load', function () {
    $('[data-pages="float"]').each(function () {
      var $pgFloat = $(this)
      $pgFloat.pgFloat($pgFloat.data())
    })
  });
}) (window.jQuery);
(function ($) {
  'use strict';
  (typeof angular === 'undefined') && $.Pages.init();
}) (window.jQuery);
