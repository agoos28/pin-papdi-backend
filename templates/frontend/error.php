<?php
defined('_JEXEC') or die('Restricted access');

$baseUrl = JURI::base();
$tmplUrl = $baseUrl . 'templates/frontend/';
$curentUrl = JURI::getInstance();

$user = JFactory::getUser();

$conf = &JFactory::getConfig();
$sitename = $conf->getValue('config.sitename');
$lang = $conf->getValue("joomfish.language");

$this->setHeadData($headers);
$this->setGenerator('agoos28');

?>
<html class="wf-active">

<head>
	<meta charset="utf-8"/>
	<jdoc:include type="head"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="shortcut icon" href="images/favicon.ico">

	<link href="<?php echo $tmplUrl; ?>plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $tmplUrl; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $tmplUrl; ?>plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $tmplUrl; ?>plugins/swiper/css/swiper.css" rel="stylesheet" type="text/css" media="screen"/>
	<link rel="stylesheet" href="<?php echo JURI::base(); ?>templates/admin/plugins/select2/select2.css">

	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/pages.css" rel="stylesheet" type="text/css"/>
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/pages-icons.css" rel="stylesheet" type="text/css"/>

	<script src="<?php echo $tmplUrl; ?>plugins/pace/pace.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/swiper/js/swiper.jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/velocity/velocity.min.js"></script>
	<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/velocity/velocity.ui.js"></script>
</head>

<body class="error-page">
<div class="container-xs-height full-height">
	<div class="row-xs-height">
		<div class="col-xs-height col-middle">
			<div class="error-container text-center p-r-15 p-l-15">
				<h1 class="error-number" style="color: #dd2219;"><?php echo $this->error->code ?></h1>
				<h2 class="semi-bold"><?php echo str_ireplace('Component', 'Page', $this->error->message); ?></h2>
				<p class="m-t-15 m-b-15">This page you are looking for does not exsist, go to
					<a style="color: #dd2219;" href="<?php echo $baseUrl; ?>">front page</a>
				</p>
			</div>
		</div>
	</div>
</div>
<div class="m-t-60">
	<div class="error-container">
		<div class="error-container-innner">
			<div class="d-flex m-r-30 align-items-center m-b-30">
				<div class="col-md-auto">
					<img alt="" class="m-t-5" src="<?php echo JURI::base(); ?>templates/frontend/images/small-logo.png"
							 width="60"
							 height="60">
				</div>
				<div class="col">
					<p class="text-justify">
						<small>
							Perhimpunan Dokter Spesialis Penyakit Dalam Indonesia @2020 All rights reserved
						</small>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
</body>

</html>