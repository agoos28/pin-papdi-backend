<?php // @version $Id: default_form.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
$user  = JFactory::getUser();
?>

<form role="form" class="m-t-15 form-validate" action="<?php echo JURI::current(); ?>" method="post" name="emailForm" id="ckform">
  <div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default">
        <label><?php echo JText::_('FULL_NAME'); ?></label>
        <input type="text" class="form-control" name="name" placeholder="<?php echo JText::_('NAME_PLACEHOLDER'); ?>">
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="form-group form-group-default">
        <label><?php echo JText::_('EMAIL'); ?></label>
        <input type="email" placeholder="<?php echo JText::_('EMAIL_PLACEHOLDER'); ?>" class="form-control">
      </div>
    </div>
    <div class="col-sm-6">
      <div class="form-group form-group-default">
        <label><?php echo JText::_('PHONE'); ?></label>
        <input type="email" placeholder="<?php echo JText::_('PHONE_PLACEHOLDER'); ?>" class="form-control">
      </div>
    </div>
  </div>
  <div class="form-group form-group-default">
        <label><?php echo JText::_('SUBJECT'); ?></label>
        <input type="email" placeholder="<?php echo JText::_('SUBJECT_PLACEHOLDER'); ?>" class="form-control">
      </div>
  <div class="form-group form-group-default">
    <label><?php echo JText::_('MESSAGE'); ?></label>
    <textarea placeholder="<?php echo JText::_('MESSAGE_PLACEHOLDER'); ?>" style="height:100px" class="form-control"></textarea>
  </div>
  <div class="sm-p-t-10 clearfix">
    <p class="pull-left small hint-text m-t-5 font-arial"><?php echo JText::_('CONTACT_FORM_DISCLAIMER'); ?></p>
    <button class="btn btn-primary font-montserrat all-caps fs-12 pull-right xs-pull-left sendck"><?php echo JText::_('SEND'); ?></button>
  </div>
  <div class="clearfix"></div>
  <input type="hidden" name="view" value="contact" />
  <input type="hidden" name="id" value="<?php echo $this->contact->id; ?>" />
  <input type="hidden" name="task" value="submit" />
  <?php echo JHTML::_('form.token'); ?>
</form>