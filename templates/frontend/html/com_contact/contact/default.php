<?php // @version $Id: default.php 9718 2007-12-20 22:35:36Z eddieajau $
defined('_JEXEC') or die('Restricted access');
$cparams = JComponentHelper::getParams ('com_media');
$messages = JApplication::getMessageQueue();
$user = JFactory::getUser();
$model		= &$this->getModel();
if($messages){
	foreach($messages as $message){
		if($message['type'] == 'message'){
			$notif = $message['message'];
		}
	}
}
$content = $model->getRawContent('762');
$poss = json_decode($content->gmap);
$form = $this->loadTemplate('form');
?>
<section class="container container-fixed-lg p-t-65 p-b-100  sm-p-b-30 sm-m-b-30">
  <?php
		if($user->usertype == 'Super Administrator'){
			echo '<a class="btn btn-xs btn-red btn-edit" href="'.JURI::base().'component/cckjseblod/?view=type&layout=form&typeid=22&cckid=762">edit</a>';
		}
		?>
  <?php 
    echo str_replace('{render_form}', $form, $content->wysiwyg_description);
  ?>
</section>
<section class="container  no-padding no-overflow">
  <div id="gmap" class="full-width demo-map no-overflow"></div>
</section>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDu-rZIRL6bsp4oj_8kL6dGQ6zu_ljo1tk "></script>
<script type="text/javascript" src="<?php echo JURI::base() . 'templates/frontend/js/gmap3.min.js'; ?>"></script>
<style>
.gm-ui-hover-effect {
    opacity: .6;
    top: 0px !important;
    right: 0px !important;
}
</style>
<script type="text/javascript">
$(document).ready(function(e) {
  	var curloc = [<?php echo $poss->H; ?>, <?php echo $poss->L; ?>]
	if(curloc){
		curloc = [<?php echo $poss->H; ?>, <?php echo $poss->L; ?>]
	}else{
		curloc = [-6.174176983010685, 106.82818221976049 ]
	}
	google.maps.controlStyle = 'azteca'
	jQuery('#gmap').gmap3({ 
	map:{
		options:{
			zoom: 12,
			zoomControl: true,
			ZoomControlOptions: {
				position: google.maps.ControlPosition.LEFT_CENTER
			},
			mapTypeControl: true,
			mapTypeControlOptions: { 
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, 
							google.maps.MapTypeId.SATELLITE,
							google.maps.MapTypeId.HYBRID,
							google.maps.MapTypeId.TERRAIN],
				position: google.maps.ControlPosition.RIGHT_TOP,
				style: google.maps.MapTypeControlStyle.VERTICAL_BAR
			},
			scrollwheel: false,
			disableDoubleClickZoom: true,
			navigationControl: true,
			scaleControl: true
		}
	},
	marker:{
    values:[{
      latLng: curloc,
      data: {
        desc: ''
      }
    }],
		options:{
			draggable: false
		},
		events: {
			mouseover: function(marker, event, context){
				var infowindow = $(this).gmap3({get:{name:"infowindow"}});
				if (infowindow){
					infowindow.close();
				}
				$(this).gmap3({
					infowindow:{
						anchor: marker, 
						options: {content: '<?php echo preg_replace( "/\r|\n/", "", $content->map_desc); ?>'}
					}
				});
			},
			mouseout: function(){
				
			},
			click: function(marker, event, context){
			
			}
		}
	},
	autofit:{maxZoom: 17}
  },
  'autofit'
);
});
$(window).load(function(){
	 $('#gmap').gmap3({trigger:"resize"});
})
</script>
