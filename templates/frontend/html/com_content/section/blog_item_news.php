<?php // @version $Id: blog_item.php 9718 2007-12-20 22:35:36Z eddieajau $
defined('_JEXEC') or die('Restricted access');

?>
<li
  class="o-grid__item c-news-item" data-animate="sequence">
  <?php echo $this->item->event->beforeDisplayContent; ?>
  
  <?php echo JFilterOutput::ampReplace($this->item->text); ?>
  
  <?php echo $this->item->event->afterDisplayContent; ?>
</li>
