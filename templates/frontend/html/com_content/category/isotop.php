<?php // @version $Id: blog.php 9722 2007-12-21 16:55:15Z mtk $
defined('_JEXEC') or die('Restricted access');
$cparams = JComponentHelper::getParams('com_media');
$categories = $this->get('Siblings');

$section = $this->get('Category');
$brands = $this->get('Brands');
$min = $this->get('Min');
$max = $this->get('Max');
$metadesc = '';
$metakey = $this->params->get('page_title');
function filters($name = NULL, $value = NULL, $text = NULL)
{
  $currenturl = JURI::getInstance()->toString();
  if (JRequest::getVar($name) == $value)
  {
	$current = 'class="current"';
  } else
  {
	$current = '';
  }
  $url = explode('?', $currenturl);
  if (count($url) <= 1)
  {
	$urlres = $currenturl . '?' . $name . '=' . $value;
  } else
  {
	$filters = explode('&', $url[1]);
	if (count($filters) <= 1)
	{
	  $filter = explode('=', $filters[0]);
	  if ($filter[0] == $name)
	  {
		if ($value == 'reset')
		{
		  $urlres = str_replace($filter[0] . '=' . $filter[1], '', $currenturl);
		} else
		{
		  $urlres = str_replace($filter[0] . '=' . $filter[1], $filter[0] . '=' . $value, $currenturl);
		}
	  } else
	  {
		if ($value == 'reset')
		{
		  $urlres = $currenturl;
		} else
		{
		  $urlres = $currenturl . '&' . $name . '=' . $value;
		}
	  }
	} else
	{
	  $urlres = $url[0];
	  $p = 0;
	  foreach ($filters as $filter)
	  {
		if ($p == 0)
		{
		  $sep = '?';
		} else
		{
		  $sep = '&';
		}
		$filter = explode('=', $filter);
		if ($filter[0] == $name)
		{
		  if ($value == 'reset')
		  {
			$urlres .= '';
			$p = $p - 1;
		  } else
		  {
			$urlres .= $sep . $filter[0] . '=' . $value;
		  }
		  $exist = 1;
		} else
		{
		  $urlres .= $sep . $filter[0] . '=' . $filter[1];
		}
		$p++;
	  }
	  if ( ! $exist)
	  {
		$addup = '&' . $name . '=' . $value;
	  }
	}
  }
  return '<a ' . $current . ' href="' . $urlres . $addup . '">' . $text . '</a>';
}

?>
<article id="page-people" class="o-wrapper o-wrapper--mid c-content">
  <header class="c-content__header" data-animate="up-fast">
    <h1 class="c-content__title"
        data-animate="up-slow"><?php echo $this->escape($this->params->get('page_title')); ?></h1>
    <div class="c-content__intro o-wrapper o-wrapper--narrow">
	  <?php echo $this->category->edit_link; ?>
	  <?php echo $this->category->desc; ?>
    </div>
  </header>
  <div class="c-content__body">
    <form
      action="https://www.vsadc.com/experts"
      method="get"
      class="o-layout o-layout--flush o-layout--center o-layout--auto c-form c-form--centered u-margin-bottom-large"
      data-people-search
    >
      <div
        class="o-layout__item c-form__field u-margin-none u-1/1 u-1/2@medium u-1/3@large"
      >
        <div class="c-form__label">
          <label for="people-search"
          >Find an expert by name or issue</label
          >
        </div>
        <div class="c-form__input js-inplace">
          <input type="search" id="people-search" name="q" value=""/>
        </div>
      </div>
    </form>
    <section
      id="people-list"
      class="c-content__block c-loadable u-align-center"
    >
      <ul class="o-grid o-grid--center o-grid--four">
		<?php $i = $this->pagination->limitstart; ?>
		
		<?php $introcount = 20;
		if ($introcount) :
		  $colcount = 1;
		  if ($colcount == 0) :
			$colcount = 1;
		  endif;
		  $rowcount = (int)$introcount / $colcount;
		  $ii = 0;
		  for ($y = 0; $y < $rowcount && $i < $this->total; $y++) : ?>
			
			<?php for ($z = 0; $z < $colcount && $ii < $introcount && $i < $this->total; $z++, $i++, $ii++) : ?>
			  <?php $this->item =& $this->getItem($i, $this->params);
			  echo $this->loadTemplate('item'); ?>
			<?php endfor; ?>
		  
		  
		  <?php endfor;
		endif; ?>
      </ul>
    </section>
  </div>
</article>
<?php
//$doc =& JFactory::getDocument();
//$metadesc =  'Toko Online Baju Anak Branded kami menjual berbagai macam '.$this->params->get('page_title').' untuk laki-laki atau perempuan.';
//$doc->setMetaData( 'description',  $metadesc);
//$doc->setMetaData( 'keywords',  $metakey);
//$doc->setBuffer('Baju Anak Branded Kategori '.$this->params->get('page_title'), 'seotitle', 'value');
?>
