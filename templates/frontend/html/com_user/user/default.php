<?php // @version $Id: default.php  $
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');
$avatar = JURI::base() . 'images/propict/' . $this->user->id . '.png';
if (!JFile::exists(JPATH_BASE . DS . 'images' . DS . 'propict' . DS . $this->user->id . '.png')) {
	$avatar = JURI::base() . 'images/propict/default.png';
}
// print_r($this->user);

?>
<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- START CONTAINER FLUID -->
	<div class="container ">
		<div class="row">
			<div class="col-sm-3"><!--left col-->
				<form method="post" class="text-center p-b-60">
					<div class="relative upload-propict">
						<div class="avatar-loader">
							<div class="progress-circle-indeterminate">
							</div>
						</div>
						<div class="avatar-holder">
							<img src="<?php echo $avatar; ?>" class="avatar full-width"
									 alt="avatar">
						</div>
						<div id="upload-propict"></div>
						<div class="buttons">
							<input id="upload" name="file" type="file" class="text-center center-block file-upload d-none">
							<label for="upload" class="btn btn-primary btn-sm">
								Ganti
							</label>
							<button id="avatar-cancel" class="btn btn-danger btn-sm">Batal</button>
							<button id="upload-result" class="btn btn-success btn-sm">Simpan</button>
						</div>
					</div>
				</form>
				<br>

			</div><!--/col-3-->
			<div class="col-md-6 col-sm-9">

				<div class="panel">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
						<li class="active relative">
							<a class="inline" data-toggle="tab" role="tab" href="#tab1"><span>Profil</span></a>
						</li>
						<li class="relative">
							<a class="inline" data-toggle="tab" role="tab" href="#tab2"><span>Organisasi</span></a>
						</li>
						<li class="relative">
							<a class="inline" data-toggle="tab" role="tab" href="#tab3"><span>Keamanan</span></a>
						</li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane slide-left active" id="tab1">
							<div id="profile" class="row">
								<div class="col-sm-12">
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;"><?php echo JText::_('Name'); ?></div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->name; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;"><?php echo JText::_('Email'); ?></div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->email; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;"><?php echo JText::_('Phone'); ?></div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->phone; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Akses</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->usertype; ?></div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Terdaftar</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->registerDate; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Terakhir Login</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->lastvisitDate; ?></div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Alamat</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->address; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Negara</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->country; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Kota</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->city; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Propinsi</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->province; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;">Kode POS</div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->postal; ?></div>
										</div>
									</div>
								</div>
								<div class="col-12 text-right">
									<button class="editToggle btn btn-primary">
										Edit
									</button>
								</div>
							</div>

							<form id="profileForm" action="<?php echo JURI::current(); ?>" method="post" name="userform"
										autocomplete="off" class="acc frm" style="display: none;">
								<div class="alert alert-danger hide">
									<div class="notification"></div>
								</div>
								<div class="form-group-attached m-b-15">
									<div class="form-group form-group-default" style="background: #f1f1f1;">
										<label>Profil</label>
									</div>
									<div class="form-group form-group-default">
										<label>Nama</label>
										<input type="text" class="form-control" name="name" value="<?php echo $this->user->name; ?>">
									</div>
									<div class="form-group form-group-default">
										<label>Telpon</label>
										<input type="text" class="form-control" name="phone" value="<?php echo $this->user->phone; ?>">
									</div>
								</div>

								<div class="form-group-attached">
									<div class="form-group form-group-default" style="background: #f1f1f1;">
										<label>Alamat Penagihan</label>
									</div>
									<div class="form-group form-group-default">
										<label>Alamat</label>
										<input type="text" class="form-control" name="address" value="<?php echo $this->user->address; ?>">
									</div>
									<div class="row clearfix">
										<div class="col-sm-6">
											<div class="form-group form-group-default">
												<label>Negara</label>
												<input type="text" class="form-control" name="country"
															 value="<?php echo $this->user->country ? $this->user->country : 'Indonesia'; ?>">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group form-group-default">
												<label>Kota</label>
												<input type="text" class="form-control" name="city" value="<?php echo $this->user->city; ?>">
											</div>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-sm-9">
											<div class="form-group form-group-default">
												<label>Propinsi</label>
												<input type="text" class="form-control" name="province" value="<?php echo $this->user->province; ?>">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group form-group-default">
												<label>Kode Pos</label>
												<input type="text" class="form-control" name="postal" value="<?php echo $this->user->postal; ?>">
											</div>
										</div>
									</div>
								</div>

								<div class="text-right m-t-15">
									<button class="btn btn-default cancelTogle" type="button"><?php echo JText::_('Cancel'); ?></button>
									<button class="btn btn-primary" type="submit"><?php echo JText::_('Save'); ?></button>
								</div>
								<input type="hidden" name="id" value="<?php echo $this->user->get('id'); ?>"/>
								<input type="hidden" name="username" value="<?php echo $this->user->get('username'); ?>"/>
								<input type="hidden" name="gid" value="<?php echo $this->user->get('gid'); ?>"/>
								<input type="hidden" name="option" value="com_user"/>
								<input type="hidden" name="task" value="save"/>
								<input type="hidden" name="return" value="<?php echo base64_encode(JURI::current()) ?>"/>
								<?php echo JHTML::_('form.token'); ?>
							</form>
						</div>
						<div class="tab-pane slide-left" id="tab2">
							<div class="row">
								<div class="col-sm-6 col-xs-12">
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;"><?php echo JText::_('Name'); ?></div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->name; ?></div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-5 col-md-4 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div class="purple" style="font-weight: bold;"><?php echo JText::_('Email'); ?></div>
										</div>
										<div class="col-lg-7 col-md-6 col-sm-6 col-6" style="margin-bottom: 10px;">
											<div style="padding-top: 0;"><?php echo $this->user->email; ?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane slide-left" id="tab3">
							<form id="profileForm" action="<?php echo JURI::current(); ?>" method="post" name="userform"
										autocomplete="off" class="acc frm">
								<div class="alert alert-danger hide">
									<div class="notification"></div>
								</div>
								<div class="form-group-attached m-b-15">
									<div class="form-group form-group-default" style="background: #f1f1f1;">
										<label>Update Password</label>
									</div>
									<div class="form-group form-group-default">
										<label>Password Lama</label>
										<input type="password" class="form-control" name="old_password" value="">
									</div>
									<div class="form-group form-group-default">
										<label>Password Baru</label>
										<input type="password" class="form-control" name="password" value="">
									</div>
									<div class="form-group form-group-default">
										<label>Ulangi Password Baru</label>
										<input type="password" class="form-control" name="password2" value="">
									</div>
								</div>
								<div class="text-right m-t-15">
									<button class="btn btn-primary" type="submit"><?php echo JText::_('Save'); ?></button>
								</div>
								<input type="hidden" name="id" value="<?php echo $this->user->get('id'); ?>"/>
								<input type="hidden" name="username" value="<?php echo $this->user->get('username'); ?>"/>
								<input type="hidden" name="gid" value="<?php echo $this->user->get('gid'); ?>"/>
								<input type="hidden" name="option" value="com_user"/>
								<input type="hidden" name="task" value="updatePassword"/>
								<input type="hidden" name="return" value="<?php echo base64_encode(JURI::current()) ?>"/>
								<?php echo JHTML::_('form.token'); ?>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php
				$document = JFactory::getDocument();
				$renderer = $document->loadRenderer('module');
				$module = JModuleHelper::getModule('login', 'Sidebar Login');
				echo $renderer->render($module);
				?>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function demoUpload() {
		var $uploadCrop;

		function readFile(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('.upload-propict').addClass('ready');
					$uploadCrop.croppie('bind', {
						url: e.target.result
					}).then(function () {
						console.log('jQuery bind complete');
					});

				}

				reader.readAsDataURL(input.files[0]);
			} else {
				swal("Sorry - you're browser doesn't support the FileReader API");
			}
		}

		$uploadCrop = $('#upload-propict').croppie({
			viewport: {
				width: '100%',
				height: '100%',
			},
			enableExif: true
		});

		$('#upload').on('change', function () {
			readFile(this);
		});
		$('#avatar-cancel').on('click', function (e) {
			e.preventDefault();
			$('.upload-propict').removeClass('ready');
		});
		$('#upload-result').on('click', function (e) {
			e.preventDefault();
			$('.upload-propict').addClass('loading');
			$uploadCrop.croppie('result', {
				type: 'base64',
				size: 'viewport'
			}).then(function (resp) {
				var postdata = {
					image: resp,
					task: 'uploadProPict'
				};
				postdata['<?php echo JUtility::getToken(); ?>'] = 1;
				$.post('<?php echo JURI::current(); ?>', postdata, function (response) {
					console.log(response);
					response = JSON.parse(response);
					if (response.uri) {
						$('.avatar').attr('src', response.uri + '?' + Date.now());
						$('.upload-propict').removeClass('ready').removeClass('loading');
					}
					if (response.error) {
						alert(response.error)
					}
				})
			});
		});
	}

	$(document).ready(function () {
		demoUpload()
		$('.editToggle').click(function (e) {
			e.preventDefault()
			$(this).parent().parent().hide()
			$(this).parent().parent().next().show()
		})
		$('.cancelTogle').click(function (e) {
			e.preventDefault()
			$(this).parent().parent().hide()
			$(this).parent().parent().prev().show()
		})
	})

</script>