<?php // @version $Id: default_login.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
?>

<div class="login-wrapper ">
	<!-- START Login Background Pic Wrapper-->
	<div class="bg-pic">
		<!-- START Background Pic-->
		<img  src="<?php echo JURI::base(); ?>templates/frontend/images/login-background.jpg" alt="" class="lazy">
		<!-- END Background Pic-->
		<!-- START Background Caption-->
		<!-- END Background Caption-->
	</div>
	<!-- END Login Background Pic Wrapper-->
	<!-- START Login Right Container-->
	<div class="login-container bg-white">
		<div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
			<h1 class="bold m-t-80">Login</h1>
			<!-- START Login Form -->
			<form id="form-login" class="p-t-15" role="form" novalidate="novalidate" method="post">
				<!-- START Form Control-->
				<div class="form-group form-group-default">
					<label>Email</label>
					<div class="controls">
						<input type="text" name="username" placeholder="<?php echo JText::_('Email'); ?>" class="form-control" required>
					</div>
				</div>
				<!-- END Form Control-->
				<!-- START Form Control-->
				<div class="form-group form-group-default">
					<label>Password</label>
					<div class="controls">
						<input type="password" class="form-control" name="passwd" placeholder="<?php echo JText::_('Password'); ?>" required>
					</div>
				</div>
				<!-- START Form Control-->
				<div class="row">
					<div class="col-md-6 no-padding">
						<div class="checkbox ">
							<input type="checkbox" value="1" id="checkbox1">
							<label for="checkbox1">Ingat saya</label>
						</div>
					</div>
					<div class="col-md-6 text-right">
						<a href="<?php echo JRoute::_('index.php?option=com_user&view=reset'); ?>" class="small"><?php echo JText::_('FORGOT_YOUR_PASSWORD'); ?></a>
					</div>
				</div>
				<!-- END Form Control-->
				<button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
				<div class="m-t-30">
					Belum punya akun?,
					<a href="<?php echo JRoute::_('index.php?option=com_user&task=register'); ?>" class="m-l-5">
						<?php echo JText::_('REGISTER'); ?>
					</a>
				</div>



				<noscript><?php echo JText::_('WARNJAVASCRIPT'); ?></noscript>
				<input type="hidden" name="option" value="com_user" />
				<input type="hidden" name="task" value="login" />
				<input type="hidden" name="return" value="<?php echo base64_encode(JURI::current()); ?>" />
				<?php echo JHTML::_('form.token'); ?>
			</form>
			<!--END Login Form-->
			<div class="m-t-60">
				<div class="d-flex m-b-30 p-r-0 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
					<div class="col-sm-3 col-md-2 no-padding">
						<img alt="" class="m-t-5" src="<?php echo JURI::base(); ?>templates/frontend/images/small-logo.png" width="60" height="60" />
					</div>
					<div class="col-sm-9 no-padding m-t-10 p-l-20">
						<p>
							<small>
								Perhimpunan Dokter Spesialis Penyakit Dalam Indonesia @2020 All rights reserved
							</small>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Login Right Container-->
</div>