<?php // @version $Id: default.php  $
defined('_JEXEC') or die('Restricted access');
?>


<div class="login-wrapper ">
	<!-- START Login Background Pic Wrapper-->
	<div class="bg-pic">
		<!-- START Background Pic-->
		<img  src="<?php echo JURI::base(); ?>templates/frontend/images/login-background.jpg" alt="" class="lazy">
	</div>
	<!-- END Login Background Pic Wrapper-->
	<!-- START Login Right Container-->
	<div class="login-container bg-white">
		<div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
			<h1 class="bold m-t-80"><?php echo JText::_('FORGOT_YOUR_PASSWORD'); ?></h1>
			<p class="p-b-15"><?php echo JText::_('RESET_PASSWORD_REQUEST_DESCRIPTION'); ?></p>
			<!-- START Login Form -->
			<form id="form-login" class="p-t-15" role="form" novalidate="novalidate" method="post">
				<!-- START Form Control-->
				<div class="form-group form-group-default">
					<label>Email</label>
					<div class="controls">
						<input type="text" name="email" placeholder="<?php echo JText::_('Email'); ?>" class="form-control" required>
					</div>
				</div>
				<button class="btn btn-primary btn-cons m-t-10" type="submit">Send</button>
				<div class="m-t-30">
					Kembali ke
					<a href="<?php echo JURI::base(); ?>login" class="m-l-5">
						<?php echo JText::_('LOGIN'); ?>
					</a>
				</div>
				<noscript><?php echo JText::_('WARNJAVASCRIPT'); ?></noscript>
				<input type="hidden" name="option" value="com_user" />
				<input type="hidden" name="task" value="requestreset" />
				<?php echo JHTML::_('form.token'); ?>
			</form>
			<!--END Login Form-->
			<div class="m-t-60">
				<div class="d-flex m-b-30 p-r-0 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
					<div class="col-sm-3 col-md-2 no-padding">
						<img alt="" class="m-t-5" src="<?php echo JURI::base(); ?>templates/frontend/images/small-logo.png" width="60" height="60" />
					</div>
					<div class="col-sm-9 no-padding m-t-10 p-l-20">
						<p>
							<small>
								Perhimpunan Dokter Spesialis Penyakit Dalam Indonesia @2020 All rights reserved
							</small>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Login Right Container-->
</div>
<style>
	.header{
		display: none;
	}
</style>