<?php // @version $Id: default.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
?>

<div class="login-wrapper">
	<!-- START Login Background Pic Wrapper-->
	<div class="bg-pic">
		<!-- START Background Pic-->
		<img src="<?php echo JURI::base(); ?>templates/frontend/images/login-background.jpg" alt="" class="lazy">
		<!-- END Background Pic-->
		<!-- START Background Caption-->
		<!-- END Background Caption-->
	</div>
	<!-- END Login Background Pic Wrapper-->
	<!-- START Login Right Container-->
	<div class="register-container bg-white">
		<div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
			<h1 class="bold">Register</h1>
			<!-- START Login Form -->
			<form class="p-t-15" role="form" novalidate="novalidate"
						action="<?php echo JRoute::_('index.php?option=com_user&view=register'); ?>" method="post">

				<div class="form-group form-group-default">
					<label for="username">Nama Lengkap</label>
					<div class="controls">
						<input class="form-control" type="text" id="username" required="" placeholder="Nama Lengkap" name="name">
					</div>
				</div>

				<div class="form-group form-group-default">
					<label for="emailaddress">Alamat Email</label>
					<div class="controls">
						<input class="form-control" type="email" id="emailaddress" required="" placeholder="nama@email.com"
									 name="email">
					</div>
				</div>

				<div class="form-group form-group-default">
					<label for="phone">Nomor Telpon</label>
					<div class="controls">
						<input class="form-control" type="text" id="phone" required placeholder="Nomor Telpon" name="phone">
					</div>
				</div>

				<div class="form-group form-group-default">
					<label for="password">Password</label>
					<div class="controls">
						<input class="form-control" type="password" required id="password" placeholder="Buat password" name="password">
					</div>
				</div>
				<div class="form-group form-group-default">
					<label for="password">Konfirmasi Password</label>
					<div class="controls">
						<input class="form-control" type="password" required id="password" placeholder="Konfirmasi password" name="password2">
					</div>
				</div>

				<div class="row m-b-20">
					<div class="col-md-12 col-xs-12">
						<div class="checkbox ">
							<input id="remember" type="checkbox" checked="">
							<label for="remember">
								Menerima <a href="#">Syarat dan Ketentuan</a>
							</label>
						</div>
					</div>
				</div>
				<div class="">
					<button class="btn btn-primary btn-cons m-t-10" type="submit">Register</button>
					<div class="m-t-15">
						<label>
							Sudah punya akun?,
							<a href="<?php echo JRoute::_('index.php?option=com_user&view=login'); ?>"
								 class="m-l-5"><?php echo JText::_('LOGIN'); ?></a>
						</label>
					</div>
				</div>
				<input type="hidden" name="option" value="com_user"/>
				<input type="hidden" name="task" value="register_save"/>
				<input type="hidden" name="id" value="0"/>
				<input type="hidden" name="gid" value="0"/>
				<input type="hidden" name="return" value="<?php echo base64_encode(JURI::current()); ?>"/>
				<?php echo JHTML::_('form.token'); ?>
			</form>
			<!--END Login Form-->
			<div class="m-t-60">
				<div class="d-flex m-b-30 p-r-0 sm-m-t-20 sm-p-r-15 sm-p-b-20 clearfix">
					<div class="col-sm-3 col-md-2 no-padding">
						<img alt="" class="m-t-5" src="<?php echo JURI::base(); ?>templates/frontend/images/small-logo.png" width="60" height="60" />
					</div>
					<div class="col-sm-9 no-padding m-t-10 p-l-20">
						<p>
							<small>
								Perhimpunan Dokter Spesialis Penyakit Dalam Indonesia @2020 All rights reserved
							</small>
						</p>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- END Login Right Container-->
</div>
