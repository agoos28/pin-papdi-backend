<?php // @version $Id: default.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
jimport('joomla.filesystem.file');


$avatar = JURI::base() . 'images/propict/' . $user->id . '.png';
if (!JFile::exists(JPATH_BASE . DS . 'images' . DS . 'propict' . DS . $user->id . '.png')) {
	$avatar = JURI::base() . 'images/propict/default.png';
}

$u =& JURI::getInstance();
$b =& JURI::base();
$c = JURI::current();
$tcount = modLoginHelper::getTransactionCount();
$rcount = modLoginHelper::getRentedProducts();


?>
<?php if ($type == 'logout') : ?>
	<li class="nav-item dropdown ">
		<a href="#" data-toggle="dropdown"
			 class="profile-dropdown-toggle nav-link dropdown-toggle d-flex align-items-center padding-sm-5"
			 aria-expanded="false">
			<span class="p-r-5 d-none d-sm-block"><?php echo $user->name; ?></span>
			<span class="thumbnail-wrapper d32 circular inline">
						<img src="<?php echo $avatar; ?>" class="avatar" alt="Avatar">
					</span>
			<b class="caret"></b>
		</a>
		<ul class="dropdown-menu pull-right no-padding position-absolute" style="width: 230px;">
			<li class="<?php if ($c == $b . 'my-booking') {
				echo 'current';
			} ?>">
				<a class="d-flex justify-content-between align-items-center" href="<?php echo $b; ?>my-booking"
					 title="<?php echo JText::_('MY_BOOKING'); ?>">
					<span><?php echo JText::_('MY_BOOKING'); ?></span>
				</a>
			</li>
			<li class="<?php if ($c == $b . 'transactions') {
				echo 'current';
			} ?>">
				<a class="d-flex justify-content-between align-items-center" href="<?php echo $b; ?>transactions"
					 title="<?php echo JText::_('TRANSACTION_HISTORY'); ?>">
					<span><?php echo JText::_('TRANSACTION_HISTORY'); ?></span>
				</a>
			</li>
			<li>
				<a class="<?php if ($c == $b . 'my-account') {
					echo 'current';
				} ?> d-flex justify-content-between align-items-center" href="<?php echo $b; ?>my-account"
					 title="<?php echo JText::_('MY_ACCOUNT'); ?>"><?php echo JText::_('MY_ACCOUNT'); ?></a>
			</li>

			<?php if ($user->usertype == 'Super Administrator') { ?>
				<li class="p-b-10">
					<a class="" target="_blank" href="<?php echo $b; ?>administration"
						 title="<?php echo JText::_('ADMIN_PAGE'); ?>"><?php echo JText::_('ADMIN_PAGE'); ?></a>
				</li>
			<?php } else { ?>
				<li class="p-b-10">
					<a class="" href="<?php echo $b; ?>contact"
						 title="<?php echo JText::_('HELP'); ?>"><?php echo JText::_('HELP'); ?></a>
				</li>
			<?php } ?>
			<li class="bg-master-lighter padding-5 text-right">
				<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" name="login"
							class="form-login fancy">
					<input type="hidden" name="option" value="com_user"/>
					<input type="hidden" name="task" value="logout"/>
					<input type="hidden" name="return" value="<?php echo base64_encode('index.php'); ?>"/>
					<button type="submit" class="btn btn-default btn-sm">
						<span class="pull-left">Logout</span>&nbsp;<span class="pull-right"><i class="pg-power"></i></span>
					</button>
				</form>
			</li>
		</ul>
	</li>
<?php else : ?>
	<li class="nav-item dropdown ">
		<a href="#" data-toggle="dropdown"
			 class="profile-dropdown-toggle nav-link dropdown-toggle d-flex align-items-center padding-sm-5"
			 aria-expanded="false">
			<span class="p-r-5 d-none d-sm-block"><?php echo $user->name; ?></span>
			<span class="thumbnail-wrapper d32 circular inline">
						<img src="<?php echo $avatar; ?>" class="avatar" alt="Avatar">
					</span>
			<b class="caret"></b>
		</a>
		<div class="panel dropdown-menu pull-right no-padding position-absolute" style="width: 300px;">
			<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" name="login"
						class="form-login fancy">
				<div class="list-view-group-container">
					<!-- BEGIN List Group Header!-->
					<div class="list-view-group-header text-uppercase">
						Login			</div>
				</div>
				<div class="panel-body" style="padding: 13px; background: #fff">
					<div class="form-group form-group-default">
						<label>Email</label>
						<div class="controls">
							<input type="text" name="username" placeholder="Email" class="form-control" required="">
						</div>
					</div>
					<div class="form-group form-group-default">
						<label>Password</label>
						<div class="controls">
							<input type="password" class="form-control" name="passwd" placeholder="Kata Kunci" required="">
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 no-padding">
							<div class="checkbox ">
								<input type="checkbox" value="1" id="checkbox1">
								<label for="checkbox1">Ingat saya</label>
							</div>
						</div>
						<div class="col-md-6 text-right">
							<a href="<?php echo $b; ?>/login/reset" class="small link">Lupa Sandi Lewat?</a>
						</div>
					</div>

					<input type="hidden" name="option" value="com_user"/>
					<input type="hidden" name="task" value="login"/>
					<input type="hidden" name="return" value="<?php echo base64_encode($u->toString()); ?>"/>
					<?php echo JHTML::_('form.token'); ?>
					<button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
					<div class="m-t-20">
						Belum punya akun?,
						<a href="<?php echo JURI::base(); ?>login?task=register" class="m-l-5">
							Daftar </a>
					</div>
				</div>
			</form>
		</div>
	</li>
<?php endif; ?>
