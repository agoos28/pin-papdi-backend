<?php
defined('_JEXEC') or die('Restricted access');

$baseUrl = JURI::base();
$tmplUrl = $baseUrl . 'templates/frontend/';
$curentUrl = JURI::getInstance();

$user = JFactory::getUser();

$conf = &JFactory::getConfig();
$sitename = $conf->getValue('config.sitename');
$lang = $conf->getValue("joomfish.language");

$menu = &JSite::getMenu();
$navigationParam = $menu->getActive();
$is_home = false;
if ($navigationParam == $menu->getDefault()) {
	$is_home = true;
} else {
	$jquery = 'jquery.js?v=3';
}
$buffer = $this->getBuffer();
// $message = JApplication::getMessageQueue();

$headers = $this->getHeadData();
reset($headers['styleSheets']);
foreach ($headers['styleSheets'] as $key => $value) {
	unset($headers['styleSheets'][$key]);
}
reset($headers['scripts']);
foreach ($headers['scripts'] as $key => $value) {
	unset($headers['scripts'][$key]);
}
reset($headers['script']);
foreach ($headers['script'] as $key => $value) {
	unset($headers['script'][$key]);
}

$this->setHeadData($headers);
$this->setGenerator('agoos28');

?>
<!DOCTYPE html>
<html class="wf-active">

<head>
	<meta charset="utf-8"/>
	<jdoc:include type="head"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<link rel="shortcut icon" href="images/favicon.ico">


	<link href="<?php echo $tmplUrl; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $tmplUrl; ?>plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css"/>
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/utilities.css" rel="stylesheet" type="text/css"/>
	<link href="<?php echo $tmplUrl; ?>plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css"/>

	<link href="<?php echo $tmplUrl; ?>plugins/swiper/css/swiper.css" rel="stylesheet" type="text/css" media="screen"/>
	<link href="<?php echo $tmplUrl; ?>plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css"
				media="screen"/>
	<link href="<?php echo $tmplUrl; ?>plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css"/>
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/croppie.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="<?php echo $tmplUrl; ?>plugins/bootstrap-select2/select2.css">
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/pages.css" rel="stylesheet" type="text/css"/>
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/pages-icons.css" rel="stylesheet" type="text/css"/>
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/style.css?ref=1" rel="stylesheet" type="text/css"/>
	<link class="main-stylesheet" href="<?php echo $tmplUrl; ?>css/responsive.css" rel="stylesheet" type="text/css"/>

	<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/jquery/jquery-1.11.1.min.js"></script>
	<script src="<?php echo $tmplUrl; ?>plugins/pace/pace.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/bootstrap/js/bootstrap.min.js"></script>
</head>

<body class="fixed-header">
<div class="page-container">
	<?php
	$hide = array(96,94,39);
	// print_r($navigationParam);
	if (!in_array($navigationParam->id, $hide)) { ?>
		<nav class="header navbar navbar-default navbar-expand-xl navbar-light md-header light-solid">
			<div class="pull-left d-none d-sm-block">
				<div class="header-inner">
					<div class="brand inline">
						<a href="<?php echo $baseUrl; ?>"><img style="height: 45px; width: auto;" src="<?php echo $tmplUrl; ?>images/main-logo.png" alt="logo"></a>
					</div>
					<!-- START NOTIFICATION LIST -->
					<ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
						<li class="p-r-15 inline">
							<a href="<?php $baseUrl; ?>events">Agenda</a>
						</li>
						<li class="p-r-15 inline">
							<a href="<?php $baseUrl; ?>faq">Bantuan</a>
						</li>
						<li class="p-r-15 inline">
							<a href="<?php $baseUrl; ?>about">Tentang</a>
						</li>
					</ul>
					<!-- END NOTIFICATIONS LIST -->
				</div>
			</div>
			<div class="pull-left d-sm-none">
				<div class="d-flex align-items-center">
				<a href="<?php echo $baseUrl; ?>"><img style="height: 40px; width: auto; margin-top: 3px;" src="<?php echo $tmplUrl; ?>images/small-logo.png" alt="logo"></a>
				<a style="margin: 10px !important;" type="button" data-target="#navbarCollapse" data-toggle="collapse"
								class="navbar-toggle ml-auto m-l-5">
					<span class="fa fa-bars fa-2x"></span>
				</a>
				</div>
				<div style="position:absolute; top: 50px;" id="navbarCollapse" class="collapse navbar-collapse justify-content-start d-sm-none">
					<ul class="no-margin list-group">
						<li class="list-group-item">
							<a href="<?php $baseUrl; ?>events">Agenda</a>
						</li>
						<li class="list-group-item">
							<a href="<?php $baseUrl; ?>events">Bantuan</a>
						</li>
						<li class="list-group-item">
							<a href="<?php $baseUrl; ?>events">Tentang</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="pull-right">
				<div class="navbar-header d-flex align-content-between">
					<ul class="nav navbar-nav navbar-right navbar-expand ml-auto align-items-center">
						<jdoc:include type="modules" name="cart"/>
						<jdoc:include type="modules" name="login"/>
					</ul>
				</div>
				<!-- Collection of nav links, forms, and other content for toggling -->
			</div>
		</nav>
	<?php } ?>
	<jdoc:include type="modules" name="maintop"/>
	<jdoc:include type="component"/>
	<jdoc:include type="modules" name="bottom"/>
</div>
<jdoc:include type="message"/>
<script>
	var baseUrl = '<?php echo $baseUrl; ?>';
</script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>js/modernizr.custom.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/jquery-ios-list/jquery.ioslist.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/swiper/js/swiper.jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/velocity/velocity.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/velocity/velocity.ui.js"></script>
<script type="text/javascript"
				src="<?php echo $tmplUrl; ?>plugins/bootstrap-collapse/bootstrap-tabcollapse.js"></script>
<script type="text/javascript"
				src="<?php echo $tmplUrl; ?>plugins/jquery-datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>js/gmap3.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/bootstrap-select2/select2.min.js"></script>
<script type="text/javascript"
				src="<?php echo $tmplUrl; ?>plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>plugins/dropzone/dropzone.min.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>js/croppie.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>js/pages.frontend.js"></script>
<script type="text/javascript" src="<?php echo $tmplUrl; ?>js/custom.js"></script>
</body>

</html>