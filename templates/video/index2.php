<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Video Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
$user = JFactory::getUser();
?>

<div class="col-lg-4 col-md-4 col-sm-6">
    <div class="main_box" style="margin-bottom: 30px;">
    <?php if($user->usertype == 'Super Administrator'){ ?>
        <a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>   
        <?php } ?>
        <div class="box_1">
        <a class="category fancybox-media" href="<?php echo $jSeblod->vd_youtube->value; ?>">
            <img alt="<?php echo $jSeblod->vd_title->value; ?>" src="<?php echo $jSeblod->vd_poster->thumb2; ?>">
        </a>
        </div>
        <div class="desc">
            <h3 style="color: rgb(33, 183, 240);"><?php echo $jSeblod->vd_title->value; ?></h3>
        </div>
    </div>
</div>