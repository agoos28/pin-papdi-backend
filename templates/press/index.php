<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Press Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
jimport( 'joomla.filter.filteroutput' );
?>


<div class="productview row">
	<div class="column two">
        <div class="flexslider">
          <ul class="slides">
          	<li>
            	<div class="imgwrapper" style="background-image: url('<?php echo JFilterOutput::linkXHTMLSafe($jSeblod->press_thumb->value); ?>');">
            		<img src="<?php echo $jSeblod->press_thumb->value; ?>" />
             	</div>   
             </li>
             <?php for ( $i = 0, $n = sizeof( @$jSeblod->imagex ); $i < $n-1; $i++ ) { 
             if(@$jSeblod->imagex[$i]['post_img']->thumb1){ ?>
            <li>
            	<div class="imgwrapper" style="background-image: url('<?php echo JFilterOutput::linkXHTMLSafe(@$jSeblod->imagex[$i]['post_img']->thumb1); ?>');">
            		<img src="<?php echo @$jSeblod->imagex[$i]['post_img']->thumb1; ?>" />
            	</div>
            </li>
            <?php }} ?>
          </ul>
        </div>
	</div>
    <div class="column two">
    	<div class="titles">
            <h1><?php echo $jSeblod->post_title->value; ?></h1>
            <div class="subtitle"><?php echo $jSeblod->meta_desc->value; ?></div>
        </div>
        <div class="text"><?php echo $jSeblod->post_wsywyg->value; ?></div>
    </div>
</div>
<div class="socialicon" style="padding: 80px 0px 0px;">
    <a href="#" class="ico icon-facebook" data-url="<?php echo JURI::current(); ?>"></a>
    <a href="#" class="ico icon-twitter"></a>
    <a href="#" class="ico icon-pinterest"></a>
    <a target="_blank" href="http://instagram.com/rumamanis" class="ico icon-instagram"></a>
</div>
