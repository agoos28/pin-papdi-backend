<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Product Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>
<div class="item pers">
	<div class="padder">
	<div class="thumb">
		<a style="background-image: url('<?php echo JFilterOutput::linkXHTMLSafe($jSeblod->press_thumb->value); ?>');" href="<?php echo $this->content->art_link; ?>"><img src="<?php echo $jSeblod->press_thumb->value; ?>" title="<?php echo $jSeblod->post_title->value; ?>" /></a>
    </div>
    <div class="text">
    	<h4><a href="<?php echo $this->content->art_link; ?>"><?php echo $jSeblod->post_title->value; ?></a></h4>
        <p><?php echo $jSeblod->meta_desc->value; ?></p>
   	</div>
    </div>
</div>
