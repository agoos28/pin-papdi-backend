<?php
/**
* @version 			1.9.0
* @author       	https://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Homepage Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
$user	= JFactory::getUser();
$base = JURI::base();
?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>
<link rel="stylesheet" href="<?php echo $base; ?>templates/blank_j15/css/settings.css?ver=3" />
<style type="text/css">
.rs-hover-ready {
	background-color: #AA5B9E !important;
	box-shadow: 0 4px 0px 0 #671a65, 0px 5px 0px 0px #000, 0px 10px 0px 0px #0000001a !important;
	-webkit-box-shadow: 0 4px 0px 0 #671a65, 0px 5px 0px 0px #000, 0px 10px 0px 0px #0000001a !important;
}
.tagcloud a, #comments .form-submit #submit, .sub-cls, .single-post-quote .entry-quote-post-format, .single-post-link {
	background-color: #6ec8bf;
}
.welcome-kidzy h2, .our-classes h2, .school-fecilities h2, .meet-staffs h2, .latest-news h2, .enrollment h2, .who-are h2, .atmosphere-details h2, .atmosphere-content ul li i, .leave-comment h2 {
	color: #6ec8bf !important;
}
.breadcrumb-section, .footer-section {
	background-color: #6ec8bf !important;
}
.vc_tta-color-grey.vc_tta-style-classic .vc_tta-tab.vc_active > a {
	background: #6ec8bf !important;
}
.author-tag li i, .author-tag li a i, .class-details h3 a, .class-meta p, .class-single-meta h4, .sub-heading h4, .sort-btn li.active a, .class-search button, .vc_tta-container h2, .title-2, .single-team > h3, .bio-details:before, a, a:focus {
	color: #6ec8bf;
}
.bio-content:after, .widget h3.widget_title, .widget-area .widget_categories h3.widget_title {
	background: #6ec8bf none repeat scroll 0 0;
}
.post-thumb-link, .post-thumb-quote, .schedule-table thead tr th:first-child, .schedule-table thead tr th {
	background: #6ec8bf;
}
.primary-btn {
	background: #6ec8bf !important;
	box-shadow: 0 4px 0px 0 #6ec8bf, 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1) !important;
	-webkit-box-shadow: 0 4px 0px 0 #6ec8bf, 0px 5px 0px 0px #000, 0px 10px 0px 0px rgba(0,0,0,0.1) !important;
}
.inputs-group input:focus, textarea:focus {
	border-color: #6ec8bf;
}
.pagination li.active a {
	background: #6ec8bf;
	border-color: #6ec8bf;
	box-shadow: 0 4px 0 0 #6ec8bf, 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1);
}
.primary-btn span {
	background: #6ec8bf none repeat scroll 0 0;
}
.page-numbers li .current {
	box-shadow: 0 4px 0 0 #6ec8bf, 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1);
	background: #6ec8bf;
}
.class-hover {
	background: #6ec8bf;
}
.event-img > a {
	background: #6ec8bf none repeat scroll 0 0;
	box-shadow: 0 4px #6ec8bf;
}
.team-img:after {
	background: #6ec8bf none repeat scroll 0 0;
}
.subscribe-form input {
	box-shadow: 0 4px #6ec8bf;
}
.pagination li.active a:hover {
	background: #eb94b7;
	border-color: #eb94b7;
	box-shadow: 0 4px 0 0 #eb94b7, 0 4px 0 0 #000, 0 8px 0 0 rgba(0, 0, 0, 0.1);
}
.widget ul li a:before {
	background: #eb94b7 none repeat scroll 0 0;
	;
}
.tagcloud a:hover, #comments .form-submit #submit:hover {
	background-color: #eb94b7;
}
.subject-list li a:hover, .wid-post-title h6 a:hover, .date li a:hover, .author-tag li a:hover, .post-details h3 a:hover, .widget ul li a:hover, .single-event h3 a:hover, .single-event > a:hover, .sort-btn li a:hover, .single-teacher .tsocial-icon li a:hover, .single-news h5 a:hover, .single-news p a:hover, a:hover {
	color: #eb94b7;
}
.header {

}
#masthead.sticky {
	background-color: rgba(255,255,255,.97);
}
nav.sticky {
	position: fixed;
	top: 0;
	z-index: 99;
	margin: 0 auto 30px;
	width: 100%;
	box-shadow: 0 0 3px 0 rgba(0, 0, 0, 0.22);
}
nav.sticky #header-container {
	padding: 0;
	transition: padding 200ms linear;
	-webkit-transition: padding 200ms linear;
}
nav.sticky .navbar.navbar-default {
	background: rgba(255,255,255,.95);
	border-bottom: 1px solid #f5f5f5
}
.site-header {
	padding-top: 2px;
}
.site-header {
	padding-bottom: 1px;
}

</style>

<script type='text/javascript' src='<?php echo $base; ?>templates/blank_j15/js/jquery.themepunch.tools.min.js?ver=5.3.0.2'></script>
<script type='text/javascript' src='<?php echo $base; ?>templates/blank_j15/js/jquery.themepunch.revolution.min.js?ver=5.3.0.2'></script>
<link href="https://fonts.googleapis.com/css?family=Chewy%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Sniglet%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<link href="https://fonts.googleapis.com/css?family=Roboto%3A500" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
<div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1471587263566 hidden-xs" style="overflow: hidden; background-color: #CFF4F1;">
  <div class="wpb_column vc_column_container vc_col-sm-12">
    <div class="vc_column-inner ">
      <div class="wpb_wrapper">
        <div class="wpb_revslider_element wpb_content_element">
          <div id="rev_slider_3_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;"> 
            <!-- START REVOLUTION SLIDER 5.3.0.2 fullwidth mode -->
            <div id="rev_slider_3_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2">
              <ul>
                <!-- SLIDE  -->
                <li data-index="rs-6" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="https://toiss.id/wp-content/uploads/2017/01/toiss_slide_bg_1-100x50.png"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> 
                  <!-- MAIN IMAGE --> 
                  <img src="<?php echo $base; ?>images/toiss_slide_bg_1.jpg"  alt="" title="toiss_slide_bg_1"  width="1920" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina> 
                  <!-- LAYERS --> 
                  
                  <!-- LAYER NR. 1 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-2" 
			 data-x="['left','left','left','left']" data-hoffset="['178','178','227','111']" 
			 data-y="['top','top','top','top']" data-voffset="['236','236','286','229']" 
						data-fontsize="['26','26','26','30']"
			data-color="['rgba(170, 91, 158, 1.00)','rgba(170, 91, 158, 1.00)','rgba(170, 91, 158, 1.00)','rgba(20, 132, 135, 1.00)']"
			data-width="['320','320','320','none']"
			data-height="['67','67','67','none']"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5; min-width: 320px; max-width: 320px; max-width: 67px; max-width: 67px; white-space: nowrap; font-size: 26px; line-height: 22px; font-weight: 400; color: rgba(170, 91, 158, 1.00);font-family:Chewy;">RENTAL MADE EASY ,<BR>
                    <BR>
                    FOR YOUR CURIOUS LITTLE ONES </div>
                  
                  <!-- LAYER NR. 2 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-3" 
			 data-x="['left','left','left','left']" data-hoffset="['177','177','149','108']" 
			 data-y="['top','top','top','top']" data-voffset="['365','365','368','302']" 
						data-fontsize="['72','72','72','50']"
			data-lineheight="['22','22','22','34']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":480,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6; white-space: nowrap; font-size: 72px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">Safe, Clean & </div>
                  
                  <!-- LAYER NR. 3 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-4" 
			 data-x="['left','left','left','left']" data-hoffset="['174','174','191','135']" 
			 data-y="['top','top','top','top']" data-voffset="['440','440','448','361']" 
						data-fontsize="['72','72','72','50']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":660,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 7; white-space: nowrap; font-size: 72px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">Affordable </div>
                  
                  <!-- LAYER NR. 4 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-7" 
			 data-x="['left','left','left','left']" data-hoffset="['867','867','867','867']" 
			 data-y="['top','top','top','top']" data-voffset="['196','196','196','196']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 8;">
                    <div class="rs-looped rs-wave"  data-speed="50" data-angle="5" data-radius="50px" data-origin="80% 20%"><img src="<?php echo $base; ?>images/slide-rain-cloud.png" alt="" data-ww="['109px','109px','109px','109px']" data-hh="['96px','96px','96px','96px']" width="76" height="96" data-no-retina> </div>
                  </div>
                  
                  <!-- LAYER NR. 5 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-8" 
			 data-x="['left','left','left','left']" data-hoffset="['1142','1142','1142','1142']" 
			 data-y="['top','top','top','top']" data-voffset="['170','170','170','170']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 9;">
                    <div class="rs-looped rs-rotate"  data-easing="Linear.easeNone" data-startdeg="0" data-enddeg="500" data-speed="10" data-origin="50% 50%"><img src="<?php echo $base; ?>images/slide-sun.png" alt="" data-ww="['118px','118px','118px','118px']" data-hh="['116px','116px','116px','116px']" width="145" height="145" data-no-retina> </div>
                  </div>
                  
                  <!-- LAYER NR. 6 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-6" 
			 data-x="['left','left','left','left']" data-hoffset="['804','804','804','804']" 
			 data-y="['top','top','top','top']" data-voffset="['221','221','221','221']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 10;"><img src="<?php echo $base; ?>images/nursery.png" alt="" data-ww="['563px','563px','563px','563px']" data-hh="['482px','482px','482px','482px']" width="820" height="663" data-no-retina> </div>
                  
                  <!-- LAYER NR. 7 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-6-layer-14" 
			 data-x="['left','left','left','left']" data-hoffset="['336','310','157','67']" 
			 data-y="['top','top','top','top']" data-voffset="['448','469','599','496']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":580,"speed":2210,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 11;"><img src="<?php echo $base; ?>images/traveling_baby_toiss.png" alt="" data-ww="['452px','452px','452px','334px']" data-hh="['326px','326px','326px','241px']" width="452" height="326" data-no-retina> </div>
                  
                  <!-- LAYER NR. 8 -->
                  <div class="tp-caption rev-btn " 
			 id="slide-6-layer-16" 
			 data-x="['left','left','left','left']" data-hoffset="['183','183','267','163']" 
			 data-y="['top','top','top','top']" data-voffset="['529','529','528','432']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="button" 
			data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"","delay":""}]'
			data-responsive_offset="on" 
			data-responsive="off"
			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","force":true,"to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bs:solid;bw:0 0 0 0;"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[12,12,12,12]"
			data-paddingright="[35,35,35,35]"
			data-paddingbottom="[12,12,12,12]"
			data-paddingleft="[35,35,35,35]"

			style="z-index: 12; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;background-color:rgba(0, 0, 0, 0.75);border-color:rgba(0, 0, 0, 1.00);border-radius:3px 3px 3px 3px;top:0px;-webkit-transition:all .3s;-o-transition:all .3s;transition:all .3s;-webkit-transition-timing-function:ease-in-out;transition-timing-function:ease-in-out;cursor:pointer;"><a href="<?php JURI::base(); ?>product" style="color:white;">Take a look</a> </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-7" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="https://toiss.id/wp-content/uploads/2017/01/toiss_slide_bg_2-100x50.png"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> 
                  <!-- MAIN IMAGE --> 
                  <img src="<?php echo $base; ?>images/toiss_slide_bg_2.png"  alt="" title="toiss_slide_bg_2"  width="1920" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina> 
                  <!-- LAYERS --> 
                  
                  <!-- LAYER NR. 9 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-9" 
			 data-x="['left','left','left','left']" data-hoffset="['1161','1161','1161','1161']" 
			 data-y="['top','top','top','top']" data-voffset="['94','94','94','94']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5;">
                    <div class="rs-looped rs-pulse"  data-easing="Power1.easeIn" data-speed="100" data-zoomstart="2" data-zoomend="200"><img src="<?php JURI::base(); ?>images/slide-circle.png" alt="" data-ww="['320px','320px','320px','320px']" data-hh="['320px','320px','320px','320px']" width="523" height="523" data-no-retina> </div>
                  </div>
                  
                  <!-- LAYER NR. 10 -->
                  <div class="tp-caption  " 
			 id="slide-7-layer-2" 
			 data-x="['left','left','center','center']" data-hoffset="['177','177','-20','-10']" 
			 data-y="['top','top','middle','middle']" data-voffset="['341','341','-161','-91']" 
						data-fontsize="['26','26','26','30']"
			data-color="['rgba(228, 113, 165, 1.00)','rgba(228, 113, 165, 1.00)','rgba(228, 113, 165, 1.00)','rgba(20, 132, 135, 1.00)']"
			data-width="['300','300','none','none']"
			data-height="['86','86','none','none']"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 
			data-responsive="off"
			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6; min-width: 300px; max-width: 300px; max-width: 86px; max-width: 86px; white-space: nowrap; font-size: 26px; line-height: 22px; font-weight: 400; color: rgba(228, 113, 165, 1.00);font-family:Chewy;">PENUHI KEBUTUHAN SI KECIL </div>
                  
                  <!-- LAYER NR. 11 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-3" 
			 data-x="['left','left','center','center']" data-hoffset="['173','173','0','-8']" 
			 data-y="['top','top','middle','middle']" data-voffset="['392','392','-94','9']" 
						data-fontsize="['50','50','40','40']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":630,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 7; white-space: nowrap; font-size: 50px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">No need to think </div>
                  
                  <!-- LAYER NR. 12 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-4" 
			 data-x="['left','left','center','center']" data-hoffset="['170','170','1','-5']" 
			 data-y="['top','top','middle','middle']" data-voffset="['444','444','-25','74']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":910,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 8; white-space: nowrap; font-size: 40px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">twice to get your<br>
                    <br>
                    baby needs </div>
                  
                  <!-- LAYER NR. 13 -->
                  <div class="tp-caption rev-btn " 
			 id="slide-7-layer-5" 
			 data-x="['left','left','center','left']" data-hoffset="['177','177','-66','95']" 
			 data-y="['top','top','middle','top']" data-voffset="['577','577','53','513']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="button" 
			data-responsive_offset="on" 
			data-responsive="off"
			data-frames='[{"delay":500,"speed":1190,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","force":true,"to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(145, 0, 140, 1.00);bw:0 0 0 0;box-shadow:none;-webkit-box-shadow:none;top:5px;"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[12,12,12,12]"
			data-paddingright="[35,35,35,35]"
			data-paddingbottom="[12,12,12,12]"
			data-paddingleft="[35,35,35,35]"

			style="z-index: 9; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;background-color:rgba(146, 39, 143, 1.00);border-color:rgba(0, 0, 0, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:3px 3px 3px 3px;top:0px;-webkit-transition:all .3s;-o-transition:all .3s;transition:all .3s;-webkit-transition-timing-function:ease-in-out;transition-timing-function:ease-in-out;cursor:pointer;"> <a href="<?php JURI::base(); ?>product/88-its-play-time" style="color:white">Take a look</a> </div>
                  
                  <!-- LAYER NR. 14 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-6" 
			 data-x="['left','left','left','left']" data-hoffset="['846','846','846','846']" 
			 data-y="['top','top','top','top']" data-voffset="['271','271','271','271']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 10;"><img src="<?php echo $base; ?>images/product_slider_strerilizer.png" alt="" data-ww="['124px','124px','124px','124px']" data-hh="['110px','110px','110px','110px']" width="620" height="620" data-no-retina> </div>
                  
                  <!-- LAYER NR. 15 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-7" 
			 data-x="['left','left','left','left']" data-hoffset="['690','690','690','690']" 
			 data-y="['top','top','top','top']" data-voffset="['365','365','365','365']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":350,"speed":1290,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 11;"><img src="<?php echo $base; ?>images/toiss_slide_5.png" alt="" data-ww="['371px','371px','371px','371px']" data-hh="['371px','371px','371px','371px']" width="1000" height="1000" data-no-retina> </div>
                  
                  <!-- LAYER NR. 16 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-8" 
			 data-x="['left','left','left','left']" data-hoffset="['1086','1086','1086','1086']" 
			 data-y="['top','top','top','top']" data-voffset="['370','370','370','370']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":280,"speed":1560,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power2.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 12;"><img src="<?php echo $base; ?>images/toiss_slide_4.png" alt="" data-ww="['317px','317px','317px','317px']" data-hh="['317px','317px','317px','317px']" width="1000" height="1000" data-no-retina> </div>
                  
                  <!-- LAYER NR. 17 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-11" 
			 data-x="['left','left','left','left']" data-hoffset="['991','991','991','991']" 
			 data-y="['top','top','top','top']" data-voffset="['286','286','286','286']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":620,"speed":810,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 13;"><img src="<?php echo $base; ?>images/toiss_slide_6.png" alt="" data-ww="['236px','236px','236px','236px']" data-hh="['236px','236px','236px','236px']" width="1000" height="1000" data-no-retina> </div>
                  
                  <!-- LAYER NR. 18 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-7-layer-12" 
			 data-x="['left','left','left','left']" data-hoffset="['511','511','511','511']" 
			 data-y="['top','top','top','top']" data-voffset="['473','473','473','473']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":340,"speed":1500,"frame":"0","from":"y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;","to":"o:1;","ease":"Power3.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 14;">
                    <div class="rs-looped rs-slideloop"  data-easing="linearEaseNone" data-speed="7" data-xs="100" data-xe="0" data-ys="55" data-ye="50"><img src="<?php echo $base; ?>images/toiss_slide_3.png" alt="" data-ww="['222px','222px','222px','222px']" data-hh="['222px','222px','222px','222px']" width="1000" height="1000" data-no-retina> </div>
                  </div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-8" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="https://toiss.id/wp-content/uploads/2017/01/toiss_slide_bg_1-100x50.png"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description=""> 
                  <!-- MAIN IMAGE --> 
                  <img src="<?php echo $base; ?>images/toiss_slide_bg_1.png"  alt="" title="toiss_slide_bg_1"  width="1920" height="827" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina> 
                  <!-- LAYERS --> 
                  
                  <!-- LAYER NR. 19 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-2" 
			 data-x="['left','left','center','center']" data-hoffset="['575','358','0','0']" 
			 data-y="['top','top','top','middle']" data-voffset="['201','184','228','-146']" 
						data-fontsize="['26','26','26','30']"
			data-color="['rgba(133, 134, 193, 1.00)','rgba(133, 134, 193, 1.00)','rgba(133, 134, 193, 1.00)','rgba(20, 132, 135, 1.00)']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5; white-space: nowrap; font-size: 26px; line-height: 22px; font-weight: 400; color: rgba(133, 134, 193, 1.00);font-family:Chewy;">If you want something that's </div>
                  
                  <!-- LAYER NR. 20 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-3" 
			 data-x="['left','left','center','center']" data-hoffset="['675','454','0','-2']" 
			 data-y="['top','top','top','middle']" data-voffset="['281','263','320','-50']" 
						data-fontsize="['32','32','32','30']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":480,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6; white-space: nowrap; font-size: 32px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">email to </div>
                  
                  <!-- LAYER NR. 21 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-4" 
			 data-x="['left','center','center','center']" data-hoffset="['576','1','-2','-1']" 
			 data-y="['top','middle','middle','top']" data-voffset="['334','-59','-89','355']" 
						data-fontsize="['56','56','56','48']"
			data-color="['rgba(133, 134, 193, 1.00)','rgba(133, 134, 193, 1.00)','rgba(133, 134, 193, 1.00)','rgba(255, 255, 255, 1.00)']"
			data-width="['310','310','310','none']"
			data-height="['31','31','31','none']"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":660,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 7; min-width: 310px; max-width: 310px; max-width: 31px; max-width: 31px; white-space: nowrap; font-size: 56px; line-height: 22px; font-weight: 400; color: rgba(133, 134, 193, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">info@toiss.id </div>
                  
                  <!-- LAYER NR. 22 -->
                  <div class="tp-caption rev-btn " 
			 id="slide-8-layer-5" 
			 data-x="['left','center','center','center']" data-hoffset="['666','0','0','-1']" 
			 data-y="['top','middle','top','middle']" data-voffset="['480','64','493','154']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="button" 
			data-responsive_offset="on" 
			data-responsive="off"
			data-frames='[{"delay":500,"speed":850,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","force":true,"to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(145, 0, 140, 1.00);bw:0 0 0 0;box-shadow:none;-webkit-box-shadow:none;top:5px;"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[12,12,12,12]"
			data-paddingright="[35,35,35,35]"
			data-paddingbottom="[12,12,12,12]"
			data-paddingleft="[35,35,35,35]"

			style="z-index: 8; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 500; color: rgba(255, 255, 255, 1.00);font-family:Roboto;background-color:rgba(146, 39, 143, 1.00);border-color:rgba(0, 0, 0, 1.00);border-style:solid;border-width:1px 1px 1px 1px;border-radius:3px 3px 3px 3px;top:0px;-webkit-transition:all .3s;-o-transition:all .3s;transition:all .3s;-webkit-transition-timing-function:ease-in-out;transition-timing-function:ease-in-out;cursor:pointer;"><a href="mailto:info@toiss.id" style="color:white">ASK NOW</a> </div>
                  
                  <!-- LAYER NR. 23 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-7" 
			 data-x="['left','left','left','left']" data-hoffset="['933','933','933','933']" 
			 data-y="['top','top','top','top']" data-voffset="['266','266','266','266']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 9;">
                    <div class="rs-looped rs-wave"  data-speed="50" data-angle="5" data-radius="50px" data-origin="80% 20%"><img src="<?php JURI::base(); ?>images/slide-rain-cloud.png" alt="" data-ww="['109px','109px','109px','109px']" data-hh="['96px','96px','96px','96px']" width="76" height="96" data-no-retina> </div>
                  </div>
                  
                  <!-- LAYER NR. 24 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-8" 
			 data-x="['left','left','left','left']" data-hoffset="['443','684','569','326']" 
			 data-y="['top','top','top','top']" data-voffset="['153','216','308','514']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 10;">
                    <div class="rs-looped rs-rotate"  data-easing="Linear.easeNone" data-startdeg="0" data-enddeg="500" data-speed="10" data-origin="50% 50%"><img src="<?php JURI::base(); ?>images/slide-sun.png" alt="" data-ww="['118px','118px','118px','118px']" data-hh="['116px','116px','116px','116px']" width="145" height="145" data-no-retina> </div>
                  </div>
                  
                  <!-- LAYER NR. 25 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-16" 
			 data-x="['left','left','center','center']" data-hoffset="['576','371','0','0']" 
			 data-y="['top','top','top','middle']" data-voffset="['234','230','275','-102']" 
						data-fontsize="['26','26','26','30']"
			data-color="['rgba(139, 140, 193, 1.00)','rgba(139, 140, 193, 1.00)','rgba(139, 140, 193, 1.00)','rgba(20, 132, 135, 1.00)']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 11; white-space: nowrap; font-size: 26px; line-height: 22px; font-weight: 400; color: rgba(139, 140, 193, 1.00);font-family:Chewy;">not available on our website </div>
                  
                  <!-- LAYER NR. 26 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-18" 
			 data-x="['left','center','center','center']" data-hoffset="['469','1','0','0']" 
			 data-y="['top','top','top','middle']" data-voffset="['391','367','420','69']" 
						data-fontsize="['52','32','32','30']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="text" 
			data-responsive_offset="on" 

			data-frames='[{"delay":500,"speed":660,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"nothing"}]'
			data-textAlign="['left','left','left','left']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 12; white-space: nowrap; font-size: 52px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);font-family:Sniglet;text-shadow:0 6px 1px rgba(0, 0, 0, 0.3);">and we'll get it for you </div>
                  
                  <!-- LAYER NR. 27 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-19" 
			 data-x="['left','left','left','left']" data-hoffset="['900','818','818','818']" 
			 data-y="['top','top','top','top']" data-voffset="['275','384','384','384']" 
						data-fontsize="['20','20','20','20']"
			data-lineheight="['22','22','22','22']"
			data-fontweight="['400','400','400','400']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 13;"><img src="<?php echo $base; ?>images/slide-girl.png" alt="" data-ww="['413','413px','413px','413px']" data-hh="['380','380px','380px','380px']" width="593" height="546" data-no-retina> </div>
                  
                  <!-- LAYER NR. 28 -->
                  <div class="tp-caption   tp-resizeme" 
			 id="slide-8-layer-20" 
			 data-x="['left','left','left','center']" data-hoffset="['50','77','-18','-2']" 
			 data-y="['top','top','top','middle']" data-voffset="['350','478','591','317']" 
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"
 
			data-type="image" 
			data-responsive_offset="on" 

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 14;"><img src="<?php echo $base; ?>images/child1.png" alt="" data-ww="['434px','434px','434px','214px']" data-hh="['409px','409px','409px','202px']" width="616" height="580" data-no-retina> </div>
                </li>
              </ul>
<script>
var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
var htmlDivCss = "";
if (htmlDiv) {
  htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
  var htmlDiv = document.createElement("div");
  htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
  document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
} 

  var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
var htmlDivCss = "";
if (htmlDiv) {
  htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
  var htmlDiv = document.createElement("div");
  htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
  document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
}
  /******************************************


  	-	PREPARE PLACEHOLDER FOR SLIDER	-


  ******************************************/
  var setREVStartSize = function() {
    try {
      var e = new Object,
        i = jQuery(window).width(),
        t = 9999,
        r = 0,
        n = 0,
        l = 0,
        f = 0,
        s = 0,
        h = 0;
      e.c = jQuery('#rev_slider_3_1');
      e.responsiveLevels = [1240, 1024, 778, 480];
      e.gridwidth = [1440, 1024, 778, 480];
      e.gridheight = [718, 768, 960, 720];
      e.sliderLayout = "fullwidth";
      if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function(e, f) {
          f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
        }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
        var u = (e.c.width(), jQuery(window).height());
        if (void 0 != e.fullScreenOffsetContainer) {
          var c = e.fullScreenOffsetContainer.split(",");
          if (c) jQuery.each(c, function(e, i) {
            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
          }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
        }
        f = u
      } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
      e.c.closest(".rev_slider_wrapper").css({
        height: f
      })
    } catch (d) {
      console.log("Failure at Presize of Slider:" + d)
    }
  };
setREVStartSize();
var tpj = jQuery;
var revapi3;

tpj(window).load(function() {
  if (tpj("#rev_slider_3_1").revolution == undefined) {
    revslider_showDoubleJqueryError("#rev_slider_3_1");
  } else {
    revapi3 = tpj("#rev_slider_3_1").show().revolution({
      sliderType: "standard",
      jsFileLocation: "<?php echo $base; ?>templates/blank_j15/js/",
      sliderLayout: "fullwidth",
      dottedOverlay: "none",
      delay: 9000,
      navigation: {
        keyboardNavigation: "off",
        keyboard_direction: "horizontal",
        mouseScrollNavigation: "off",
        mouseScrollReverse: "default",
        onHoverStop: "off",
        arrows: {
          style: "uranus",
          enable: true,
          hide_onmobile: false,
          hide_onleave: false,
          tmp: '',
          left: {
            h_align: "left",
            v_align: "center",
            h_offset: 20,
            v_offset: 50
          },
          right: {
            h_align: "right",
            v_align: "center",
            h_offset: 20,
            v_offset: 50
          }
        }
      },
      responsiveLevels: [1240, 1024, 778, 480],
      visibilityLevels: [1240, 1024, 778, 480],
      gridwidth: [1440, 1024, 778, 480],
      gridheight: [718, 768, 960, 720],
      lazyType: "none",
      shadow: 0,
      spinner: "spinner0",
      stopLoop: "off",
      stopAfterLoops: -1,
      stopAtSlide: -1,
      shuffle: "off",
      autoHeight: "off",
      disableProgressBar: "on",
      hideThumbsOnMobile: "off",
      hideSliderAtLimit: 0,
      hideCaptionAtLimit: 0,
      hideAllCaptionAtLilmit: 0,
      debugMode: false,
      fallbacks: {
        simplifyAll: "off",
        nextSlideOnWindowFocus: "off",
        disableFocusListener: false,
      }
    });
  }
});

  var htmlDivCss = unescape("%23rev_slider_3_1%20.uranus.tparrows%20%7B%0A%20%20width%3A50px%3B%0A%20%20height%3A50px%3B%0A%20%20background%3Argba%28255%2C255%2C255%2C0%29%3B%0A%20%7D%0A%20%23rev_slider_3_1%20.uranus.tparrows%3Abefore%20%7B%0A%20width%3A50px%3B%0A%20height%3A50px%3B%0A%20line-height%3A50px%3B%0A%20font-size%3A40px%3B%0A%20transition%3Aall%200.3s%3B%0A-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%20%23rev_slider_3_1%20.uranus.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20%20opacity%3A0.75%3B%0A%20%20%7D%0A");
var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
if (htmlDiv) {
  htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
} else {
  var htmlDiv = document.createElement('div');
  htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
  document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
}
				  </script> 
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="m-t-50 m-b-50">
  <?php 
	$document = JFactory::getDocument();
	$renderer = $document->loadRenderer('module');
	?>
  <div class="container m-t-50 m-b-0">
  <h1 style="font-family: Fredoka One;text-align:center;font-size:44px;font-weight:400;color:#6ec8bf ;padding:0px 0px 0px 0px;">Welcome to TOISS</h1>
    <h2 style="text-align:center;font-size:24px;color:#a65a9d;padding:0px 0px 55px 0px;">Tempat Penyewaan Kebutuhan Bayi &amp; Anak</h2>
  </div>
	<?php 
    $module = JModuleHelper::getModule('newsflash_jseblod','Home Carousel');
    echo $renderer->render($module);
    ?>
    <?php
		$catmodule = JModuleHelper::getModule('newsflash_jseblod','Home Product Categories');
		echo $renderer->render($catmodule);
    ?>
</section>
<section class="hm_header">
  <div class="background paralax background-cover height-100 no-padding middle-center" style="background-image: url('<?php echo $jSeblod->hm_header_img_bg->value; ?>'); max-height: 600px;">
    <div class="container">
      <?php echo $jSeblod->hm_header_text->value; ?>
    </div>
  </div>
  <?php if($user->usertype == 'Super Administrator'){ ?>
  <a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
  <?php } ?>
</section>
<section class="m-t-50 m-b-50"> <?php echo $jSeblod->hm_content->value; ?> </section>

<?php
$doc =& JFactory::getDocument();

$metadesc = $jSeblod->pr_meta_desc->value;
$metakey = str_replace(' ', ', ', $metadesc);
$doc->setDescription($metadesc);
$doc->setMetaData( 'image',   $base.'images/tois-thumb.jpg', 'og');
$doc->setMetaData( 'author',  'Toiss');

?>