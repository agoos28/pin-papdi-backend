<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Accordion Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
$user =& JFactory::getUser();
?>
<div class="container m-t-50 m-b-50" style="position: relative;">
<?php if($user->usertype == 'Super Administrator'){ ?>
<a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
<?php } ?>
<div class="panel-group accordion" id="accordion">
<?php for ( $i = 0, $n = sizeof( @$jSeblod->accordion_item ); $i < $n-1; $i++ ) { 

?>
  <div class="panel panel-white">
    <div class="panel-heading">
      <h5 class="panel-title">
      <a class="accordion-toggle <?php if($i != 0){echo 'collapsed';} ?>" data-toggle="collapse" data-parent="#accordion" href="#panel<?php echo $i; ?>">
        <i class="icon-arrow"></i> <?php echo @$jSeblod->accordion_item[$i]['acc_title']->value; ?>
      </a></h5>
    </div>
    <div id="panel<?php echo $i; ?>" class="panel-collapse <?php if($i != 0){echo 'collapse';} ?>">
      <div class="panel-body">
        <?php echo @$jSeblod->accordion_item[$i]['acc_content']->value; ?>
      </div>
    </div>
  </div>
<?php } ?>
</div>
</div>
