<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			How We Style Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
$user = JFactory::getUser();
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>
<div class="home-slider" style="margin-top: -30px;"> 
	<ul class="slides">
    <?php for ( $i = 0, $n = count( @$jSeblod->hws_imgx ); $i < $n; $i++ ) { ?>
		<li class="item">
			<div class="imgwrapper" style="background-image: url('images/<?php echo JRequest::getInt('id').'/'.basename(@$jSeblod->hws_imgx[$i]->value); ?>');">
				<img style="opacity: 0;" src="images/<?php echo JRequest::getInt('id').'/'.basename(@$jSeblod->hws_imgx[$i]->value); ?>" />
			</div>
		</li> 
        <?php } ?>
	</ul>
</div>
<div class="container hws" style="position: relative;">
    <div class="row">
        <div class="col-lg-4 col-md-12 col-sm-12 ttl">
            <h3><?php echo $jSeblod->category_title->value; ?></h3>
            <p><?php echo $jSeblod->hws_subtitle->value; ?></p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 desc">
            <?php echo $jSeblod->wysiwyg_description->value; ?>
        </div>
    </div>
	<hr />
</div>