<?php // @version $Id: default.php  $
defined('_JEXEC') or die('Restricted access');
$user = &JFactory::getUser();
$userData = clone $user;

unset($userData->params);
unset($userData->_params);
unset($userData->password);
unset($userData->password_clear);
unset($userData->_errorMsg);
unset($userData->_errors);
unset($userData->activation);
//unset($userData->group_id);


if(!JRequest::getVar('id')){
    echo json_encode($userData);
}
?>

