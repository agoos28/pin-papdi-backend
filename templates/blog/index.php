<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Blog Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
$document = JFactory::getDocument();
$renderer = $document->loadRenderer('module');
$user =& JFactory::getUser();
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>

<div id="content" class="blog-full p-t-80 m-b-30 p-t-xs-50 m-b-xs-15">
  <div class="content-body">
    <div class="container">
      <div class="row m-b-50 m-b-xs-30">
      	<div class="col-md-3 col-sm-2 col-xs-12 pull-right hidden-sm hidden-xs">
        <?php 
				$popular = JModuleHelper::getModule('newsflash_jseblod','MOST POPULAR POST');
				echo $renderer->render($popular);
				?>
        </div>
        <div class="col-md-8 col-sm-10 col-xs-12 pull-right">
        	<div>
            <div id="main-content" class="text-content m-b-15 text-large">
            <?php if($user->usertype == 'Super Administrator'){ ?>
              	<a href="<?php echo $jSeblod->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
              <?php } ?>
              <div class="m-b-30 m-b-xs-15">
              	<img src="<?php echo $jSeblod->bl_img->value; ?>" alt="<?php echo $jSeblod->bl_title->value; ?> image" class="image-responsive-height">
              </div>
            	<h3 class="m-b-25 m-b-xs-15"><?php echo $jSeblod->bl_title->value; ?></h3>
              <?php echo $jSeblod->bl_content->value; ?>
              <?php echo JHTML::_('date',$this->content->created); ?>
            </div>
            <div class="comments m-b-15">
              <div class="fb-comments" data-width="100%" data-href="<?php echo JURI::current(); ?>" data-numposts="5"></div>
              </div>
          </div>
        </div>
        
        <div class="col-md-1 col-sm-2 col-xs-12">
          <div class="blogshare-buttons">
          <h6>Share</h6>
          <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo JURI::current(); ?>" class="popup btn btn-social-icon btn-facebook"><span class="fa fa-facebook"></span></a> <a href="http://twitter.com/home?status=<?php echo $jSeblod->pr_title->value; ?> | <?php echo JURI::current(); ?>" class="popup btn btn-social-icon btn-twitter"><span class="fa fa-twitter"></span></a> 
          </div>
          
        </div>
      </div>
      <div class="row">
        <div class="col-sm-11 col-xs-12 pull-right">
        <?php 
          $similar = JModuleHelper::getModule('newsflash_jseblod','Blog Similar');
          $similar->params = $similar->params;
          $similar->params = str_replace('catid=0','catid='.$this->content->catid."\r\nexclude=".$this->content->id,$similar->params);
          echo $renderer->render($similar);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php 
$doc =& JFactory::getDocument();
$metadesc = $jSeblod->pr_meta_desc->value;
$metakey = str_replace(' ', ', ', $metadesc);
$doc->setDescription($metadesc);
$doc->setMetaData( 'title',  $jSeblod->pr_title->value, 'og');
$doc->setMetaData( 'image',   str_replace(' ', '%20',$base.$jSeblod->imagex[0]->thumb4), 'og');
$doc->setMetaData( 'description', $metadesc);
$doc->setMetaData( 'author',  'LUXATME');

//$doc->setMetaData( 'description',  $jSeblod->pr_title->value, 'og');
//$doc->setBuffer('Baju Anak Branded '.strtoupper($this->content->brand).' '.$this->content->category.' '.$jSeblod->pr_title->value, 'seotitle', 'value');

?>