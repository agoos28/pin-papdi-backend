<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Blog Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
$user =& JFactory::getUser();
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );

?>
<div class="row p-t-100 m-b-50 p-t-xs-30 m-b-xs-40">
<div class="col-sm-10 col-xs-12 pull-right">
  <div class="blogdate text-center">
    <div class="text-extra-large"><?php echo JHTML::_('date',$this->content->created,'%e'); ?></div>
    <div class="text-large"><?php echo JHTML::_('date',$this->content->created,'%B'); ?></div>
  </div>
  <div class="text-content m-b-30 text-large">
  <?php if($user->usertype == 'Super Administrator'){ ?>
      <a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
    <?php } ?>
    <div class="m-b-20 m-b-xs-15" style="max-height: 400px;overflow: hidden;">
      <a href="<?php echo $this->content->art_link; ?>"><img src="<?php echo $jSeblod->bl_img->value; ?>" alt="<?php echo $jSeblod->bl_title->value; ?> image" class="image-responsive-height"></a>
    </div>
    <a style="color: #3d3d3d;" href="<?php echo $this->content->art_link; ?>"><h3 class="m-b-20 m-b-xs-15 text-w-500"><?php echo $jSeblod->bl_title->value; ?></h3></a>
    <?php echo $jSeblod->bl_content->value; ?>
  </div>
  <div class="clearfix">
  	<a href="<?php echo $this->content->art_link; ?>" class="btn btn-lg btn-dark-blue rounded text-white m-r-40 m-r-xs-20" style="background-color: #004358;"><span class="hidden-xs">Baca</span> Selengkapnya</a> 
    <a class="fa-lg" style="color: #3d3d3d;" href="<?php echo $this->content->art_link; ?>"> <i class="fa fa-comment fa-2x" style="color: #94d3e7;transform: translate(0px ,5px);margin-right: 10px;"></i>
  <span class="fb-comments-count text-w-500" data-href="<?php echo $this->content->art_link; ?>">0</span> <span class="hidden-xs">Comment</span>
</a>
  </div>
</div>
</div>

	