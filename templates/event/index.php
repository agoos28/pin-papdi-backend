<?php

// No Direct Access
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_eventorg' . DS . 'models' . DS . 'manage.php');
include(dirname(__FILE__) . '/params.php');

$baseUrl = JURI::base();
$jSeblod = clone $this;
$user = JFactory::getUser();

JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_eventorg' . DS . 'models');
$event_model =& JModel::getInstance('manage', 'EventOrgModel');
$event_data = $event_model->getDataFromContentId($this->content->id);
$packages = $event_model->getPackages($event_data->event->id, true);
$activePackages = $event_model->getPackages($event_data->event->id, false);
$groupPackages = $event_model->getPackageGroups($event_data->event->id);

if (JRequest::getVar('platform') === 'api') {
	$data = new stdClass();
	$data->id = $event_data->event->id;
	$data->catId = $this->content->catid;
	$data->title = $jSeblod->ev_title->value;
	$data->description = html_entity_decode(strip_tags($jSeblod->ev_short_description->value));
	$data->image = $baseUrl . $jSeblod->ev_cover->value;
	$data->event = $event_data;
	$data->pengantar = $jSeblod->ev_description->value;
	$data->info = $jSeblod->ev_info->value;
	$data->ketentuan = $jSeblod->ev_registration_policy->value;

	$data->activePackages = $activePackages;

	$data->packages = array();
	for ($i = 0; $i < count($groupPackages); $i++) {
		$group = $groupPackages[$i];
		$group->items = array();
		foreach ($packages as $package) {
			if ($package->package_group == $group->id) {
				$group->items[] = $package;
			}
		}
		$data->packages[] = $group;
	}

	$data->agenda = array();
	$data->workshops = array();
	$str = array('Pertama', 'Kedua', 'Ketiga', 'Keempat', 'Kelima', 'Keenam', 'Ketujuh', 'Kedelapan', 'Kesembilan', 'Kesepuluh', 'Kesebelas', 'Keduabelas');
	$day = 0;
	$prevDate = JHTML::_('date', $event_data->schedule[0]->start_time, '%d');
	for ($i = 0; $i < count($event_data->schedule); $i++) {
		$schedule = $event_data->schedule[$i];
		$date = JHTML::_('date', $schedule->start_time, '%d');
		if ($date !== $prevDate) {
			$day++;
			$prevDate = $date;
		}
		if (!$data->agenda[$day]) {
			$data->agenda[$day] = new stdClass();
		}
		$data->agenda[$day]->id = $day;
		$data->agenda[$day]->title = 'Hari ' . $str[$day];
		$data->agenda[$day]->description = JHTML::_('date', $schedule->start_time, '%A, %d %b %Y');
		$data->agenda[$day]->timeline[] = $schedule;

		if ($schedule->activity_type == 2) {
			$data->workshops[] = $schedule;
		}
	}


	echo json_encode($data);

} else {

	?>
	<div class="content ">
		<div class="jumbotron margin-xs-0" data-pages="parallax">
			<div class="container  sm-p-l-20 sm-p-r-20">
				<div class="inner" style="transform: translateY(0px); opacity: 1;">
					<ul class="breadcrumb">
						<li>
							<p>Pages</p>
						</li>
						<li><a href="#" class="active">Blank template</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row sm-gutter m-b-30">
				<div class="col-md-4">
					<div class="card share full-width m-b-0">
						<div class="circle" data-toggle="tooltip" title="" data-container="body" data-original-title="Label">
						</div>
						<div class="card-content">
							<img alt="<?php echo $jSeblod->ev_title->value; ?>"
									 src="<?php echo $baseUrl . $jSeblod->ev_cover->value; ?>">
						</div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="panel bg-white shadow-sm full-height p-b-100">
						<div class="padding-20">
							<h1 class="font-arial m-t-0">
								<strong><?php echo $jSeblod->ev_title->value; ?></strong>
							</h1>
							<h5 class="m-b-0"><?php echo $jSeblod->ev_short_description->value; ?></h5>
						</div>
						<div style="position: absolute;right: 25px;bottom: 28px;">
							
						</div>
					</div>
				</div>
			</div>
			<div class="panel bg-white shadow-sm full-height m-b-30">
				<ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
					<li class="active">
						<a class="inline" href="#tab_about" data-toggle="tab" role="tab">Pengantar</a>
					</li>
					<li class="">
						<a class="inline" href="#tab_info" data-toggle="tab" role="tab">Info Kegiatan</a>
					</li>
					<li class="">
						<a class="inline" href="#tab_location" data-toggle="tab" role="tab">Peta Lokasi</a>
					</li>
				</ul>
				<div class="tab-content padding-20">
					<div class="tab-pane active" id="tab_about">
						<?php echo $jSeblod->ev_description->value; ?>
					</div>
					<div class="tab-pane relative" id="tab_info">
						<?php echo $jSeblod->ev_info->value; ?>
					</div>
					<div class="tab-pane" id="tab_location">
						<div id="gmap" style="min-height: 500px;"></div>
					</div>
				</div>
			</div>
			<div class="panel bg-white m-b-30 shadow-sm">
				<ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
					<li class="active">
						<a class="inline" href="#paket" data-toggle="tab" role="tab">Paket Pendaftaran</a>
					</li>
					<li class="">
						<a class="inline" href="#agenda" data-toggle="tab" role="tab">Agenda Kegiatan</a>
					</li>
					<li class="">
						<a class="inline" href="#workshop" data-toggle="tab" role="tab">Workshop</a>
					</li>
					<li class="">
						<a class="inline" href="#ketentuan" data-toggle="tab" role="tab">Ketentuan Pendaftaran</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active p-t-10" id="paket">
						<?php
						$colors = array('bg-success', 'bg-complete', 'bg-danger');
						for ($i = 0; $i < count($groupPackages); $i++) {
							$group = $groupPackages[$i]; ?>
							<div class="panel bg-master-lightest m-b-30 border d-none d-md-block">
								<div class="panel-body relative no-padding">
									<div class="p-t-0 m-b-20">
										<table class="table m-t-0 no-footer" id="stripedTable" role="grid">
											<thead>
											<tr role="row" class="<?php echo $colors[$i]; ?>">
												<th class="border-top-0 text-light" style="width: 40%;">
													<h5 class="font-montserrat text-white m-t-0 m-b-0"><?php echo $group->title; ?></h5>
												</th>
												<th class="border-top-0 text-light text-center" style="width: 15%;">
													<span class="small">Anggota PAPDI</span>
												</th>
												<th class="border-top-0 text-light text-center" style="width: 15%;">
													<span class="small">Anggota PAPDI FINASIM</span>
												</th>
												<th class="border-top-0 text-light text-center" style="width: 15%;">
													<span class="small">Spesialis Lainnya</span>
												</th>
												<th class="border-top-0 text-light text-center" style="width: 15%;">
													<span class="small">Dokter Umum</span>
												</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach ($packages as $package) {
												if ($package->package_group == $group->id) {
													?>
													<tr role="row" class="odd">
														<td class="v-align-middle semi-bold sorting_1">
															<h5 class="no-margin"><strong><?php echo $package->title; ?></strong></h5>
															<small><?php echo $package->description; ?></small>
														</td>
														<td class="v-align-middle text-center">
															<?php if ($package->price_member) { ?>
																<p class="font-montserrat">Rp.<?php echo number_format($package->price_member); ?></p>
															<?php } else {
																echo '-';
															} ?>
														</td>
														<td class="v-align-middle text-center">
															<?php if ($package->price_finasim) { ?>
																<p class="font-montserrat">Rp.<?php echo number_format($package->price_finasim); ?></p>
															<?php } else {
																echo '-';
															} ?>
														</td>
														<td class="v-align-middle text-center">
															<?php if ($package->price) { ?>
																<p class="font-montserrat">Rp.<?php echo number_format($package->price); ?></p>
															<?php } else {
																echo '-';
															} ?>
														</td>
														<td class="v-align-middle text-center">
															<?php if ($package->price_dokter_umum) { ?>
																<p class="font-montserrat">
																	Rp.<?php echo number_format($package->price_dokter_umum); ?></p>
															<?php } else {
																echo '-';
															} ?>
														</td>
													</tr>
												<?php }
											} ?>
											</tbody>
										</table>
										<div class="padding-15">
										<?php echo $group->description; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="panel bg-master-lightest m-b-30 border d-md-none">
								<div class="panel-body relative no-padding">
									<div class="p-t-0 m-b-20">
										<table class="table m-t-0 m-b-0 no-footer" id="stripedTable" role="grid">
											<thead>
											<tr role="row" class="<?php echo $colors[$i]; ?>">
												<th class="border-top-0 text-light" style="width: 40%;">
													<h5 class="font-montserrat text-white m-t-0 m-b-0"><?php echo $group->title; ?></h5>
													<small style="text-transform: none;">
														Dari <?php echo JHTML::_('date', $group->start_date, '%A, %d %B %Y'); ?>
														Sampai <?php echo JHTML::_('date', $group->end_date, '%A, %d %B %Y'); ?></small>
												</th>
											</tr>
											</thead>
											<tbody>
											<?php foreach ($packages as $package) {
												if ($package->package_group == $group->id) {
													?>
													<tr role="row" class="odd">
														<td class="v-align-middle semi-bold sorting_1">
															<div class="m-b-10">
																<h5 class="no-margin"><strong><?php echo $package->title; ?></strong></h5>
																<small><?php echo $package->description; ?></small>
															</div>
															<div class="d-flex justify-content-between border-top p-t-5 p-b-5">
																<div><strong>Anggota PAPDI</strong></div>
																<div
																	class="font-montserrat"><?php echo $package->price_member ? 'Rp ' . number_format($package->price_member) : ' - '; ?></div>
															</div>
															<div class="d-flex justify-content-between border-top p-t-5 p-b-5">
																<div><strong>Anggota PAPDI FINASIM</strong></div>
																<div
																	class="font-montserrat"><?php echo $package->price_finasim ? 'Rp ' . number_format($package->price_finasim) : ' - '; ?></div>
															</div>
															<div class="d-flex justify-content-between border-top p-t-5 p-b-5">
																<div><strong>Spesialis Lainnya</strong></div>
																<div
																	class="font-montserrat"><?php echo $package->price ? 'Rp ' . number_format($package->price) : ' - '; ?></div>
															</div>
															<div class="d-flex justify-content-between border-top p-t-5 p-b-5">
																<div><strong>Dokter Umum</strong></div>
																<div
																	class="font-montserrat"><?php echo $package->price_dokter_umum ? 'Rp ' . number_format($package->price_dokter_umum) : ' - '; ?></div>
															</div>
														</td>
													</tr>
												<?php }
											} ?>
											</tbody>
										</table>
										<div class="package-description padding-15">
											<?php echo $group->description; ?>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="tab-pane" id="agenda">
						<?php
						$d = 0;
						$str = array('Pertama', 'Kedua', 'Ketiga', 'Keempat', 'Kelima', 'Keenam', 'Ketujuh', 'Kedelapan', 'Kesembilan', 'Kesepuluh', 'Kesebelas', 'Keduabelas');
						$prevDate = JHTML::_('date', $event_data->schedule[0]->start_time, '%A, %d %b %Y');
						?>
						<div class="table-responsive d-none d-md-block">
							<table class="table table-bordered" id="detailedTable">
								<thead>
								<tr role="row" class="bg-warning-light text-center">
									<th style="width:110px;" class="padding-5 text-dark">Jam</th>
									<th class="padding-5 text-dark">
										<div>Hari <?php echo $str[$d]; ?></div>
										<div><?php echo $prevDate; ?></div>
									</th>
								</tr>
								</thead>
								<tbody>
								<?php
								for ($i = 0;
								$i < count($event_data->schedule);
								$i++) {
								$event_item = $event_data->schedule[$i];
								$date = JHTML::_('date', $event_item->start_time, '%A, %d %b %Y');
								if ($prevDate !== $date){
								$d++; ?>
								</tbody>
							</table>
							<table class="table table-bordered" id="detailedTable">
								<thead>
								<tr role="row" class="bg-warning-light text-center">
									<th style="width:110px;" class="padding-5 text-dark">Jam</th>
									<th class="padding-5 text-dark">
										<div>Hari <?php echo $str[$d]; ?></div>
										<div><?php echo $date; ?></div>
									</th>
								</tr>
								</thead>
								<tbody>
								<?php } ?>
								<tr class="disable">
									<td class="v-align-middle">
										<?php echo JHTML::_('date', $event_item->start_time, '%H:%M'); ?> -
										<?php echo JHTML::_('date', $event_item->end_time, '%H:%M'); ?>
									</td>
									<?php if ($event_item->activity_type == 2) { ?>
									<td class="v-align-middle no-padding">
										<div class="bg-success-light text-center text-dark padding-5 text-bold">
											<?php echo $event_item->name; ?>
										</div>
										<table class="table table-inline table-striped table-condensed m-t-0 m-b-0">
											<tbody>
											<?php foreach ($event_item->activities as $event_activities) {
												?>
												<tr>
													<td style="width: 100px;"><?php echo $event_activities->code; ?></td>
													<td><?php echo $event_activities->name; ?></td>
												</tr>
												<?php
											} ?>
											</tbody>
										</table>
										<?php }else{ ?>
									<td class="v-align-middle ">
										<?php echo $event_item->name; ?>
										<?php } ?>
									</td>
								</tr>
								<?php
								$prevDate = $date;
								}
								?>
								</tbody>
							</table>
						</div>
						<div class="table-responsive d-md-none">
							<table class="table" id="detailedTable">
								<thead>
								<tr role="row">
									<th style="width:20%" class="border-top-0">Agenda</th>
									<th style="width:15%" class="border-top-0">Waktu</th>
								</tr>
								</thead>
								<tbody>
								<?php
								foreach ($event_data->schedule as $event_item) { ?>
									<tr class="disable">
										<td class="v-align-middle ">
											<?php echo $event_item->name; ?><br/>
											<span
												class="small"><?php echo JHTML::_('date', $event_item->start_time, '%A, %d %b %Y'); ?></span>
										</td>
										<td class="v-align-middle">
											<?php echo JHTML::_('date', $event_item->start_time, '%H:%M'); ?> -
											<?php echo JHTML::_('date', $event_item->end_time, '%H:%M'); ?>
										</td>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="tab-pane" id="workshop">
						<div class="table-responsive d-none d-md-block">
							<table class="table table-condensed table-detailed" id="detailedTable">
								<thead>
								<tr role="row">
									<th style="width:20%" class="border-top-0">Judul</th>
									<th style="width:15%" class="border-top-0">Waktu</th>
									<th style="width:15%" class="border-top-0">Tanggal</th>
								</tr>
								</thead>
								<tbody>
								<?php
								foreach ($event_data->workshops as $event_item) {
									if ($event_item->activity_type == 2) {
										?>
										<tr class="disable">
											<td class="v-align-middle ">
												<?php echo $event_item->name; ?>
											</td>
											<td class="v-align-middle">
												<?php echo JHTML::_('date', $event_item->start_time, '%H:%M'); ?> -
												<?php echo JHTML::_('date', $event_item->end_time, '%H:%M'); ?>
											</td>
											<td class="v-align-middle">
												<?php echo JHTML::_('date', $event_item->start_time, '%A, %d %b %Y'); ?>
											</td>
										</tr>
										<tr class="row-details" style="display: none;">
											<td colspan="3">
												<table class="table table-inline table-striped table-condensed m-t-0">
													<thead>
													<tr>
														<th width="100">Kode</th>
														<th width="100%">Aktifitas</th>
														<th class="text-center" width="200">Kapasitas Peserta</th>

													</tr>
													</thead>
													<tbody>
													<?php foreach ($event_item->activities as $event_activities) {

														?>
														<tr>
															<td class="p-l-20"><?php echo $event_activities->code; ?></td>
															<td class="p-l-20"><?php echo $event_activities->name; ?></td>
															<td class="text-center"><?php echo $event_activities->seats; ?></td>
														</tr>
														<?php $i++;
													} ?>
													</tbody>
												</table>
											</td>
										</tr>
									<?php }
								} ?>
								</tbody>
							</table>
						</div>
						<div class="table-responsive d-md-none">
							<table class="table table-condensed table-detailed" id="detailedTable">
								<thead>
								<tr role="row">
									<th style="width:20%" class="border-top-0">Judul</th>
									<th style="width:20%" class="border-top-0">Waktu</th>
								</tr>
								</thead>
								<tbody>
								<?php
								foreach ($event_data->schedule as $event_item) {
									if ($event_item->activity_type == 2) {
										?>
										<tr class="disable">
											<td class="v-align-middle ">
												<?php echo $event_item->name; ?>
												<div class="small m-l-20">
													<?php echo JHTML::_('date', $event_item->start_time, '%A, %d %b %Y'); ?>
												</div>
											</td>
											<td><?php echo JHTML::_('date', $event_item->start_time, '%H:%M'); ?> -
												<?php echo JHTML::_('date', $event_item->end_time, '%H:%M'); ?></td>
										</tr>
										<tr class="row-details" style="display: none;">
											<td colspan="2">
												<table class="table table-inline table-striped m-t-0">
													<thead>
													<tr>
														<th width="100">Aktifitas</th>
													</tr>
													</thead>
													<tbody>
													<?php foreach ($event_item->activities as $event_activities) {
														?>
														<tr>
															<td><?php echo $event_activities->code; ?> - <?php echo $event_activities->name; ?></td>
														</tr>
														<?php $i++;
													} ?>
													</tbody>
												</table>
											</td>
										</tr>
									<?php }
								} ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="tab-pane" id="ketentuan">
						<?php echo $jSeblod->ev_registration_policy->value; ?>
					</div>
				</div>
				<div class="panel-footer padding-15 border-top text-right">
				<?php if (!$user->id) { ?>
					<a href="<?php echo JURI::base(); ?>login?return=<?php echo base64_encode(JURI::current()); ?>"
							class="btn btn-lg  btn-primary">
						Daftar Sekarang
					</a>
				<?php } else { ?>
					<button class="btn btn-lg  btn-primary" data-toggle="modal" data-target="#modalSlideUp">
						Daftar Sekarang
					</button>
				<?php } ?>
				</div>
			</div>
			<!-- END PLACE PAGE CONTENT HERE -->
		</div>
		<!-- END CONTAINER FLUID -->
	</div>
	<div class="modal fade slide-up disable-scroll" id="modalSlideUp" tabindex="-1" role="dialog" aria-hidden="false">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content-wrapper full-width">
				<div class="modal-content">
					<form id="registration">
						<div class="modal-header clearfix text-left">
							<button id="reg_close" type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
									class="pg-close fs-14"></i>
							</button>
							<h5>Registrasi <span class="semi-bold">Acara</span></h5>
						</div>
						<div class="modal-body padding-xs-15 p-t-0 p-b-5">
							<ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
								<li class="active">
									<a class="inline" href="#reg_as_tab" data-toggle="tab" role="tab"
										 style="pointer-events: none;">Sebagai</a>
								</li>
								<li class="">
									<a class="inline" href="#reg_package_tab" data-toggle="tab" role="tab" style="pointer-events: none;">Paket</a>
								</li>
								<li class="">
									<a class="inline" href="#reg_workshop_tab" data-toggle="tab" role="tab" style="pointer-events: none;">Workshop</a>
								</li>
								<li class="">
									<a class="inline" href="#reg_confirm_tab" data-toggle="tab" role="tab" style="pointer-events: none;">Konfirmasi</a>
								</li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="reg_as_tab">
									<div class="credential_error error hide"></div>
									<ol class="p-l-15">
										<li>
											<p class="bold">Mendaftarkan sebagai?</p>
											<div class="radio radio-success">
												<input class="hide" id="reg_as_self" type="radio" name="reg_as" value="self"/>
												<label for="reg_as_self">Individual</label>
												<input id="reg_as_sponsor" type="radio" name="reg_as" value="sponsor"/>
												<label for="reg_as_sponsor">Farmasi/Institusi</label>
											</div>
											<div id="reg_as_info" class="small text-success"></div>
											<div id="reg_as_alert" class="small text-info"></div>

										</li>
										<li>
											<div class="p-t-20">
												<p class="bold">Apakah peserta merupakan anggota PAPDI yang terdaftar?</p>
												<div class="radio radio-success">
													<input class="hide" id="anggota" type="radio" name="reg_keanggotaan" value="anggota"/>
													<label for="anggota">Anggota PAPDI</label>
													<input id="non_anggota" type="radio" name="reg_keanggotaan" value="non-anggota"/>
													<label for="non_anggota">Bukan Anggota PAPDI</label>
												</div>
												<div class="row">
													<div id="anggota_container" class="col-sm-8 col-xs-12 hide">
														<div class="form-group">
															<label>No Pokok Anggota (NPA PAPDI) </label>
															<input id="reg_npa" class="form-control" type="text" name="reg_npa"
																		 placeholder="format, 000.0000.0000.00000"/>
															<div id="npa_error" class="error"></div>
														</div>
														<p class="help">Bagi peserta yang memerlukan konfirmasi NPA dapat hubungin ke Nomor 
														(<strong>Rahmi</strong> : <a href="tel:+6281617489717">0816 1748 9717</a>, <strong>Husni</strong> : <a href="tel:+6281511218113">0815 1121 8113</a>)</p>
													</div>
													<div id="non_anggota_container" class="col-sm-12 col-xs-12 hide">
														<div class="row">
															<div class="col-sm-12 col-xs-12">
																<div class="form-group-attached m-b-15">
																	<div class="form-group form-group-default">
																		<label>Nama peserta</label>
																		<input type="text" class="form-control" name="reg_name"
																					 placeholder="Nama lengkap"
																					 value="">
																	</div>
																	<div class="row clearfix">
																		<div class="col-sm-6">
																			<div class="form-group form-group-default">
																				<label>Alamat email peserta</label>
																				<input class="form-control" type="text" name="reg_email"
																							 placeholder="cth, email@anda.com" value=""/>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group form-group-default">
																				<label>Nomor telpon peserta</label>
																				<input class="form-control" type="text" name="reg_phone2"
																							 placeholder="Nomor seluler" value=""/>
																			</div>
																		</div>
																	</div>
																	<div class="row clearfix">
																		<div class="col-sm-6">
																			<div class="form-group form-group-default">
																				<label>Bidang peserta</label>
																				<div class="radio radio-success d-inline-block m-r-15">
																					<input class="hide" id="dokter_umum" type="radio" name="reg_bidang"
																								 value="dokter umum"/>
																					<label for="dokter_umum">Dokter Umum</label>
																				</div>
																				<div class="radio radio-success d-inline-block">
																					<input class="hide" id="spesialis" type="radio" name="reg_bidang"
																								 value="spesialis"/>
																					<label for="spesialis">Spesialis</label>
																				</div>
																			</div>
																		</div>
																		<div class="col-sm-6">
																			<div class="form-group form-group-default">
																				<div id="spesialis_container" class="hide">
																					<label>Pilih bidang</label>
																					<select name="reg_spesialis" class="full-width select2"
																									data-init-plugin="select2">
																						<option value="">Pilih bidang spesialis</option>
																						<option value="Jantung dan Pembuluh Darah">Jantung dan Pembuluh Darah
																						</option>
																						<option value="Paru">Paru</option>
																						<option value="Saraf">Saraf</option>
																						<option value="Anestesi">Anestesi</option>
																						<option value="Lain-lain">Lain-lain</option>
																					</select>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ol>
									<div class="d-flex justify-content-end row">
										<a class="btn btn-md btn-primary to_package text-white">Lanjut</a>
									</div>
								</div>
								<div class="tab-pane" id="reg_package_tab">
									<p class="m-b-15">Pilih paket sebagai <span id="sebagai_desc"></span></p>
									<div id="package_container"></div>
									<div class="d-flex justify-content-between row">
										<a class="btn totab" href="#reg_as_tab">Kembali</a>
									</div>
								</div>
								<div class="tab-pane" id="reg_workshop_tab">
									<p>Pilih workshop yang akan diikuti, maximum 8 workshop</p>
									<div id="workshop_selection_container"></div>
									<div id="workshop_error" class="error"></div>
									<div class="d-flex justify-content-between row">
										<a class="btn totab" href="#reg_package_tab">Kembali</a>
										<button id="confirmButton" class="btn btn-md btn-primary text-white">Selanjutnya</button>
									</div>
								</div>
								<div class="tab-pane" id="reg_confirm_tab">
									<div class="confirm_error error hide"></div>
									<div id="confirm_data" class="m-b-20">

									</div>

									<div class="d-flex justify-content-between row">
										<a class="btn totab" href="#reg_workshop_tab">Kembali</a>
										<button class="btn btn-md btn-primary text-white confirmToggle">OK</button>

									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	</div>
	<div class="modal fade slide-up" id="confirmModal" style="z-index: 9999;">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content-wrapper" style="margin: auto;">
				<div class="modal-content">
					<div class="modal-header clearfix text-left">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
						</button>
						<h5>Konfirmasi</h5>
					</div>
					<div class="modal-body">
						<p>Apakah anda sudah yakin dengan pilihan workshop ini?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
						<button class="btn btn-md btn-primary addToCart text-white"
										data-id="<?php echo $this->content->id; ?>"
										data-title="<?php echo $jSeblod->pr_title->value; ?>"
										data-thumb="<?php echo $baseUrl . $jSeblod->ev_cover->value; ?>"
										data-text="Book Now"
										data-review="undefined"
										data-pricing=""
						>Masukkan Keranjang
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/template" data-template="confirm_data">
		<div class="form-group-attached">
			<div class="row clearfix">
				<div class="col-sm-6">
					<div class="form-group form-group-default">
						<label>Nama</label>
						<div>${name}</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group form-group-default">
						<label>Keanggotaan</label>
						<div>${package_member}</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-sm-6">
					<div class="form-group form-group-default">
						<label>Email</label>
						<div>${email}</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group form-group-default">
						<label>No Telpon</label>
						<div>${phone}</div>
					</div>
				</div>
			</div>
			<div class="form-group form-group-default">
				<label>Bidang</label>
				<div>${bidang}</div>
			</div>
			<div class="form-group form-group-default">
				<label>Pilihan Workshop</label>
				<div class="size text-small">
					<ul class="opt-desc p-l-15 m-b-5">
						${workshops}
					</ul>
				</div>
			</div>
		</div>
	</script>
	<script type="text/template" data-template="packageItem">
		<div class="row border-bottom m-b-15 p-b-15 align-items-md-center">
			<div class="col-md-6 col-sm-12 col-xs-12">
				<h6 class="no-margin">
					<strong>${group_name} - ${title}</strong>
				</h6>
				<small>${description}</small>
			</div>
			<div class="col-md-3 col-sm-6 col-6">
				${formated_pricing}
			</div>
			<div class="col-md-3 col-sm-6 col-6 text-right m-t-xs-15">
				<a class="btn btn-sm btn-primary text-white to_workshop" data-id="${id}" data-name="${title}"
					 data-price="${pricing}">Pilih</a>
			</div>
		</div>
	</script>
	<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
	<script type="text/javascript">
		var componentUrl = '<?php echo JRoute::_('index.php?option=com_eventorg'); ?>';
		var activePackage = <?php echo $activePackages ? json_encode($activePackages) : '[]'; ?>;
		var eventId = '<?php echo $event_data->id; ?>';
		var regAs = {};
		var selectedPackage = {};
		var registrationData = {};
		var validateWorkshops = [];
		$(window).load(function () {
			jQuery('#gmap').gmap3({
				map: {
					options: {
						zoom: 12,
						center: <?php echo str_replace('&quot;', '"', $jSeblod->ev_location->value); ?>,
						mapTypeControlOptions: {
							mapTypeIds: [google.maps.MapTypeId.ROADMAP,
								google.maps.MapTypeId.SATELLITE,
								google.maps.MapTypeId.HYBRID,
								google.maps.MapTypeId.TERRAIN
							]
						}
					},
					events: {
						rightclick: function (map, event) {
							addMarker(event.latLng)
						}
					}
				},
				marker: {
					values: [{
						latLng: <?php echo str_replace('&quot;', '"', $jSeblod->ev_location->value); ?>
					},],
					options: {
						draggable: true
					},
					events: {
						dragend: function (marker, event, context) {
							updateForm(marker.position)
						}
					}
				}
			});
		});

		function clearData() {
			selectedPackage = {};
			registrationData = {};
			validateWorkshops = [];

			$('input[name="reg_keanggotaan"]').attr("checked", false);
			$('input[name="reg_npa"]').val('');
			$('input[name="reg_as"]').attr("checked", false);
			$('#workshop_selection_container').empty();
			checkSebagai();
			checkBidang();
			checkKeanggotaan();
			$('a[href="#reg_as_tab"]').trigger('click')
		}

		function checkRegAs() {
			var as = '';
			var cartItem = $('.shoppingcart').find('.cart_row .cartItem');

			if (cartItem.length) {
				$('input[name="reg_as"]').attr("disabled", true);
				as = $(cartItem[0]).attr('data-as');
				// console.log(as);
				$('input[value="' + as + '"]').prop('checked', true).trigger('click');
				$('#reg_as_info').html('Untuk merubah pilihan kosongkan keranjang')
			} else {
				$('input[name="reg_as"]').attr("disabled", false);
				$('#reg_as_info').empty();
			}
		}


		async function getPackageOptions(id) {
			var postData = {
				task: 'getPackageOptions',
				'<?php echo JUtility::getToken(); ?>': 1,
				id: id
			};

			var fetch = await $.ajax({
				url: componentUrl,
				method: 'post',
				data: postData
			});

			//console.log(fetch);

			if (!fetch) {
				alert('paket tidak ditemukan')
				return;
			} else {
				return JSON.parse(fetch);
			}
		}

		async function getCart() {
			var baseUrl = '<?php echo $baseUrl; ?>';
			var href = baseUrl + 'cart';
			var postData = {
				task: 'getCart',
				'<?php echo JUtility::getToken(); ?>': 1,
				format: 'json'
			};

			var fetch = await $.ajax({
				url: href,
				method: 'post',
				data: postData
			})

			//console.log(fetch);

			if (!fetch) {
				// alert('paket tidak ditemukan')
				return;
			} else {
				return JSON.parse(fetch);
			}
		}

		function packageRules() {
			// console.log(activePackage);
			var packages = '';
			activePackage.map(function (item, index) {
				var rules = item.package_rules ? JSON.parse(item.package_rules) : [];
				// console.log(rules);
				// console.log(selectedPackage);
				if (rules.indexOf(selectedPackage.regAs) >= 0) {
					var priceIndex = {
						umum: item.price_dokter_umum,
						finasim: item.price_finasim,
						anggota: item.price_member,
						spesialis: item.price
					};
					item.pricing = priceIndex[selectedPackage.regAs];
					item.formated_pricing = (item.pricing).replace(/\d(?=(\d{3})+\.)/g, '$&.');
					var packageTemplate = $('script[data-template="packageItem"]').text().split(/\$\{(.+?)\}/g);
					packages += packageTemplate.map(render(item)).join('');
				}
			});
			$('#package_container').html(packages);
		}

		async function processMember() {
			var sebagai_desc;
			var keanggotaan = $('input[name="reg_keanggotaan"]:checked').val();
			var npa = $('input[name="reg_npa"]').val();
			var phone = $('input[name="reg_phone2"]').val();
			regAs.as = $('input[name="reg_as"]:checked').val();

			if (!regAs.as) {
				return;
			}

			selectedPackage.regAs = 'umum';
			if (keanggotaan === 'anggota') {
				if (!npa) {
					$('#npa_error').html('Masukkan nomor anggota');
					return
				}
				var postData = {
					task: 'checkNpa',
					'<?php echo JUtility::getToken(); ?>': 1,
					npa: npa
				};
				$.post(componentUrl, postData, function (data) {
					if (data == '0') {
						$('#npa_error').html('Nomor anggota tidak ditemukan');
						return;
					} else {
						// console.log(JSON.parse(data));
						var member = JSON.parse(data);
						var memberName = member.name;
						$('#npa_error').html('');
						$('#sebagai_desc').text(memberName);
						if (memberName.indexOf('FINASIM') >= 0) {
							selectedPackage.regAs = 'finasim';
							packageRules(selectedPackage.regAs)
						} else {
							selectedPackage.regAs = 'anggota';
							packageRules(selectedPackage.regAs)
						}
						regAs.name = member.name;
						if (member.email[0]) {
							regAs.email = member.email[0];
						}
						regAs.bidang = 'Spesialis ' + member.bidang;
						regAs.phone = $('input[name="reg_phone2"]').val();
						regAs.npa = npa;
						packageRules(selectedPackage.regAs);
						$('a[href="#reg_package_tab"]').trigger('click');
						return;
					}
				})
			} else {
				regAs.name = $('input[name="reg_name"]').val();
				regAs.email = $('input[name="reg_email"]').val();
				regAs.phone = $('input[name="reg_phone2"]').val();

				var reg_bidang = $('input[name="reg_bidang"]:checked').val();
				if (reg_bidang === 'spesialis') {
					sebagai_desc = 'spesialis ' + $('select[name="reg_spesialis"]').val();
					selectedPackage.regAs = 'spesialis';
				} else {
					sebagai_desc = 'dokter umum ';
				}

				$('#sebagai_desc').text(sebagai_desc);

				regAs.bidang = sebagai_desc;
				if ((!regAs.name) || (!regAs.email) || (!regAs.phone)) {
					// console.log(regAs);
					$('.credential_error').html('<div class="alert alert-danger" role="alert">' +
						'<button class="close" data-dismiss="alert"></button>Data belum lengkap</div>').removeClass('hide')
					return;
				} else {
					$('.credential_error').addClass('hide')
				}

				packageRules(selectedPackage.regAs);
				$('a[href="#reg_package_tab"]').trigger('click');
			}
		}

		function getAvailable(option, cart) {
			var seat = option.seats - option.registered;
			if (!cart) {
				return seat
			}
			cart.map(function (item) {
				item.workshops.map(function (_item) {
					if (_item.id === option.id) {
						seat = seat - 1
					}
				})
			});

			return seat
		}

		function SortByName(a, b) {
			var aName = a.group.name.toLowerCase();
			var bName = b.group.name.toLowerCase();
			return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
		}


		async function processPackage(data) {
			var options = await getPackageOptions(data.id);
			var cart = await getCart();

			// console.log(options);
			// console.log(cart);
			selectedPackage.id = data.id;
			selectedPackage.price = data.price;
			selectedPackage.name = data.name;
			validateWorkshops = [];
			var html = '';
			if (options.options) {
				var shortedOption = options.options.sort(SortByName);
				shortedOption.map(function (item) {
					var haveSelection = false;
					html += '<table class="table table-bordered workshopTable m-b-15"><thead><tr><th width="70%" class="p-l-5 text-dark"><strong>' + item.group.name + '</strong><br/><span style="text-transform: none;font-weight: normal;font-size: 10px;">' + item.group.date + '</span></th><th width="150" class="text-center">Sisa Kursi</th><th width="10%">Pilih</th></tr></thead><tbody>';
					item.selections.map(function (option) {
						var available = getAvailable(option, cart);

						if (available > 0) {
							haveSelection = true;
							html += '<tr class="available"><td class="p-l-20"><label class="p-l-0" for="opt' + option.id + '">' + option.name + '</label></td>';
							html += '<td class="text-center"><label for="opt' + option.id + '">' + available + '</label></td>';
							html += '<td class="text-center" align="no-padding center"><input class="workshops" id="opt' + option.id + '" type="radio" name="option[' + item.group.id + ']" value="' + option.id + '"></td></tr>';
						} else {
							html += '<tr><td class="p-l-20"><label class="p-l-0" for="opt' + option.id + '">' + option.name + '</label></td>';
							html += '<td class="text-center"><label for="opt' + option.id + '">Habis</label></td>';
							html += '<td class="text-center">&nbsp;</td></tr>';
						}
					});
					html += '</table>';
					if (haveSelection) {
						validateWorkshops.push(item.group)
					}
				});
			} else {
				html = 'Tidak ada workshop';
			}

			$('#workshop_selection_container').html(html);

			$('a[href="#reg_workshop_tab"]').trigger('click')
		}

		function confirmData() {
			var formdata = $('form#registration').serializeArray();
			var selectedWorkshopCount = 0;
			var data = {};
			$(formdata).each(function (index, obj) {
				data[obj.name] = obj.value;
			});
			// console.log(data);
			var workshopError = '';

			console.log(data['0ption*']);

			validateWorkshops.map(function (item) {
				// console.log(data['option[' + item.id + ']'])
				if (data['option[' + item.id + ']']) {
					selectedWorkshopCount++
					//workshopError += 'Anda belum memilih opsi ' + item.name + '<br/>'
				}
			});

			// console.log(selectedWorkshopCount);
			if (selectedWorkshopCount < 8) {
				workshopError = 'anda belum memilih 8 workshop'
			}

			if (workshopError) {
				$('#workshop_error').html(
					'<div class="alert alert-danger" role="alert">' +
					'<button class="close" data-dismiss="alert"></button>' + workshopError + '</div>'
				);
				return;
			} else {
				$('#workshop_error').html('');
			}


			var confirm_data = $('script[data-template="confirm_data"]').text().split(/\$\{(.+?)\}/g);
			var workshopItems = '';
			$('input.workshops:checked').each(function () {
				var th = $(this);
				workshopItems += '<li>' + th.parent().parent().find('label:first').text() + '</li>';
			});

			registrationData = {
				package_member: selectedPackage.regAs,
				price: selectedPackage.price,
				workshops: workshopItems,
				title: selectedPackage.name,
				name: regAs.name,
				email: regAs.email,
				phone: regAs.phone,
				npa: regAs.npa,
				bidang: regAs.bidang,
			};

			// console.log(registrationData);

			var html = confirm_data.map(render(registrationData)).join('');
			$('#confirm_data').html(html)
			$('a[href="#reg_confirm_tab"]').trigger('click')
		}

		function addToCart(elem) {
			var similar;
			var formdata = $('form#registration').serializeArray();
			var data = {};
			$(formdata).each(function (index, obj) {
				data[obj.name] = obj.value;
			});


			// console.log(data);

			var workshops = [];
			$('input.workshops:checked').each(function () {
				var th = $(this);
				workshops.push(th.val())
			});

			var baseUrl = '<?php echo $baseUrl; ?>';
			var href = baseUrl + 'cart';
			var postdata = {
				id: Date.now(),
				event_id: '<?php echo $event_data->event->id; ?>',
				package_id: selectedPackage.id,
				package_member: selectedPackage.regAs,
				price: selectedPackage.price,
				workshops: workshops,
				title: selectedPackage.name,
				as: regAs.as,
				name: regAs.name,
				email: regAs.email,
				phone: regAs.phone,
				npa: regAs.npa,
				bidang: regAs.bidang,
				form_data: JSON.stringify(data),
				useajax: 'true',
				task: 'addCart'
			};

			// console.log(postdata);

			var btntext = elem.text();
			elem.data('text', btntext);
			elem.parent().find('.cancel').addClass('hidden');
			elem.show();

			var cartCount = 0;

			$('.shoppingcart .cart_row .cartItem').each(function (index) {
				cartCount++;
				if ($(this).attr('data-email') === postdata.email && $(this).attr('data-event_id') === postdata.event_id) {
					similar = true;
					return false
				}
			});

			if (similar) {
				$('.confirm_error').html('<div class="alert alert-danger" role="alert">' +
					'<button class="close" data-dismiss="alert"></button>Peserta sudah ada dalam keranjang</div>').removeClass('hide')
				return false
			}

			elem.text('Wait');

			$.post(href, postdata, function (response) {
				// console.log(response);
				if ($(window).width() < 753) {
					$('body, html').animate({
						scrollTop: 0
					})
				}

				var html = '<div id="' + postdata.id + '"' +
					' data-event_id="' + postdata.event_id + '" ' +
					' data-email="' + postdata.email + '" ' +
					' data-name="' + postdata.name + '" ' +
					' data-price="' + postdata.price + '" ' +
					' data-as="' + postdata.as + '" ' +
					' style="display: none;" ' +
					' class="cartItem row no-margin p-t-15 p-b-15 border-bottom new">' +
					'<div class="col-sm-7 col-12">' +
					'<div class="">' +
					'<h6 class="m-b-5 m-t-0">' + postdata.name + '</h6>' +
					'<div class="size">' + '<?php echo $jSeblod->ev_title->value; ?> <br/>' + postdata.title + '</div>' +
					'</div></div><div class="col-sm-4 col-10">' +
					'<div class="price">Rp. <span class="subtotal">' + addCommas(postdata.price) + '</span>' +
					'</div></div>' +
					'<div class="col-sm-1 col-2 p-l-0"><a href="#" class="del_btn del" data-id="' + postdata.id + '">' +
					'<i class="fa fa-trash-o"></i></a></div>' +
					'</div>';

				setTimeout(function () {
					elem.text(elem.data('text'));
					$('#confirmModal').modal('hide')
					$('.shoppingcart .cart_row').prepend(html);
					$('#reg_close').trigger('click');
					setTimeout(function () {
						clearData();
						$('.cart-container').removeClass('hide');
						setTimeout(function () {
							$('.shoppingcart .cart_row').find('.new').slideDown();
							updatecart();
						}, 100);
					}, 1000)
				}, 1000)
			})
		}


		function checkSebagai() {
			var reg_as = $('[name="reg_as"]:checked').val();
			if (reg_as === 'self') {
				$('#reg_as_alert').html('')
			} else if (reg_as === 'sponsor') {
				$('#reg_as_alert').html('Pembayaran dengan Guarantee Letter maksimal 20 orang peserta')
			} else {
				$('#reg_as_alert').html('')
			}
		}

		function checkKeanggotaan() {
			var reg_keanggotaan = $('[name="reg_keanggotaan"]:checked').val();
			if (reg_keanggotaan === 'anggota') {
				$('#anggota_container').removeClass('hide');
				$('#non_anggota_container').addClass('hide');
			} else if (reg_keanggotaan === 'non-anggota') {
				$('#anggota_container').addClass('hide');
				$('#non_anggota_container').removeClass('hide')
			} else {
				$('#anggota_container').addClass('hide');
				$('#non_anggota_container').addClass('hide')
			}
		}

		function checkBidang() {
			var reg_bidang = $('[name="reg_bidang"]:checked').val();
			if (reg_bidang === 'spesialis') {
				$('#spesialis_container').removeClass('hide');
			} else {
				$('#spesialis_container').addClass('hide');
			}
		}

		function render(props) {
			return function (tok, i) {
				return (i % 2) ? props[tok] : tok;
			};
		}

		function clearError() {
			$('.error').empty()
		}


		$(document).ready(function () {
			// Add event listener for opening and closing details

			$('#modalSlideUp').on('shown.bs.modal', function () {
				checkRegAs()
			});


			$('#reg_npa').mask('999.9999.9999.99999');

			$('.addToCart').click(function (e) {
				e.preventDefault()

				addToCart($(this));
			});

			$('#confirmButton').click(function (e) {
				e.preventDefault()
				confirmData();
			});

			checkSebagai()
			$('[name="reg_as"]').change(function () {
				checkSebagai()
			});

			checkKeanggotaan()
			$('[name="reg_keanggotaan"]').change(function () {
				checkKeanggotaan()
			});

			checkBidang()
			$('[name="reg_bidang"]').change(function () {
				checkBidang()
			});

			$('.to_package').click(function () {
				processMember()
			});

			$('body').on('click', '.to_workshop', function () {
				var data = $(this).data();
				processPackage(data);
			});

			$('.totab').click(function (e) {
				e.preventDefault();
				var target = $(this).attr('href');
				$('.nav-tabs').find('a[href="' + target + '"]').trigger('click')
				clearError();
			});

			$('#confirmModal').on('show.bs.modal', function (e) {
				$('.modal-backdrop:first').css({
					zIndex: 1050
				})
			})

			$('#confirmModal').on('hide.bs.modal', function (e) {
				$('.modal-backdrop:first').css({
					zIndex: 1040
				})
				$('body').css({overflow: 'hidden'})
				setTimeout(function () {
					if($('#modalSlideUp').hasClass('show')){
						$('body').addClass('modal-open').attr('style', '')
					}
					$('body').attr('style', '')
				}, 500)
			})

			$('.confirmToggle').click(function (e) {
				e.preventDefault();
				$('#confirmModal').modal('show')
			});


			$('#detailedTable > tbody > tr').on('click', function () {
				//var row = $(this).parent()
				if ($(this).hasClass('row-details')) {
					return;
				}
				var next = $(this).next();
				$(this).parents('tbody').find('.row-details').not(next).hide();
				if ($(this).hasClass('shown') && $(this).next().hasClass('row-details')) {
					next.hide();
					$(this).removeClass('shown');
					return;
				} else {
					next.show()
				}
				var tr = $(this).closest('tr');
				$(this).parents('tbody').find('.shown').removeClass('shown');
				tr.addClass('shown');
				if (next.hasClass('row-details')) {
				}
			});

			$('.select2').select2()

			$('#workshop_selection_container').on('click', 'input.workshops', function (e) {
				var th = $(this);
				// console.log(th);
				// console.log($('#workshop_selection_container').find('input.workshops:checked').length)
				if($('#workshop_selection_container').find('input.workshops:checked').length > 8){
					e.preventDefault();
					th.prop('checked', false)
					alert('Maximum 8 workshop')
				}
				if(th.parents('tr').hasClass('selected')){
					e.preventDefault()
					th.prop('checked', false)
					th.parents('tr').removeClass('selected')
					console.log(th.prop('checked'));
					setTimeout(() => {
						th.prop('checked', false)
					}, 100);
				}
			})

			$('#workshop_selection_container').on('change', 'input.workshops', function () {
				var th = $(this);
				// console.log(th);
				th.parents('tr').addClass('selected');
				th.parents('table').find('tr').not(th.parents('tr')).removeClass('selected');
			})
		})
	</script>

<?php } ?>