<?php

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php

$baseUrl = JURI::base();
$jSeblod	=	clone $this;

?>

<?php 


require_once (JPATH_SITE.DS.'components'.DS.'com_eventorg'.DS.'models'.DS.'manage.php');

include( dirname(__FILE__) . '/params.php' );

JModel::addIncludePath(JPATH_SITE.DS.'components'.DS.'com_eventorg'.DS.'models');
$event_model =& JModel::getInstance('manage', 'EventOrgModel');
$event_data = $event_model->getDataFromContentId($this->content->id);

if(JRequest::getVar('platform') === 'api'){
	$data = new stdClass();
	$data->id = $this->content->id;
	$data->catId = $this->content->catid;
	$data->title = $jSeblod->ev_title->value;
	$data->description = html_entity_decode(strip_tags($jSeblod->ev_short_description->value));
	$data->image = $baseUrl . $jSeblod->ev_cover->value;
	$data->detailUrl = $this->content->art_link;
	echo json_encode($data);

}else{
?>

	<div class="row no-gutters m-t-30 m-b-30">
		<div class="col-md-4 col-sm-12">
			<div class="card share full-width m-b-0 full-height">
				<img class="full-height"
					alt="<?php echo $jSeblod->ev_title->value; ?>"
						 src="<?php echo $baseUrl . $jSeblod->ev_cover->value; ?>">
			</div>
		</div>
		<div class="col-md-8 col-sm-12">
			<div class="panel bg-white shadow-sm full-height p-b-100">
				<div class="padding-20">
					<h1 class="font-arial m-t-0">
						<strong><?php echo $jSeblod->ev_title->value; ?></strong>
					</h1>
					<h5 class="m-b-0"><?php echo $jSeblod->ev_short_description->value; ?></h5>
				</div>
				<div style="position: absolute;right: 25px;bottom: 28px;">
					<a href="<?php echo $this->content->art_link; ?>" class="btn btn-lg  btn-rounded btn-primary">
						Selengkapnya
					</a>
				</div>
			</div>
		</div>
	</div>
<?php } ?>