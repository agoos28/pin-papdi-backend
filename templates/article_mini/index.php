<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Article Mini Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/


$jSeblod	=	clone $this;
$user =& JFactory::getUser();
$menus = &JSite::getMenu();
$menu  = $menus->getActive();
$menu_params = new JParameter( $menu->params );


?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
function getDiscount($code=0, $total=0){
	$result = new stdClass();
	$result->discount = '';
	$result->total = $total;
		
	$db = JFactory::getDBO();
	$query = "SELECT * FROM #__promo_code WHERE code = '".$code."'";
	$db->setQuery($query);
	$disc = $db->loadObject();
	$error = $db->getErrorMsg();
	
	if($disc->product_id){
		$cart = $this->getCart();
		$valid = false;
		foreach($cart as $item){
			if($item->id == $disc->product_id){
				$valid = 1;
				$product_subtotal = $item->price * $item->count;
				break;
			}
		}
		if($valid){
			if($disc->type == 'percent'){
				$result->discount = $disc->value;
				$result->value = $product_subtotal * ($disc->value/100);
				$result->total = $total - $result->value;
			}elseif($disc->type == 'amount'){
				$result->discount = $disc->value;
				$result->value = $product_subtotal - $disc->value;
				$result->total = $total - $result->value;
			}
			return $result;
		}else{
			$result->err = 'Missing specified product';
			return $result;
		}
	}
	
	if($disc->rule == 'limited' && $disc->use_count >= $disc->max_usage ){
		$result->err = 'The code expired.';
		return $result;
	}
	if($disc->type == 'percent'){
		$result->discount = $disc->value;
		$result->total = $total - ($total * ($disc->value/100));
	}elseif($disc->type == 'amount'){
		$result->discount = $disc->value;
		$result->total = $total - $disc->value;
	}
	
	return $result;
}
function getProductOpt($id = 0, $key = null, $val = null){
		$db 	= JFactory::getDBO();
		$query	= 'SELECT options FROM #__content WHERE id = '.$id;
		$db->setQuery($query);
		$opt = $db->loadResult();
		$options = json_decode($opt);
		if($key == null && $val == null){
			return $options;
		}else{
			foreach($options as $opt){
				if($opt->$key == $val){
					$result = $opt;
					break;
				}
			}
			return $result;
		}
	}
function getOrder($id = 0){
	$result = new stdClass();
	$db = JFactory::getDBO();
	$query = 	"SELECT a.*, b.*, c.name FROM #__cart_order AS a".
				" LEFT JOIN #__cart_payment AS b ON b.order_id = a.id".
				" LEFT JOIN #__users AS c ON c.id = a.user".
				" WHERE a.id=".$id;
	$db->setQuery($query);
	$order = $db->loadObject();
	$error = $db->getErrorMsg();
	if($error){
		JFactory::getApplication()->enqueueMessage('DB Query ERROR!, '.$error, 'ERROR');
	}
	$products = json_decode($order->products);
	$prod_tot = 0;
	$total = 0;
	$shipment = json_decode($order->shipment);
	$product = '';
	foreach($products as $item){
		$thumb = str_replace('_thumb1','_thumb1',$item->thumb);
		$opt = getProductOpt($item->id, 'pr_option', $item->option);
		$subtotal = (int)$item->count * (int)$item->price;
		$prod_tot += $subtotal;
		$product .= '<div class="row gutter-20">
                <div class="col-xs-3">
                  <div class="thumbnail no-margin"><img src="'.JURI::base().$thumb.'"></div>
                </div>
                <div class="col-xs-6"> <a href="#"><strong>'.$item->title.'</strong></a>
                  <div class="size text-w-400">'.$item->option.'</div>
                  <div class="itemcount type-sm-bl">';
                  
		if($item->extend_from) {
      $product .= 'Extend rent order - #'.$item->extend_from.' <br />';
      $product .= 'From : '.JHtml::_('date', $item->deliver).'<br />';
     }else{
      $product .= 'Deliver on : '.JHtml::_('date', $item->deliver).' <br />';
     }							
    $product .= 'Pickup on : '.JHtml::_('date', $item->pickup).'
									</div>
                </div>
                <div class="col-xs-3 text-right">
                  <div class="price">Rp. <span class="subtotal">'.number_format($subtotal, 0, '', '.').'</span></div>
                </div>
                <div class="col-xs-12">
                  <div class="border-bottom border-dark p-t-15 m-b-15"></div>
                </div>
              </div>';
	}
	
	
	if($shipment->pricing > 100){
		$shipment_cost = 'Rp. '.number_format($shipment->pricing, 0, '', '.');
		$grand_total = $prod_tot + ($shipment->pricing);
	}else{
		$shipment_cost = 'Free';
		$grand_total = $prod_tot;
	}
	
	
	$product .= '';
	
	$product .= '<div class="row">
                <div class="col-xs-6 m-b-5 purple">Subtotal</div>
                <div class="col-xs-6 m-b-5 text-right">Rp. <span class="carttotal addDolar">'.number_format($prod_tot, 0, '', '.').'</span></div>
                '.html_entity_decode($shipment->method_desc).'
                <div class="col-xs-12">
                  <div class="border-bottom border-dark p-t-15 m-b-15"></div>
                </div>
              </div>';
							
	if($order->disc_code){
		$discount = $grand_total - $order->value;
		$product .= '<div class="row purple">
                <div class="col-xs-6 m-b-5">Discount</div>
                <div class="col-xs-6 m-b-5 text-right">-Rp. <span class="carttotal addDolar">'.number_format($discount, 0, '', '.').'</span></div></div>';
	}
		
   $product .= '<div class="row purple">
                <div class="col-xs-6">Total</div>
                <div class="col-xs-6 text-right text-extra-large text-w-500">Rp. <span class="cktotal addDolar">'.number_format($order->value, 0, '', '.').'</span> </div>
              </div>';
	
	$result->product = $product;
	$result->order = $order;
	$result->shipment = $shipment;
	return $result;
}
if(JRequest::getVar('order_id')){
	$order_detail = getOrder(JRequest::getVar('order_id'));
}
$user	= JFactory::getUser();
$var = array(
	'{user_name}' => $user->name,
	'{confirm_link}' => JURI::base().'payment-confirmation?id='.base64_encode($order_detail->order->id),
	'{total_price}' => number_format($order_detail->order->value, 0, '', '.')
);
$content = strtr($jSeblod->wysiwyg_text->value, $var);

$shipment = $order_detail->shipment;
?>
<?php if(JRequest::getVar('order_id')){ ?>

<div id="content" class="checkout-view no-padding <?php echo $menu_params->get( 'pageclass_sfx'); ?>">
  <div class="container ">
    <div class="cart_c">
      <div class="row gutter-60">
      	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 p-t-80 p-b-100 white-bg">
          <?php if($user->usertype == 'Super Administrator'){ ?>
          <a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
          <?php } ?>
          <?php echo $content; ?>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 text-w-500 p-t-100 p-b-100">
          <div class="m-b-20">
            <div class="">
              <div class="h5 m-b-10 purple">Delivery Address</div>
              <div class="recap text-w-400">
              	<?php echo $shipment->address->name; ?><br />
                <?php echo $shipment->address->address; ?> <?php echo $shipment->address->address_opt; ?>, 
                <?php echo $shipment->address->district_name; ?>, <?php echo $shipment->address->province; ?> <?php echo $shipment->address->postal; ?><br /><?php echo $shipment->address->country_name; ?><br /><?php echo $shipment->address->phone; ?>
                </div>
                <div class="border-bottom border-dark p-t-15 m-b-15"></div>
            </div>
          </div>
          <div class="ck-cart">
            <div class=""> 
						<div class="h5 m-b-10 purple">Product</div>
						<?php echo $order_detail->product; ?> </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php }else{ ?>
<div id="content" class="<?php echo $menu_params->get( 'pageclass_sfx'); ?>">
  <div class="container textcontent" style="position: relative;">
    <?php if($user->usertype == 'Super Administrator'){ ?>
    <a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
    <?php } ?>
    <?php echo $content; ?> </div>
</div>
<?php } ?>
