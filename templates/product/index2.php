<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Product Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
//echo'<pre>';print_r($this->pr_sizex);die()
?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
$base = JURI::base();
$user = JFactory::getUser();
$available_date = false;
if(JRequest::getVar('debug')){
	//print_r($this->content);
}

$db = JFactory::getDBO();
$query = "SELECT *, CURDATE() as now FROM #__booking WHERE product_id = ".$this->content->id." AND (pickup > CURDATE()) AND book_status IN ('pending','processing','booked') ORDER BY deliver" ;
$db->setQuery($query);
$booking = $db->loadObjectList();

if(count($booking)){
	if(strtotime($booking[0]->deliver) > strtotime($booking[0]->now)){
		$last_loop = $booking[0]->now;
	}else{
		foreach($booking as $book){	
			if($last_loop){
				if(strtotime($last_loop.'+7 days') < strtotime($book->deliver)){
					break;
				}
			}
			$last_loop = $book->pickup;
		}
	}
}

// Convert to timestamp
$end_ts = strtotime($last_loop);
$now_date = strtotime(date('Y-m-d'));
// Check that user date is between start & end

if($now_date <= $end_ts){
	if(date('D', $end_ts) == 'Sat'){
		$available_date = 'Available on <br />'.date('d M Y', strtotime($last_loop.'+2 days'));
	}else if(date('D', $end_ts) == 'Sun'){
		$available_date = 'Available on <br />'.date('d M Y', strtotime($last_loop.'+1 days'));
	}else{
		$available_date = 'Available on <br />'.date('d M Y', strtotime($last_loop.'+1 days'));
	}
}
if(date('D', $now_date) == 'Sat' || date('D', $now_date) == 'Sun'){
	if($available_date == 'Available on <br />'.date('d M Y', strtotime('next Monday'))){
		$available_date = false;
	}
}

	
?>


<div class="col-lg-3 col-md-4 col-sm-6 m-b-30" data-end_date="<?php echo $this->content->pickup; ?>">
  <div class="product type-product" >
			<?php
			if($available_date){ ?>
			<div class="badge padding-5">
				 <?php echo $available_date; ?>
			</div>
     	<?php } ?>
    <div class="product-thumbnail-outer <?php if($available_date){ echo 'unavailable'; } ?>">
      <div class="product-thumbnail-outer-inner"> <a href="<?php echo $this->content->art_link; ?>"> <img src="<?php echo $base.$jSeblod->imagex[0]->thumb3; ?>" alt="<?php echo $jSeblod->pr_title->value; ?>" /></a>
        <div class="addtocart-btn">
          <div class="svg-overlay-shop">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
              <path d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
            </svg>
          </div>
          <a href="<?php echo $this->content->art_link; ?>" class="button add_to_cart_button product_type_external"><i class="fa fa-bars cart-icons-shop" aria-hidden="true"></i> Take a look</a> </div>
      </div>
      <div class="product-content-inner">
        <h3><a href="<?php echo $this->content->art_link; ?>"><?php echo $jSeblod->pr_title->value; ?></a></h3>
        <span class="price"><span class="woocommerce-Price-amount amount"><span class="woocommerce-Price-currencySymbol">Rp</span>&nbsp;<?php echo number_format($jSeblod->pr_price->value, 0, '', '.'); ?></span></span> 
     	</div>
    </div>
  </div>
</div>
<?php $last_loop = ''; ?>
