<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Product Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );

if($this->content->stock > 0){
	$sisa = 'Tersedia : '.$this->content->sisa.' pcs';
}else{
	$sisa = 'Barang Habis';
}
$referringPage = parse_url( $_SERVER['HTTP_REFERER'] );
$user =& JFactory::getUser();
$base = JURI::base();

$db = JFactory::getDBO();
$query = "SELECT * FROM #__booking WHERE product_id = ".
          $this->content->id.
          " AND book_status IN ('pending','processing','booked')".
          " AND deliver > NOW()";
$db->setQuery($query);
$booking = $db->loadObjectList();
$block_date = array();
if(count($booking)){
	foreach($booking as $book){
    
    $end = new DateTime($book->pickup);
    if($user->usertype == 'Super Administrator' || date('D', strtotime($book->pickup)) == 'Thu'){
      $end = $end->modify( '+1 day' );  // default is +1
    }else{
      $end = $end->modify( '+2 day' ); // extra 1 day
    }
  
		$period = new DatePeriod(new DateTime($book->deliver), new DateInterval('P1D'), $end);
		foreach($period as $date) { 
        $block_date[] = $date->format('Y-m-d'); 
    }
	}
}
$block_date[] = date('Y-m-d'); 


if(JRequest::getVar('sizeinfo')){
	echo $jSeblod->pr_size_info->value;
}else{
?>
<script type='text/javascript'>
/* <![CDATA[ */
block_dates = <?php echo json_encode($block_date); ?>;
// console.log(block_dates);
/* ]]> */
</script>

<div id="content">
  <div class="container">
    <div class="pro_main_c" style="position: relative;">
      <?php if($user->usertype == 'Super Administrator'){ ?>
      <a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
      <?php } ?>
      <div class="row">
        <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
          <div class="slider_1 clearfix">
            <div class="clearfix" id="image-block">
              <div id="slider-product" class="flexslider">
                <ul class="slides">
                  <?php for ( $i = 0, $n = sizeof( @$jSeblod->imagex ); $i < $n; $i++ ) { ?>
                  <li><img src="<?php echo $base.@$jSeblod->imagex[$i]->thumb3; ?>" data-zoom-image="<?php echo $base.@$jSeblod->imagex[$i]->value; ?>" /> </li>
                  <?php } ?>
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  <?php for ( $i = 0, $n = sizeof( @$jSeblod->imagex ); $i < $n; $i++ ) { ?>
                  <li id="item-<?php echo $i; ?>">
                    <div class="carousel-item"><img src="<?php echo @$jSeblod->imagex[$i]->thumb1; ?>"/></div>
                  </li>
                  <?php } ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-5 col-sm-6 col-xs-12">
          <div class="desc_blk">
            <div class="itm">
              <div class="lead">
                <h1 class="product_title entry-title m-b-20" style='font-family: "Fredoka One";font-weight: 400;font-style: normal;color: #ed137e;font-size: 22px;'><?php echo $jSeblod->pr_title->value; ?></h1>
                <div class="m-b-30"><?php echo $jSeblod->pr_subtitle->value; ?></div>
                <div id="date-alert" class="alert alert-danger" style="display: none;">
                  <p class="m-b-0"></p>
                </div>
                <div class="row">
                	<div class="col-xs-12">
                    <div class="m-b-30">
                      <h4 class="m-b-15 pink">Pilih Tanggal Booking</h4>
                      <div class="pick-up-date-picker">
                        <input type="text" name="pickup_date" id="start-date" placeholder="Start Date" value="">
                      </div>
                      <div class="legends">
                      	<div class="booked"><span></span> Tidak tersedia</div>
                        <div class="available"><span></span> Tersedia</div>
                        <div class="selected"><span></span> Terpilih</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12">
                    <?php $options = json_decode($this->content->options); ?>
                    <div class="m-b-20">
                      <h4 class="m-b-15 pink">Pilih Durasi Sewa</h4>
                      <table class="table table-bordered option-container">
                        <tbody>
                          <?php $i=0; foreach($options as $opt){ $opt = (object) $opt; $i++; ?>
                          <tr>
                            <td><label class="m-l-20 m-b-0 small">
                                <input type="radio" name="option" value="<?php echo $opt->pr_option; ?>" data-price="<?php echo $opt->pr_opt_price; ?>" data-days="<?php echo $opt->pr_opt_days; ?>" />
                                <?php echo $opt->pr_option; ?> (<?php echo $opt->pr_opt_days; ?> hari) - <span class="woocommerce-Price-amount amount">Rp <?php echo number_format($opt->pr_opt_price, 0, '', '.'); ?></span></label></td>
                          </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                      
                    </div>
                  </div>
                  
                </div>
                <div class="booking-pricing-info m-b-20">
                  <div class="row no-margin">
                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12"><label class="no-margin"> Periode Sewa </label></div>
                    <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12 booking_duration text-right" style="color: #ed2a88;">---</div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 col-xs-12"><label class="no-margin"> Biaya </label></div>
                    <div class="col-sm-6 col-xs-12 booking_cost text-right" style="color: #ed2a88;">---</div>
                  </div>
                </div>
                <div class="clearfix text-right"> <button class="addcart btn btn-lg btn-red rounded autocolor text-white" 
                data-id="<?php echo $this->content->id; ?>"
                data-title="<?php echo $jSeblod->pr_title->value; ?>"
                data-thumb="<?php echo $jSeblod->imagex[0]->thumb1; ?>"
                data-text="Book Now"
                disabled="disabled"
            	>Book Now</button> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tabing m-b-50 p-b-50">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#desc" data-toggle="tab">Product Description</a></li>
          <!--<li><a href="#information" data-toggle="tab">Additional Information</a></li>-->
          
        </ul>
        
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active" id="desc">
          	<h2 class="m-b-30 m-t-30"><span style="font-family: Fredoka One; color: #eb94b7;">Product Description</span></h2>
           	<?php echo $jSeblod->pr_text->value; ?> 
           </div>
          <div class="tab-pane" id="information"> </div>

        </div>
      </div>
      <?php 
    $document = JFactory::getDocument();
    $renderer = $document->loadRenderer('module');
    $module = JModuleHelper::getModule('newsflash_jseblod','Similar Items');
    $module->params = str_replace('catid=0','catid='.$this->content->catid."\r\nexclude=".$this->content->id,$module->params);
    echo $renderer->render($module);
		
		$catmodule = JModuleHelper::getModule('newsflash_jseblod','Home Product Categories');
		//echo $renderer->render($catmodule);
    ?>
    </div>
  </div>
</div>
<?php 
$doc =& JFactory::getDocument();

$metadesc = $jSeblod->pr_meta_desc->value;
if($metadesc){
	$doc->setDescription($metadesc);
	$doc->setMetaData( 'description', $metadesc);
}
$metakey = $jSeblod->tags->value;
if($metakey){
	$doc->setMetaData( 'keywords', $metakey);
}
$doc->setMetaData( 'title',  'Sewa '.$jSeblod->pr_title->value);
$doc->setMetaData( 'title',  'Sewa '.$jSeblod->pr_title->value, 'og');
$doc->setMetaData( 'image',   str_replace(' ', '%20',$base.@$jSeblod->imagex[0]->thumb3), 'og');
$doc->setMetaData( 'author',  'Toiss');

JDocument::addScript($base.'templates/blank_j15/js/jquery.datetimepicker.full.js');


}
?>
