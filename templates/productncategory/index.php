<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Productncategory Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>
<div class="padder">
    <img src="<?php echo $jSeblod->prod_cat_img->value; ?>" />
    <h3><?php echo $this->category_title->value; ?></h3>
    <div class="text">
    	<?php echo $jSeblod->wysiwyg_description->value; ?>
        <br />MORE+
    </div>
</div>