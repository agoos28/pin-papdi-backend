<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			About Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
jimport( 'joomla.filter.filteroutput' );
$user =& JFactory::getUser();
?>
<?php if($user->usertype == 'Super Administrator'){ ?>
<div style="position:relative; height: 50px;">
	<a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
    </div>
<?php } ?>
<div class="hws">
    <div class="row">
        <div style="height:60px;"></div>
        <div class="col-lg-4 col-md-12 col-sm-12 ttl">
            <h3><?php echo $jSeblod->ab_title->value; ?></h3>
            <p><?php echo $jSeblod->ab_subtitle->value; ?></p>
        </div>
        <div class="col-lg-8 col-md-12 col-sm-12 desc">
        <?php echo $jSeblod->ab_intro->value; ?>
        </div>
    </div>
	<hr>
</div>
