<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Contact Info Content Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>

<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
$user =& JFactory::getUser();
?>

<?php if($user->usertype == 'Super Administrator'){ ?>
<div style="position:relative; height: 50px;">
	<a href="<?php echo $this->content->editart_link; ?>" class="btn btn-xs btn-red btn-edit">edit</a>
    </div>
<?php } ?>
<div class="row header">
    <div class="col-lg-3 col-md-4 col-sm-12 ttl">
        <h3><?php echo $jSeblod->ct_title->value; ?></h3>
        <p><?php echo $jSeblod->ct_subtitle->value; ?></p>
    </div>
    <div class="col-lg-6 col-md-4 col-sm-12 ep">
        <p><a href="mailto: <?php echo $jSeblod->ct_email->value; ?>"><?php echo $jSeblod->ct_email->value; ?></a><br /><?php echo $jSeblod->ct_phone->value; ?></p>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-12 addr">
		<div class="ct_i"><span class="fa fa-instagram"> </span> <?php echo $jSeblod->ct_insta->value; ?></div>
		<div class="ct_i"><span class="fa fa-twitter"> </span> <?php echo $jSeblod->ct_twitter->value; ?></div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 wrd" style="text-align: center;">
        <p style="font-size: 16px; font-weight: bold; line-height: 150%; color: #b3b3b3; margin: 50px 0;">
        	<?php echo $jSeblod->ct_text->value; ?>
        </p>
    </div>
</div>
