<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			Product Form Template (Custom) - jSeblod CCK ( Content Construction Kit )
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
/**
 * Init jSeblod Process Object { !Important; !Required; }
 **/
$jSeblod	=	clone $this;
?>
<?php 
/**
 * Init Style Parameters
 **/
include( dirname(__FILE__) . '/params.php' );
?>
<?php echo $jSeblod->product->form; ?>

<div class="row">
  <div class="col-md-9">
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Product <span class="text-bold">Information</span></h4>
        <div class="panel-tools">
          <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i class="fa fa-cog"></i> </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li> <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group">
          <label for="pr_title" class="control-label">Product Title</label>
          <input placeholder="Product Title" class="form-control" id="pr_title" name="pr_title" type="text" value="<?php echo $jSeblod->pr_title->value; ?>">
        </div>
        <div class="form-group">
          <label class="control-label" style="margin-bottom: 12px;">Product Display</label>
          <div class="row">
            <div class="col-md-4"><?php echo $jSeblod->pr_pick->form; ?></div>
            <div class="col-md-4"><?php echo $jSeblod->pr_special->form; ?></div>
          </div>
        </div>
        <div class="form-group">
          <label for="pr_subtitle" class="control-label"><?php echo $jSeblod->pr_subtitle->label; ?></label>
          <?php echo $jSeblod->pr_subtitle->form; ?>
        </div>
        
        <div class="form-group">
          <label for="pr_text" class="control-label">Product Details</label>
          <?php echo $jSeblod->pr_text->form; ?>
        </div>
      </div>
    </div>
    <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Product <span class="text-bold">Options</span></h4>
        <div class="panel-tools">
          <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i class="fa fa-cog"></i> </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li> <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group">
            <label for="pr_text" class="control-label">Option Label</label>
            <?php echo $jSeblod->pr_opt_label->form; ?>
        </div>
      <div id="prod_variation" class="dd">
    		<ol class="dd-list">
      <?php 
			$col = explode('-',$jSeblod->pr_options['group']->content);
			
			for($i=0; $i < count($jSeblod->pr_options)-1; $i++){ $items = $jSeblod->pr_options[$i]; ?>
      	<li class="dd-item dd3-item" data-id="<?php echo $i+1; ?>">
        	<div class="dd-handle dd3-handle"></div>
					<div class="dd3-content">
          	<div class="row">
            <div class="col-md-11">
            	<div class="row gutter-10">
      	<?php if($count = count($items)){ $ii=0; foreach($items as $item){ ?>
      				<div class="col-md-<?php echo $col[$ii]; ?>">
              <div class="form-group">
              <label class="text-small"><?php echo $item->label; ?></label><?php echo $item->form; ?>
              </div>
              </div>
					<?php $ii++; } ?>
          			</div>
          		</div>
							<div class="col-md-1"><a href="#" class="btn btn-xs btn-red tooltips del" style="margin-top: 6px;" data-delete-id="61" data-placement="top" data-original-title="Delete"><i class="fa fa-times fa fa-white"></i></a></div>
				<?php } ?>
        		</div>
        	</div>
        </li>
			<?php } ?>
        </ol>
      </div>
    </div>
    <div class="panel-footer">
    	<a class="btn btn-primary additem" href="#"><i class="fa fa-plus"></i> Add product option</a>
    </div>
  </div>
  <div class="panel panel-white">
      <div class="panel-heading">
        <h4 class="panel-title">Product <span class="text-bold">Images</span></h4>
        <div class="panel-tools">
          <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i class="fa fa-cog"></i> </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li> <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel-body">
      <div id="prod_images" class="dd">
    		<ol class="dd-list">
					<?php for($i=0; $i < count($jSeblod->imagex); $i++){ $item = $jSeblod->imagex[$i]?>
          <li class="dd-item dd3-item" data-id="<?php echo $i+1; ?>">
            <div class="dd-handle dd3-handle"></div>
            <div class="dd3-content">
                <?php print_r($item->form); ?>
                <a href="#" class="btn btn-xs btn-red tooltips del" data-delete-id="61" data-placement="top" data-original-title="Delete"><i class="fa fa-times fa fa-white"></i></a>
              </div>
          </li>
          <?php } ?>
        </ol>
      </div>
    </div>
    <div class="panel-footer">
    	<a class="btn btn-primary additem" href="#"><i class="fa fa-plus"></i> Add more image</a>
    </div>
  </div>
  </div>
  <div class="col-md-3">
  	<div class="row" style="margin-bottom: 15px;">
    <div class="col-md-6" style="padding-right: 5px;"><button class="btn btn-light-grey btn-lg btn-block" onclick="history.go(-1);return false;" name="cancel" style="">Cancel</button></div>
    	<div class="col-md-6"  style="padding-left: 5px;"><button class="btn btn-purple btn-lg btn-block" type="submit" onclick="javascript: submitbutton('save');" name="button_submit" style="">Save It !</button></div>
    </div>
    <div class="panel panel-blue">
      <div class="panel-heading">
        <h4 class="panel-title">Product <span class="text-bold">Management</span></h4>
        <div class="panel-tools">
          <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i class="fa fa-cog"></i> </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li> <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group">
              <label for="pr_price" class="control-label">Active Price</label>
              <div class="input-group"> <span class="input-group-addon">IDR</span> <?php echo $jSeblod->pr_price->form; ?> </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="form-group">
              <label for="pr_price" class="control-label">Old Price</label>
              <div class="input-group"> <span class="input-group-addon">IDR</span> <?php echo $jSeblod->pr_old_price->form; ?> </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="pr_sku" class="control-label">Product Code</label>
          <?php echo $jSeblod->pr_sku->form; ?> </div>
        <div class="form-group">
          <label for="pr_save" class="control-label">Product Category</label>
          <?php echo $jSeblod->pr_save->form; ?> </div>
        <div class="form-group">
          <label for="pr_save" class="control-label">Product Group</label>
          <?php echo $jSeblod->pr_group->form; ?> </div>
        <div class="form-group">
        	<label for="pr_meta_desc" class="control-label">Meta Description</label>
        	<?php echo $jSeblod->pr_meta_desc->form; ?>
        </div>
        <div>
          <label>Product Tags separated by (,)</label>
          <input id="tags" type="text" name="tags" class="tags" value="<?php echo $jSeblod->tags->value; ?>">
        </div>
      </div>
    </div>
    <div class="panel panel-blue">
      <div class="panel-heading">
        <h4 class="panel-title">Shipping <span class="text-bold">Options</span></h4>
        <div class="panel-tools">
          <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i class="fa fa-cog"></i> </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li> <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="checkbox"> <?php echo $jSeblod->pr_shipping->form; ?> </div>
        <div class="form-group">
          <label for="pr_weight" class="control-label">Product Weight (Kg)</label>
          <?php echo $jSeblod->pr_weight->form; ?> </div>
      </div>
    </div>
    <div class="panel panel-blue">
      <div class="panel-heading">
        <h4 class="panel-title">Other <span class="text-bold">Setting</span></h4>
        <div class="panel-tools">
          <div class="dropdown"> <a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i class="fa fa-cog"></i> </a>
            <ul class="dropdown-menu dropdown-light pull-right" role="menu">
              <li> <a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="form-group">
        	<label for="color_text" class="control-label">Text Color</label>
        	<?php echo $jSeblod->color_text->form; ?>
        </div>
        <div class="form-group">
        	<label for="color_bg" class="control-label">Backgound Color</label>
        	<?php echo $jSeblod->color_bg->form; ?>
        </div>
        <div class="form-group">
        	<label for="color_btn" class="control-label">Button Color</label>
        	<?php echo $jSeblod->color_btn->form; ?>
        </div>
      </div>
    </div>
    <div class="row" style="margin-bottom: 15px;">
    <div class="col-md-6" style="padding-right: 5px;"><button class="btn btn-light-grey btn-lg btn-block" onclick="history.go(-1); return false;" name="cancel" style="">Cancel</button></div>
    	<div class="col-md-6"  style="padding-left: 5px;"><button class="btn btn-purple btn-lg btn-block" type="submit" onclick="javascript: submitbutton('save');" name="button_submit" style="">Save It !</button></div>
    </div>
  </div>
</div>
<input type="hidden" name="my_readmore" value="1" />
<script>
$(document).ready(function(e) {
	var imgOpt
	$('.dd').on('click', '.del', function(e){
		if($(this).parentsUntil('.dd-list').parent().find('.dd-item').length > 1){
			$(this).parentsUntil('.dd-item').parent().remove()
		}
		e.preventDefault()
	})
	$('.additem').click(function(e){
		e.preventDefault()
		var panel = $(this).parent().parent()
		var newItem = panel.find('.dd-list li:first').clone(true,true)
		var length = panel.find('.dd-list li').length
		newItem = newItem.prop('outerHTML').replace(/\[[0-9]+?\]/g,'['+length+']').replace(/\-[0-9]+?\-/g,'-'+length+'-')
		panel.find('ol').append(newItem)
		var newItem = panel.find('.dd-list li:last')
		newItem.find('.fileupload').removeClass('fileupload-exists').addClass('fileupload-new')
		newItem.find('.fileupload-preview').empty()
		newItem.find('img').attr('src', '')
	})
	$('.colorpicker-component').colorpicker({
		customClass: 'colorpicker-2x',
		format: 'hex'
	})
	$('#prod_variation').nestable({
		maxDepth: 1
	})
	$('#prod_images').nestable({
		maxDepth: 1,
		enableHMove: false
	})
  $('#tags').tagsInput({
			width: 'auto'
		});
	$('#prod_variation').on('click','input[style="display:inherit"]',function(e){
		e.preventDefault()
		imgOpt = $(this)
		$('#iframemodal').modal()
	})
	$('#iframemodal').find('iframe').load(function(){
		var th = $(this)
		th.css({
			width: '100%',
			height: th.contents().height() + 20
		})
		th.contents().find('button.insert').attr('onclick','')
		th.contents().find('button.insert').click(function(e){
				var idtarget = th.contents().find('button.insert').attr('rel')
				var value = th.contents().find('input#f_url').val()
				imgOpt.val(value)
				setTimeout(function(){
					$('#iframemodal').modal('hide')
				}, 100)
				e.preventDefault()
			})
	})
});
</script> 
<style>
#prod_images .del{
	position: absolute;
	top: 10px;
	right: 10px;
}
#prod_images .dd-item{
	margin-bottom: 5px;
}
#prod_images .dd3-content{
	margin: 0;
}
</style>

<div class="modal fade" id="iframemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        <iframe src="index.php?option=com_media&view=images&layout=default&tmpl=component" frameborder="0" scrolling="no"></iframe>
      </div>
      
    </div>
  </div>
</div>