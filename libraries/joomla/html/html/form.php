<?php
/**
 * @version    $Id: form.php 14401 2010-01-26 14:10:00Z louis $
 * @package    Joomla.Framework
 * @subpackage  HTML
 * @copyright  Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license    GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('JPATH_BASE') or die();

/**
 * Utility class for form elements
 *
 * @static
 * @package  Joomla.Framework
 * @subpackage  HTML
 * @version    1.5
 */
class JHTMLForm
{

	/**
	 * Displays a hidden token field to reduce the risk of CSRF exploits
	 *
	 * Use in conjuction with JRequest::checkToken
	 *
	 * @static
	 * @return  void
	 * @since  1.5
	 */
	function token()
	{
		return '<input type="hidden" name="' . JUtility::getToken() . '" value="1" />';
	}

	function renderProps($props = []){
		$elementProps = '';
		foreach ($props as $key => $prop) {
			$elementProps .= $key . '="' . $prop . '" ';
		}
		return $elementProps;
	}

	function label($text ='', $for='', $class = '')
	{
		return '<label class="' . $class . '" for="' . $for . '">' . $text . '</label>';
	}

	function text($name, $value, $class = '', $props = [])
	{

		$value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES), ENT_QUOTES);
		return '<input name="' . $name . '"  value="' . $value . '" type="text" class="form-control ' . $class . '" '.$this->renderProps($props).'/>';
	}

	function password($name, $value, $class = '', $props = [])
	{
		$value = htmlspecialchars(html_entity_decode($value, ENT_QUOTES), ENT_QUOTES);
		return '<input name="' . $name . '" value="' . $value . '" id="' . $name . '" type="password" class="form-control ' . $class . '" '.
			$this->renderProps($props).'/>';
	}

	function formGroup($element = 'text', $name = '', $value='',  $label = '', $props = [] )
	{
		$html = '<div class="form-group">';
		$html .= $this->label($label, $name);
		$html .= $this->$element($name, $value, $class = '', $props = []);
		$html .= '</div>';
		return $html;
	}


}