<?php
/* Copyright (C) 2012 SEBLOD. All Rights Reserved. */

// No Direct Access
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');

$model              = $this->getModel();
$acceptedStatus     = array(200, 201, 202);
$instantPaymentType = array('bca_klikpay', 'akulaku', 'cimb_clicks', 'danamon_online', 'bri_epay');
$contentId          = array(
	//transfer payment
	'bca'            => 780,
	'mandiri'        => 781,
	'bni'            => 782,
	'permata'        => 783,
	//instant payment
	'bca_klikpay'    => 780,
	'akulaku'        => 781,
	'cimb_clicks'    => 782,
	'danamon_online' => 783,
	'bri_epay'       => 783
);
$statusLabels = array(
	10 => 'Unpaid',
	12 => '<strong style="color: #4ea549">Paid</strong>',
	13 => '<strong style="color: #f59065">Pending</strong>',
	14 => 'expired'
);

$statusCommitment = array(
	10 => 'Unpaid',
	12 => 'Disetujui',
	13 => 'Pending',
	14 => 'expired'
);
?>
<?php if (JRequest::getVar('invoice'))
{

	$user       = JFactory::getUser();
	$ts         = $model->getTransaction(JRequest::getVar('invoice'));
	$this->data = $ts;

	//echo '<pre>';print_r($ts);echo '</pre>';


	?>
	<style>
		.d-flex {
			display: flex;
		}

		.justify-content-between {
			justify-content: space-between;
		}
	</style>
	<div class="row">
		<div class="col-md-8">

			<?php if ($this->data) { ?>
				<div class="list-group m-b-15">
					<div class="list-group-item d-flex justify-content-between">
						<h5 class="no-margin">
							Transaction Detail
						</h5>
					</div>
					<div class="d-flex list-group-item justify-content-between">
						<div>
							<strong>ID Transaksi</strong>
						</div>
						<div>
							<?php echo $this->data->id; ?>
						</div>
					</div>
					<div class="d-flex list-group-item justify-content-between">
						<div>
							<strong>Nama</strong>
						</div>
						<div>
							<strong><?php echo $this->data->name; ?></strong>
						</div>
					</div>
					<div class="d-flex list-group-item justify-content-between">
						<div>
							<strong>Tanggal</strong>
						</div>
						<div>
							<?php echo $this->data->created_date; ?>
						</div>
					</div>
					<div class="d-flex list-group-item justify-content-between">
						<div>
							<strong>Status</strong>
						</div>
						<div>
							<?php echo $this->data->status_name; ?>
						</div>
					</div>
					<div class="d-flex list-group-item justify-content-between">
						<div>
							<strong>Total</strong>
						</div>
						<div>
							Rp. <?php echo number_format($this->data->value); ?>
						</div>
					</div>
				</div>
				<?php
				$acceptedStatus = array(200, 201, 202);
				$payment        = $this->data->ext_response;
				if (in_array($payment->status_code, $acceptedStatus))
				{ ?>

					<div class="list-group m-b-15">
						<div class="list-group-item d-flex justify-content-between">
							<h5 class="no-margin">
								Payment Detail
							</h5>
						</div>
						<div class="d-flex list-group-item justify-content-between">
							<div>
								<strong>Status Pembayaran</strong>
							</div>
							<div style="text-transform: capitalize;">
								<?php echo $payment->transaction_status; ?>
							</div>
						</div>


						<?php if ($payment->payment_type === 'bank_transfer' || $payment->payment_type === 'echannel')
						{
							if ($payment->va_numbers)
							{
								$bank = $payment->va_numbers[0]->bank;
								$va   = $payment->va_numbers[0]->va_number;
							}
							if ($payment->bill_key)
							{
								$bank   = 'mandiri';
								$biller = $payment->biller_code;
								$va     = $payment->bill_key;
							}
							if ($payment->permata_va_number)
							{
								$bank = 'permata';
								$va   = $payment->permata_va_number;
							}
							?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Metode Bayar</strong>
								</div>
								<div style="text-transform: uppercase;">
									Transfer Bank <?php echo $bank; ?>
								</div>
							</div>
							<?php if ($biller) { ?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Kode Perusahaan</strong>
								</div>
								<div style="letter-spacing: 1px;">
									<strong><?php echo $biller; ?></strong>
								</div>
							</div>
						<?php } ?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Kode Pembayaran</strong>
								</div>
								<div style="letter-spacing: 1px;">
									<strong><?php echo $va; ?></strong>
								</div>
							</div>
						<?php } ?>

						<?php if ($payment->payment_type === 'cstore') { ?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Metode Bayar</strong>
								</div>
								<div style="text-transform: uppercase;">
									<?php echo $payment->store; ?>
								</div>
							</div>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Kode Pembayaran</strong>
								</div>
								<div style="letter-spacing: 1px;">
									<strong><?php echo $payment->payment_code; ?></strong>
								</div>
							</div>
						<?php } ?>
						<?php
						if (in_array($this->data->midtrans->payment_type, $instantPaymentType))
						{
							?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Metode Bayar</strong>
								</div>
								<div style="text-transform: uppercase;">
									<?php echo str_replace('_', ' ', $this->data->midtrans->payment_type); ?>
								</div>
							</div>
							<?php if ($this->data->payment_status == 12) { ?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Tanggal Pembayaran</strong>
								</div>
								<div style="text-transform: uppercase;">
									<?php echo $payment->transaction_time; ?>
								</div>
							</div>
						<?php } ?>
						<?php } ?>
						<?php if ($payment->payment_type === 'gopay') { ?>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Metode Bayar</strong>
								</div>
								<div style="text-transform: uppercase;">
									GOPAY
								</div>
							</div>
						<?php } ?>
						<div class="d-flex list-group-item justify-content-between">
							<div>
								<strong>Jumlah Bayar</strong>
							</div>
							<div>
								Rp. <?php echo number_format($payment->gross_amount); ?>
							</div>
						</div>
					</div>
				<?php } ?>
				<?php if ($this->data->method === 'commitment') { ?>

					<div class="list-group m-b-15">
						<div class="list-group-item d-flex justify-content-between">
							<h5 class="no-margin">
								<?php echo JText::_('MY_TRANSACTION_PAYMENT'); ?>
							</h5>
						</div>
						<div class="d-flex list-group-item justify-content-between">
							<div>
								<strong>Status</strong>
							</div>
							<div style="text-transform: capitalize;">
								<?php echo $statusCommitment[$this->data->status]; ?>
							</div>
						</div>
						<div class="d-flex list-group-item justify-content-between">
							<div>
								<strong>Metode Bayar</strong>
							</div>
							<div style="text-transform: uppercase;">
								Surat Komitmen
							</div>
						</div>
						<div class="d-flex list-group-item justify-content-between">
							<div>
								<strong>Files</strong>
							</div>
							<div style="text-align: right;">
								<?php
								if (count($this->data->commitment_files))
								{
									foreach ($this->data->commitment_files as $file)
									{
										echo '<a target="_blank" href="' . $file->url . '">' . $file->filename . '</a><br/>';
									}
								}
								else
								{
									echo '--';
								}
								?>
							</div>
						</div>
					</div>

				<?php } ?>

				<div class="list-group m-b-15">
					<div class="list-group-item">
						<h5 class="no-margin"><?php echo JText::_('ORDER_DETAIL'); ?></h5>
					</div>
					<div class="list-group-item">
						<?php
						if ($this->data->cart_data)
						{ ?>
							<?php foreach ($this->data->cart_data as $item) { ?>
							<div class="m-b-15">
								<div class="form-group-attached">
									<div class="form-group form-group-default d-flex justify-content-between">
										<div>
											<div><strong><?php echo $item->package->title; ?></strong></div>
											<div><?php echo $item->package->description; ?></div>
										</div>
										<div class="padding-5">
											<strong>Rp. <?php echo number_format($item->price); ?></strong>
										</div>
									</div>
									<div class="row clearfix">
										<div class="col-sm-8">
											<div class="form-group form-group-default">
												<label>Nama</label>
												<div><?php echo $item->name; ?></div>
											</div>
										</div>
										<div class="col-sm-4">
											<div class="form-group form-group-default">
												<label>Keanggotaan</label>
												<div><?php echo $item->npa ? $item->package_member : 'non-anggota'; ?></div>
											</div>
										</div>
									</div>
									<div class="form-group form-group-default">
										<label>Email</label>
										<div><?php echo $item->email; ?></div>
									</div>
									<div class="form-group form-group-default">
										<label>Bidang</label>
										<div><?php echo $item->bidang; ?></div>
									</div>
									<div class="form-group form-group-default">
										<label>Pilihan Workshop</label>
										<div class="size text-small">
											<ul class="opt-desc p-l-15 m-b-5">
												<?php foreach ($item->workshops as $workshop) { ?>
													<li><strong><?php echo $workshop->timeline; ?></strong> -
														<?php echo $workshop->code . ' ' . $workshop->name; ?></li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
						<?php } ?>
					</div>
				</div>
			<?php } else { ?>
				<div class="alert alert-danger text-center">
					<h5 class="pink">Transaction not found!</h5>
				</div>
			<?php } ?>
		</div>
		<div class="col-md-4">
			<div class="panel panel-white">
				<div class="panel-heading">
					<h4 class="panel-title">Status : <span
							class="text-bold"> <?php echo $model->statuslabel($ts->status); ?> </span></h4>
					<div class="panel-tools">
						<div class="dropdown"><a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey"> <i
									class="fa fa-cog"></i> </a>
							<ul class="dropdown-menu dropdown-light pull-right" role="menu">
								<li><a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span> </a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel-body">
					<form method="post" action="<?php echo JURI::current(); ?>">
						<div class="form-group">
							<label for="status">Change Status</label>
							<select class="form-control" name="status">
								<?php
								$statusOptions = array(
									13 => 'Unpaid/Pending',
									12 => 'Dibayar/Disetujui',
									15 => 'Ditolak',
									14 => 'Expired'
								);
								foreach ($statusOptions as $key => $value)
								{
									if ($ts->status == $key)
									{
										echo '<option value="' . $key . '" selected="selected">' . $value . '</option>';
									}
									else
									{
										echo '<option value="' . $key . '">' . $value . '</option>';
									}

								}
								?>
							</select>
						</div>
						<div class="form-group">
							<label>Admin message (optional)</label>
							<textarea name="admin_message" placeholder="Message will show on body email (see email template)"
												class="form-control"></textarea>
						</div>
						<div class="form-group">
							<input type="checkbox" name="sendmail" value="1" id="sendmail"/>
							<label for="sendmail">Send Notification Email To User</label>
						</div>
						<div class="form-group">
							<input type="hidden" name="task" value="transactionStatus"/>
							<input type="hidden" name="return" value="<?php echo JURI::current(); ?>?invoice=<?php echo $ts->id; ?>"/>
							<input type="hidden" name="id" value="<?php echo $ts->id; ?>"/>
							<button class="btn btn-primary" type="submit">Update Status</button>
						</div>
					</form>
				</div>
			</div>

			<a class="btn btn-block btn-lg btn-red text-small m-b-15" target="_blank"
				 href="<?php echo JURI::base(); ?>transactions?layout=transaction&id=<?php echo $ts->id; ?>&format=pdf"
				 style="display: inline-block; margin-bottom: 18px"><i class="fa fa-file-pdf-o"></i> Download PDF</a>


		</div>
	</div>
<?php } else
{
$listing = $model->transaction;
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-white">
			<div class="panel-heading border-light">
				<h4 class="panel-title text-bold">Orders</h4>
				<form id="searchform" method="post" action="<?php echo JURI::current(); ?>">
					<ul class="panel-heading-tabs border-light" style="list-style: none;right: 60px;">
						<li>
							<div class="pull-right" style="margin-top: 8px; width: 250px">
								<div class="input-group">
									<span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
									<input
										name="date"
										type="text"
										class="form-control date-range active"
										placeholder="Tanggal"
										value="<?php echo JRequest::getVar('date'); ?>"
									>
								</div>
							</div>
						</li>
						<li>
							<div class="pull-right" style="margin-top: 8px;">
								<div class="input-group">
									<input
										name="name"
										type="text"
										placeholder="Nama Peserta/customer"
										class="form-control"
										style="width: 250px"
										value="<?php echo JRequest::getVar('name'); ?>"
									>
								</div>
						</li>
						<li>
							<div class="pull-right">
								<button type="submit" class="btn btn-primary" >
									Search
								</button>
							</div>
						</li>
					</ul>
				</form>
				<div class="dropdown" style="position: absolute;right: 16px;top: 9px;">
					<a data-toggle="dropdown" class="btn btn-primary" href="#"><i
							class="fa fa-download fa fa-white"></i></a>
					<form>
						<ul class="dropdown-menu dropdown-light pull-right" role="menu" style="list-style: none">
							<li>
								<a>
									<button name="data_type" value="master" type="submit" class="btn btn-link"
													style="width: 100%;text-align: left; text-decoration: none">
										Master Data
									</button>
								</a>
							</li>
						</ul>
						<input type="hidden" name="task" value="downloadReport"/>
					</form>
				</div>
				<script>
					jQuery(document).ready(function (e) {
						jQuery('.date-range').daterangepicker();
						jQuery('.reset').click(function () {
							jQuery('#searchform').find('input').val('')
							jQuery('#searchform').submit()
						})
					});
				</script>
			</div>
			<div class="panel-body">
				<table class="table table-striped table-hover">
					<thead>
					<tr>
						<th width="5%" class="sectiontableheader">ID</th>
						<th width="40%" class="sectiontableheader">Customer</th>
						<th width="10%" class="sectiontableheader" style="text-align: right">Harga</th>
						<th width="10%" class="sectiontableheader" style="text-align: left">Metode Bayar</th>
						<th width="10%" class="sectiontableheader" style="text-align: left">Status</th>
						<th width="25%" class="sectiontableheader" style="text-align: right">Tanggal</th>
						<th width="5%" class="sectiontableheader" style="text-align: right">Action</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$base = JURI::base();
					$i    = 0;
					foreach ($this->data as $item)
					{
						$i++;

						?>
						<tr class="sectiontableentry<?php echo ($i % 2 == 0) ? 2 : 1; ?>">
							<td><?php echo $item->id; ?></td>
							<td><a
									href="<?php echo $base; ?>administration/users/registered-user?view=user&layout=customform&userid=<?php echo $item->user_id; ?>"><i
										class="fa fa-user" style="margin-right: 8px"></i> </a>
								<a href="<?php echo JURI::base() ?>administration/store/invoices?id=<?php echo $item->id; ?>"><?php echo strtoupper($item->name); ?></a></td>
							<td style="text-align: right"><?php echo number_format($item->value, 0, '', '.') ?></td>
							<td style="text-align: left"><?php echo $item->method; ?></td>
							<td style="text-align: left"><?php echo $statusLabels[$item->status]; ?></td>
							<td style="text-align: right"><?php echo JHtml::_('date', $item->created_date, '%a, %d %B %Y %H:%M'); ?></td>
							<td style="text-align: right"><a class="btn btn-xs btn-primary"
										 href="<?php echo JURI::base() ?>administration/store/invoices?id=<?php echo $item->id; ?>">View</a>
							</td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
				<?php echo $this->pageNav->getPagesLinks(); ?>
			</div>
		</div>
	</div>
	<?php } ?>
		