<?php
/* Copyright (C) 2012 SEBLOD. All Rights Reserved. */

// No Direct Access
defined('_JEXEC') or die('Restricted access');


$baseUrl = JURI::base();
$jSeblod = clone $this;
$user = JFactory::getUser();
$model = $this->getModel();
$cart = $model->getCart();
$session =& JFactory::getSession();
$discount = $session->get('discount');
$discount_type = $session->get('discount_type');
$count = 0;
$total = 0;
if ($cart) {
	foreach ($cart as $item) {
		$item->options = json_decode($item->option);

		$total += (int)$item->price;
		$count += (int)$item->count;
	}
	$cart = array_reverse($cart);
}
// echo '<pre>';print_r($user);echo '</pre>';
?>


<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- START CONTAINER FLUID -->
	<div class="container ">
		<div id="rootwizard" class="m-t-50">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator nav-stack-sm">
				<li class="relative active">
					<a data-toggle="tab" href="#tab1"><i class="fa fa-shopping-cart tab-icon"></i> <span>Pesanan Anda</span></a>
				</li>
				<li class="relative">
					<a data-toggle="tab" href="#tab2"><i class="fa fa-user tab-icon"></i> <span>Data Pengguna</span></a>
				</li>
				<li class="relative">
					<a data-toggle="tab" href="#tab3"><i class="fa fa-credit-card tab-icon"></i> <span>Metode Bayar</span></a>
				</li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane padding-20 active slide-left" id="tab1">
					<div class="row row-same-height">
						<div class="col-md-5 b-r b-dashed b-grey sm-b-b">
							<div class="padding-30 m-t-50">
								<h2>Pesanan siap untuk diproses</h2>
							</div>
						</div>
						<div class="col-md-7">
							<div class="padding-md-30 padding-xs-0">
								<form method="post" action="<?php echo JURI::base(); ?>checkout" id="cart">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 prodright">
											<h3 class="text-extra-large uppercase text-w-400"><?php echo JText::_('SHOPPING_CART'); ?></h3>
											<div class="cart_tbl checkout_tbl m-b-60">
												<div class="clearfix cart_row con_row p-l-15 p-r-15">
													<?php if ($cart) {
														//print_r($cart);
														?>
														<?php foreach ($cart as $item) {
															$item->event = $model->getEvent($item->event_id);
															?>
															<div id="<?php echo $item->id ?>" class="row border-top">
																<div class="col-sm-7 col-xs-7 p-t-20 p-b-10">
																	<h5 class="font-arial m-b-5 m-t-0" style="line-height: 22px;"><?php echo $item->name ?></h5>
																	<p><a href="<?php echo $item->event->link; ?>"><?php echo $item->event->title ?></a>
																		<br/> <?php echo $item->package->title ?> - <?php echo $item->package->description ?></p>
																</div>
																<div class="col-sm-3 col-xs-3 p-t-20 p-b-10 text-center">
																	<div class="font-arial price text-bold">Rp. <span
																			class="subtotal"><?php echo number_format(($item->price), 0, '', '.') ?> </span>
																	</div>
																</div>
																<div class="col-sm-2 col-xs-2 p-t-20 p-b-10 text-right">
																	<a href="#" class="del_btn delbag m-r-5" data-id="<?php echo $item->id ?>">Hapus</a>
																</div>
															</div>
														<?php } ?>
													<?php } else { ?>
														<h5 class="emptyCart text-center padding-20">Keranjang Kosong</h5>
													<?php } ?>
												</div>
											</div>
										</div>
										<input type="hidden" name="updatecart" value="1"/>

									</div>
								</form>

								<div class="container-sm-height">
									<div class="row row-sm-height b-a b-grey">
										<div class="col-sm-3 col-sm-height col-middle p-l-10 sm-padding-15">
											<h5 class="font-montserrat all-caps small no-margin hint-text bold">total</h5>
										</div>
										<div class="col-sm-7 col-sm-height col-middle sm-padding-15 ">
										</div>
										<div class="col-sm-2 text-right bg-info col-sm-height col-middle padding-10">
											<h4 class="no-margin text-white">Rp. <span
													class="alltot"><?php echo number_format($total, 0, '', '.') ?></span></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane slide-left padding-20" id="tab2">
					<div class="row row-same-height">
						<div class="col-md-6 b-r b-dashed b-grey ">
							<div class="padding-md-30 padding-xs-0">
								<h2>Informasi Pemesan</h2>
								<form id="userForm" role="form">
									<div class="form-group-attached">
										<div class="form-group form-group-default" style="background: #f1f1f1;">
											<label>Nama dan Alamat Email</label>
										</div>
										<div class="row clearfix">
											<div class="col-sm-12">
												<div class="form-group form-group-default required">
													<label>Nama Lengkap</label>
													<p><?php echo $user->name; ?></p>
												</div>
											</div>
										</div>
										<div class="form-group form-group-default required">
											<label>Email</label>
											<p><?php echo $user->email; ?></p>
										</div>
										<div class="form-group form-group-default required">
											<label>No. Telpon</label>
											<input type="text" class="form-control" name="phone" value="<?php echo $user->phone; ?>" required>
										</div>
									</div>
									<br>
									<?php if($cart[0]->as == 'sponsor'){ ?>
									<p>Organisasi</p>
									<div class="form-group-attached">
										<div class="row clearfix">
											<div class="col-sm-12">
												<div class="form-group form-group-default">
													<label>Nama Organisasi</label>
													<input id="org_name" name="org_name" type="text" class="form-control" value="<?php echo $user->org_name; ?>" required>
												</div>
											</div>
										</div>
										<div class="form-group form-group-default">
											<label>Alamat Organisasi</label>
											<input name="org_address" type="text" class="form-control" value="<?php echo $user->org_address; ?>" required>
										</div>
									</div>
									<br>
									<?php } ?>
									<div class="form-group-attached">
										<div class="form-group form-group-default" style="background: #f1f1f1;">
											<label>Alamat Penagihan</label>
										</div>
										<div class="form-group form-group-default">
											<label>Alamat</label>
											<input type="text" class="form-control" name="address" value="<?php echo $user->address; ?>">
										</div>
										<div class="row clearfix">
											<div class="col-sm-6">
												<div class="form-group form-group-default">
													<label>Negara</label>
													<input type="text" class="form-control" name="country" value="<?php echo $user->country ? $user->country : 'Indonesia'; ?>">
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group form-group-default">
													<label>Kota</label>
													<input type="text" class="form-control" name="city" value="<?php echo $user->city; ?>">
												</div>
											</div>
										</div>
										<div class="row clearfix">
											<div class="col-sm-9">
												<div class="form-group form-group-default">
													<label>Propinsi</label>
													<input type="text" class="form-control" name="province" value="<?php echo $user->province; ?>">
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group form-group-default">
													<label>Kode Pos</label>
													<input type="text" class="form-control" name="postal" value="<?php echo $user->postal; ?>">
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-6">
							<div class="padding-md-30 padding-xs-0">
								<h2>Informasi Peserta</h2>
								<form role="form">
									<?php if ($cart) { $i = 0;?>
										<?php foreach ($cart as $item) { ?>
											<!--<pre><?php /*print_r($item); */?></pre>-->
											<div class="form-group-attached m-b-15">
												<div class="form-group form-group-default" style="background: #f1f1f1;">
													<label>Peserta ke <?php echo ++$i; ?></label>
												</div>
												<div class="row clearfix">
													<div class="col-sm-8">
														<div class="form-group form-group-default">
															<label>Nama</label>
															<div><?php echo $item->name; ?></div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="form-group form-group-default">
															<label>Keanggotaan</label>
															<div><?php echo $item->npa ? $item->package_member : 'non-anggota'; ?></div>
														</div>
													</div>
												</div>
												<div class="form-group form-group-default">
													<label>Email</label>
													<div><?php echo $item->email; ?></div>
												</div>
												<div class="form-group form-group-default">
													<label>Bidang</label>
													<div><?php echo $item->bidang; ?></div>
												</div>
												<div class="form-group form-group-default">
													<label>Pilihan Workshop</label>
													<div class="size text-small">
														<ul class="opt-desc p-l-15 m-b-5">
															<?php foreach ($item->workshops as $workshop) { ?>
																<li><strong><?php echo $workshop->timeline; ?></strong> -
																	<?php echo $workshop->code.' '.$workshop->name; ?></li>
															<?php	} ?>
														</ul>
													</div>
												</div>
											</div>
										<?php } ?>
									<?php } else { ?>
										<div class="ckinfo padding-50 text-center border-dark border-bottom">
											<h3>Cart Empty!</h3>
										</div>
									<?php } ?>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane slide-left padding-20" id="tab3">
					<div class="row row-same-height">
						<div class="col-md-5 b-r b-dashed b-grey ">
							<div class="padding-md-30 padding-xs-0">
								<h2>Metode Pembayaran</h2>
								<p>Pilih metode pembayaran</p>
								<div class="list-view boreded m-b-15">
									<ul class="list-view-group-container">
										<li class="p-l-0">
											<a class="paymentSelection block padding-10 relative checked" data-type="midtrans">
												<strong>Pembayaran Online</strong><br/>
												Verifikasi pembayaran otomatis.
												<div class="selected-payment"><i class="fa fa-check"></i></div>
											</a>
										</li>
										<?php
										if($cart[0]->as == 'sponsor'){ ?>
										<li class="p-l-0">
											<a class="paymentSelection block padding-10 relative" data-type="commitment">
												<strong>Surat Komitmen</strong><br/>
												Verifikasi dengan mengirim surat komitmen.<br/>
												Maksimal 20 orang peserta
												<div class="selected-payment"><i class="fa fa-check"></i></div>
											</a>
										</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<p>mohon dapat melakukan pembayaran dalam 1x 24 jam setelah pemesanan. Bila pembayaran melewati 1 x 24 jam, maka pemesanan akan dibatalkan secara otomatis, dan peserta harus melakukan registrasi dari awal kembali.</p>
						</div>
						<div class="col-md-7">
							<div class="padding-md-30 padding-xs-0">
								<div id="midtrans_container">
									<img src="https://api-docs.midtrans.com/images/logo-mt.png" />
									<div class="m-t-15 m-b-15">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</div>
									<div class="row">
										<div class="col-md-6 col-sm-12">
											<h5 class="m-b-5">Debit Rekening</h5>
											<p>Pembayaran melalui fasilitas Internet Banking dari berbagai Bank ternama.</p>
										</div>

										<div class="col-md-6 col-sm-12">
											<h5 class="m-b-5">Transfer Bank</h5>
										<p>Pembayaran Transfer baik melalui ATM, mobile, atau internet banking.</p>
									</div>
									<div class="col-md-6 col-sm-12">
											<h5 class="m-b-5">kartu kredit/debit</h5>
											<p>Pembayaran menggunakan kartu kredit/debit dari semua Bank yang berlogo VISA/MasterCard/JCB/Amex.</p>
										</div>
									</div>

								</div>
								<div class="hide" id="commitment_container">
									<h2>Upload dokumen komitmen</h2>
									<div class="m-b-15">Sistem pembayaran menggunakan surat komitmen yang akan disetujui oleh admin.</div>
									<div id="commitment-file" class="dropzone">
										<div class="fallback">
											<input name="file" type="file" multiple/>
										</div>
									</div>
									<div class="uploadError error">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="padding-20 bg-white p-b-50 m-b-50">
					<ul class="pager wizard">
						<li class="<?php echo !count($cart) ? 'disabled' : 'next'; ?>">
							<button class="btn btn-primary btn-cons btn-animated from-left fa fa-truck pull-right" type="button">
								<span>Lanjut</span>
							</button>
						</li>
						<li class="next finish hidden">
							<button id="pay-button" class="btn btn-primary btn-cons btn-animated from-left fa fa-cog pull-right" type="button">
								<span>Finish</span>
							</button>
						</li>
						<li class="previous">
							<button class="btn btn-default btn-cons pull-right" type="button">
								<span>Sebelum</span>
							</button>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- END CONTAINER FLUID -->
</div>

<style>
	.pager {
		padding-left: 0;
		margin: 20px 0;
		text-align: center;
		list-style: none;
	}

	.list-inline > li {
		display: inline-block;
		padding-right: 5px;
		padding-left: 5px;
	}

	a.checked{
		background-color: #ecf8fd;
	}

	.selected-payment {
		display: none;
		position: absolute;
		right: 20px;
		top: 18px;
		width: 30px;
		height: 30px;
		border-radius: 20px;
		background: #efefef;
		text-align: center;
		font-size: 22px;
		line-height: 28px;
		color: #307fc2;
	}

	.checked .selected-payment{
		display: block;
	}
</style>
<script type="text/javascript" src="<?php echo JURI::base(); ?>templates/frontend/js/jquery.autocomplete.js"></script>
<script src="https://app.midtrans.com/snap/snap.js" data-client-key="Mid-client-tbjm_9qn4fuYprzI"></script>
<script type="text/javascript">

	var oncheckout = false;

	var user = {
		id: '<?php echo $user->id; ?>',
		name: '<?php echo $user->name; ?>',
		email: '<?php echo $user->email; ?>',
	};

	var payment = {
		payment_method: 'midtrans',
		description: '',
		task: 'checkoutCart',
		ajax: 1
	};
	payment['<?php echo JUtility::getToken();?>'] = 1;

	function checkout(){
		if(oncheckout){
			return false;
		}

		var userData = $('form#userForm').serializeArray();
		$(userData).each(function (index, obj) {
			user[obj.name] = obj.value;
		});

		var baseUrl = '<?php echo $baseUrl; ?>';
		var href = baseUrl + 'cart';

		var postdata = $.extend(user, payment);

		var btnNext = $('#rootwizard').find('.pager .next').find('button');

		btnNext.addClass('disabled');
		oncheckout = true;

		if(payment.payment_method === 'midtrans'){
			snap.show();
		}

		$.post(href, postdata, function (response) {
			oncheckout = false;
			var jsonResponse = JSON.parse(response);

			if(payment.payment_method === 'midtrans'){
				snap.pay(jsonResponse.token, {
					onSuccess: function(){
						checkPayment(jsonResponse.id)
					},
					onPending: function(){
						checkPayment(jsonResponse.id)
					},
					onError: function(){
						window.location = '<?php echo JURI::base(); ?>transactions?id=' + jsonResponse.id
					},
					onClose: function(){
						window.location = '<?php echo JURI::base(); ?>transactions?id=' + jsonResponse.id
					},
				});
			}

			if(payment.payment_method === 'commitment'){
				window.location = '<?php echo JURI::base(); ?>transactions?id=' + jsonResponse.id
			}
		})
	}

	function checkPayment(order_id){
		var baseUrl = '<?php echo $baseUrl; ?>';
		var href = baseUrl + 'cart';

		var postdata = {
			order_id: order_id,
			task: 'checkPayment'
		}

		console.log()

		$.post(href, postdata, function (response) {
			console.log(response);
			window.location = '<?php echo JURI::base(); ?>transactions?id=' + order_id
		})
	}

	$(document).ready(function () {

		Dropzone.autoDiscover = false;

		$('#rootwizard').bootstrapWizard({
			onTabShow: function (tab, navigation, index) {
				var $total = navigation.find('li').length;
				var $current = index + 1;

				// If it's the last tab then hide the last button and show the finish instead
				if ($current >= $total) {
					$('#rootwizard').find('.pager .next').hide();
					$('#rootwizard').find('.pager .finish').show().removeClass('disabled hidden');
				} else {
					$('#rootwizard').find('.pager .next').show();
					$('#rootwizard').find('.pager .finish').hide();
				}

				var li = navigation.find('li.active');

				var btnNext = $('#rootwizard').find('.pager .next').find('button');
				var btnPrev = $('#rootwizard').find('.pager .previous').find('button');

				// remove fontAwesome icon classes
				function removeIcons(btn) {
					btn.removeClass(function (index, css) {
						return (css.match(/(^|\s)fa-\S+/g) || []).join(' ');
					});
				}

				if ($current > 1 && $current < $total) {

					var nextIcon = li.next().find('.fa');
					var nextIconClass = nextIcon.attr('class').match(/fa-[\w-]*/).join();

					removeIcons(btnNext);
					btnNext.addClass(nextIconClass + ' btn-animated from-left fa');

					var prevIcon = li.prev().find('.fa');
					var prevIconClass = prevIcon.attr('class').match(/fa-[\w-]*/).join();

					removeIcons(btnPrev);
					btnPrev.addClass(prevIconClass + ' btn-animated from-left fa');
				} else if ($current == 1) {
					// remove classes needed for button animations from previous button
					btnPrev.removeClass('btn-animated from-left fa');
					removeIcons(btnPrev);
				} else {
					// remove classes needed for button animations from next button
					btnNext.removeClass('btn-animated from-left fa');
					removeIcons(btnNext);
				}
			},
			onNext: function (tab, navigation, index) {

				if(index === 1){

				}
				if(index === 2){

				}
				if(index === 3){
					if(payment.payment_method === 'midtrans'){
						checkout()
					}
					if(payment.payment_method === 'commitment'){
						if(!myDropzone.files.length){
							$('.uploadError').html('Pilih file untuk upload');
							return
						}
						myDropzone.processQueue();
					}
				}
			},
			onPrevious: function (tab, navigation, index) {
				console.log(tab, navigation, index);
			},
			onInit: function () {
				$('#rootwizard ul').removeClass('nav-pills');
			}

		});

		$('.remove-item').click(function () {
			$(this).parents('tr').fadeOut(function () {
				$(this).remove();
			});
		});

		var myDropzone = new Dropzone('#commitment-file', {
			url: baseUrl + 'cart',
			params: {
				task: 'uploadImage',
				type: 'commitment'
			},
			dictRemoveFile: 'Hapus',
			addRemoveLinks: true,
			autoProcessQueue: false,
			uploadMultiple: true,
			maxFiles: 3,
			parallelUploads: 3,
		});

		myDropzone.on("addedfile", function(file) {
			$('.uploadError').empty()
		});

		myDropzone.on("complete", function(file) {
			//myDropzone.removeAllFiles()
		});

		myDropzone.on("success", function(response) {
			console.log(response);
			if(response.xhr.status == 200){
				payment.description = response.xhr.responseText;
				checkout()
			}
		});

		$('.paymentSelection').click(function(){
			var th = $(this);
			var data = th.data();
			if(data.type === 'midtrans'){
				payment.payment_method = 'midtrans';
				$('#midtrans_container').removeClass('hide');
				$('#commitment_container').addClass('hide')
			}
			if(data.type === 'commitment'){
				payment.payment_method = 'commitment';
				$('#midtrans_container').addClass('hide');
				$('#commitment_container').removeClass('hide')
			}
			th.addClass('checked');
			$('.paymentSelection').not(th).removeClass('checked');
		});

		$('#submit-all').click(function(){
			myDropzone.processQueue();
		});

		$('input#org_name').autocomplete({
			ajax:{
				url: '<?php echo JURI::base(); ?>?option=com_user&task=autoComplete&type=organization',
			},
			name: 'key',
			minLength: 0,
			match: false
		})
	});
</script>





