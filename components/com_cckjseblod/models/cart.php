<?php
/**
 * @version      1.9.0
 * @author        http://www.seblod.com
 * @copyright    Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license      GNU General Public License version 2 or later; see _LICENSE.php
 * @package      SEBLOD 1.x (CCK for Joomla!)
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');
jimport('phpbarcode.barcode');
JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_cckjseblod' . DS . 'tables');
JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_eventorg' . DS . 'tables');
JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'tables');
require_once(dirname(__FILE__) . DS . 'Veritrans.php');

/**
 * Article    Model Class
 **/
class CCKjSeblodModelCart extends JModel
{
	/**
	 * Vars
	 **/
	var $_request = null;
	var $_user = null;
	var $_errors = null;
	var $_pageNav = null;

	var $_session = null;
	var $_cart = null;

	var $_VTis3ds = true;

	// sanbox
	// var $_VTlive = false;
	// var $_VTclientKey = 'SB-Mid-client-L7EDySDs1NtFGOsh';
	// var $_VTserverKey = 'SB-Mid-server-Sd_UnXwRG8NlgwRKYCjAuSUv';
	// var $_admin_email = 'agus_riyanto_28@yahoo.com';

	// live
	var $_VTlive = true;
	var $_VTclientKey = 'Mid-client-tbjm_9qn4fuYprzI';
	var $_VTserverKey = 'Mid-server-cD2bZCqx2_YnsNSOE0KiUH0_';
	var $_admin_email = 'pbpapdi.pin@gmail.com';


	/**
	 * Constructor
	 **/
	function __construct()
	{
		parent::__construct();
		$this->_request = (object) JRequest::get('default');
		$this->_session = JFactory::getSession();
		$this->_user    = JFactory::getUser();
		$this->_cart    = $this->getCart();
	}

	function getCart($format = null)
	{
		//$cart = $this->_session->get('cart');
		$cartTable = JTable::getInstance('cart');
		$cart      = $cartTable->load($this->_user->id);
		$this->_session->set('cart', $cart);

		return ($format == 'json') ? $cart : (array) json_decode($cart);
	}

	function addCart()
	{

		$cart  = $this->getCart();
		$count = count($cart);

		if ($count)
		{
			foreach ($cart as $key => $cartItem)
			{
				if ($cartItem->event_id == $this->_request->event_id && $cartItem->email == $this->_request->email)
				{
					$similar = 1;
				}
			}
			if ($similar == 1)
			{
				die('exist');
			}

		}
		else
		{
			$count = 0;
			$cart  = array();
		}

		$package = $this->getPackage($this->_request->package_id);


		$workshops = array();
		if ($options = $this->_request->workshops)
		{
			if (is_string($options))
			{
				$options = json_decode($options);
			}
			foreach ($options as $option)
			{
				$workshops[] = $this->getWorkshop($option);
			}
		}

		if ($this->checkBooking($this->_request->event_id, $this->_request->email))
		{
			$error = array(
				'error' => 'Alamat email sudah terdaftar pada event ini, hubungi kami untuk informasi lebih lanjut.'
			);

			return $error;
		}

		$event = $this->getEvent($this->_request->event_id);

		$cart[$count]['id']             = $this->_request->id;
		$cart[$count]['event_id']       = $this->_request->event_id;
		$cart[$count]['event_name']     = $event->title;
		$cart[$count]['title']          = $this->_request->title;
		$cart[$count]['name']           = $this->_request->name;
		$cart[$count]['email']          = $this->_request->email;
		$cart[$count]['phone']          = $this->_request->phone;
		$cart[$count]['bidang']         = $this->_request->bidang;
		$cart[$count]['npa']            = $this->_request->npa;
		$cart[$count]['package_id']     = $this->_request->package_id;
		$cart[$count]['package_member'] = $this->_request->package_member;
		$cart[$count]['price']          = $this->_request->price;
		$cart[$count]['workshops']      = $workshops;
		$cart[$count]['package']        = $package;
		$cart[$count]['as']             = $this->_request->as;

		$encodedCart = json_encode($cart);

		if ($this->_user->id)
		{
			$cartTable = JTable::getInstance('cart');
			$cartData  = array(
				'user_id' => $this->_user->id,
				'items'   => $encodedCart,
			);

			(!$cartTable->bind($cartData)) && die($cartTable->getError());
			(!$cartTable->store()) && die($cartTable->getError());
		}

		$this->_session->set('cart', $encodedCart);

		return $cart;
	}

	function delCart($cid = null)
	{
		$cartItems = $this->getCart();

		foreach ($cartItems as $key => $cartItem)
		{
			if ($cartItem->id == $cid)
			{
				unset($cartItems[$key]);
				$cartItems = array_values($cartItems);
			}
		}

		$encodedCart = json_encode($cartItems);

		if ($this->_user->id)
		{
			$cartTable = JTable::getInstance('cart');
			$cartData  = array(
				'user_id' => $this->_user->id,
				'items'   => $encodedCart,
			);
			(!$cartTable->bind($cartData)) && die($cartTable->getError());
			(!$cartTable->store()) && die($cartTable->getError());
		}

		$this->_session->set('cart', $encodedCart);

		return $cartItems;
	}

	function clearCart()
	{
		if ($this->_user->id)
		{
			$cartTable = JTable::getInstance('cart');
			$cartData  = array(
				'user_id' => $this->_user->id,
				'items'   => '',
			);

			(!$cartTable->bind($cartData)) && die($cartTable->getError());
			(!$cartTable->store()) && die($cartTable->getError());
		}
		$this->_session->set('cart', '');
	}

	function checkExpired()
	{
		$db    = JFactory::getDBO();
		$query = "SELECT a.created_date AS 'orderDate', UNIX_TIMESTAMP(NOW()) AS 'now', a.id, b.email, UNIX_TIMESTAMP(a.created_date) AS 'date', (UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(a.created_date)) AS diff FROM #__transaction_order AS a LEFT JOIN #__users AS b ON b.id = a.user_id WHERE status = 10 ORDER BY id";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$error  = $db->getErrorMsg();
		// print_r($result);die();
		if ($error)
		{
			JFactory::getApplication()->enqueueMessage('DB Query ERROR!, ' . $error, 'ERROR');
		}
		else
		{
			jimport('joomla.error.log');
			$log     = JLog::getInstance('email_notification_log.php', '', JPATH_BASE . DS . 'logs'); // get an instance of JLog
			$results = array();
			$i       = 0;
			foreach ($result as $item)
			{
				$i++;
				$diff = $item->diff - (3600 * 7); // timezone wib
				if ($diff > 3600 * 12 && $diff < 3600 * 13)
				{
					$this->sendMailMessage($this->getTransaction($item->id), 'remindTransaction');
					$log_entry = array('LEVEL' => '1', 'STATUS' => "Sent order reminder:", 'COMMENT' => 'Order ' . $item->id . ' to ' . $item->email);
					$log->addEntry($log_entry);
				}
				else if ($diff > 3600 * 24)
				{
					if ($diff > 3600 * 24 * 7)
					{
						$this->expireTransaction($this->getTransaction($item->id), 'silent');
						$log_entry = array('LEVEL' => '1', 'STATUS' => "Silent order expired:", 'COMMENT' => 'Order ' . $item->id . ' email ' . $item->email);
					}
					else
					{
						$this->expireTransaction($this->getTransaction($item->id));
						$log_entry = array('LEVEL' => '1', 'STATUS' => "Sent order expired:", 'COMMENT' => 'Order ' . $item->id . ' to ' . $item->email);
					}
					$log->addEntry($log_entry);
				}
			}
		}
		$log->_closeLog();
		die();
	}

	function searchInBooking($name) {
		$query = "SELECT order_id FROM #__event_booking WHERE order_id != '' AND user_name LIKE '%" . $name . "%'";
		$this->_db->setQuery($query);
		$result = $this->_db->loadResultArray();
		return implode(',', $result);
	}

	function getTransactions($all = false)
	{
		global $mainframe;
		jimport('joomla.html.pagination');
		$name = JRequest::getVar('name');
		$date = JRequest::getVar('date');
		$result = new stdClass();

		$where = " WHERE a.id != 0 ";

		if (!!$name && is_string($name))
		{
			$where .= " AND c.name LIKE '%" . $name . "%'";

			$ids = $this->searchInBooking($name);
			if ($ids) {
				$where .= " OR a.id IN (".$ids.")";
			}
		}

		if ($date) {
			$date = explode(' - ', $date);
			$where .= " AND a.created_date BETWEEN STR_TO_DATE('".$date[0]."', '%m/%d/%Y') AND STR_TO_DATE('".$date[1]."', '%m/%d/%Y') + interval 1 day";
		}

		if (!$all)
		{
			$lim  = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', 30, 'int');
			$lim0 = JRequest::getVar('limitstart', 0, '', 'int');
		}

		$query = "SELECT a.*, b.*, c.*,  a.id AS id, a.status AS status FROM #__transaction_order AS a" .
			" LEFT JOIN #__transaction_payment AS b ON b.order_id = a.id" .
			" LEFT JOIN #__users AS c ON c.id = a.user_id" . $where .
			" ORDER BY a.created_date DESC";
		$this->_db->setQuery($query, $lim0, $lim);
		$result->data = $this->_db->loadObjectList();
		$error        = $this->_db->getErrorMsg();

		if ($error)
		{
			die('getTransactions' . $error);
		}

		if (!$all)
		{
			$result->pageNav = new JPagination($this->getTotal($query), $lim0, $lim);
		}

		return $result;
	}


	function getCArtDataFromBooking($orderId) 
	{
		$query = "SELECT a.*, b.title AS package_title, b.description AS package_description, b.price AS price_umum, b.price_member, b.price_finasim, b.price_dokter_umum FROM #__event_booking AS a".
				" LEFT JOIN #__event_package AS b ON b.id = a.package_id".
				" WHERE a.order_id = ". (int) $orderId;

		$this->_db->setQuery($query);
		$bookings = $this->_db->loadObjectList();
		
		foreach($bookings as $booking) {
			$booking->name = $booking->user_name;
			$booking->email = $booking->user_email;
			$booking->npa = $booking->user_npa;
			$booking->package = (object) array(
				'title' => $booking->package_title,
				'description' => $booking->package_description
			);
			$booking->workshops = json_decode($booking->workshops);
			switch($booking->price) {
				case $booking->price_umum:
					$booking->package_member = 'Umum';
					break;
				case $booking->price_member:
					$booking->package_member = 'Anggota';
					break;
				case $booking->price_finasim:
					$booking->package_member = 'Finasim';
					break;
				case $booking->price_dokter_umum:
					$booking->package_member = 'Dokter Umum';
					break;
			}
		}
		// echo '<pre>';
		// print_r($bookings[0]);
		// echo '</pre>';
		return $bookings;
	}

	function getTransaction($orderId)
	{

		$query = "SELECT a.*, b.*, c.*, a.id AS id, a.status AS status, b.status AS payment_status, d.name AS status_name FROM #__transaction_order AS a" .
			" LEFT JOIN #__transaction_payment AS b ON b.order_id = a.id" .
			" LEFT JOIN #__users AS c ON c.id = a.user_id" .
			" LEFT JOIN #__status_code AS d ON d.id = a.status" .
			" WHERE a.id = " . $orderId;
		$this->_db->setQuery($query);
		$transaction = $this->_db->loadObject();

		/*if($this->_user->id != $transaction->user_id && $this->_user->usertype != 'Super Administrator'){
			return false;
		}*/

		$transaction->cart_data = $this->getCArtDataFromBooking($orderId);

		if ($transaction->ext_response)
		{
			$transaction->ext_response = json_decode($transaction->ext_response);
		}

		if ($transaction->method == 'commitment')
		{
			$transaction->commitment_files = $this->getFiles('commitment' . DS . $transaction->description);
		}

		return $transaction;
	}

	function createTransaction()
	{
		global $mainframe;
		$date  =& JFactory::getDate();
		$date  = $date->toMySQL();
		$cart  = $this->getCart();
		$total = 0;

		foreach ($cart as $cartItem)
		{
			$total += (int) $cartItem->price;
		}

		$transaction     = JTable::getInstance('transactionOrder');
		$transactionData = array(
			'user_id'      => $this->_user->id,
			'value'        => $total,
			'product_id'   => $this->_request->event_id,
			'product_type' => 'event_package',
			'cart_data'    => json_encode($cart),
			'status'       => 10,
			'created_date' => $date
		);

		if ($this->_request->payment_method == 'commitment')
		{
			$transactionData['status'] = 13;
		}

		(!$transaction->bind($transactionData)) && die($transaction->getError());
		(!$transaction->store()) && die($transaction->getError());

		$payment     = JTable::getInstance('transactionPayment');
		$paymentData = array(
			'order_id'    => $transaction->id,
			'method'      => $this->_request->payment_method,
			'description' => $this->_request->description,
			'status'      => 10
		);

		if ($this->_request->payment_method == 'commitment')
		{
			$paymentData['status'] = 13;
		}

		(!$payment->bind($paymentData)) && die($payment->getError());
		(!$payment->store()) && die($payment->getError());

		foreach ($cart as $cartItem)
		{
			$this->storeBooking($transaction, $cartItem);
			if ($cartItem->workshops)
			{
				foreach ($cartItem->workshops as $workshop)
				{
					$this->updateActivitySeat($workshop->id, 1);
				}
			}
		}

		$this->clearCart();

		// $this->updateUserProfile($this->_user, $this->_request);

		$transaction = $this->getTransaction($transaction->id);

		if ($this->_request->payment_method == 'midtrans')
		{
			$snap = $this->midtransPayment($transaction);
			if ($snap)
			{
				if ($this->_request->ajax)
				{
					echo json_encode($snap);
					die();
				}
			}
		}

		if ($this->_request->payment_method == 'commitment')
		{
			// echo json_encode($transaction); ob_end_flush(); flush();
			$this->sendMailMessage($transaction, 'newTransaction');
			$this->sendMailMessage($transaction, 'adminNewTransaction');
		}

		return $transaction;
	}

	function updateTransaction($id, $status, $paymentData = null)
	{
		$transaction = JTable::getInstance('transactionOrder');
		$transaction->load($id);

		if ($status == $transaction->status)
		{
			return;
		}

		$orderData = array('status' => $status);

		(!$transaction->bind($orderData)) && die($transaction->getError());
		(!$transaction->store()) && die($transaction->getError());

		if ($paymentData)
		{
			$payment = JTable::getInstance('transactionPayment');
			$payment->load($id);
			$payment->status = $status;
			(!$payment->bind($paymentData)) && die($payment->getError());
			(!$payment->store()) && die($payment->getError());
		}

		$cart = $transaction->cart_data ? json_decode($transaction->cart_data) : array();

		foreach ($cart as $cartItem)
		{
			$this->storeBooking($transaction, $cartItem);
			if (in_array($status, array(14, 15, 16)))
			{
				foreach ($cartItem->workshops as $workshop)
				{
					$this->updateActivitySeat($workshop->id, -1);
				}
			}
		}

		$transaction = $this->getTransaction($transaction->id);

		switch ($status)
		{
			case null:
			case 10:
				$this->sendMailMessage($transaction, 'newTransaction');
				$this->sendMailMessage($transaction, 'adminNewTransaction');
				break;
			case 12:
				$this->sendMailMessage($transaction, 'successTransaction');
				$this->sendMailMessage($transaction, 'adminSuccessTransaction');
				break;
			case 14:
				$this->sendMailMessage($transaction, 'expiredTransaction');
				break;
			case 15:
				$this->sendMailMessage($transaction, 'rejectedCommitment');
				break;
		}
	}

	function expireTransaction($transaction, $silent = false)
	{
		if ($transaction->status == 14)
		{
			return;
		}
		$orderData = array(
			'status'       => 14,
			'updated_date' => JHTML::_('date')
		);

		$this->updateOrder($transaction->id, $orderData);

		foreach ($transaction->cart_data as $cartItem)
		{
			if ($cartItem->workshops)
			{
				foreach ($cartItem->workshops as $workshop)
				{
					$this->updateActivitySeat($workshop->id, -1);
				}
			}
		}
		if (!$silent)
		{
			$this->sendMailMessage($transaction, 'expiredTransaction');
		}
	}

	function getMidtransPaymentData($transaction = null)
	{
		if (!$transaction->id)
		{
			return false;
		}

		global $mainframe;

		Veritrans_Config::$isProduction = $this->_VTlive;
		Veritrans_Config::$serverKey    = $this->_VTserverKey;
		Veritrans_Config::$is3ds        = false;

		$midtransData = Veritrans_Transaction::status($transaction->id);

		if (!$midtransData)
		{
			$mainframe->enqueueMessage('Unable get data from payment gateway!', 'error');

			return null;
		}

		$paymentData = array(
			'method'       => 'midtrans',
			'channel'      => $midtransData->payment_type,
			'ext_id'       => $midtransData->transaction_id,
			'updated_date' => $midtransData->transaction_time,
			'ext_response' => json_encode($midtransData),
		);

		if ($midtransData->status_code === '200')
		{
			$paymentData['status'] = 12;
		}

		if ($midtransData->status_code === '201')
		{
			$paymentData['status'] = 13;
		}

		if ($midtransData->status_code === '407')
		{
			$paymentData['status'] = 14;
		}

		if ($midtransData->status_code === '202')
		{
			$paymentData['status'] = 15;
		}

		if ($midtransData->status_code === '404')
		{
			$mainframe->enqueueMessage('Transaction #' . $transaction->id . ' not found at payment gateway!', 'error');

			return null;
		}

		$this->updateTransaction($transaction->id, $paymentData['status'], $paymentData);

		return $midtransData;
	}

	function midtransPayment($order)
	{
		global $mainframe;

		Veritrans_Config::$isProduction = $this->_VTlive;
		Veritrans_Config::$serverKey    = $this->_VTserverKey;
		Veritrans_Config::$is3ds        = false;


		if ($order->status === 14)
		{
			$referer = $_SERVER['HTTP_REFERER'];
			// $this->redirect(null, $referer, 'Order is Expired', 'error');
		}

		$items = $order->cart_data;


		foreach ($items as $item)
		{
			$package        = $this->getPackage($item->package_id);
			$name           = (strlen($item->name) > 24) ? substr($item->name, 0, 21) . '...' : $item->name;
			$item_details[] = array(
				'id'       => $item->id,
				'price'    => (int) $item->price,
				'quantity' => 1,
				'name'     => $name . ', ' . $package->title
			);
		}

		// Required
		$transaction_details = array(
			'order_id'     => $order->id,
			'gross_amount' => $order->value, // no decimal allowed for creditcard
		);

		// Optional
		$customer_details = array(
			'first_name' => $this->_user->name,
			'email'      => $this->_user->email,
			'phone'      => $this->_user->phone,
		);

		// Fill transaction details
		$transaction = array(
			'transaction_details' => $transaction_details,
			'customer_details'    => $customer_details,
			'item_details'        => $item_details,
		);


		$snapToken = Veritrans_Snap::getSnapToken($transaction);

		$ret        = new stdClass();
		$ret->token = $snapToken;
		$ret->id    = $order->id;

		/*$ret->success = JURI::base() . 'thank-you?order_id=' . $order->id;
		$ret->pending = JURI::base() . 'payment-pending?order_id=' . $order->id;
		$ret->unfinish = JURI::base() . 'transactions?layout=transaction_view&err=unfinish&id=' . $order->id;
		$ret->error = JURI::base() . 'transactions?layout=transaction_view&err=err&id=' . $order->id;*/

		return $ret;
	}

	function getBookings($email = null)
	{
		$result = new stdClass();

		$query = "SELECT a.status as status, a.id AS id, b.start_date, b.end_date, c.title AS package_name, c.description AS package_description, d.title AS title" .
			" FROM #__event_booking AS a" .
			" LEFT JOIN #__event AS b ON b.id = a.event_id" .
			" LEFT JOIN #__event_package AS c ON c.id = a.package_id" .
			" LEFT JOIN #__content AS d ON d.id = b.content_id";
		if ($email)
		{
			$query .= " WHERE a.user_email = '$email'";
		}
		$query .= ' ORDER BY b.start_date DESC, a.id DESC';

		$this->_db->setQuery($query);
		$bookings = $this->_db->loadObjectList();

		// echo'<pre>';print_r($query);echo'</pre>'; die();

		return $bookings;
	}

	function checkBooking($event_id, $user_email)
	{
		$query = 'SELECT id ' .
			' FROM #__event_booking AS a' .
			' WHERE event_id = ' . (int) $event_id .
			" AND user_email = '" . $user_email . "'";
		$this->_db->setQuery($query);
		$result = $this->_db->loadResult();

		// return $result ? true : false;
		return false;
	}

	function storeBooking($order, $cartItem = array())
	{
		$booking     = JTable::getInstance('eventBooking');
		$bookingData = array(
			'user_id'    => $cartItem->user_id,
			'user_name'  => $cartItem->name,
			'user_email' => $cartItem->email,
			'user_phone' => $cartItem->phone,
			'user_npa'   => $cartItem->npa,
			'event_id'   => $cartItem->event_id,
			'package_id' => $cartItem->package_id,
			'price'      => $cartItem->price,
			'order_id'   => $order->id,
			'workshops'  => $cartItem->workshops ? json_encode($cartItem->workshops) : '',
			'status'     => $order->status
		);

		(!$booking->bind($bookingData)) && die($booking->getError());
		(!$booking->store()) && die($booking->getError());


		if ($cartItem->workshops)
		{
			foreach ($cartItem->workshops as $workshop)
			{
				$this->storeAttendance($workshop, $booking, $cartItem);
			}
		}

		if ($order->status != 12)
		{
			return $booking;
		}

		// execute only if status is 12/paid

		$barcode       = barcode_generator::getInstance();
		$barcodeOption = array('sf' => 20);
		$barcodeImage  = $barcode->render_image('qr', base64_encode($booking->id), $barcodeOption);
		$filepath      = JPATH_BASE . DS . 'files' . DS . 'booking' . DS . $this->id . DS . $booking->id . '.jpg';
		imagejpeg($barcodeImage, $filepath);

		$barcodeData  = JURI::base() . 'transactions?layout=bookings&id=' . $booking->id . '&format=pdf';
		$barcodeImage = $barcode->render_image('qr', $barcodeData, $barcodeOption);
		$filepath     = JPATH_BASE . DS . 'files' . DS . 'booking' . DS . $this->id . DS . 'download_' . $booking->id . '.jpg';
		imagejpeg($barcodeImage, $filepath);

		$booking->email = $booking->user_email;

		$this->sendBookingEmail($booking);

		return $booking;
	}

	function sendBookingEmail($booking)
	{
		$user = $this->findUser('email', $booking->email);
		if (!$user)
		{
			$this->sendMailMessage($booking, 'registeredBooking');
		}
		else
		{
			$this->sendMailMessage($booking, 'registeredBooking');
		}
	}

	function storeAttendance($activity, $booking, $cartItem)
	{
		$attendance     = JTable::getInstance('eventAttendance');
		$attendanceData = array(
			'booking_id'  => $booking->id,
			'event_id'    => $cartItem->event_id,
			'activity_id' => $activity->id,
			'status'      => $booking->status
		);
		(!$attendance->bind($attendanceData)) && die($attendance->getError());
		(!$attendance->store()) && die($attendance->getError());
	}

	function updateActivitySeat($activityId, $seat = 0)
	{
		$activity = JTable::getInstance('activity');
		$activity->load($activityId);
		$activity->registered = $activity->registered + $seat;
		(!$activity->store()) && die($activity->getError());
	}

	function updateUserPhone()
	{
		$user  = JFactory::getUser();
		$array = array(
			'phone' => $this->_request->phone
		);
		$user->bind($array);
		$user->save();
	}

	function updateUserOrganization($name, $address)
	{
		$organization     = JTable::getInstance('organization');
		$organizationData = array(
			'name'    => $name,
			'address' => $address,
		);

		(!$organization->bind($organizationData)) && die($organization->getError());
		(!$organization->store()) && die($organization->getError());

		return $organizationData;
	}

	function updateUserProfile($user, $data)
	{
		$this->updateUserPhone();

		$profile = JTable::getInstance('profile');

		$profileData = array(
			'user_id'     => $user->id,
			'org_name'    => $data->org_name,
			'org_address' => $data->org_address,
			'address'     => $data->address,
			'country'     => $data->country,
			'city'        => $data->city,
			'province'    => $data->province,
			'postal'      => $data->postal,
		);

		(!$profile->bind($profileData)) && die($profile->getError());
		(!$profile->store()) && die($profile->getError());

		$this->updateUserOrganization($data->org_name, $data->org_address);

		return $profileData;
	}

	function updateOrder($id, $orderData = array())
	{
		$order = JTable::getInstance('transactionOrder');
		$order->load($id);
		if (!$order->id)
		{
			return false;
		}
		(!$order->bind($orderData)) && die($order->getError());
		(!$order->store()) && die($order->getError());


		return $this->getOrder($order->id);
	}

	function sendMailMessage($data, $type)
	{
		$config   = JFactory::getConfig();
		$mailFrom = $config->getValue('config.mailfrom');
		$fromName = $config->getValue('config.fromname');

		$templateType = array(
			'newTransaction'          => array(
				'templateId' => 152,
				'dataType'   => 'transaction'
			),
			'adminNewTransaction'     => array(
				'receiverType' => 'admin',
				'templateId'   => 154,
				'dataType'     => 'transaction'
			),
			'successTransaction'      => array(
				'templateId' => 158,
				'dataType'   => 'transaction'
			),
			'adminSuccessTransaction' => array(
				'receiverType' => 'admin',
				'templateId'   => 154,
				'dataType'     => 'transaction'
			),
			'remindTransaction'       => array(
				'templateId' => 305,
				'dataType'   => 'transaction'
			),
			'expiredTransaction'      => array(
				'templateId' => 306,
				'dataType'   => 'transaction'
			),
			'approvedCommitment'      => array(
				'templateId' => 158,
				'dataType'   => 'transaction'
			),
			'rejectedCommitment'      => array(
				'templateId' => 499,
				'dataType'   => 'transaction'
			),
			'registeredBooking'       => array(
				'templateId' => 785,
				'dataType'   => 'booking'
			),
			'remindBooking'           => array(
				'templateId' => 785,
				'dataType'   => 'booking'
			),
			'userRegistration'        => array(
				'templateId' => 156,
				'dataType'   => 'register'
			),
		);

		$selectedType = $templateType[$type];

		$template = $this->createMailTemplate($data, $selectedType);

		$to = $data->email;
		// $to = $this->_user->email; // use this for testing

		if ($selectedType['receiverType'] === 'admin')
		{
			$to = $this->_admin_email;
		}

		return $this->sendEmail($to, $template['content'], $template['subject']);
	}

	function getHowToPay($transaction)
	{
		$va   = '';
		$text = '';

		$contentId = array(
			//transfer payment
			'bca'            => 780,
			'mandiri'        => 781,
			'bni'            => 782,
			'permata'        => 783,
			//instant payment
			'bca_klikpay'    => 780,
			'akulaku'        => 781,
			'cimb_clicks'    => 782,
			'danamon_online' => 783,
			'bri_epay'       => 783
		);

		$instantPaymentType = array('bca_klikpay', 'akulaku', 'cimb_clicks', 'danamon_online', 'bri_epay');

		if (in_array($transaction->ext_response->payment_type, $instantPaymentType))
		{
			$method = $transaction->ext_response->payment_type;
			$text   = $this->getRawContent($contentId[$transaction->ext_response->payment_type]);
		}

		if ($transaction->ext_response->payment_type === 'bank_transfer' || $transaction->ext_response->payment_type === 'echannel')
		{

			if ($transaction->ext_response->va_numbers)
			{
				$bank = $transaction->ext_response->va_numbers[0]->bank;
				$va   = $transaction->ext_response->va_numbers[0]->va_number;
			}
			if ($transaction->ext_response->bill_key)
			{
				$bank   = 'mandiri';
				$biller = $transaction->ext_response->biller_code;
				$va     = $transaction->ext_response->bill_key;
			}
			if ($transaction->ext_response->permata_va_number)
			{
				$bank = 'permata';
				$va   = $transaction->ext_response->permata_va_number;
			}
			$text   = $this->getRawContent($contentId[$bank]);
			$method = 'Bank Transfer ' . $bank;
		}

		return (object) array(
			'method'  => $method,
			'biller'  => $biller,
			'va'      => $va,
			'expired' => JHTML::_('date', strtotime($transaction->date . ' + 1 days'), JText::_('DATE_FORMAT_LC2')),
			'text'    => $text->wysiwyg_text
		);
	}

	function createTransactionEmailData($transaction)
	{
		$howToPay = $this->getHowToPay($transaction);

		$peserta = '';

		$status = array(
			10 => 'Belum Terbayar',
			12 => 'Disetujui/Lunas',
			13 => 'Pending',
			14 => 'Dibatalkan',
			15 => 'Ditolak'
		);

		// print_r($transaction); die();

		foreach ($transaction->cart_data as $item)
		{

			$workshopItems = '<ol style="margin: 0; padding: 0">';
			if (count($item->workshops))
			{
				foreach ($item->workshops as $workshop)
				{
					$workshopItems .= '<li>' . $workshop->code . ' ' . $workshop->name . '</li>';
				}
			}
			$workshopItems .= '</ol>';

			$rows    = array(
				'Nama'      => '<strong>' . $item->name . '</strong>',
				'Paket'     => $item->title . '<br/>' . $item->package->description,
				'Bidang'    => $item->bidang,
				'Email'     => $item->email,
				'Telpon'    => $item->phone,
				'Harga'     => 'Rp.' . number_format($item->price),
				'Workshops' => $workshopItems
			);
			$peserta .= '<table width="100%" cellpadding="4" style="border: 1px solid #ccc;border-collapse: collapse; border-spacing:0px 0px"><tbody>';
			foreach ($rows as $key => $value)
			{
				$peserta .= '<tr>';
				$peserta .= '<td style="width: 15%; text-align: left; vertical-align: top; border-bottom: 0.1px solid #ccc;"><strong>' . $key . '</strong></td>';
				$peserta .= '<td style="width: 5%; text-align: center; vertical-align: top; border-bottom: 0.1px solid #ccc;">:</td>';
				$peserta .= '<td style="width: 80%; text-align: left; vertical-align: top; border-bottom: 0.1px solid #ccc;">' . $value . '</td>';
				$peserta .= '</tr>';
			}
			$peserta .= '</tbody></table><h4>&nbsp;</h4>';
		};

		$paymentMethod = '';
		if ($transaction->method === 'midtrans')
		{
			$paymentMethod .= $howToPay->method . '<br/>';
		}
		else
		{
			$paymentMethod .= $transaction->method . '<br/>';
		}
		if ($howToPay->va)
		{
			$paymentMethod .= 'Kode Pembayaran/Virtual account <strong>' . $howToPay->va . '</strong><br/>Bayarkan sebelum ' . $howToPay->expired;
		}


		$contentData = array(
			'{id}'             => $transaction->id,
			'{date}'           => JHTML::_('date', $transaction->date, JText::_('DATE_FORMAT_LC2')),
			'{name}'           => $transaction->name,
			'{email}'          => $transaction->email,
			'{status}'         => $status[$transaction->status],
			'{total}'          => 'Rp.' . number_format($transaction->value),
			'{paymentMethod}'  => $paymentMethod,
			'{paymentCode}'    => $howToPay->va,
			'{paymentExpired}' => $howToPay->expired,
			'{howToPay}'       => $howToPay->text,
			'{dataPeserta}'    => $peserta,
			'{detail_url}'     => JURI::base() . 'transactions?layout=transaction&id=' . $transaction->id,
			'{pdf_button}'     => '<a href="' . JURI::base() . 'transactions?layout=transaction&format=pdf&id=' . $transaction->id . '" class="btn btn-azure">Download PDF</a>',
		);

		return $contentData;
	}

	function createBookingEmailData($booking)
	{
		$booking->workshops = json_decode($booking->workshops);

		$workshops = '';
		foreach ($booking->workshops as $workshop)
		{
			$timeline  = $this->getTimeline($workshop->timeline_id);
			$workshops .= '<p><strong>' . $workshop->timeline . ' - ' . $workshop->name . '</strong>';
			$workshops .= '<br/>' . JHTML::_('date', $timeline->start_time, '%A, %d %b %Y %H:%M') . ' s/d ' . JHTML::_('date', $timeline->end_time, '%H:%M') . '</p>';
		}

		$contentData = array(
			'{name}'       => $booking->user_name,
			'{event}'      => $booking->event->title,
			'{event_date}' => JHTML::_('date', $booking->event->start_date),
			'{package}'    => $booking->package_name,
			'{email}'      => $booking->email,
			'{workshops}'  => $workshops,
			'{qr_code}'    => '<img style="width: 200px; height: 200px;" src="' . JURI::base() . 'files/booking/' . $booking->id . '.jpg" />',
			'{pdf_button}' => '<a href="' . JURI::base() . 'transactions?layout=bookings&format=pdf&id=' . $booking->id . '" class="btn btn-azure">Download PDF</a>',
		);

		return $contentData;
	}

	function createMailTemplate($data, $type)
	{

		if ($type['dataType'] === 'transaction')
		{
			$contentData = $this->createTransactionEmailData($data);
		}

		if ($type['dataType'] === 'booking')
		{
			$contentData = $this->createBookingEmailData($data);
		}

		$template = $this->getRawContent($type['templateId']);

		$html = str_replace('img src="images', 'img src="' . JURI::base() . 'images', $template->wysiwyg_text);
		$html = str_replace('url(\'images/', 'url(\'' . JURI::base() . 'images/', $html);
		$html = strtr($html, $contentData);

		$css     = '<style type="text/css"></style>';
		$content = '<html><head>' . $css . '</head><body>' . $html . '</body></html>';

		$subjectData = array(
			'{order_number}' => $data->id,
			'{name}'         => $data->name,
		);

		$subject = strtr($template->title, $subjectData);

		return array(
			'content' => $content,
			'subject' => $subject
		);
	}

	function getRawContent($cid = null)
	{
		if ($cid == null)
		{
			return 'Content not found!';
		}
		$db    = JFactory::getDBO();
		$query = "SELECT * FROM #__content WHERE id = " . $cid;
		$db->setQuery($query);
		$result = $db->loadObject();
		$error  = $db->getErrorMsg();

		if ($error)
		{
			return $error;
		}
		$group = array();
		$regex = '#::(.*?)::(.*?)::/(.*?)::#s';
		preg_match_all($regex, $result->introtext, $matches);

		if (sizeof($matches[1]))
		{
			foreach ($matches[1] as $key => $val)
			{
				$gmatch = explode('|', $val);
				if (count($gmatch) > 1)
				{
					$gkey                 = $gmatch[1];
					$iname                = $gmatch[0];
					$gname                = $gmatch[2];
					$group[$gkey][$iname] = $matches[2][$key];
					$res[$gname]          = $group;
				}
				else
				{
					$res[$val] = $matches[2][$key];
				}
			}
		}
		$res        = (object) $res;
		$res->id    = $cid;
		$res->stock = $result->stock;
		$res->title = $result->title;

		return $res;
	}

	function sendEmail($to, $content, $subject)
	{
		global $mainframe;
		$from    = $mainframe->getCfg('mailfrom');
		$replyto = $mainframe->getCfg('mailfrom');

		$mail =& JFactory::getMailer();
		$mail->isHTML(true);

		$mail->setBody($content);
		$mail->addReplyTo(array($replyto));
		$mail->setSender($from);
		$mail->setSubject($mainframe->getCfg('sitename') . ' - ' . $subject);
		$mail->addRecipient($to);
		//$mail->setAltBody($content);
		$stat = $mail->Send();
		if ($stat != true)
		{
			$mainframe->enqueueMessage('Email ERROR!, ' . $to . ', ' . $subject, 'ERROR');

			return false;
		}
		else
		{
			//$mainframe->enqueueMessage('Email sent, '.$to.', '.$subject);
			return true;
		}
	}

	function findUser($key = 'id', $value = 0)
	{
		$query = "SELECT * FROM #__users WHERE id != 0";
		switch ($key)
		{
			case 'email':
				$query .= "AND email = " . $value;
				break;
			case 'name':
				$query .= "AND name = " . $value;
				break;
			default :
				$query .= "AND id = " . $value;
		}
		$this->_db->setQuery($query);

		return $this->_db->loadObject();
	}

	function getWorkshop($id)
	{

		$query = 'SELECT a.*, b.name AS timeline' .
			' FROM #__event_activity AS a' .
			' LEFT JOIN #__event_timeline AS b ON b.id = a.timeline_id' .
			' WHERE b.activity_type = 2 AND a.id = ' . (int) $id;
		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		return $result;
	}

	function getPackage($id)
	{
		$query = 'SELECT a.*' .
			' FROM #__event_package AS a' .
			' WHERE a.id = ' . (int) $id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();

		return $result;
	}

	function getEvent($id)
	{
		require_once(JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');
		jimport('joomla.application.component.helper');
		$query = 'SELECT a.*, b.title AS title, b.catid, b.sectionid ' .
			' FROM #__event AS a' .
			' LEFT JOIN #__content AS b ON b.id = a.content_id' .
			' WHERE a.id = ' . (int) $id;
		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		if ($result)
		{
			$result->link = JRoute::_(
				ContentHelperRoute::getArticleRoute(
					$result->content_id . ':' . $result->title,
					$result->catid,
					$result->sectionid
				));
		}

		return $result;

	}

	function getTimeline($id)
	{
		$query = "SELECT a.* FROM #__event_timeline AS a" .
			" WHERE a.id = " . $id;
		$this->_db->setQuery($query);

		return $this->_db->loadObject();
	}

	function getActivity($id)
	{

		$query = 'SELECT a.*, b.introtext, c.name AS typename ' .
			' FROM #__event_activity AS a' .
			' LEFT JOIN #__content AS b ON b.id = a.content_id' .
			' LEFT JOIN #__event_activity_type AS c ON c.id = a.activity_type' .
			' WHERE a.id = ' . (int) $id;
		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();

		return $result;
	}

	function getTotal($query)
	{
		// Load the content if it doesn't already exist
		if (empty($this->_total))
		{
			$this->_total = $this->_getListCount($query);
		}

		return $this->_total;
	}

	function statuslabel($id)
	{
		$query = 'SELECT name FROM #__status_code WHERE id = ' . $id;
		$this->_db->setQuery($query);

		return $this->_db->loadResult();
	}

	function getMasterData()
	{
		$data = array();
		$excel_config = new stdClass();
		$excel_config->title = 'Master Data';

		$query = "SELECT a.*, a.id AS registration_is , b.*, c.*, d.*, d.name AS bill_name, e.*, f.name AS status_name" .
			' FROM #__event_booking AS a' .
			' LEFT JOIN #__transaction_order AS b ON b.id = a.order_id' .
			' LEFT JOIN #__transaction_payment AS c ON c.order_id = a.order_id'.
			' LEFT JOIN #__users AS d ON d.id = b.user_id' .
			' LEFT JOIN #__user_profile AS e ON e.user_id = d.id' .
			' LEFT JOIN #__status_code AS f ON f.id = b.status' .
			' ORDER BY a.id DESC';

		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();

		echo $this->_db->getErrorMsg();

		foreach ($result as $d) {
			
			
			$d->cart_data = json_decode($d->cart_data);
			if (!$d->cart_data) {
				$d->cart_data = $this->getCArtDataFromBooking($d->order_id);
			}
			if($d->cart_data){
				foreach ($d->cart_data as $cart_data) {
					if ($cart_data->email == $d->user_email) {
						$d->registration_data = $cart_data;
						if ($d->registration_data->as) {
							$d->registration_data->as = $d->registration_data->as == 'self' ? 'Personal' : 'Institusi';
						}
					}
				}
				$d->workshops = '';
				foreach ($d->registration_data->workshops as $workshop) {
					$d->workshops .= $workshop->code. ' ' .$workshop->name.', ';
				}
			}

			$d->payment = $d->ext_response ? json_decode($d->ext_response) : null;

			if ($d->registration_data->as == 'Institusi') {
				$d->pemesan = (object) array(
					'nama' => $d->org_name,
					'email' => $d->email,
					'cp' => $d->name,
					'telepon' => $d->phone,
					'alamat' => $d->address,
					'city_province' => $d->city.' '.$d->province,
				);
			}
			$data[] = (object) array(
				'nomor-transaksi' => ' '.$d->order_id,
				'nomor-registrasi' => ' '.$d->registration_is,
				'status-transaksi' => ' '.$d->status_name,
				'kategori-pendaftaran' => $d->registration_data->as,
				'jenis-peserta' => $d->registration_data->package_member,
				'nomor-anggota' => ' '.$d->registration_data->npa,
				'tgl-registrasi' => JHTML::_('date', $d->created_date, '%A, %d %b %Y %H:%M'),
				'nama-peserta' => strtoupper($d->user_name),
				'telepon-peserta' => ' '.$d->user_phone,
				'email-peserta' => $d->user_email,
				'paket-pendaftaran' => $d->registration_data->title,
				'nama-sponsor' => $d->pemesan->nama,
				'cp-sponsor' => $d->pemesan->nama,
				'telepon-sponsor' => ' '.$d->pemesan->telepon,
				'email-sponsor' => $d->pemesan->email,
				'alamat-sponsor' => $d->pemesan->alamat,
				'jenis-paket' => $d->registration_data->title,
				'workshop' => $d->workshops,
				'harga' => $d->price,
				'tanggal-pembayaran' => $d->payment->settlement_time,
				'jenis-pembayaran' => $d->method.' '.$d->channel
			);
		}

		/*echo '<pre>';
		print_r($data);
		echo '</pre>';
		die();*/


		return $this->composeExcel($excel_config, $data);

	}

	function composeExcel($config, $data)
	{
		$alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("PIN PAPDI")
			->setLastModifiedBy("")
			->setTitle("Office 2007, XLSX ".($config->title || 'XLSX Generated report'))
			->setSubject($config->subject || "Office 2007 XLSX Generated report")
			->setDescription($config->description ||"Report generated from database")
			->setKeywords($config->keyword || "Generated report")
			->setCategory($config->category || "Uncategorized");

		$objPHPExcel->setActiveSheetIndex(0);


		$i=0; foreach ($data[0] as $key => $first_data) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($alpha[$i])->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->setCellValue($alpha[$i].'1', $key); $i++;
		}

		foreach ($data as $index => $row) {
			if($row){
				$ii=0; foreach ($row as $col) {
					$objPHPExcel->getActiveSheet()->setCellValue($alpha[$ii].($index + 2), $col); $ii++;
				};
			}
		}

		$objPHPExcel->getActiveSheet()->setTitle('$config');

		return $objPHPExcel;
	}

	function downloadReport($type)
	{
		jimport('PHPExcel');
		global $mainframe;
		$user = JFactory::getUser();

		// print_r($data); die();

		if ($user->gid < 25) {
			JFactory::getApplication()->enqueueMessage('Forbidden!', 'ERROR');
			return false;
		}

		switch ($type) {
			case 'master':
				$objPHPExcel = $this->getMasterData('all');
				break;
		}

	/*	echo '<pre>';
		print_r($objPHPExcel);
		echo '</pre>';
		die();*/

		if (!$objPHPExcel) {
			return;
		}


	/*	$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setTitle('Invoice');
		foreach (range('A', 'G') as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
		}
		$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()
			->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

		$objPHPExcel->getActiveSheet()->getStyle('B')->getNumberFormat()
			->setFormatCode('dd/mm/yyyy');*/


		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$type.'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}

	function getFiles($folderPath)
	{

		$path = JPATH_BASE . DS . 'files'. DS . $folderPath;
		$folder = JFolder::files($path);
		if(!$folder){
			return array();
		}
		$files = array();

		foreach ($folder as $file){
			$uri = $path.DS.$file;
			$date = filemtime($uri);
			$size = filesize($uri);
			$thumnail = 0;
			if (preg_match('#\.(jpg|jpeg|bmp|gif|tiff|png)#i', $file)) {
				$thumnail = 1;
			}

			$files[] = (object) array(
				'filename' => $file,
				'url' => str_replace('\\', '/', JURI::base().'files/'.$folderPath.'/'.$file),
				'size' => $size,
				'modified' => $date,
				'thumbnail' => $thumnail
			);
		}

		return $files;
	}

	function redirect($cid = null, $url = null, $message = null, $msgType = null, $order_id = null)
	{
		global $mainframe;
		if ($cid) {
			require_once(JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');
			jimport('joomla.application.component.helper');
			$url = JRoute::_(ContentHelperRoute::getArticleRoute($cid));
			if ($order_id) {
				$url .= '?order_id=' . $order_id;
			}
		}

		$mainframe->redirect($url, $message = null, $msgType = null, true);
	}

}

?>