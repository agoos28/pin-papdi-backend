<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.helper');
require_once(JPATH_COMPONENT . DS . 'controller.php');
// Create the controller
$controller = new eventorgController();
// Perform the Request task
$controller->execute(JRequest::getVar('task', NULL, 'default', 'cmd'));
// Redirect if set by the controller
$controller->redirect();