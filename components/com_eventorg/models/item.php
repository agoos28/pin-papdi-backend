<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.model');

/**
 * EventOrg Component Item Model
 * @package EventOrg
 */
class EventOrgModelItem extends JModel {
  /**
   * Item id
   *
   * @var int
   */
  var $_id = NULL;
  /**
   * Item data
   *
   * @var array
   */
  var $_data = NULL;
  
  /**
   * Constructor
   *
   * @since 1.5
   */
  function __construct()
  {
	parent::__construct();
	$id = JRequest::getVar('id', 0, '', 'int');
	$this->setId((int)$id);
  }
  
  /**
   * Method to set the item identifier
   *
   * @access public
   * @param int Item identifier
   */
  function setId($id)
  {
	// Set item id and wipe data
	$this->_id = $id;
	$this->_data = NULL;
  }
  
  /**
   * Method to get a item
   *
   * @since 1.5
   */
  function &getData()
  {
	// Load the item data
	if ($this->_loadData())
	{
	  // Initialize some variables
	  $user = &JFactory::getUser();
	  // Make sure the item is published
	  if ( ! $this->_data->published)
	  {
		JError::raiseError(404, JText::_("Resource Not Found"));
		return FALSE;
	  }
	  // Check to see if the category is published
	  if ( ! $this->_data->cat_pub)
	  {
		JError::raiseError(404, JText::_("Resource Not Found"));
		return;
	  }
	  // Check whether category access level allows access
	  if ($this->_data->cat_access > $user->get('aid', 0))
	  {
		JError::raiseError(403, JText::_('ALERTNOTAUTH'));
		return;
	  }
	}
	//else $this->_initData();
	return $this->_data;
  }
  
  /**
   * Method to load content item data
   *
   * @access private
   * @return boolean True on success
   */
  function _loadData()
  {
	// Lets load the content if it doesn't already exist
	if (empty($this->_data))
	{
	  $query = 'SELECT w.*, cc.title AS category,' .
		' cc.published AS cat_pub, cc.access AS cat_access' .
		' FROM #__eventorg AS w' .
		' LEFT JOIN #__categories AS cc ON cc.id = w.catid' .
		' WHERE w.id = ' . (int)$this->_id;
	  $this->_db->setQuery($query);
	  $this->_data = $this->_db->loadObject();
	  return (boolean)$this->_data;
	}
	return TRUE;
  }
  
  /**
   * Method to increment the hit counter for the item
   *
   * @access public
   * @return boolean True on success
   * @since 1.5
   */
  function hit()
  {
	global $mainframe;
	
	if ($this->_id)
	{
	  $item = &$this->getTable();
	  $item->hit($this->_id);
	  return TRUE;
	}
	return FALSE;
  }
}