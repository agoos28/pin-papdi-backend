<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.model');
jimport('joomla.filesystem.file');
JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_cckjseblod' . DS . 'models');

/**
 * EventOrg Component Item Model
 * @package EventOrg
 */
class EventOrgModelManage extends JModel
{
	/**
	 * Item id
	 *
	 * @var int
	 */
	var $_id = NULL;
	/**
	 * Item data
	 *
	 * @var object
	 */
	var $_data = NULL;

	var $cart_model = null;

	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();
		$this->cart_model = JModel::getInstance('cart', 'CCKjSeblodModel');
		$id = JRequest::getVar('id', 0, '', 'int');
		$this->setId((int)$id);
	}

	/**
	 * Method to set the item identifier
	 *
	 * @access public
	 * @param int Item identifier
	 */
	function setId($id)
	{
		// Set item id and wipe data
		$this->_id = $id;
		$this->_data = new stdClass();
	}

	function getDataFromContentId($id)
	{
		$query = 'SELECT a.id ' .
			' FROM #__event AS a' .
			' WHERE a.content_id = ' . (int)$id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadResult();

		$this->setId((int)$result);
		return $this->getData();
	}

	/**
	 * Method to get a item
	 *
	 * @since 1.5
	 */
	function &getData()
	{
		$user = &JFactory::getUser();
		if ($user->get('gid', 0) === 25) {
			JError::raiseError(403, JText::_('ALERTNOTAUTH'));
			return;
		}
		$this->_loadEvent();
		$this->_loadActivities();
		$this->_loadTimelines();
		$this->_loadWorkshops();
		/*if ($this->_loadEvent())
			{
					JError::raiseError(404, 'Event not found');
					return FALSE;
			}*/
		//else $this->_initData();
		return $this->_data;
	}

	function _loadEvent()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data->event)) {
			$query = 'SELECT a.*, b.introtext ' .
				' FROM #__event AS a' .
				' LEFT JOIN #__content AS b ON b.id = a.content_id' .
				' WHERE a.id = ' . (int)$this->_id;
			$this->_db->setQuery($query);
			$this->_data->event = $this->_db->loadObject();
			return $this->_data->event;
		}
	}

	function getRegisteredCount($activity_id) {
		$query = 'SELECT count(*)'.
			' FROM #__event_attendance as a'.
			' LEFT JOIN #__event_booking AS b ON b.id = a.booking_id'.
			' LEFT JOIN #__transaction_order AS c ON c.id = b.order_id'.
			' WHERE a.activity_id = ' . (int)$activity_id.
			' AND c.status IN (12, 13)';
		$this->_db->setQuery($query);
		return $this->_db->loadResult();
	}

	function _loadTimelines()
	{
		$query = 'SELECT a.*' .
			' FROM #__event_timeline AS a' .
			' WHERE a.event_id = ' . (int)$this->_id .
			' ORDER BY a.start_time ASC, a.name ASC';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();

		for ($i = 0; $i < (count($result)); $i++) {
			$this->_data->schedule[$i] = $result[$i];
			$this->_data->schedule[$i]->activities = $this->_loadActivities($result[$i]->id);
		}
		return $this->_data->schedule;
	}

	function _loadWorkshops()
	{
		$query = 'SELECT a.*' .
			' FROM #__event_timeline AS a' .
			' WHERE a.event_id = ' . (int)$this->_id .
			' ORDER BY a.name ASC, a.start_time ASC';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();

		for ($i = 0; $i < (count($result)); $i++) {
			$this->_data->workshops[$i] = $result[$i];
			$this->_data->workshops[$i]->activities = $this->_loadActivities($result[$i]->id);
		}
		return $this->_data->workshops;
	}

	function loadTimeline($id = 0)
	{
		$query = 'SELECT a.*' .
			' FROM #__event_timeline AS a' .
			' WHERE a.id = ' . (int)$id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();

		return $result;
	}

	function searchTimeline()
	{
		$keyword = JRequest::getVar('search');
		$query = 'SELECT a.id AS id, a.name AS text' .
			' FROM #__event_timeline AS a' .
			' WHERE a.name LIKE "%' . $keyword .'%"'.
			' ORDER BY a.name ASC';
		$this->_db->setQuery($query, 0, 30);
		$result = new stdClass();
		$result->results = $this->_db->loadObjectList();
		$include = (object) array('text' => $keyword, 'id' => $keyword);
		array_unshift($result->results, $include);
		return $result;
	}

	function _loadActivities($timeline_id = NULL)
	{

		$where = ' WHERE a.event_id = ' . (int)$this->_id;
		if ($timeline_id) {
			$where .= ' AND a.timeline_id = ' . (int)$timeline_id;
		}else{
			$where .= ' AND a.timeline_id = 0 ';
		}

		$query = 'SELECT a.*, b.introtext ' .
			' FROM #__event_activity AS a' .
			' LEFT JOIN #__content AS b ON b.id = a.content_id' . $where.
			' ORDER BY a.ordering ASC';
		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();
		if ($timeline_id) {
			foreach ($result as $item) {
				$item->registered = $this->getRegisteredCount($item->id);
			}
			return $result;
		}
		for ($i = 0; $i < (count($result)); $i++) {
			$this->_data->activities[$i] = $result[$i];
			$this->_data->activities[$i]->persons = $this->getPersons($result[$i]->id);
		}
		$this->_data->activities = $result;
		return $this->_data->activities;
	}

	function getWorkshops(){
		$query = 'SELECT a.* ' .
			' FROM #__event_timeline AS a' .
			' WHERE a.name LIKE "%workshop%"';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		// echo '<pre>'; print_r($result); echo '</pre>';
		return $result;
	}

	function getPackages($event_id, $all = false){
		$query = 'SELECT a.*, b.title AS group_name' .
			' FROM #__event_package AS a' .
			' LEFT JOIN #__event_package_group AS b ON b.id = a.package_group'.
			' WHERE a.event_id = ' . (int)$event_id;

		if(!$all){
			$query .= ' AND b.start_date < CURRENT_DATE AND CURRENT_DATE < b.end_date';
		}
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getPackagesAdmin($event_id){
		$query = 'SELECT a.*, b.title AS group_name' .
			' FROM #__event_package AS a' .
			' LEFT JOIN #__event_package_group AS b ON b.id = a.package_group'.
			' WHERE a.event_id = ' . (int)$event_id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getPackage($id){
		$query = 'SELECT a.*' .
			' FROM #__event_package AS a' .
			' WHERE a.id = ' . (int)$id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		return $result;
	}

	function getPackageGroup($id){
		$query = 'SELECT a.*' .
			' FROM #__event_package_group AS a WHERE a.id = '.$id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		return $result;
	}

	function getPackageGroups($event_id){
		$query = 'SELECT a.*' .
			' FROM #__event_package_group AS a WHERE a.event_id = '.$event_id.' ORDER BY a.start_date ASC';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getActivityByType($id) {
		$query = 'SELECT a.*, b.introtext, c.name AS typename ' .
			' FROM #__event_activity AS a' .
			' LEFT JOIN #__content AS b ON b.id = a.content_id'.
			' LEFT JOIN #__event_activity_type AS c ON c.id = '. (int)$id.
			' WHERE a.activity_type = '. (int)$id;
		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getActivity($id) {

		$query = 'SELECT a.*, b.name AS timeline_name, b.start_time, b.end_time, b.activity_type, c.name AS typename ' .
			' FROM #__event_activity AS a' .
			' LEFT JOIN #__event_timeline AS b ON b.id = a.timeline_id'.
			' LEFT JOIN #__event_activity_type AS c ON c.id = b.activity_type'.
			' WHERE a.id = '. (int)$id;
		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();
		return $result;
	}

	function getPersons($activityId){
		$query = 'SELECT a.*' .
			' FROM #__event_activity_person AS a' .
			' WHERE a.activity_id = ' . (int)$activityId;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();

		return $result;
	}

	function getUserList(){
		$keyword = JRequest::getVar('search');
		$query = 'SELECT a.name AS id, a.name AS text' .
			' FROM #__users AS a' .
			' WHERE a.name LIKE "%' . $keyword .'%"'.
			' ORDER BY a.name ASC';
		$this->_db->setQuery($query, 0, 10);
		$result = new stdClass();
		$result->results = $this->_db->loadObjectList();
		$include = (object) array('text' => $keyword, 'id' => $keyword);
		array_unshift($result->results, $include);
		return $result;
	}

	function getActivityType(){
		$keyword = JRequest::getVar('search');
		$query = 'SELECT a.id AS id, a.name AS text' .
			' FROM #__event_activity_type AS a' .
			' ORDER BY a.name ASC';
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getParticipants($activity_id){
		$query = 'SELECT a.checked_in, a.checked_out, a.booking_id, b.order_id, b.user_name, b.user_email, c.status, d.name AS status_name' .
			' FROM #__event_attendance AS a' .
			' LEFT JOIN #__event_booking AS b ON b.id = a.booking_id'.
			' LEFT JOIN #__transaction_order AS c ON c.id = b.order_id'.
			' LEFT JOIN #__status_code AS d ON d.id = c.status'.
			' WHERE a.activity_id = '. (int)$activity_id.
			' AND c.status IN (12, 13)'.
			' ORDER BY c.id DESC';
		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();
		return $result;
	}

	function getParticipantsCount($id) {
		$query = 'SELECT c.name, count(*) AS count'.
			' FROM #__event_booking as a' .
			' LEFT JOIN #__transaction_order AS b ON b.id = a.order_id'.
			' LEFT JOIN #__status_code AS c ON c.id = b.status'.
			' WHERE a.event_id = '. (int)$id.
			' GROUP BY b.status';
		$this->_db->setQuery($query);

		$result = $this->_db->loadObjectList();
		$ret = array();
		$total = 0;
		foreach ($result as $item) {
			$ret[strtolower($item->name)] = $item->count;
			$total += $item->count;
		}
		$ret['total'] = $total;
		return (object) $ret;
	}

	function getAttendance($activity_id, $booking_id){
		$query = "SELECT * FROM #__event_attendance ".
			" WHERE booking_id = $booking_id AND activity_id = $activity_id";
		$this->_db->setQuery($query);

		$result = $this->_db->loadObject();
		return $result;
	}

	function storeTimeline()
	{

	}

	function getImages(){

	}

	function getFiles($id)
	{

		$path = JPATH_BASE . DS . 'files'. DS .'activity'.DS.$id.DS;
		$folder = JFolder::files($path);
		if(!$folder){
			return array();
		}
		$files = array();

		foreach ($folder as $file){
			$uri = $path.$file;
			$date = filemtime($uri);
			$size = filesize($uri);
			$thumnail = 0;
			if (preg_match('#\.(jpg|jpeg|bmp|gif|tiff|png)#i', $file)) {
				$thumnail = 1;
			}

			$files[] = (object) array(
				'filename' => $file,
				'url' => JURI::base().'files/activity/'.$id.'/'.$file,
				'size' => $size,
				'modified' => $date,
				'thumbnail' => $thumnail
			);
		}

		return $files;
	}

	function getBooking($id){
		$query = "SELECT a.* FROM #__event_booking AS a WHERE a.id = ". $id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();

		return $result;
	}

	function getBookings($event_id){
		$query = "SELECT a.id, a.user_name, a.user_email, a.workshops, c.title AS event_title, c.start_date AS event_start, c.end_date AS event_end". 
			" FROM #__event_booking AS a".
			" LEFT JOIN #__transaction_order AS b ON b.id = a.order_id".
			" LEFT JOIN #__event AS c ON c.id = a.event_id".
		 	" WHERE a.event_id = ". $event_id.
		 	" AND b.status = 12".
			" ORDER BY a.id ASC";
			
		$this->_db->setQuery($query);
		$result = $this->_db->loadObjectList();
		foreach($result as $item) {
			$item->workshops = json_decode($item->workshops);
		}
		return $result;
	}

	function getUser($email){
		$query = 'SELECT a.*, b.*, b.user_id AS id' .
			' FROM #__users AS a' .
			' LEFT JOIN #__user_profile AS b ON b.user_id = a.id'.
			" WHERE a.email = '$email'";

		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();

		return $result;
	}

	function updateAttendance($data = array()){
		JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_eventorg' . DS . 'tables');
		$row = JTable::getInstance('EventAttendance');
		if (!$row->bind($data)) {
			$this->setError($row->getError());
		}
		if (!$row->store()) {
			$this->setError($row->getError());
			print_r($row->getError()); die();
		}
	}


	function captureAttendance($activityId, $bookingId){
		$data = new stdClass();
		$data->activity = $this->getActivity($activityId);
		$data->booking = $this->getBooking($bookingId);
		$data->attendance = $this->getAttendance($activityId, $bookingId);

		if(!$data->activity){
			$data->error = 'Aktifitas tidak ditemukan';
			return $data;
		}

		if(!$data->booking){
			$data->error = 'Booking tidak ditemukan';
			return $data;
		}

		if(!$data->attendance){
			$data->error = 'Anda tidak terdaftar di kegiatan ini';
			return $data;
		}

		$nowTime = new DateTime();
		$startTime = new DateTime($data->activity->start_time);
		$endTime = new DateTime($data->activity->end_time);


		if(!$data->attendance->checked_in){

			if($nowTime > $endTime){
				$data->error = true;
				$data->message = 'Maaf, kegiatan sudah selesai';
				return $data;
			}

			$diff = strtotime($data->activity->start_time) - strtotime('now');

			if($diff > (60*30)){
				$data->error = true;
				$data->message = 'Cek-in hanya dapat dilakukan 30 menit sebelum kegiatan.';
				return $data;
			}

			$this->updateAttendance(array(
				'id' => $data->attendance->id,
				'checked_in' => $data->nowTime->format('Y-m-d H:i:s')
			));
			$data->attendance->checked_in =  $data->nowTime->format('Y-m-d H:i:s');
			$data->message = 'Terima kasih telah hadir';
			return $data;

		}else if(!$data->attendance->checked_out){

			$diff = strtotime($data->activity->end_time) - strtotime('now');

			if($diff > 0){
				$data->error = true;
				$data->message = 'Cek-out hanya dapat dilakukan setelah kegiatan selesai.';
				return $data;
			}

			$this->updateAttendance(array(
				'id' => $data->attendance->id,
				'checked_out' => $data->now->format('Y-m-d H:i:s')
			));
			$data->attendance->checked_out =  $data->nowTime->format('Y-m-d H:i:s');
			return $data;

		}else{
			$data->error = true;
			$data->message = 'Anda sudah melakukan check-in dan check-out';
			return $data;
		}

	}

	function createEventReminder($booking)
	{

		$workshops = '';
		foreach ($booking->workshops as $workshop)
		{
			$timeline  = $this->cart_model->getTimeline($workshop->timeline_id);
			$workshops .= '<p><strong>' . $workshop->code . ' - ' . $workshop->name . '</strong>';
			$workshops .= '<br/>' . JHTML::_('date', $timeline->start_time, '%A, %d %b %Y %H:%M') . ' s/d ' . JHTML::_('date', $timeline->end_time, '%H:%M') . '</p>';
		}

		$contentData = array(
			'{name}'       => $booking->user_name,
			'{event}'      => $booking->event_title,
			'{event_date}' => JHTML::_('date', $booking->event_start).' - '.JHTML::_('date', $booking->event_end),
			'{email}'      => $booking->user_email,
			'{workshops}'  => $workshops, 
		);

		$template = $this->cart_model->getRawContent(794);

		$html = str_replace('img src="images', 'img src="' . JURI::base() . 'images', $template->wysiwyg_text);
		$html = str_replace('url(\'images/', 'url(\'' . JURI::base() . 'images/', $html);
		$html = strtr($html, $contentData);

		$css     = '<style type="text/css"></style>';
		$content = '<html><head>' . $css . '</head><body>' . $html . '</body></html>';

		$subject = 'Reminder Peserta '.$booking->event_title;

		$data = array(
			'destination' => $booking->user_email,
			'subject' => $subject,
			'content' => $content
		);
		
		JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_eventorg' . DS . 'tables');
		$row = JTable::getInstance('BatchEmail');
		
		if (!$row->bind($data)) {
			$this->setError($row->getError());
		}
		if (!$row->store()) {
			$this->setError($row->getError());
			print_r($row->getError());
		}
	}

}