<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');

class eventorgController extends JController
{
	/**
	 * Method to show a eventorg view
	 */
	function display()
	{
		if (!JRequest::getCmd('view')) {
			JRequest::setVar('view', 'categories');
		}
		//update the hit count for the item

		parent::display();
	}

	function update_activities()
	{

		$post = (object)JRequest::get('post');

		for ($i = 0; $i < count($post->items); $i++) {
			$params = $post->items[$i];
			$params['type'] = 'activity';

			if (!$this->store($params)) {
				// print_r($params);
			}
		}
		die();
	}

	function store($params = null)
	{
		global $mainframe;
		$post = JRequest::get('post');
		$type = JRequest::getVar('type');


		JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_eventorg' . DS . 'tables');
		$row = JTable::getInstance($type);

		if($post['package_options']){
			$post['package_options'] = json_encode($post['package_options']);
		}
		if($post['package_rules']){
			$post['package_rules'] = json_encode($post['package_rules']);
		}


		if (!$params) {
			$data = $post;
		} else {
			$data = $params;
		}

		if($post['description']){
			$data['description'] = JRequest::getVar('description', null, 'POST', 'STRING',
				JREQUEST_ALLOWRAW);
		}


		if (!$row->bind($data)) {
			$this->setError($row->getError());
		}


		$row->id = (int)$row->id;
		$isNew = ($row->id < 1);

		if (!$row->store()) {
			$this->setError($row->getError());
			print_r($row->getError()); die();
		}

		if ($isNew) {
			$this->id = $row->id;
		}

		if ($params) {
			return true;
		} else {
			parent::display();
		}

	}

	function remove()
	{
		global $mainframe;
		$type = JRequest::getVar('type');
		$id = JRequest::getVar('id', 0, 'post', 'string');

		JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_eventorg' . DS . 'tables');
		$row =& JTable::getInstance($type);

		JArrayHelper::toInteger($ids);
		if (!$id) {
			JError::raiseError(500, JText::_('Select an item to delete'));
		}
		if (!$row->delete($id)) {
			JError::raiseError(500, $row->getError());
			return false;
		}
		die();
		// parent::display();
	}

	function getBidang($name){
		$result = array();
		$bidang = array(
			//'SpPD' => 'Spesialis Penyakit Dalam',
			'K-AI' => 'Alergi-Immunologi Klinik',
			'K-GEH' => 'Gastroenterologi-Hepatologi',
			'K-Ger' => 'Geriatri',
			'K-GH' => 'Ginjal-Hipertensi',
			'K-HOM' => 'Hematologi-Onkologi Medik',
			'K-H' => 'Hepatologi',
			'K-KV' => 'Kardiovaskular',
			'K-EMD' => 'Endokrin-Metabolik-Diabetes',
			'K-Psi' => 'Psikosomatik',
			'K-P' => 'Pulmonologi',
			'K-R' => 'Reumatologi',
			'K-PTI' => 'Penyakit Tropik-Infeksi'
		);

		$arr = explode(',', $name);

		foreach ($arr as $item){
			if($bidang[trim($item)]){
				$result[] = $bidang[trim($item)];
			}
		}

		return $result;

	}

	function checkNpa(){
		$db = JFactory::getDBO();
		$npa = JRequest::getVar('npa');
		$query = 'SELECT a.*' .
			' FROM #__npa AS a' .
			' WHERE a.id = "' . $npa.'"';
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result){
			$result->bidang = $this->getBidang($result->name);
			if($result->email){
				$emails = explode('/', $result->email);
				$result->email = array();
				foreach ($emails as $email){
					$result->email[] = trim($email);
				}
			}
			echo json_encode($result);
		}else{
			echo 0;
		}
		die();
	}

	function getPackage(){
		$db = JFactory::getDBO();
		$id = JRequest::getVar('id');
		$query = 'SELECT a.*' .
			' FROM #__event_package AS a' .
			' WHERE a.id = "' . $id .'"';
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result){
			echo json_encode($result);
		}else{
			echo 0;
		}
		die();
	}

	function getTimeline($id){
		$db = JFactory::getDBO();
		$result = new stdClass();
		$query = 'SELECT a.*' .
			' FROM #__event_timeline AS a' .
			' WHERE a.id = "' . $id .'"';
		$db->setQuery($query);
		$result->group = $db->loadObject();
		$result->group->date = JHtml::_('date', $result->group->start_time, '%A, %d %B %Y %H:%M').' - '.JHtml::_('date', $result->group->end_time, '%H:%M');
		$result->selections = $this->getActivities($id);
		return $result;
	}

	function getActivities($timeline_id){
		$model = $this->getModel('manage');
		$db = JFactory::getDBO();
		$query = 'SELECT a.*' .
			' FROM #__event_activity AS a' .
			' WHERE a.timeline_id = "' . $timeline_id .'" ORDER BY a.ordering ASC';
		$db->setQuery($query);
		$result = $db->loadObjectList();
		foreach ($result as $item) {
			$item->registered = $model->getRegisteredCount($item->id);
		}
		return $result;
	}

	function getPackageOptions(){
		$db = JFactory::getDBO();
		$id = JRequest::getVar('id');
		$result = new stdClass();
		$query = 'SELECT a.*' .
			' FROM #__event_package AS a' .
			' WHERE a.id = "' . $id .'"';
		$db->setQuery($query);
		$result->package = $db->loadObject();

		if($result->package && $result->package->package_options){
			$options = json_decode($result->package->package_options);
			for($i=0; $i < count($options); $i++){
				$option = $this->getTimeline($options[$i]);
				if($option){
					$result->options[] = $option;
				}

			}
		}

		if($result){
			echo json_encode($result);
		}else{
			echo 0;
		}
		die();
	}

	function deleteActivityFile()
	{
		$activityId = JRequest::getVar('activityId');
		$filename = JRequest::getVar('filename');
		$path = JPATH_BASE . DS . 'files' . DS . 'activity' . DS . $activityId . DS . $filename;
		if (!JFile::delete($path)) {
			$error = JError::getError();
			JError::raiseError('500', $error);
		}
		die();
	}

	function captureAttendance(){
		$activityId = JRequest::getVar('activityId');
		$bookingId = JRequest::getVar('bookingId');
		$model = $this->getModel('manage');

		$data = $model->captureAttendance($activityId, base64_decode($bookingId));

		echo json_encode($data);
		die();
	}

	function createEventReminder()
	{
		
		$eventId = JRequest::getVar('id');
		
		$model = $this->getModel('manage');

		$bookings = $model->getBookings($eventId);

		foreach($bookings as $booking) {
			$model->createEventReminder($booking);
		}
	
		die();
	}

	function composeExcel($config, $data)
	{
		$alpha = array('A','B','C','D','E','F','G','H','I','J','K', 'L','M','N','O','P','Q','R','S','T','U','V','W','X ','Y','Z');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("PIN PAPDI")
			->setLastModifiedBy("")
			->setTitle("Office 2007, XLSX ".($config->title || 'XLSX Generated report'))
			->setSubject($config->subject || "Office 2007 XLSX Generated report")
			->setDescription($config->description ||"Report generated from database")
			->setKeywords($config->keyword || "Generated report")
			->setCategory($config->category || "Uncategorized");

		$objPHPExcel->setActiveSheetIndex(0);


		$i=0; foreach ($data[0] as $key => $first_data) {
		$objPHPExcel->getActiveSheet()->getColumnDimension($alpha[$i])->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue($alpha[$i].'1', $key); $i++;
	}

		foreach ($data as $index => $row) {
			if($row){
				$ii=0; foreach ($row as $col) {
					$objPHPExcel->getActiveSheet()->setCellValue($alpha[$ii].($index + 2), $col); $ii++;
				};
			}
		}

		$objPHPExcel->getActiveSheet()->setTitle('$config');

		return $objPHPExcel;
	}

	function downloadReport()
	{
		jimport('PHPExcel');
		$user = JFactory::getUser();
		$model = $this->getModel('manage');
		$data = array();

		$id = JRequest::getVar('id');

		if ($user->gid < 25) {
			JFactory::getApplication()->enqueueMessage('Forbidden!', 'ERROR');
			return false;
		}

		$activity = $model->getActivity($id);
		$participant = $model->getParticipants($id);
		$excel_config = new stdClass();
		$excel_config->title = $activity->name;

		foreach ($participant as $d) {
			$data[] = (object) array(
				'id-transaksi' => ' '.$d->order_id,
				'nama' => ' '.strtoupper($d->user_name),
				'email' => ' '.$d->user_email,
				'status' => ' '.$d->status_name,
				'checked-in' => $d->checked_in,
				'checked-out' => $d->checked_out,
			);
		}

		$objPHPExcel = $this->composeExcel($excel_config, $data);

		if (!$objPHPExcel) {
			return;
		}

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$activity->code.'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you're serving to IE over SSL, then the following may be needed
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}