<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.view');

/**
 * @package EventOrg
 */
class EventOrgViewManage extends JView {
  function display($tpl = NULL)
  {

	global $mainframe;
	$mainframe->setTemplate = "admin";
	
	$pathway = &$mainframe->getPathway();
	$document = &JFactory::getDocument();
	$model =& $this->getModel();
	$layout = $this->getLayout();

	$menus = &JSite::getMenu();
	$menu = $menus->getActive();
	$pparams = &$mainframe->getParams('eventorg');

	$menu_params = new JParameter($menu->params);
	$document->setTitle($pparams->get('page_title'));



	if ($layout == 'dashboard') {
			$id = JRequest::getVar('id');
			$activities = $model->getDataFromContentId($id);
			$participant_count = $model->getParticipantsCount($activities->event->id);
			$this->assignRef('data', $activities);
			$this->assignRef('count', $participant_count);
	}

	if ($layout == 'activitydashboard') {
		if (JRequest::getVar('id')) {
			$activity = $model->getActivity(JRequest::getVar('id'));
			$participants = $model->getParticipants(JRequest::getVar('id'));
			$this->assignRef('activity', $activity);
			$this->assignRef('participants', $participants);
		}
	}

	if ($layout == 'scanqr') {
		$mainframe->setTemplate = "system";
		if (JRequest::getVar('id')) {
			$activity = $model->getActivity(JRequest::getVar('id'));
			$this->assignRef('activity', $activity);
		}
	}

		if ($layout == 'printtag') {
			$mainframe->setTemplate = "system";
			if (JRequest::getVar('id')) {
				$activity = $model->getActivity(JRequest::getVar('id'));
				$booking = $model->getBooking(base64_decode(JRequest::getVar('bookingId')));
				$participants = $model->getUser($booking->user_email);
				$this->assignRef('booking', $booking);
				$this->assignRef('activity', $activity);
				$this->assignRef('participants', $participants);
			}
		}

	parent::display($tpl);

  }
}