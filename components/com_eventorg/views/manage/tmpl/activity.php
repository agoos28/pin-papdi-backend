<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');

// uncomment to add custom css file for this view,
// save the css file to media/com_eventorg/css/yourcss.css
//$document =& JFactory::getDocument();
//$document->addStyleSheet($this->baseurl.'/media/com_eventorg/css/yourcss.css');
//use media/com_eventorg folder to store your css,js,images,...
?>
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
      <h4 id="myModalLabel" class="modal-title">Modal Heading</h4>
    </div>
    <div class="modal-body">
      <fieldset>
        <div class="row">
          <div class="form-group col-sm-3 form-group-label">
            <label class="control-label" for="name" data-column="name">
              Name
              <span class="required-mark" style="display: none">*</span>
              <div class="pgui-field-options btn-group btn-group-xs btn-group-justified" data-toggle="buttons">
              </div>
            </label>
          </div>
          <div class="form-group col-sm-9">
            <div class="col-input"  data-column="name">
              <input id="name" name="name" data-editor="text" data-field-name="name"
                     data-legacy-field-name="name" data-pgui-legacy-validate="true" class="form-control" value=""
                     type="text">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-sm-3 form-group-label">
            <label class="control-label" for="start_time" data-column="start_time">
              Start Time
              <span class="required-mark">*</span>
              <div class="pgui-field-options btn-group btn-group-xs btn-group-justified" data-toggle="buttons">
              </div>
            </label>
          </div>
          <div class="form-group col-sm-9">

            <div class="col-input"  data-column="start_time">
              <div class="input-group pgui-date-time-edit js-datetime-editor-wrap">
                <input id="start_time" name="start_time" data-editor="datetime"
                       data-field-name="start_time" data-required-error-message="Start Time is required."
                       data-validation="required" data-legacy-field-name="start_time" data-pgui-legacy-validate="true"
                       class="form-control" type="text" value="" data-picker-format="YYYY-MM-DD HH:mm:ss"
                       data-picker-show-time="false">
                <span class="input-group-addon" style="cursor: pointer">
        <span class="icon-datetime-picker"></span>
    </span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">


          <div class="form-group col-sm-3 form-group-label">

            <label class="control-label" for="end_time" data-column="end_time">
              End Time

              <span class="required-mark" style="display: none">*</span>
              <div class="pgui-field-options btn-group btn-group-xs btn-group-justified" data-toggle="buttons">

              </div>
            </label>
          </div>
          <div class="form-group col-sm-9">

            <div class="col-input"  data-column="end_time">
              <div class="input-group pgui-date-time-edit js-datetime-editor-wrap">
                <input id="end_time" name="end_time" data-editor="datetime"
                       data-field-name="end_time" data-legacy-field-name="end_time" data-pgui-legacy-validate="true"
                       class="form-control" type="text" value="" data-picker-format="YYYY-MM-DD HH:mm:ss"
                       data-picker-show-time="false">
                <span class="input-group-addon" style="cursor: pointer">
        <span class="icon-datetime-picker"></span>
    </span>
              </div>
            </div>
          </div>
        </div>
      </fieldset>
    </div>
    <div class="modal-footer">
      <button data-dismiss="modal" class="btn btn-default" type="button">
        Close
      </button>
      <button class="btn btn-primary" type="submit">
        Save changes
      </button>
    </div>
  </div>
  <!-- /.modal-content -->
</div>
