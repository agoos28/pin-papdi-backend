<?php 

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


  
// uncomment to add custom css file for this view,
// save the css file to media/com_eventorg/css/yourcss.css
  //$document =& JFactory::getDocument();
  //$document->addStyleSheet($this->baseurl.'/media/com_eventorg/css/yourcss.css');
  //use media/com_eventorg folder to store your css,js,images,...
  ?>
<h1><?php echo $this->item->title; ?> </h1>
<div>

<?php if( $this->item->text ):?>
<h3>Text</h3>
<?php echo $this->item->text; ?>
<?php endif; ?>


<?php if( $this->item->picture ):?>
<h3>Picture</h3>
<?php echo $this->item->picture; ?>
<?php endif; ?>


<?php if( $this->item->date ):?>
<h3>Date</h3>
<?php echo $this->item->date; ?>
<?php endif; ?>
</div>