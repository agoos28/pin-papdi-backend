<?php
/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/
// no direct access
defined('_JEXEC') or die('Restricted access');
// uncomment to add custom css file for this view,
// save the css file to media/com_eventorg/css/yourcss.css
//$document =& JFactory::getDocument();
//$document->addStyleSheet($this->baseurl.'/media/com_eventorg/css/yourcss.css');
//use media/com_eventorg folder to store your css,js,images,...
function renderActivityItem($activity)
{
	if (!$activity) {
		return '';
	}
	$persons = '';
	if (count($activity->persons)) {
		$persons = implode(', ', $activity->persons);
	}
	$activityUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=activity&format=raw&activityId='.$activity->id);
	$html =
		'<li class="dd-item dd3-item" id="activity_' . $activity->id .
		'" data-id="' . $activity->id .
		'" data-parentid="' . $activity->timeline_id .
		'" data-index="' . $activity->order .'">' .
		'<div class="dd-handle dd3-handle"></div>' .
		'<div class="dd3-content clearfix">' .
		'<div class="pull-right">' .
		'<a class="btn btn-xs btn-default ajax-modal" href="'.$activityUri.'">Edit</a> ' .
		'<a class="btn btn-xs btn-default delete-activity" href="#" data-id="' . $activity->id .'"><i class="fa fa-times"></i></a>' .
		'</div>' .
		'<div class="bold-text">' . $activity->name . '</div>' .
		'<span class="text-light text-extra-small">' . $persons . '</span>' .
		'</div>' .
		'</li>';

	return $html;
}

function renderScheduleItem($schedule)
{
	$html = '';
	if (!$schedule) {
		return '';
	}
	for ($i = 0; $i < count($schedule); $i++) {
		$editUrlri = JRoute::_( 'index.php?option=com_eventorg&view=manage&layout=schedule&format=raw&scheduleId='.$schedule[$i]->id.'&eventId='. $schedule[$i]->event_id);
		$html .=
			'<div id="schedule_' . $schedule[$i]->id . '" data-id="' . $schedule[$i]->id . '" class="list-group">' .
			'<div class="list-group-item no-padding clearfix">' .
			'<div class="pull-left padding-10">' . $schedule[$i]->name . '</div>' .
			'<div class="pull-right">' .
			'<div class="inline-block padding-10 border-right border-light">' .
			'<span class="bold-text text-small"><i class="fa fa-clock-o"></i> ' . JHTML::_('date', $schedule[$i]->start_time, '%H:%M') . '</span>' .
			'<span class="text-light text-extra-small"> ' . JHTML::_('date', $schedule[$i]->start_time, '%d/%m/%y') . '</span>' .
			'</div>' .
			'<div class="inline-block padding-10 border-right border-light">' .
			'<span class="bold-text text-small"><i class="fa fa-clock-o"></i> ' . JHTML::_('date', $schedule[$i]->end_time, '%H:%M') . '</span>' .
			'<span class="text-light text-extra-small"> ' . JHTML::_('date', $schedule[$i]->end_time, '%d/%m/%y') . '</span>' .
			'</div>' .
			'<div class="inline-block padding-5">' .
			'<a class="ajax-modal btn btn-default btn-xs" href="' . $editUrlri . '"><i class="fa fa-plus"></i> Edit</a> ' .
			'<a class="btn btn-default btn-xs delete-schedule" data-id="' . $schedule[$i]->id . '" href="#"><i class="fa fa-times"></i></a>' .
			'</div>' .
			'</div>' .
			'</div>' .
			'<div class="list-group-item partition-light-grey">' .
			'<div class="dd" id="nestable_' . $schedule[$i]->id . '">';
		if (count($schedule[$i]->activities)) {
			for ($ii = 0; $ii < count($schedule[$i]->activities); $ii++) {
				$html .= renderActivityItem($schedule[$i]->activities[$ii]);
			}
		} else {
			$html .= '<div class="dd-empty"></div>';
		}
		$html .= '</div></div></div>';
	}

	return $html;
}

$model = &$this->getModel();
if (JRequest::getVar('scheduleId')) {
	$formData = $model->loadTimeline();
}
if (JRequest::getVar('form')) {
	$isNew = TRUE;
	if (JRequest::getVar('scheduleId')) {
		$isNew = FALSE;
		$formData = $model->loadTimeline(JRequest::getVar('scheduleId'));
	}
	$scheduleUri = JRoute::_( 'index.php?option=com_eventorg&view=manage&layout=schedule' );
	$activityUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=activity&format=raw');
	?>
	<div class="modal-dialog">
		<form class="form-horizontal" id="schedule-form" method="post" action="<?php echo $scheduleUri; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 id="myModalLabel" class="modal-title">Event Schedule</h4>
				</div>
				<div class="modal-body p-l-30 p-r-30">
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="name" data-column="name">
								Name
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="col-input" data-column="name">
								<input id="name" name="name" data-editor="text" data-field-name="name"
											 data-legacy-field-name="name" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->name; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="activity_type">
							Type
						</label>
						<div class="col-sm-9">
							<div class="row gutter-5">
								<div class="col-xs-12">
									<select id="activity_type" class="form-control full-width" name="activity_type">
										<option value="">Default</option>
										<?php
										$activityType = $model->getActivityType();
										for($i = 0; $i < count($activityType); $i++) { ?>
											<option
												<?php if($activityType[$i]->id == $formData->activity_type){ echo 'selected="selected"'; } ?>
												value="<?php echo $activityType[$i]->id; ?>"><?php echo $activityType[$i]->text; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="start_time" data-column="start_time">
								Start Time
								<span class="required-mark">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">

							<div class="col-input" data-column="start_time">
								<div class="input-append date input-group datetime" id="datetimepicker"
										 data-date-format="yyyy-mm-dd hh:ii">
                                    <span class="add-on input-group-addon"><i class="icon-th"><i
																					class="fa fa-calendar"></i></i></span>
									<input class="form-control " type="text" id="start_time" name="start_time"
												 value="<?php echo $formData->start_time; ?>"/>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="end_time" data-column="end_time">
								End Time

								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">

								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="input-append date input-group datetime" id="datetimepicker"
									 data-date-format="yyyy-mm-dd hh:ii">
								<span class="add-on input-group-addon"><i class="icon-th"><i class="fa fa-calendar"></i></i></span>
								<input class="form-control " type="text" id="end_time" name="end_time"
											 value="<?php echo $formData->end_time; ?>"/>
							</div>
						</div>
					</div>
					<?php echo JHTML::_('form.token'); ?>
					<input type="hidden" name="task" value="store"/>
					<input type="hidden" name="type" value="timeline"/>
					<input type="hidden" name="event_id" value="<?php echo JRequest::getVar('eventId'); ?>"/>
					<input type="hidden" name="id" value="<?php echo JRequest::getVar('scheduleId', 0); ?>"/>
				</div>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">
					Close
				</button>
				<button class="btn btn-primary submit-ajax" type="submit">
					Save changes
				</button>
			</div>
		</form>
	</div>
	<script>
		var ajaxModal = $('#ajax-modal');
		$('#schedule-form').on('submit', function (e) {
			e.preventDefault();
			var form = $('#schedule-form');
			console.log(form.serialize());
			$.post(form.attr('action'), form.serialize(), function (data) {
				console.log(data);
				getSchedule();
				ajaxModal.modal('hide')
			});
		});
		ajaxModal.off('show');
		ajaxModal.on('show', function () {
			setTimeout(function () {
				// alert('asd')
				$('#schedule-form').find('.datetime').datetimepicker({
					autoclose: true,
					initialDate: new Date()
				});
			}, 1000)
		});

	</script>
<?php } else { ?>
	<?php echo renderScheduleItem($model->_loadTimelines()); ?>
<?php } ?>


