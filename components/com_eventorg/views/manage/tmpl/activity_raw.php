<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');

// uncomment to add custom css file for this view,
// save the css file to media/com_eventorg/css/yourcss.css
//$document =& JFactory::getDocument();
//$document->addStyleSheet($this->baseurl.'/media/com_eventorg/css/yourcss.css');
//use media/com_eventorg folder to store your css,js,images,...
$model = &$this->getModel();

$activityUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=activity&format=raw');

function renderActivityItems($activity)
{
	$html = '<ol class="dd-list">';
	if (!$activity) {
		return '';
	}
	for ($i = 0; $i < count($activity); $i++) {
		$persons = '';
		$activityUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=activity&format=raw&activityId=' . $activity[$i]->id);
		if (count($activity->persons)) {
			$persons = implode(', ', $activity->persons);
		}
		$html .=
			'<li class="dd-item dd3-item" id="activity_' . $activity[$i]->id .
			'" data-id="' . $activity[$i]->id .
			'" data-parentid="' . $activity[$i]->timeline_id .
			'" data-index="' . $activity[$i]->order . '">' .
			'<div class="dd-handle dd3-handle"></div>' .
			'<div class="dd3-content clearfix">' .
			'<div class="pull-right">' .
			'<a class="btn btn-xs btn-default ajax-modal" href="' . $activityUri . '">Edit</a> ' .
			'<a class="btn btn-xs btn-default delete-activity" href="#" data-id="' . $activity[$i]->id . '"><i class="fa fa-times"></i></a>' .
			'</div>' .
			'<div class="bold-text">' . $activity[$i]->name . '</div>' .
			'<span class="text-light text-extra-small">' . $persons . '</span>' .
			'</div>' .
			'</li>';
	}
	$html .= '</ol>';
	return $html;
}

if (JRequest::getVar('fragment') == 'typeSelect') {
	$list = $model->getActivityType();
	echo json_encode($list);
	die();
}
if (JRequest::getVar('fragment') == 'userSelect') {
	$list = $model->getUserList();
	echo json_encode($list);
	die();
}

if (JRequest::getVar('fragment') == 'list') {
	echo renderActivityItems($model->_loadActivities());
	die();
}

if (JRequest::getVar('activityId')) {
	$activity = $model->getActivity(JRequest::getVar('activityId'));
}


$files = $model->getFiles(JRequest::getVar('activityId'));
$files = json_encode($files);

?>
<style>
	.select2-container--bootstrap {
		z-index: 9999;
		width: 100% !important;
		font-size: 1em;
	}

	.select2-container--bootstrap .select2-selection--multiple .select2-selection__rendered {
		margin: 0 !important;
		list-style: none !important;
	}

	.select2-container .select2-search--inline .select2-search__field {
		box-shadow: none !important;
		border: none !important;
		background-color: unset !important;
	}

	.select2-container--bootstrap.select2-container--focus .select2-selection, .select2-container--bootstrap.select2-container--open .select2-selection, .select2-container--bootstrap .select2-dropdown {
		box-shadow: 0px 0px 2px #0000001a !important;
		border-color: #b2b2b2 !important;
		background-color: #fbfbfb !important;
	}

	.select2-container--bootstrap .select2-results > .select2-results__options {
		margin-bottom: 0 !important;
	}

	.select2-container--bootstrap .select2-selection--single .select2-selection__placeholder, .select2-container--bootstrap .select2-selection {
		color: #c8c8c8;
		font-size: 12px;
		line-height: 20px;
	}

	.select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
		color: #8b91a0;
		padding: 0;
	}
</style>
<div class="modal-dialog modal-lg">
	<form id="activity-form" action="<?php echo $activityUri; ?>">
		<div class="modal-content">
			<div class="modal-header">
				<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
				<h4 id="myModalLabel" class="modal-title">Event Activity</h4>
			</div>
			<div class="modal-body p-l-30 p-r-30">
				<div class="row gutter-30">
					<div class="col-md-6">
						<fieldset class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="code">
									Code
								</label>
								<div class="col-sm-9">
									<input type="text" name="code" placeholder="Code" id="code" class="form-control"
												 value="<?php echo $activity->code; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="seats">
									Capacity
								</label>
								<div class="col-sm-9">
									<input type="text" name="seats" placeholder="Capacity" id="seats" class="form-control"
												 value="<?php echo $activity->seats; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="name">
									Name
								</label>
								<div class="col-sm-9">
									<input type="text" name="name" placeholder="Event name" id="name" class="form-control"
												 value="<?php echo $activity->name; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="persons">
									Person
								</label>
								<div class="col-sm-9">
									<div class="row gutter-5">
										<div class="col-xs-12">
											<select id="persons" name="persons" class="form-control full-width" multiple="multiple"></select>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="location">
									Location
								</label>
								<div class="col-sm-9">
									<textarea name="location" placeholder="Location name" id="location" rows="3"
														class="form-control"><?php echo $activity->location; ?></textarea>
								</div>
							</div>
						</fieldset>
					</div>
					<div class="col-md-6">
						<fieldset class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-3 control-label" for="seats">
									Zoom link
								</label>
								<div class="col-sm-9">
									<input type="text" name="zoomlink" placeholder="Url Link" id="seats" class="form-control"
												 value="<?php echo $activity->zoomlink; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="seats">
									Pre Test Link
								</label>
								<div class="col-sm-9">
									<input type="text" name="pretestlink" placeholder="Url Link" id="seats" class="form-control"
												 value="<?php echo $activity->pretestlink; ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label" for="seats">
									Post Test Link
								</label>
								<div class="col-sm-9">
									<input type="text" name="posttestlink" placeholder="Url Link" id="seats" class="form-control"
												 value="<?php echo $activity->posttestlink; ?>">
								</div>
							</div>
							<label class="control-label" for="files">
								Downloadable Files
							</label>
							<div class="dropzone" style="min-height: 160px;" id="files">
							</div>
						</fieldset>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">
					Close
				</button>
				<button class="btn btn-primary" type="submit">
					Save changes
				</button>
				<?php echo JHTML::_('form.token'); ?>
				<input type="hidden" name="task" value="store"/>
				<input type="hidden" name="type" value="activity"/>
				<input type="hidden" name="event_id" value="<?php echo JRequest::getVar('eventId'); ?>"/>
				<input type="hidden" name="id" value="<?php echo JRequest::getVar('activityId', 0); ?>"/>
			</div>
		</div>
		</form>
		<!-- /.modal-content -->

	<script>
		function addPerson(name) {
			$.post('<?php echo $activityUri; ?>', form.serialize(), function (data) {
				console.log(data);
				getSchedule();
				ajaxModal.modal('hide')
			});
		}

		function deleteFile(filename) {
			console.log(filename);
			var params = {
				task: 'deleteActivityFile',
				activityId: <?php echo JRequest::getVar('activityId', 0); ?>,
				'<?php echo JUtility::getToken(); ?>': 1,
				filename: filename,
			};

			$.post('<?php echo $activityUri; ?>', params, function (data) {
				console.log(data);
			});
		}

		$("#activity_type").select2({
			theme: "bootstrap",
			allowClear: true,
			placeholder: 'Select activity type',
			ajax: {
				url: '<?php echo $activityUri; ?>',
				dataType: 'json',
				data: function (params) {
					var query = {
						search: params.term,
						fragment: 'typeSelect'
					};
					// Query parameters will be ?search=[term]&type=public
					return query;
				}
			}
		});
		$("#persons").select2({
			theme: "bootstrap",
			ajax: {
				url: '<?php echo $activityUri; ?>',
				dataType: 'json',
				data: function (params) {
					var query = {
						search: params.term,
						fragment: 'userSelect'
					};
					// Query parameters will be ?search=[term]&type=public
					return query;
				}
			}
		});


		var ajaxModal = $('#ajax-modal');
		var activityForm = $('#activity-form');
		activityForm.on('submit', function (e) {
			e.preventDefault();
			$.post(activityForm.attr('action'), activityForm.serialize(), function (data) {
				console.log(data);
				getActivities();
				ajaxModal.modal('hide')
          toastr.success('Saved');
			});
		});
		ajaxModal.off('show');
		ajaxModal.on('show', function () {
			var files = JSON.parse('<?php echo $files; ?>');
			console.log(files);
			setTimeout(function () {
				$("div#files").dropzone({
					url: '<?php echo $activityUri; ?>',
					paramName: "files",
					params: {
						task: 'store',
						type: 'activity',
						id: <?php echo JRequest::getVar('activityId', 0); ?>
					},
					maxFilesize: 5.0, // MB
					addRemoveLinks: true,
					autoProcessQueue: true,
					dictRemoveFileConfirmation: true,
					removedfile: function(file) {
						deleteFile(file.name);
						var dropZone = this;
						var _ref = file.previewElement;
						console.log(_ref);
						if(_ref != null){
							_ref.style.transition = 'all .6s ease';
							_ref.style.opacity = 0;
							setTimeout(function () {
								_ref.parentNode.removeChild(file.previewElement)
							}, 600)
						}
						//(_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
						return this._updateMaxFilesReachedClass();
					},
					autoDiscover: true,
					init: function(){
						var dropZone = this;
						files.map(function(item){
							var mockFile = { name: item.filename, size: item.size };
							dropZone.files.push(mockFile);
							dropZone.emit("addedfile", mockFile);
							if(item.thumbnail){
								dropZone.emit("thumbnail", mockFile,  item.url);
							}
							dropZone.emit("complete", mockFile);
						})

					}
			})
				;
			}, 1000)
		});
	</script>
