<?php
/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/
// no direct access
defined('_JEXEC') or die('Restricted access');
// uncomment to add custom css file for this view,
// save the css file to media/com_eventorg/css/yourcss.css
//$document =& JFactory::getDocument();
//$document->addStyleSheet($this->baseurl.'/media/com_eventorg/css/yourcss.css');
//use media/com_eventorg folder to store your css,js,images,...
function renderPackageOption($option)
{

	if (!$option) {
		return '';
	}
	$optionUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=activity&format=raw&activityId=' . $option->id);
	$html = '';

	return $html;
}

function renderPackageItem($packages)
{
	$html = '';
	if (!$packages) {
		return '';
	}
	for ($i = 0; $i < count($packages); $i++) {
		$editUrlri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=package&format=raw&id=' . $packages[$i]->id . '&eventId=' . $packages[$i]->event_id);
		$html .=
			'<div id="schedule_' . $packages[$i]->id . '" data-id="' . $packages[$i]->id . '" class="list-group m-b-5">' .
			'<div class="list-group-item no-padding clearfix">' .
			'<div class="pull-left padding-10"><strong>' . $packages[$i]->title . ' - ' . $packages[$i]->group_name . '</strong><br />'.$packages[$i]->description.'</div>' .
			'<div class="pull-right">' .
			'<div class="inline-block padding-5">' .
			'<a class="ajax-modal btn btn-default btn-xs" href="' . $editUrlri . '"><i class="fa fa-plus"></i> Edit</a> ' .
			'<a class="btn btn-default btn-xs delete-package" data-id="' . $packages[$i]->id . '" href="#"><i class="fa fa-times"></i></a>' .
			'</div>' .
			'</div>' .
			'</div>';
		if (count($packages[$i]->activities)) {
			for ($ii = 0; $ii < count($packages[$i]->activities); $ii++) {
				$html .= renderPackageOption($packages[$i]->activities[$ii]);
			}
		} else {
			$html .= '';
		}
		$html .= '</div>';
	}

	return $html;
}

$model = &$this->getModel();
if (JRequest::getVar('fragment') == 'timelineSelect') {
	$list = $model->searchTimeline();
	echo json_encode($list);
	die();
}
if (JRequest::getVar('scheduleId')) {
	$formData = $model->loadTimeline();
}
if (JRequest::getVar('form')) {
	$isNew = TRUE;
	if (JRequest::getVar('id')) {
		$isNew = FALSE;
		$formData = $model->getPackage(JRequest::getVar('id'));
	}
	$optionUri = JRoute::_('index.php?option=com_eventorg&view=manage&layout=package');
	?>
	<style>
		.select2-container--bootstrap {
			z-index: 9999;
			width: 100% !important;
			font-size: 1em;
		}

		.select2-container--bootstrap .select2-selection--multiple .select2-selection__rendered {
			margin: 0 !important;
			list-style: none !important;
		}

		.select2-container .select2-search--inline .select2-search__field {
			box-shadow: none !important;
			border: none !important;
			background-color: unset !important;
		}

		.select2-container--bootstrap.select2-container--focus .select2-selection, .select2-container--bootstrap.select2-container--open .select2-selection, .select2-container--bootstrap .select2-dropdown {
			box-shadow: 0px 0px 2px #0000001a !important;
			border-color: #b2b2b2 !important;
			background-color: #fbfbfb !important;
		}

		.select2-container--bootstrap .select2-results > .select2-results__options {
			margin-bottom: 0 !important;
		}

		.select2-container--bootstrap .select2-selection--single .select2-selection__placeholder, .select2-container--bootstrap .select2-selection {
			color: #c8c8c8;
			font-size: 12px;
			line-height: 20px;
		}

		.select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
			color: #8b91a0;
			padding: 0;
		}
	</style>
	<div class="modal-dialog">
		<form class="form-horizontal" id="package-form" method="post" action="<?php echo $optionUri; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 id="myModalLabel" class="modal-title">Event Package</h4>
				</div>
				<div class="modal-body p-l-30 p-r-30">
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="title" data-column="title">
								Name
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="col-input" data-column="name">
								<input id="title" name="title" data-editor="text" data-field-name="title"
											 data-legacy-field-name="name" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->title; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="package_group" data-column="package_group">
								Group
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="col-input" data-column="package_group">
								<select id="package_group" name="package_group" class="form-control">
								<?php
								$groups = $model->getPackageGroups(JRequest::getVar('eventId'));
								foreach ($groups as $option){
									if($formData->package_group == $option->id){
										echo '<option value="'.$option->id.'" selected="selected">'.$option->title.'</option>';
									}else{
										echo '<option value="'.$option->id.'">'.$option->title.'</option>';
									}
								}
								?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="pricing" data-column="pricing">
								Price Spesialis Lainnya
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="input-group" data-column="pricing">
								<span class="add-on input-group-addon"><small>Rp</small></span>
								<input id="name" name="price" data-editor="text" data-field-name="pricing"
											 data-legacy-field-name="pricing" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->price; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="pricing_member" data-column="pricing_member">
								Price Member
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="input-group" data-column="pricing_member">
								<span class="add-on input-group-addon"><small>Rp</small></span>
								<input id="pricing_member" name="price_member" data-editor="text" data-field-name="pricing_member"
											 data-legacy-field-name="pricing_member" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->price_member; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="pricing_finasim" data-column="pricing_finasim">
								Price Finasim
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="input-group" data-column="pricing">
								<span class="add-on input-group-addon"><small>Rp</small></span>
								<input id="name" name="price_finasim" data-editor="text" data-field-name="pricing_finasim"
											 data-legacy-field-name="pricing_finasim" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->price_finasim; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="pricing_finasim" data-column="pricing_finasim">
								Price Dokter Umum
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="input-group" data-column="pricing">
								<span class="add-on input-group-addon"><small>Rp</small></span>
								<input id="name" name="price_dokter_umum" data-editor="text" data-field-name="price_dokter_umum"
											 data-legacy-field-name="pricing_finasim" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->price_dokter_umum; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="package_description" data-column="package_description">
								Description
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<textarea class="form-control" id="package_description"
												name="description"><?php echo $formData->description; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="package_options">
							Options
						</label>
						<div class="col-sm-9">
							<div class="row gutter-5">
								<div class="col-xs-12">
									<select id="package_options" name="package_options[]" class="form-control full-width" multiple="multiple">
										<?php
										if($formData->package_options){
											foreach (json_decode($formData->package_options) as $option){
												$option_obj = $model->loadTimeline($option);
												?>
												<option value="<?php echo $option; ?>" selected="selected"><?php echo $option_obj->name; ?></option>
											<?php }
										} ?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="package_rules">
							Rule for
						</label>
						<div class="col-sm-9">
							<div class="row gutter-5">
								<?php
									$rules = array();
									if($formData->package_rules){
										$rules = json_decode($formData->package_rules);
									}
								?>
								<div class="col-xs-12">
									<div class="checkbox checkbox-inline  check-success">
										<input id="rule_anggota" type="checkbox" name="package_rules[]" value="anggota"
											<?php if(in_array('anggota', $rules)){ echo 'checked="checked"'; } ?>
										>
										<label for="rule_anggota">Anggota Papdi</label>
									</div>
									<div class="checkbox checkbox-inline  check-success">
										<input id="rule_finasim" type="checkbox" name="package_rules[]" value="finasim"
											<?php if(in_array('finasim', $rules)){ echo 'checked="checked"'; } ?>
										>
										<label for="rule_finasim">Anggota Finasim</label>
									</div>
									<div class="checkbox checkbox-inline  check-success">
										<input id="rule_umum" type="checkbox" name="package_rules[]" value="umum"
											<?php if(in_array('umum', $rules)){ echo 'checked="checked"'; } ?>
										>
										<label for="rule_umum">Dokter Umum</label>
									</div>
									<div class="checkbox checkbox-inline  check-success">
										<input id="rule_spesialis" type="checkbox" name="package_rules[]" value="spesialis"
											<?php if(in_array('spesialis', $rules)){ echo 'checked="checked"'; } ?>
										>
										<label for="rule_spesialis">Spesialis</label>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php echo JHTML::_('form.token'); ?>
					<input type="hidden" name="task" value="store"/>
					<input type="hidden" name="type" value="package"/>
					<input type="hidden" name="event_id" value="<?php echo JRequest::getVar('eventId'); ?>"/>
					<input type="hidden" name="id" value="<?php echo JRequest::getVar('id', 0); ?>"/>
				</div>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn btn-default" type="button">
					Close
				</button>
				<button class="btn btn-primary submit-ajax" type="submit">
					Save changes
				</button>
			</div>
		</form>
	</div>
	<script>
      $("#package_options").select2({
          theme: "bootstrap",
          ajax: {
              url: '<?php echo JRoute::_('index.php?option=com_eventorg&view=manage'); ?>',
              dataType: 'json',
              data: function (params) {
                  var query = {
                      search: params.term,
                      fragment: 'timelineSelect',
											layout: 'package',
											format: 'raw'
                  };
                  // Query parameters will be ?search=[term]&type=public
                  return query;
              }
          }
      });
      var ajaxModal = $('#ajax-modal');
      $('#package-form').on('submit', function (e) {
          e.preventDefault();
          var form = $('#package-form');
          console.log(form.serialize());
          $.post(form.attr('action'), form.serialize(), function (data) {
              console.log(data);
              getPackage();
              ajaxModal.modal('hide')
              toastr.success('Saved');
          });
      });
      ajaxModal.off('show');
      ajaxModal.on('show', function () {
          setTimeout(function () {
              // alert('asd')
              $('#package-form').find('.datetime').datetimepicker({
                  autoclose: true,
                  initialDate: new Date()
              });
          }, 1000)
      });

	</script>
<?php } else { ?>
	<?php
	$event_id = JRequest::getVar('eventId');
	echo renderPackageItem($model->getPackagesAdmin($event_id));
	?>
<?php } ?>


