<?php

defined('_JEXEC') or die('Restricted access');

/*echo '<pre>';
print_r($this->data->schedule);
echo '</pre>';*/

function renderProgresBar($current = 0, $total = 100)
{
	if (!$total) {
		return '';
	}

	$percentage = ((int)$current / (int)$total) * 100;
	$bar = '';
	if ($percentage > 49) {
		$bar = 'progress-bar-warning';
	}
	if ($percentage > 79) {
		$bar = 'progress-bar-danger';
	}

	if (!$current) {
		$current = '0';
	}
	return '<div class="tooltips" data-original-title="' . $current . '/' . $total . '" data-placement="top"><div class="progress progress-xs inline-block m-b-0 m-l-5" style="width: 50px;"><div style="width: ' . $percentage . '%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="' . $percentage . '" role="progressbar" class="progress-bar ' . $bar . '"></div></div></div>';
}
?>
<div class="row">
	<div class="col-md-6"></div>
	<div class="col-md-6">
		<div class="panel panel-blue m-b-15 no-overflow">
			<div class="panel-body no-padding">
				<div class="clearfix">
					<div class="col-xs-3 text-center padding-15">
						<div class="">
							<span class="text-bold block text-super-large"><?php echo $this->count->total; ?></span>
							<span class="text-strong">Register</span>
						</div>
					</div>
					<div class="col-xs-3 text-center padding-15 partition-green">
						<span class="text-bold block text-super-large"><?php echo $this->count->paid; ?></span>
						<span class="text-strong">Paid</span>
					</div>
					<div class="col-xs-3 text-center padding-15 partition-orange">
						<div class="">
							<span class="text-bold block text-super-large"><?php echo $this->count->pending; ?></span>
							<span class="text-strong">Pending</span>
						</div>
					</div>
					<div class="col-xs-3 text-center padding-15 partition-light-grey">
						<div class="">
							<span class="text-bold block text-super-large"><?php echo $this->count->expired; ?></span>
							<span class="text-strong">Expired</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="tabbable">
			<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue m-b-0" id="myTab4">
				<li>
					<a data-toggle="tab" href="#panel_overview">
						Aktifitas
					</a>
				</li>
				<li class="active">
					<a data-toggle="tab" href="#panel_workshop">
						Workshops
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#panel_projects">
						Projects
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="panel_overview" class="tab-pane fade">
					<?php
					foreach ($this->data->schedule as $schedule) {
						if($schedule->activity_type != 2 && count($schedule->activities) != 0){
							?>
							<div class="m-b-15">
								<table class="table table-bordered table-hover" id="sample-table-4">
									<thead>
									<tr>
										<th colspan="4">
											<div class="row">
												<div class="col-md-6 col-sm-4 col-xs-12"><h4><?php echo $schedule->name; ?></h4></div>
												<div class="col-md-3 col-sm-4 col-xs-6 p-t-5">
													<div>From</div>
													<?php echo JHTML::_('date', $schedule->start_time, '%A, %d %b %Y %H:%M'); ?>
												</div>
												<div class="col-md-3 col-sm-4 col-xs-6 p-t-5">
													<div>Until</div>
													<?php echo JHTML::_('date', $schedule->end_time, '%A, %d %b %Y %H:%M'); ?>
												</div>
											</div>

										</th>
									</tr>
									<tr>
										<th style="width: 100px;">Code</th>
										<th>Name</th>
										<th class="text-center" style="width: 100px;">Capacity</th>
										<th class="text-center" style="width: 100px;">Actions</th>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($schedule->activities as $activity) { ?>
										<tr>
											<td><?php echo $activity->code; ?></td>
											<td><?php echo $activity->name; ?></td>
											<td class="center">
												<?php echo renderProgresBar($activity->registered, $activity->seats); ?></td>
											<td class="center">
												<a href="<?php echo JURI::base().'administration/event/attendance-qr?id='.$activity->id; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top"
													 data-original-title="QR Attendance"><i
														class="fa fa-qrcode"></i></a>
												<a href="<?php echo JURI::base().'administration/event/activity-dashboard?id='.$activity->id; ?>" class="btn btn-xs btn-green tooltips" data-placement="top"
													 data-original-title="View Detail"><i class="fa fa-eye"></i></a>
										</tr>
									<?php } ?>
									</tbody>
								</table>
							</div>
						<?php }} ?>
				</div>
				<div id="panel_workshop" class="tab-pane fade in active">
					<?php
					foreach ($this->data->schedule as $schedule) {
						if($schedule->activity_type == 2){
						?>
						<div class="m-b-15">
							<table class="table table-bordered table-hover" id="sample-table-4">
								<thead>
								<tr>
									<th colspan="4">
										<div class="row">
											<div class="col-md-6 col-sm-4 col-xs-12"><h4><?php echo $schedule->name; ?></h4></div>
											<div class="col-md-3 col-sm-4 col-xs-6 p-t-5">
												<div>From</div>
												<?php echo JHTML::_('date', $schedule->start_time, '%A, %d %b %Y %H:%M'); ?>
											</div>
											<div class="col-md-3 col-sm-4 col-xs-6 p-t-5">
												<div>Until</div>
												<?php echo JHTML::_('date', $schedule->end_time, '%A, %d %b %Y %H:%M'); ?>
											</div>
										</div>

									</th>
								</tr>
								<tr>
									<th style="width: 100px;">Code</th>
									<th>Name</th>
									<th class="text-center" style="width: 100px;">Capacity</th>
									<th class="text-center" style="width: 100px;">Actions</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($schedule->activities as $activity) { ?>
									<tr>
										<td><?php echo $activity->code; ?></td>
										<td><?php echo $activity->name; ?></td>
										<td class="center">
											<div style="font-size: 9px"><?php echo $activity->registered; ?>/<?php echo $activity->seats; ?></div>
											<?php echo renderProgresBar($activity->registered, $activity->seats); ?>
											<div style="font-size: 9px"><?php if($activity->seats - $activity->registered < 0) {echo '+'.$activity->registered - $activity->seats; } ?></div>
										</td>
										<td class="center">
											<a href="<?php echo JURI::base().'administration/event/attendance-qr?id='.$activity->id; ?>" class="btn btn-xs btn-blue tooltips" data-placement="top"
												 data-original-title="QR Attendance"><i
													class="fa fa-qrcode"></i></a>
											<a href="<?php echo JURI::base().'administration/event/activity-dashboard?id='.$activity->id; ?>" class="btn btn-xs btn-green tooltips" data-placement="top"
												 data-original-title="View Detail"><i class="fa fa-eye"></i></a>
									</tr>
								<?php } ?>
								</tbody>
							</table>
						</div>
					<?php }} ?>
				</div>
				<div id="panel_projects" class="tab-pane fade"></div>
			</div>
		</div>
	</div>
</div>