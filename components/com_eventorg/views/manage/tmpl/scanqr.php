<?php

defined('_JEXEC') or die('Restricted access');

$doc =& JFactory::getDocument();
$doc->addStyleSheet(JURI::base() . 'templates/admin/plugins/bootstrap/css/bootstrap.min.css');
$doc->addScript(JURI::base() . 'media/jui/jquery.min.js');
$doc->addScript(JURI::base() . 'media/jui/bootstrap.min.js');
$doc->addScript(JURI::base() . 'media/qrscanner/qrcode-scanner.js');

?>
<style>
	h3{
		line-height: 1.5;
	}
	#scanClamp{
		fill: #ccc;
	}
	.scanContainer{
		position: relative;
	}
	.scanInfo{
		text-align: center;
		font-size: 32px;
		font-weight: bold;
		position: absolute;
		top: 50%;
		left: 0;
		right: 0;
		transform: translate(0,-50%);
	}
</style>




<div class="container">
	<div style="margin-bottom: 30px; margin-top: 50px;">
		<img style="width: 300px;" src="<?php echo JURI::base() . 'images/main-logo.png'; ?>"/>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<h1><?php echo $this->activity->timeline_name; ?></h1>
			<h3><?php echo $this->activity->code . ' ' . $this->activity->name; ?></h3>
			<div class="row">
				<div class="col-xs-6">
					<label>Tanggal</label>
					<h3><?php echo JHTML::_('date', $this->activity->start_time); ?></h3>
				</div>
				<div class="col-xs-3">
					<label>Mulai</label>
					<h2><?php echo JHTML::_('date', $this->activity->start_time, '%H:%M'); ?> WIB</h2>
				</div>
				<div class="col-xs-3">
					<label>Selesai</label>
					<h2><?php echo JHTML::_('date', $this->activity->end_time, '%H:%M'); ?> WIB</h2>
				</div>
			</div>
			<h3 id="qr-reader-results"></h3>
		</div>
		<div class="col-sm-6">
			<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue m-b-0" id="myTab4">
				<li class="active">
					<a data-toggle="tab" href="#panel_scanner">
						Use Scanner
					</a>
				</li>
				<li class="">
					<a data-toggle="tab" href="#panel_cam">
						Use Webcam
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="panel_scanner" class="tab-pane fade active in">
					<div class="scanContainer">
						<div class="scanInfo">
							Scanner Siap
						</div>
						<svg id="scanClamp" x="0px" y="0px" viewBox="0 0 600 600" xml:space="preserve">
							<g>
								<g>
									<polygon points="547.3,101 563.3,101 563.3,37.3 499.3,37.3 499.3,53.3 547.3,53.3 		"/>
									<polygon points="547.3,499 547.3,547.3 499.3,547.3 499.3,563.3 563.3,563.3 563.3,499 		"/>
								</g>
								<g>
									<polygon points="53.3,101 53.3,53.3 101.3,53.3 101.3,37.3 37.3,37.3 37.3,101 		"/>
									<polygon points="53.3,499 37.3,499 37.3,563.3 101.3,563.3 101.3,547.3 53.3,547.3 		"/>
								</g>
							</g>
				</svg>
					</div>
					<form id="form" style="opacity: 0;">
						<input id="code" type="text" name="code"/>
					</form>
				</div>
				<div id="panel_cam" class="tab-pane fade">
					<div id="qr-reader" style="width:500px"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	var activityType = '<?php echo $this->activity->activity_type; ?>';
	var baseUrl = '<?php echo JURI::base(); ?>';
	function submit(code) {
		var url = '<?php echo JURI::current(); ?>';
		var postData = {
			task: 'captureAttendance',
			activityId: '<?php echo $this->activity->id; ?>',
			bookingId: code
		};
		postData['<?php echo JUtility::getToken(); ?>'] = 1;

		$('.scanInfo').html('Loading..');
		$('#scanClamp').css('fill', '#e2a84a');

		$.post(url, postData, function (data) {
			$('.scanInfo').html('Scanner Siap');
			$('#scanClamp').css('fill', '#ccc');
			if (data) {
				var jsonData = JSON.parse(data);
				console.log(jsonData);
				showResult(jsonData);
				if(activityType === '3'){
					var printUrl = baseUrl + 'administration/event/print-tag?id=' + postData.activityId + '&bookingId=' + postData.bookingId;
					window.open(printUrl, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
				}
				if(activityType === '4'){
					var printUrl = baseUrl + 'administration/event/print-tag?id=' + postData.activityId + '&bookingId=' + postData.bookingId;
					window.open(printUrl, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
				}
			}
			$('#code').val('').focus()
		})
	}

	function showResult(data) {
		$('#qr-reader-results').html(
			'<div class="jumbotron"> <h3>Halo, ' + data.booking.user_name + '</h3><p>' + data.message + '</p></div>'
		);
		setTimeout(function () {
			$('#qr-reader-results').empty();
		}, 6000)
	}


	$(document).ready(function () {
		$('#code').val('').focus();
		$('#code').blur(function () {
			setTimeout(function () {
				$('#code').focus();
			}, 200)
		});
		$('#form').submit(function (e) {
			e.preventDefault();
			var code = $('#code').val();
			submit(code)
		})
		setInterval(function () {
			$('#code').focus();

		}, 3000)
	})
</script>

<script>
	var resultContainer = document.getElementById('qr-reader-results');
	var lastResult, countResults = 0;

	function onScanSuccess(qrCodeMessage) {
		if (qrCodeMessage !== lastResult) {
			++countResults;
			lastResult = qrCodeMessage;
			submit(lastResult)
		}
	}

	var html5QrcodeScanner = new Html5QrcodeScanner(
		"qr-reader", {fps: 10, qrbox: 250});
	html5QrcodeScanner.render(onScanSuccess);
</script>