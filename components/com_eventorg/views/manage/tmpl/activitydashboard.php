<?php

defined('_JEXEC') or die('Restricted access');
/*echo '<pre>';
print_r($this->activity);
echo '</pre>';

echo '<pre>';
print_r($this->participants);
echo '</pre>';*/

$statusLabels = array(
	10 => 'Unpaid',
	12 => '<strong style="color: #4ea549">Paid</strong>',
	13 => '<strong style="color: #f59065">Pending</strong>',
	14 => 'expired'
);


?>
<div class="row">
	<div class="col-md-6">
		<h2><?php echo $this->activity->timeline_name; ?></h2>
		<h4><?php echo $this->activity->code.' '.$this->activity->name; ?></h4>
	</div>
	<div class="col-md-6">
		<div class="panel panel-blue m-b-15 no-overflow">
			<div class="panel-body no-padding">
				<div class="clearfix">
					<div class="col-xs-6 text-center padding-15">
						<div class="">
							<span class="text-bold block text-super-large"><?php echo JHTML::_('date', $this->activity->start_time, '%A, %d %b %Y %H:%M'); ?></span>
							<span class="text-strong">Waktu Mulai</span>
						</div>
					</div>
					<div class="col-xs-6 text-center padding-15 partition-green">
						<span class="text-bold block text-super-large"><?php echo JHTML::_('date', $this->activity->end_time, '%A, %d %b %Y %H:%M'); ?></span>
						<span class="text-strong">Waktu Selesai</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="tabbable">
			<ul class="nav nav-tabs tab-padding tab-space-3 tab-blue m-b-0" id="myTab4">
				<li class="active">
					<a data-toggle="tab" href="#panel_participants">
						Participants
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="panel_participants" class="tab-pane fade in active">
					<div class="m-b-15">
						<table class="table table-bordered table-hover" id="sample-table-4">
							<thead>
							<tr>
								<th style="width: 30px;">&nbsp;</th>
								<th>Name</th>
								<th>Email</th>
								<th>Invoice</th>
								<th>Status</th>
								<th class="text-center" style="width: 100px;">Checked In</th>
								<th class="text-center" style="width: 100px;">Checked Out</th>
							</tr>
							</thead>
							<tbody>
							<?php $i=0; foreach ($this->participants as $participants) { ?>
								<tr>
									<td><?php echo ++$i; ?></td>
									<td><?php echo $participants->user_name; ?></td>
									<td><?php echo $participants->user_email; ?></td>
									<td><a href="<?php echo JURI::base(); ?>administration/store/invoices?id=<?php echo $participants->order_id; ?>" target="_blank">#<?php echo $participants->order_id; ?></a></td>
									<td><?php echo $statusLabels[$participants->status]; ?></td>
									<td><?php echo $participants->checked_in; ?></td>
									<td><?php echo $participants->checked_out; ?></td>
								</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<form>
						<button type="submit" name="task" value="downloadReport" class="btn btn-primary">Download Excel</button>
						<input type="hidden" name="id" value="<?php echo $this->activity->id; ?>">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>