<?php
/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/
// no direct access
defined('_JEXEC') or die('Restricted access');
// uncomment to add custom css file for this view,
// save the css file to media/com_eventorg/css/yourcss.css
//$document =& JFactory::getDocument();
//$document->addStyleSheet($this->baseurl.'/media/com_eventorg/css/yourcss.css');
//use media/com_eventorg folder to store your css,js,images,...

function renderGroupItem($groups)
{
	$html = '';
	if (!$groups) {
		return '';
	}
	for ($i = 0; $i < count($groups); $i++) {
		$editUrl = JRoute::_('index.php?option=com_eventorg&view=manage&layout=packagegroup&format=raw&id=' . $groups[$i]->id . '&eventId=' . $groups[$i]->event_id);
		$html .=
			'<div id="schedule_' . $groups[$i]->id . '" data-id="' . $groups[$i]->id . '" class="list-group m-b-5">' .
			'<div class="list-group-item no-padding clearfix">' .
			'<div class="pull-left padding-10"><strong>' . $groups[$i]->title . '</strong></div>' .
			'<div class="pull-right">' .
			'<div class="inline-block padding-10 border-right border-light">' .
			'<span class="bold-text text-small"><i class="fa fa-clock-o"></i> ' . JHTML::_('date', $groups[$i]->start_date, '%H:%M') . '</span>' .
			'<span class=""> ' . JHTML::_('date', $groups[$i]->start_date, '%d/%m/%y') . '</span>' .
			'</div>' .
			'<div class="inline-block padding-10 border-right border-light">' .
			'<span class="bold-text text-small"><i class="fa fa-clock-o"></i> ' . JHTML::_('date', $groups[$i]->end_date, '%H:%M') . '</span>' .
			'<span class=""> ' . JHTML::_('date', $groups[$i]->end_date, '%d/%m/%y') . '</span>' .
			'</div>' .
			'<div class="inline-block padding-5">' .
			'<a class="ajax-modal btn btn-default btn-xs" href="' . $editUrl . '"><i class="fa fa-plus"></i> Edit</a> ' .
			'<a class="btn btn-default btn-xs delete-package-group" data-id="' . $groups[$i]->id . '" href="#"><i class="fa fa-times"></i></a>' .
			'</div>' .
			'</div>' .
			'</div>';

		$html .= '</div>';
	}

	return $html;
}

$model = &$this->getModel();

if (JRequest::getVar('fragment') == 'groupSelect') {
	$list = $model->getPackageGroups();
	echo json_encode($list);
	die();
}

if (JRequest::getVar('form')) {
	$isNew = TRUE;
	if (JRequest::getVar('id')) {
		$isNew = FALSE;
		$formData = $model->getPackageGroup(JRequest::getVar('id'));
	}
	$formUrl = JRoute::_('index.php?option=com_eventorg&view=manage&layout=packagegroup');
	?>
	<style>
		.select2-container--bootstrap {
			z-index: 9999;
			width: 100% !important;
			font-size: 1em;
		}

		.select2-container--bootstrap .select2-selection--multiple .select2-selection__rendered {
			margin: 0 !important;
			list-style: none !important;
		}

		.select2-container .select2-search--inline .select2-search__field {
			box-shadow: none !important;
			border: none !important;
			background-color: unset !important;
		}

		.select2-container--bootstrap.select2-container--focus .select2-selection, .select2-container--bootstrap.select2-container--open .select2-selection, .select2-container--bootstrap .select2-dropdown {
			box-shadow: 0px 0px 2px #0000001a !important;
			border-color: #b2b2b2 !important;
			background-color: #fbfbfb !important;
		}

		.select2-container--bootstrap .select2-results > .select2-results__options {
			margin-bottom: 0 !important;
		}

		.select2-container--bootstrap .select2-selection--single .select2-selection__placeholder, .select2-container--bootstrap .select2-selection {
			color: #c8c8c8;
			font-size: 12px;
			line-height: 20px;
		}

		.select2-container--bootstrap .select2-selection--single .select2-selection__rendered {
			color: #8b91a0;
			padding: 0;
		}
	</style>
	<div class="modal-dialog">
		<form class="form-horizontal" id="package-form" method="post" action="<?php echo $formUrl; ?>">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
					<h4 id="myModalLabel" class="modal-title">Event Package</h4>
				</div>
				<div class="modal-body p-l-30 p-r-30">
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="title" data-column="title">
								Name
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="col-input" data-column="name">
								<input id="title" name="title" data-editor="text" data-field-name="title"
											 data-legacy-field-name="name" data-pgui-legacy-validate="true"
											 class="form-control"
											 value="<?php echo $formData->title; ?>"
											 type="text">
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="start_date" data-column="start_date">
								Start Time
								<span class="required-mark">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">

							<div class="col-input" data-column="start_date">
								<div class="input-append date input-group datetime" id="datetimepicker"
										 data-date-format="yyyy-mm-dd hh:ii">
                                    <span class="add-on input-group-addon"><i class="icon-th"><i
																					class="fa fa-calendar"></i></i></span>
									<input class="form-control " type="text" id="start_date" name="start_date"
												 value="<?php echo $formData->start_date; ?>"/>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="end_date" data-column="end_date">
								End Time

								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">

								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<div class="input-append date input-group datetime" id="datetimepicker"
									 data-date-format="yyyy-mm-dd hh:ii">
								<span class="add-on input-group-addon"><i class="icon-th"><i class="fa fa-calendar"></i></i></span>
								<input class="form-control " type="text" id="end_date" name="end_date"
											 value="<?php echo $formData->end_date; ?>"/>
							</div>
						</div>
					</div>
					<div class="form-group ">
						<div class="col-sm-3 form-group-label">
							<label class="control-label" for="group_description" data-column="group_description">
								Description
								<span class="required-mark" style="display: none">*</span>
								<div class="pgui-field-options btn-group btn-group-xs btn-group-justified"
										 data-toggle="buttons">
								</div>
							</label>
						</div>
						<div class="col-sm-9">
							<textarea class="ckeditor" name="description"><?php echo $formData->description; ?></textarea>
							</div>
						</div>
						<?php echo JHTML::_('form.token'); ?>
						<input type="hidden" name="task" value="store"/>
						<input type="hidden" name="type" value="packageGroup"/>
						<input type="hidden" name="event_id" value="<?php echo JRequest::getVar('eventId'); ?>"/>
						<input type="hidden" name="id" value="<?php echo JRequest::getVar('id', 0); ?>"/>
					</div>
				</div>
				<div class="modal-footer">
					<button data-dismiss="modal" class="btn btn-default" type="button">
						Close
					</button>
					<button class="btn btn-primary submit-ajax" type="submit">
						Save changes
					</button>
				</div>
		</form>
	</div>
	<script>
		$("#package_options").select2({
			theme: "bootstrap",
			ajax: {
				url: '<?php echo JRoute::_('index.php?option=com_eventorg&view=manage'); ?>',
				dataType: 'json',
				data: function (params) {
					var query = {
						search: params.term,
						fragment: 'timelineSelect',
						layout: 'package',
						format: 'raw'
					};
					// Query parameters will be ?search=[term]&type=public
					return query;
				}
			}
		});
		var ajaxModal = $('#ajax-modal');
		$('#package-form').on('submit', function (e) {
			e.preventDefault();
			var form = $('#package-form');
			console.log(form.serialize());
			$.post(form.attr('action'), form.serialize(), function (data) {
				console.log(data);
				getPackage();
				getPackageGroups();
				ajaxModal.modal('hide')
				toastr.success('Saved');
			});
		});
		ajaxModal.off('show');
		ajaxModal.on('show', function () {
			try {
				$('textarea.ckeditor').ckeditor()
			} catch (e) {
				console.debug(e);
			}
			setTimeout(function () {
				// alert('asd')
				$('#package-form').find('.datetime').datetimepicker({
					autoclose: true,
					initialDate: new Date()
				});
			}, 1000)
		});

	</script>
<?php } else { ?>
	<?php
	$event_id = JRequest::getVar('eventId');
	echo renderGroupItem($model->getPackageGroups($event_id));
	?>
<?php } ?>


