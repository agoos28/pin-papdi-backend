<?php 

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


jimport('joomla.application.component.view');
/**
  * @package EventOrg
  */
  class EventOrgViewItem extends JView
  {
  function display($tpl = null)
  {
  global $mainframe;
 
$pathway = &$mainframe->getPathway();
$document = & JFactory::getDocument();
  $model = &$this->getModel();
  
// Get the parameters of the active menu item
$menus = &JSite::getMenu();
$menu = $menus->getActive();
 $pparams = &$mainframe->getParams('eventorg');
$item =& $this->get('data');
 // Set the document page title
  // because the application sets a default page title, we need to get it
  // right from the menu item itself
  if (is_object( $menu ) && isset($menu->query['view']) && $menu->query['view'] == 'item' && isset($menu->query['id']) && $menu->query['id'] == $item->id) {
  $menu_params = new JParameter( $menu->params );
  if (!$menu_params->get( 'page_title')) {
  $pparams->set('page_title', $item->title);
  }
  } else {
  $pparams->set('page_title', $item->title);
  }
$document->setTitle( $pparams->get( 'page_title' ) );
 //set breadcrumbs
  if (isset( $menu ) && isset($menu->query['view']) && $menu->query['view'] != 'item'){
  $pathway->addItem($item->title, '');
  }

 $this->assignRef('item', $item);
  
  parent::display($tpl);
  }
  }