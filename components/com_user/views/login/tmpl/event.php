<?php // @version $Id: default_login.php 9830 2008-01-03 01:09:39Z eddieajau $
defined('_JEXEC') or die('Restricted access');
?>

<div id="content" style="">
	<div class=""
			 style="width: auto; margin: 0; background: #f8f8f8; display: flex; flex: 1; align-items: center; justify-content: center; position: absolute; top: 0; left: 0; right: 0; bottom: 0;">
		<form style="display: block; max-width: 400px; padding: 15px; background: #ffffff"
					method="post" name="login" id="login"
					class="login_form frm <?php echo $this->params->get('pageclass_sfx'); ?>">
			<h4 style="margin-top: 0; margin-bottom: 15px" class="bold">Login Peserta</h4>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="form-group form-group-default">
						<label>Nomor Transaksi</label>
						<div class="controls">
							<input type="text" name="order_id" placeholder="" class="form-control" required="required">
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="form-group form-group-default">
						<label>Email Peserta</label>
						<div class="controls">
							<input type="text" name="user_email" placeholder="" class="form-control" required="required">
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="form-group form-group-default">
						<label>5 digit terakhir NPA atau Telepon Peserta</label>
						<div class="controls">
							<input type="text" name="password" placeholder="" class="form-control" required="required">
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="lbltxt">&nbsp;</div>
					<input type="submit" name="submit" class="btn btn-sm btn-primary" style="line-height: 33px; width: 125px;"
								 value="<?php echo JText::_('Login'); ?>"/>
				</div>
			</div>
			<noscript><?php echo JText::_('WARNJAVASCRIPT'); ?></noscript>
			<input type="hidden" name="option" value="com_user"/>
			<input type="hidden" name="task" value="viewBooking"/>
			<input type="hidden" name="return" value="<?php echo JURI::current(); ?>"/>
			<?php echo JHTML::_('form.token'); ?>
		</form>
	</div>
</div>