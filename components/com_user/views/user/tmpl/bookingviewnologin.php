<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$model = $this->getModel();


if(JRequest::getVar('platform') === 'api'){
	$result = new stdClass();
	$result->booking = $this->data;
	$result->qr = JURI::base() . 'files/booking/' . $this->data->id . '.jpg';

	$result->workshops = json_decode($this->data->workshops);

	foreach ($result->workshops as $workshop) {
		$workshop->timeline = $model->getTimeline($workshop->timeline_id);
		$workshop->links = $model->getActivityLinks($workshop->id);
	}

	echo json_encode($result);
} else {
?>

<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- START CONTAINER FLUID -->
	<div class="container ">
		<div class="cart_c frm">
			<div class="row m-b-30">
				<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12"></div>
				<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
					<?php if ($this->data) {
						$this->data->workshops = json_decode($this->data->workshops);
						?>
						<div class="row m-b-15">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<img style="width: 100%;"
										 src="<?php echo JURI::base() . 'files/booking/' . $this->data->id . '.jpg'; ?>"/>
							</div>
							<div class="col-md-8 col-sm-6 col-xs-12">
								<div class="list-group m-b-15">
									<div class="list-group-item d-flex justify-content-between">
										<h5 class="no-margin">
											<?php echo $this->data->event->title; ?>
										</h5>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Tanggal</strong>
										</div>
										<div style="text-transform: capitalize;">
											<?php echo JHTML::_('date', $this->data->event->start_date); ?>
										</div>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Paket</strong>
										</div>
										<div style="text-transform: capitalize;">
											<?php echo $this->data->package_title; ?>
										</div>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Status</strong>
										</div>
										<div style="text-transform: capitalize;">
											<?php echo $model->statusLabel($this->data->status); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="list-group m-b-15">
							<div class="list-group-item d-flex justify-content-between">
								<h5 class="no-margin">
									Agenda Simposium
								</h5>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 1 - Tema: Broadening Therapeutic Horizons in Diabetes and Angina Management</strong>
									</div>
									<div>
									Sabtu, 16 Oktober 2021 (08.15-10.15)
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/81818998718" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 2 - Tema: Simplifying Treatment for People with Type 2 Diabetes</strong>
									</div>
									<div>
									Sabtu, 16 Oktober 2021 (08.15-10.15)
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/84980060792" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 3 - Tema : An Update in T2D Treatment Beyond Glycemic Control with Once Weekly</strong>
									</div>
									<div>
									Minggu, 17 Oktober 2021 (08.15-10.15)
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/85190334342" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 4 : Tema : Managing Hypertension and Dyslipidemia to Minimize Cardiovascular Risk </strong>
									</div>
									<div>
									Sabtu, 23 Oktober 2021 (08.15-10.15)
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/87158146286" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 5 : Tema : Parenteral Nutrition Supplement : Beneficial or Not </strong>
									</div>
									<div>
									Sabtu, 23 Oktober 2021 (08.15-10.15)
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/81975345130" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 6 : Tema :  The Link between Pre diabetes and Thyroid </strong>
									</div>
									<div>
									Minggu, 24 Oktober 2021 (08.15-10.15) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/82049926470" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>

							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong>SIMPOSIUM 7 : Tema : The Importance of Flu Vaccination for at-Risk Population</strong>
									</div>
									<div>
									Sabtu, 30 Oktober 2021 (08:00-10:00)
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/81964285419" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 8 : Tema : Recent Management of Viral Chronic Hepatitis and Coinfection in Indonesia</strong>
									</div>
									<div>
									Sabtu, 30 Oktober 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/85144211777" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 9 : Tema :   The Importance of Flu Vaccination for at-Risk Population</strong>
									</div>
									<div>
									Minggu, 31 Oktober 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/86552547710" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 10 : Tema :  Treatment and Management Approach of ACS</strong>
									</div>
									<div>
									Sabtu, 06 November 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/87186537075" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 11 : Tema : Update on Malnutrition Assessment and Intervention : Key Aspect of Nutrition Management in Elderly to Prevent Sarcopenia and Muscle Loss </strong>
									</div>
									<div>
									Sabtu, 06 November 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/84490888523" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 12 : Tema :   The Role of Nutrition to Maintaining Elderly Quality of Life </strong>
									</div>
									<div>
									Minggu, 07 November 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/89291222080" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 13 : Tema :  The Role of Natural Medicines as an Adjuvant Therapy in Chronics Diseases</strong>
									</div>
									<div>
									Sabtu, 13 November 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/81091590914" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 14 : Tema :  Management of Co-infection and Long Pulmonary Complication in COVID-19 </strong>
									</div>
									<div>
									Sabtu, 13 November 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/81890212427" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
							<div class="list-group-item" style="display: flex; justify-content: space-between;">
								<div style="flex-grow: 1">
									<div>
										<strong> SIMPOSIUM 15 : Tema :  Early Aggressive Therapy for T2DM Patients: For who? When and How? </strong>
									</div>
									<div>
									Minggu, 14 November 2021 (08:00-10:00) 
									</div>
								</div>
								<div style="margin-top: 8px">
									<a target="_blank" href="https://us02web.zoom.us/j/87813791199" class="btn btn-xs btn-complete">ZOOM Link</a>
								</div>
							</div>
						</div>
						<div class="list-group m-b-15">
							<div class="list-group-item d-flex justify-content-between">
								<h5 class="no-margin">
									Agenda Workshop 
								</h5>
							</div>
							<?php foreach ($this->data->workshops as $workshop) {
								$timeline = $model->getTimeline($workshop->timeline_id);
								$links = $model->getActivityLinks($workshop->id);
								?>
								<div class="list-group-item" style="display: flex; justify-content: space-between;">
									<div style="flex-grow: 1">
										<div>
											<strong><?php echo $workshop->code . ' ' . $workshop->name; ?></strong>
										</div>
										<div>
											<?php echo JHTML::_('date', $timeline->start_time, '%A, %d %b %Y %H:%M'); ?> s/d <?php echo JHTML::_('date', $timeline->end_time, '%H:%M'); ?>
										</div>
									</div>
									<div style="margin-top: 8px">
										<a target="_blank" href="<?php echo $links->zoomlink; ?>"
											 class="btn btn-xs <?php echo $links->zoomlink ? 'btn-complete' : 'disabled'; ?>"
										>Download Sertifikat</a>
									</div>
								</div>
							<?php } ?>
							<form type="post" style="margin-top: 30px; text-align: right;">
								<button type="submit" name="task" class="btn btn-sm btn-primary" style="line-height: 33px; width: 125px;"
												value="clearBookingView"><?php echo JText::_('logout'); ?></button>
							</form>
						</div>
					<?php } else { ?>
						<div class="alert alert-danger text-center">
							<h5 class="pink">Booking not found!</h5>
						</div>
					<?php } ?>
				</div>
				<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12"></div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
