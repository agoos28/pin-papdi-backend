<?php // no direct access
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');

JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_cckjseblod' . DS . 'models');
$cart_model = &JModel::getInstance('cart', 'CCKjSeblodModel');


$user = JFactory::getUser();
$model = $this->getModel();
$id = JRequest::getVar('id');
//echo '<pre>';
//print_r($this->data);
//echo '</pre>';
$acceptedStatus = array(200, 201, 202);
$instantPaymentType = array('bca_klikpay', 'akulaku', 'cimb_clicks', 'danamon_online', 'bri_epay');
// $paymentUrl = array(
// 	'bca' => 'https://api.sandbox.veritrans.co.id/v3/bca/klikpay/redirect/',
// 	'akulaku' => 'https://api.sandbox.midtrans.com/v2/akulaku/redirect/',
// 	'cimb_clicks' => 'https://api.sandbox.veritrans.co.id/cimb-clicks/request?id=',
// 	'danamon_online' => 'https://api.sandbox.veritrans.co.id/v2/danamon/online/redirect/',
// 	'bri_epay' => 'https://api.sandbox.veritrans.co.id/v3/bri/epay/redirect/'
// );
$paymentUrl = array(
	'bca' => 'https://api.veritrans.co.id/v3/bca/klikpay/redirect/',
	'akulaku' => 'https://api.midtrans.com/v2/akulaku/redirect/',
	'cimb_clicks' => 'https://api.veritrans.co.id/cimb-clicks/request?id=',
	'danamon_online' => 'https://api.veritrans.co.id/v2/danamon/online/redirect/',
	'bri_epay' => 'https://api.veritrans.co.id/v3/bri/epay/redirect/'
);
$contentId = array(
	//transfer payment
	'bca' => 780,
	'mandiri' => 781,
	'bni' => 782,
	'permata' => 783,
	//instant payment
	'bca_klikpay' => 780,
	'akulaku' => 781,
	'cimb_clicks' => 782,
	'danamon_online' => 783,
	'bri_epay' => 783
);

$statusCommitment = array(
	10 => 'Unpaid',
	12 => 'Disetujui',
	13 => 'Pending',
	14 => 'Expired'
);

if(JRequest::getVar('platform') === 'api'){
	$result = new stdClass();

	$result->transaction = new stdClass();
	$result->transaction->id = $this->data->id;
	$result->transaction->status = $this->data->status;
	$result->transaction->value = $this->data->value;


	$result->payment = new stdClass();
	$result->payment->method = $this->data->method;


	if($this->data->method === 'midtrans'){

		$result->payment->status = $this->data->ext_response->status_code;

		if($result->payment->status === '404') {
			$result->payment->snapToken = $this->data->ext_response->id;
		}

		if ($this->data->ext_response->payment_type === 'bank_transfer' || $this->data->ext_response->payment_type === 'echannel') {

			$result->payment->type = 'Bank Transfer';

			if ($this->data->ext_response->va_numbers) {
				$result->payment->bank = $this->data->ext_response->va_numbers[0]->bank;
				$result->payment->va = $this->data->ext_response->va_numbers[0]->va_number;
			}
			if ($this->data->ext_response->bill_key) {
				$result->payment->bank = 'mandiri';
				$result->payment->biller = $this->data->ext_response->biller_code;
				$result->payment->va = $this->data->ext_response->bill_key;
			}
			if ($this->data->ext_response->permata_va_number) {
				$result->payment->bank = 'permata';
				$result->payment->va = $this->data->ext_response->permata_va_number;
			}
		}
		if (in_array($this->data->ext_response->payment_type, $instantPaymentType)) {
			$result->payment->bank =  str_replace('_', ' ', $this->data->ext_response->payment_type);;
		}

		$result->payment->time = $this->data->ext_response->transaction_time;

	}

	$result->detail = $this->data->cart_data;

	// echo json_encode($this->data);
	echo json_encode($result);
} else {

?>
<script type="text/javascript" src="https://app.midtrans.com/snap/snap.js"
				data-client-key="<?php echo $cart_model->_VTclientKey; ?>"></script>

<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- START CONTAINER FLUID -->
	<div class="container ">
		<div class="cart_c frm">
			<div class="row m-b-30">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right">
					<?php if ($this->data) { ?>
						<div class="list-group m-b-15">
							<div class="list-group-item d-flex justify-content-between p-l-0">
								<h5 class="no-margin">
									<a href="<?php echo JURI::base(); ?>transactions"
										 class="btn btn-sm btn-link btn-xs-block"><i
											class="fa fa-lg fa-chevron-left"></i></a>
									<?php echo JText::_('MY_TRANSACTION_DETAIL'); ?>
								</h5>
								<a class="btn btn-sm btn-primary text-small m-b-5"
									 target="_blank"
									 href="<?php echo JURI::base(); ?>transactions?layout=transaction&id=<?php echo $this->data->id; ?>&format=pdf"><i class="fa fa-file-pdf-o"></i> Download PDF</a>
							</div>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>ID Transaksi</strong>
								</div>
								<div>
									<?php echo $this->data->id; ?>
								</div>
							</div>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Status</strong>
								</div>
								<div>
									<?php if ($this->data->method === 'commitment') {
										echo $statusCommitment[$this->data->status];
									} else{
										echo $this->data->status_name;
									}
									?>
								</div>
							</div>
							<div class="d-flex list-group-item justify-content-between">
								<div>
									<strong>Total</strong>
								</div>
								<div>
									Rp. <?php echo number_format($this->data->value); ?>
								</div>
							</div>
							<?php if (!$this->data->ext_response->order_id && $this->data->method === 'midtrans') { ?>
								<div class="d-flex list-group-item justify-content-end">
									<a id="midtransPaymentButton" href="#" data-id="<?php echo $this->data->id; ?>"
										 class="btn btn-danger">Bayar Sekarang</a>
								</div>
							<?php } ?>
						</div>

						<?php if ($this->data->method === 'commitment') {
							$files = $model->getFiles('commitment'.DS.$this->data->description);
							?>

							<div class="list-group m-b-15">
								<div class="list-group-item d-flex justify-content-between">
									<h5 class="no-margin">
										<?php echo JText::_('MY_TRANSACTION_PAYMENT'); ?>
									</h5>
								</div>
								<div class="d-flex list-group-item justify-content-between">
									<div>
										<strong>Status</strong>
									</div>
									<div style="text-transform: capitalize;">
										<?php echo $statusCommitment[$this->data->status]; ?>
									</div>
								</div>
								<div class="d-flex list-group-item justify-content-between">
									<div>
										<strong>Metode Bayar</strong>
									</div>
									<div style="text-transform: uppercase;">
										Surat Komitmen
									</div>
								</div>
								<div class="d-flex list-group-item justify-content-between">
									<div>
										<strong>Files</strong>
									</div>
									<div style="text-align: right;">
										<?php
										$filenameArr = array();
										if(count($files)){
											foreach ($files as $file){
												$filenameArr[] = $file->filename;
											}
										}
										echo implode('<br/>', $filenameArr);
										?>
									</div>
								</div>
							</div>

						<?php } ?>


						<?php
						$acceptedStatus = array(200, 201, 202);
						if (in_array($this->data->ext_response->status_code, $acceptedStatus)) { ?>
							<div class="list-group m-b-15">
								<div class="list-group-item d-flex justify-content-between">
									<h5 class="no-margin">
										<?php echo JText::_('MY_TRANSACTION_PAYMENT'); ?>
									</h5>
								</div>
								<div class="d-flex list-group-item justify-content-between">
									<div>
										<strong>Status Pembayaran</strong>
									</div>
									<div style="text-transform: capitalize;">
										<?php echo $this->data->ext_response->transaction_status; ?>
									</div>
								</div>

								<?php if ($this->data->ext_response->payment_type === 'bank_transfer' || $this->data->ext_response->payment_type === 'echannel') {
									if ($this->data->ext_response->va_numbers) {
										$bank = $this->data->ext_response->va_numbers[0]->bank;
										$va = $this->data->ext_response->va_numbers[0]->va_number;
									}
									if ($this->data->ext_response->bill_key) {
										$bank = 'mandiri';
										$biller = $this->data->ext_response->biller_code;
										$va = $this->data->ext_response->bill_key;
									}
									if ($this->data->ext_response->permata_va_number) {
										$bank = 'permata';
										$va = $this->data->ext_response->permata_va_number;
									}
									$howTo = $model->getRawContent($contentId[$bank]);
									?>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Metode Bayar</strong>
										</div>
										<div style="text-transform: uppercase;">
											Transfer Bank <?php echo $bank; ?>
										</div>
									</div>
									<?php if ($biller) { ?>
										<div class="d-flex list-group-item justify-content-between">
											<div>
												<strong>Kode Perusahaan</strong>
											</div>
											<div style="letter-spacing: 1px;">
												<strong><?php echo $biller; ?></strong>
											</div>
										</div>
									<?php } ?>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Kode Pembayaran/VA</strong>
										</div>
										<div style="letter-spacing: 1px;">
											<strong><?php echo $va; ?></strong>
										</div>
									</div>
								<?php } ?>

								<?php if ($this->data->ext_response->payment_type === 'cstore') { ?>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Metode Bayar</strong>
										</div>
										<div style="text-transform: uppercase;">
											<?php echo $this->data->ext_response->store; ?>
										</div>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Kode Pembayaran</strong>
										</div>
										<div style="letter-spacing: 1px;">
											<strong><?php echo $this->data->ext_response->payment_code; ?></strong>
										</div>
									</div>
								<?php } ?>
								<?php
								if (in_array($this->data->ext_response->payment_type, $instantPaymentType)) {
									$howTo = $model->getRawContent($contentId[$this->data->ext_response->payment_type]);
									?>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Metode Bayar</strong>
										</div>
										<div style="text-transform: uppercase;">
											<?php echo str_replace('_', ' ', $this->data->ext_response->payment_type); ?>
										</div>
									</div>
									<?php if ($this->data->payment_status == 12) { ?>
										<div class="d-flex list-group-item justify-content-between">
											<div>
												<strong>Tanggal Pembayaran</strong>
											</div>
											<div style="text-transform: uppercase;">
												<?php echo $this->data->ext_response->transaction_time; ?>
											</div>
										</div>
									<?php } ?>
									<?php if ($this->data->payment_status != 12) { ?>
										<div class="d-flex list-group-item justify-content-between">
											<div>
												<strong>Halaman Pembayaran</strong>
											</div>
											<div style="text-transform: uppercase;">
												<a target="_blank"
													 href="<?php echo $paymentUrl[$this->data->ext_response->payment_type] . $this->data->ext_response->transaction_id; ?>">Bayar
													Sekarang</a>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
								<?php if ($this->data->ext_response->payment_type === 'gopay') { ?>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Metode Bayar</strong>
										</div>
										<div style="text-transform: uppercase;">
											GOPAY
										</div>
									</div>
								<?php } ?>
								<div class="d-flex list-group-item justify-content-between">
									<div>
										<strong>Jumlah Bayar</strong>
									</div>
									<div>
										Rp. <?php echo number_format($this->data->ext_response->gross_amount); ?>
									</div>
								</div>
								<?php if ($this->data->payment_status != 12) { ?>
									<div class="list-group-item">
										<p class="m-b-15"><strong><?php echo $howTo->title; ?></strong></p>
										<div class="m-b-15"><?php echo $howTo->wysiwyg_text; ?></div>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
						<div class="list-group m-b-15">
							<div class="list-group-item">
								<h5 class="no-margin"><?php echo JText::_('ORDER_DETAIL'); ?></h5>
							</div>
							<div class="list-group-item">
								<?php
								if ($this->data->cart_data) { ?>
									<?php foreach ($this->data->cart_data as $item) { ?>
										<pre><?php /*print_r($item); */ ?></pre>
										<div class="m-b-15">
											<div class="form-group-attached">
												<div class="form-group form-group-default d-flex justify-content-between">
													<div>
														<div><strong><?php echo $item->package->title; ?></strong></div>
														<div><?php echo $item->package->description; ?></div>
													</div>
													<div class="padding-5">
														<strong>Rp. <?php echo number_format($item->price); ?></strong>
													</div>
												</div>
												<div class="row clearfix">
													<div class="col-sm-8">
														<div class="form-group form-group-default">
															<label>Nama</label>
															<div><?php echo $item->name; ?></div>
														</div>
													</div>
													<div class="col-sm-4">
														<div class="form-group form-group-default">
															<label>Keanggotaan</label>
															<div><?php echo $item->npa ? $item->package_member : 'non-anggota'; ?></div>
														</div>
													</div>
												</div>
												<div class="form-group form-group-default">
													<label>Email</label>
													<div><?php echo $item->email; ?></div>
												</div>
												<div class="form-group form-group-default">
													<label>Bidang</label>
													<div><?php echo $item->bidang; ?></div>
												</div>
												<div class="form-group form-group-default">
													<label>Pilihan Workshop</label>
													<div class="size text-small">
														<ul class="opt-desc p-l-15 m-b-5">
															<?php foreach ($item->workshops as $workshop) { ?>
																<li><strong><?php echo $workshop->timeline; ?></strong> -
																	<?php echo $workshop->code . ' ' . $workshop->name; ?></li>
															<?php } ?>
														</ul>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					<?php } else { ?>
						<div class="alert alert-danger text-center">
							<h5 class="pink">Transaction not found!</h5>
						</div>
					<?php } ?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<?php
					$document = JFactory::getDocument();
					$renderer = $document->loadRenderer('module');
					$module = JModuleHelper::getModule('login', 'Sidebar Login');
					echo $renderer->render($module);
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	function payment(id) {
		var url = '<?php echo JURI::base() . 'cart'; ?>';
		var postdata = {
			task: 'payment',
			id: id
		};

		postdata['<?php echo JUtility::getToken(); ?>'] = 1;

		snap.show();

		$.post(url, postdata, function (response) {
			console.log(response);
			var jsonResponse = JSON.parse(response);
			if (jsonResponse.error_messages) {

			}
			snap.pay(jsonResponse.token);
		})
	}

	$(document).ready(function () {
		$('#midtransPaymentButton').click(function (e) {
			e.preventDefault();
			var id = $(this).attr('data-id');
			payment(id);
		})
	})
</script>
<?php } ?>