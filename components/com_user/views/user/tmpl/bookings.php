<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$baseUrl = JURI::base();

JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_cckjseblod' . DS . 'models');
JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_eventorg' . DS . 'models');

$user_model = $this->getModel();
$cart_model =& JModel::getInstance('cart', 'CCKjSeblodModel');
$event_model =& JModel::getInstance('manage', 'EventOrgModel');

// echo '<pre>';print_r($bookings);echo '</pre>';

if(JRequest::getVar('platform') === 'api'){
	$result = new stdClass();
	$result->booking = $this->data;

	echo json_encode($result);
} else {

?>

<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right">
				<div class="list-group">
					<div class="list-group-item">
						<h5 class="no-margin"><?php echo JText::_('MY_BOOKING'); ?></h5>
					</div>
					<?php
					if ($this->data) {
						foreach ($this->data as $booking) {
							?>
							<a class="d-flex list-group-item justify-content-between"
								 href="<?php echo $baseUrl; ?>my-booking?id=<?php echo $booking->id; ?>"
								 title="<?php echo $booking->title; ?>"
							>
								<div>
									<h5 class="no-margin"><?php echo $booking->title; ?> - <?php echo $booking->package_name; ?></h5>
									<p><strong><?php echo JHTML::_('date', $booking->start_date); ?></strong></p>
								</div>
								<div>
									<span class="badge"><?php echo $cart_model->statusLabel($booking->status); ?></span>
								</div>

							</a>
							<?php
						}
					}
					?>

				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php
				$document = JFactory::getDocument();
				$renderer = $document->loadRenderer('module');
				$module = JModuleHelper::getModule('login', 'Sidebar Login');
				echo $renderer->render($module);
				?>
			</div>
		</div>
	</div>
</div>
<?php } ?>