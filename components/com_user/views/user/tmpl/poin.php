<?php // no direct access
defined('_JEXEC') or die('Restricted access');
require_once(JPATH_SITE . DS . 'components' . DS . 'com_content' . DS . 'helpers' . DS . 'route.php');

JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_cckjseblod' . DS . 'models');
$cart_model = &JModel::getInstance('cart', 'CCKjSeblodModel');


$user  = JFactory::getUser();
$model = $this->getModel();
$poin = $model->getPoints();
$id = base64_decode(JRequest::getVar('id'));
$ts = $model->viewTransaction($id);

$shipment = json_decode($ts->shipment);

$pi = json_decode($ts->products);

//echo'<pre>';print_r($ts->products);echo'</pre>';

if ($ts->resi) {
  $resi = json_decode($ts->resi);
  $tracking = $cart_model->getShippingTracking($resi->code, $resi->courier);
  //echo'<pre>';print_r($tracking);echo'</pre>';
}


?>

<div id="content">
  <div class="container">
    <div class="cart_c frm">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right">
          <div class="row">
             <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
          <div class="list-group">
            <div class="list-group-item">
              <div class="row">
                <div class="col-xs-12">
                  <h5 class="pink">Active Point</h5>
                </div>
              </div>
            </div>
            <div class="list-group-item">
              <div style="font-size: 40px; font-weight: bold; text-align: center;"><?php if($poin->totals->active){ echo $poin->totals->active; }else{ echo 0; } ?></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
          <div class="list-group">
            <div class="list-group-item">
              <div class="row">
                <div class="col-xs-12">
                  <h5 class="pink">Used Point</h5>
                </div>
              </div>
            </div>
            <div class="list-group-item">
              <div style="font-size: 40px; font-weight: bold; text-align: center;"><?php if($poin->totals->used){ echo $poin->totals->used; }else{ echo 0; } ?></div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
          <div class="list-group">
            <div class="list-group-item">
              <div class="row">
                <div class="col-xs-12">
                  <h5 class="pink">All Point</h5>
                </div>
              </div>
            </div>
            <div class="list-group-item">
              <div style="font-size: 40px; font-weight: bold; text-align: center;"><?php if($poin->totals->total){ echo $poin->totals->total; }else{ echo 0; }; ?></div>
            </div>
          </div>
        </div>
          </div>
          <div class="list-group">
            <div class="list-group-item">
              <div class="row">
                <div class="col-xs-6">
                  <h5 class="pink">Earned</h5>
                </div>
                <div class="col-xs-6 text-right">
                  Total : <strong><?php echo $poin->totals->total; ?></strong>
                </div>
              </div>
            </div>
            <div class="list-group-item">
              <table class="table table-striped m-b-0">
                <tbody>
                  <?php for ($i = 0; $i < count($poin->earned); $i++) { ?>
                    <tr>
                      <td><strong>From transaction #<?php echo $poin->earned[$i]->order_id; ?></strong></td>
                      <td><?php echo $poin->earned[$i]->value; ?> poin</td>
                      <td align="right"><?php echo $poin->earned[$i]->date; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="list-group">
            <div class="list-group-item">
              <div class="row">
                <div class="col-xs-6">
                  <h5 class="pink">Used</h5>
                </div>
                <div class="col-xs-6 text-right">
                  Total : <strong><?php echo $poin->totals->used; ?></strong>
                </div>
              </div>
            </div>
            <div class="list-group-item">
              <table class="table table-striped m-b-0">
                <tbody>
                  <?php for ($i = 0; $i < count($poin->used); $i++) { ?>
                    <tr>
                      <td><strong>Used in transaction #<?php echo $poin->used[$i]->order_id; ?></strong></td>
                      <td><?php echo $poin->used[$i]->value; ?> poin</td>
                      <td align="right"><?php echo $poin->used[$i]->date; ?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
          <?php
          $document = JFactory::getDocument();
          $renderer = $document->loadRenderer('module');
          $module = JModuleHelper::getModule('login', 'Sidebar Login');
          echo $renderer->render($module);
          ?>
        </div>
      </div>
    </div>
  </div>
</div>