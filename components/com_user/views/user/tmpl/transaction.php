<?php // no direct access
defined('_JEXEC') or die('Restricted access');

if(JRequest::getVar('platform') === 'api'){
	$result = new stdClass();

	foreach ($this->data as $data) {
		$data->cart_data = json_decode($data->cart_data);
		$data->ext_response = json_decode($data->ext_response);
	}
	$result->transaction = $this->data;
	$result->pagination = (object) array(
		'start' => $this->pagination->get('limitstart'),
		'limit' => $this->pagination->get('limit'),
		'total' => $this->pagination->get('total')
	);
	echo json_encode($result);
} else {

// echo '<pre>';print_r($this->data[0]);echo '</pre>';

?>

<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<div class="container ">
		<div class="row m-b-30">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right">
				<div class="list-group m-b-5">
					<div class="list-group-item">
						<h5 class="no-margin">List Transaksi</h5>
					</div>
					<?php
					if ($this->data) {
						// echo '<pre>';print_r($this->data[0]);echo '</pre>';
						foreach ($this->data as $transaction) {
							?>
							<a class="d-flex list-group-item justify-content-between"
								 href="<?php echo $baseUrl; ?>transactions?id=<?php echo $transaction->id; ?>"
								 title="Transaksi ID-<?php echo $transaction->id; ?>"
							>
								<div>
									<h5 class="no-margin">Transaksi ID-<?php echo $transaction->id; ?></h5>
									<p><?php echo JHTML::_('date', $transaction->created_date, JText::_('DATE_FORMAT_LC2')); ?></p>
								</div>
								<div style="text-align: right;">
									<div class="m-b-5">Rp.<?php echo number_format($transaction->value); ?></div>
									<span class="badge"><?php echo $transaction->status_name; ?></span>
								</div>

							</a>
							<?php
						}
					}
					?>
				</div>
				<?php echo $this->pagination->getPagesLinks(); ?>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<?php
				$document = JFactory::getDocument();
				$renderer = $document->loadRenderer('module');
				$module = JModuleHelper::getModule('login', 'Sidebar Login');
				echo $renderer->render($module);
				?>
			</div>
		</div>
	</div>
</div>
<?php } ?>