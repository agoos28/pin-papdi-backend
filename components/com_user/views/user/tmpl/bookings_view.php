<?php // no direct access
defined('_JEXEC') or die('Restricted access');
$model = $this->getModel();


if(JRequest::getVar('platform') === 'api'){
	$result = new stdClass();
	$result->booking = $this->data;
	$result->qr = JURI::base() . 'files/booking/' . $this->data->id . '.jpg';

	$result->workshops = json_decode($this->data->workshops);

	foreach ($result->workshops as $workshop) {
		$workshop->timeline = $model->getTimeline($workshop->timeline_id);
		$workshop->links = $model->getActivityLinks($workshop->id);
	}

	echo json_encode($result);
} else {
?>

<div class="content ">
	<div class="jumbotron" data-pages="parallax">
		<div class="container  sm-p-l-20 sm-p-r-20">
			<div class="inner" style="transform: translateY(0px); opacity: 1;">
				<!-- START BREADCRUMB -->
				<ul class="breadcrumb">
					<li>
						<p>Pages</p>
					</li>
					<li><a href="#" class="active">Blank template</a>
					</li>
				</ul>
				<!-- END BREADCRUMB -->
			</div>
		</div>
	</div>
	<!-- START CONTAINER FLUID -->
	<div class="container ">
		<div class="cart_c frm">
			<div class="row m-b-30">
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 pull-right">
					<?php if ($this->data) {
						$this->data->workshops = json_decode($this->data->workshops);
						?>
						<div class="row m-b-15">
							<div class="col-md-4 col-sm-6 col-xs-12">
								<img style="width: 100%;"
										 src="<?php echo JURI::base() . 'files/booking/' . $this->data->id . '.jpg'; ?>"/>
							</div>
							<div class="col-md-8 col-sm-6 col-xs-12">
								<div class="list-group m-b-15">
									<div class="list-group-item d-flex justify-content-between">
										<h5 class="no-margin">
											<?php echo $this->data->event->title; ?>
										</h5>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Tanggal</strong>
										</div>
										<div style="text-transform: capitalize;">
											<?php echo JHTML::_('date', $this->data->event->start_date); ?>
										</div>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Paket</strong>
										</div>
										<div style="text-transform: capitalize;">
											<?php echo $this->data->package_title; ?>
										</div>
									</div>
									<div class="d-flex list-group-item justify-content-between">
										<div>
											<strong>Status</strong>
										</div>
										<div style="text-transform: capitalize;">
											<?php echo $model->statusLabel($this->data->status); ?>
										</div>
									</div>
								</div>
								<a target="_blank" class="btn btn-success" href="<?php echo JURI::base(); ?>transactions?layout=bookings&id=<?php echo $this->data->id; ?>&format=pdf">Download PDF</a>
							</div>
						</div>

						<div class="list-group m-b-15">
							<div class="list-group-item d-flex justify-content-between">
								<h5 class="no-margin">
									Agenda Workshop
								</h5>
							</div>
							<?php foreach ($this->data->workshops as $workshop) {
								$timeline = $model->getTimeline($workshop->timeline_id);
								$links = $this->data->status == 12 ? $model->getActivityLinks($workshop->id) : null;
								?>
								<div class="list-group-item">
									<div style="flex-grow: 1">
										<div>
											<strong><?php echo $workshop->code . ' ' . $workshop->name; ?></strong>
										</div>
										<div>
											<?php echo JHTML::_('date', $timeline->start_time, '%A, %d %b %Y %H:%M'); ?> s/d <?php echo JHTML::_('date', $timeline->end_time, '%H:%M'); ?>
										</div>
									</div>
									<div style="margin-top: 8px">
										<a target="_blank" href="<?php echo $links->zoomlink; ?>"
											 class="btn btn-xs <?php echo $links->zoomlink ? 'btn-complete' : 'disabled'; ?>"
											 >ZOOM Link</a>
										<a target="_blank" href="<?php echo $links->pretestlink; ?>"
											 class="btn btn-xs <?php echo $links->pretestlink ? 'btn-success' : 'disabled'; ?>"
										>Pre Test Link</a>
										<a target="_blank" href="<?php echo $links->posttestlink; ?>"
											 class="btn btn-xs <?php echo $links->posttestlink ? 'btn-success' : 'disabled'; ?>"
										>Post Test Link</a>
									</div>
								</div>
							<?php } ?>
						</div>
					<?php } else { ?>
						<div class="alert alert-danger text-center">
							<h5 class="pink">Booking not found!</h5>
						</div>
					<?php } ?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<?php
					$document = JFactory::getDocument();
					$renderer = $document->loadRenderer('module');
					$module = JModuleHelper::getModule('login', 'Sidebar Login');
					echo $renderer->render($module);
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php } ?>
