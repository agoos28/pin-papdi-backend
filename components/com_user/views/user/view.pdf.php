<?php
/**
 * @version    $Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
 * @package    Joomla
 * @subpackage  Weblinks
 * @copyright  Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license    GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

/**
 * HTML View class for the Users component
 *
 * @static
 * @package    Joomla
 * @subpackage  Weblinks
 * @since 1.0
 */
class UserViewUser extends JView
{
	function display($tpl = null)
	{
		global $mainframe;

		$model =& $this->getModel();
		$layout = $this->getLayout();


		if ($layout == 'transaction') {
			if (JRequest::getVar('id')) {
				$transaction = $model->getOrder(JRequest::getVar('id'));
				$this->renderTransaction($transaction);
			}
		}

		if ($layout == 'bookings') {
			if (JRequest::getVar('id')) {
				$booking = $model->getBooking(JRequest::getVar('id'));
				$this->renderBooking($booking);
			}
		}
	}

	function getHowToPay($transaction){
		$va = '';
		$text = '';

		$contentId = array(
			//transfer payment
			'bca' => 780,
			'mandiri' => 781,
			'bni' => 782,
			'permata' => 783,
			//instant payment
			'bca_klikpay' => 780,
			'akulaku' => 781,
			'cimb_clicks' => 782,
			'danamon_online' => 783,
			'bri_epay' => 783
		);

		$instantPaymentType = array('bca_klikpay', 'akulaku', 'cimb_clicks', 'danamon_online', 'bri_epay');

		if (in_array($transaction->ext_response->payment_type, $instantPaymentType)) {
			$method = $transaction->ext_response->payment_type;
			$text = $this->getRawContent($contentId[$transaction->ext_response->payment_type]);
		}

		if ($transaction->ext_response->payment_type === 'bank_transfer' || $transaction->ext_response->payment_type === 'echannel') {

			if ($transaction->ext_response->va_numbers) {
				$bank = $transaction->ext_response->va_numbers[0]->bank;
				$va = $transaction->ext_response->va_numbers[0]->va_number;
			}
			if ($transaction->ext_response->bill_key) {
				$bank = 'mandiri';
				$biller = $transaction->ext_response->biller_code;
				$va = $transaction->ext_response->bill_key;
			}
			if ($transaction->ext_response->permata_va_number) {
				$bank = 'permata';
				$va = $transaction->ext_response->permata_va_number;
			}
			$text = $this->getRawContent($contentId[$bank]);
			$method = 'Bank Transfer '.$bank;
		}

		return (object)array(
			'method' => $method,
			'biller' => $biller,
			'va' => $va,
			'expired' => JHTML::_('date', strtotime($transaction->date. ' + 1 days'), JText::_('DATE_FORMAT_LC2')),
			'text' => $text->wysiwyg_text
		);
	}

	function renderTransaction($transaction)
	{
		$document = &JFactory::getDocument();

		// set document information
		$document->setTitle('Transaction #' . $transaction->id);
		$document->setName('Transaction #' . $transaction->id);

		$howToPay = $this->getHowToPay($transaction);

		$peserta = '';

		$status = array(
			10 => 'Belum Terbayar',
			12 => 'Disetujui/Lunas',
			13 => 'Pending',
			14 => 'Dibatalkan',
			15 => 'Ditolak'
		);

		foreach($transaction->cart_data as $item) {

			$workshopItems = '<ol>';
			if(count($item->workshops)){
				foreach ($item->workshops as $workshop) {
					$workshopItems .= '<li>'.  $workshop->code . ' ' . $workshop->name . '</li>';
				}
			}
			$workshopItems .= '</ol>';

			$rows = array(
				'Nama' => '<strong>'.$item->name.'</strong>',
				'Paket' => $item->title.'<br/>'.$item->package->description,
				'Bidang' => $item->bidang,
				'Email' => $item->email,
				'Telpon' => $item->phone,
				'Harga' => 'Rp.'.number_format($item->price),
				'Workshops' => $workshopItems
			);
			$peserta .= '<br pagebreak="true" /><h1>&nbsp;</h1><h1>Data Peserta</h1><table width="100%" cellpadding="4" style="border: 0.1px solid #ccc;border-collapse: collapse;border-spacing:0px 0px"><tbody>';
			foreach ($rows as $key => $value){
				$peserta .= '<tr>';
				$peserta .= '<td style="width: 15%; text-align: left; vertical-align: top; border-bottom: 0.1px solid #ccc;"><strong>'.$key.'</strong></td>';
				$peserta .= '<td style="width: 5%; text-align: center; vertical-align: top; border-bottom: 0.1px solid #ccc;">:</td>';
				$peserta .= '<td style="width: 80%; text-align: left; vertical-align: top; border-bottom: 0.1px solid #ccc;">'.$value.'</td>';
				$peserta .= '</tr>';
			}
			$peserta .= '</tbody></table><h4>&nbsp;</h4>';
		};

		$paymentMethod = '';
		if($transaction->method === 'midtrans') {
			$paymentMethod .= $howToPay->method . '<br/>';
		} else {
			$paymentMethod .= $transaction->method . '<br/>';
		}
		if($howToPay->va){
			$paymentMethod .= 'Kode Pembayaran/Virtual account <strong>'.$howToPay->va.'</strong><br/>Bayarkan sebelum '.$howToPay->expired;
		}

		$contentData = array(
			'{id}' => $transaction->id,
			'{date}' => JHTML::_('date', $transaction->date, JText::_('DATE_FORMAT_LC2')),
			'{name}' => $transaction->name,
			'{email}' => $transaction->email,
			'{status}' => $status[$transaction->status],
			'{total}' => 'Rp.'.number_format($transaction->value),
			'{paymentMethod}' => $paymentMethod,
			'{paymentChannel}' => $howToPay->channel,
			'{paymentCode}' => $howToPay->va ? '<h1>Kode bayar '.$howToPay->va. '</h1><p>Batas waktu pembayaran '.$howToPay->expired.'</p>' : '',
			'{howToPay}' => $howToPay->text,
			'{dataPeserta}' => $peserta
		);
		// echo '<pre>';print_r($transaction);echo '</pre>';

		echo $this->createPdfTemplate($contentData, 787); // die();
		return;
	}

	function renderBooking($booking)
	{
		$model = $this->getModel();
		$document = &JFactory::getDocument();

		// set document information
		$document->setTitle('Booking #' . $booking->id);
		$document->setName('Booking #' . $booking->id);

		$booking->workshops = json_decode($booking->workshops);

		// print_r($booking);die();

		$workshops = '';
		foreach ($booking->workshops as $workshop){
			$timeline = $model->getTimeline($workshop->timeline_id);
			$workshops .= '<p><strong>'.$workshop->timeline.' - '. $workshop->name.'</strong>';
			$workshops .= '<br/>'. JHTML::_('date', $timeline->start_time, '%A, %d %b %Y %H:%M') .' s/d '. JHTML::_('date', $timeline->end_time, '%H:%M') .'</p>';
		}

		$contentData = array(
			'{name}' => $booking->user_name,
			'{event}' => $booking->event->title,
			'{event_date}' => JHTML::_('date', $booking->event->start_date),
			'{package}' => $booking->package_title,
			'{email}' => $booking->user_email,
			'{workshops}' => $workshops,
			'{qr_code}' => '<img style="width: 200px; height: 200px; border: 1px;" src="'.JURI::base() . 'files/booking/' . $booking->id . '.jpg" />',
		);

		//echo '<pre>';print_r($booking);echo '</pre>';

		echo (string)$this->createPdfTemplate($contentData, 786);  //die();
		return;
	}

	function createPdfTemplate($contentData, $templateId)
	{

		$template = $this->getRawContent($templateId);
		$html = strtr($template->wysiwyg_text, $contentData);

		$css = '<style>table{border-spacing:0px 4px;}h5{font-size: 22px; line-height: 6px;}</style>';
		$content = '' . $css . '' . $html . '';

		return $content;
	}

	function getRawContent($cid = null)
	{
		if ($cid == null) {
			return 'Content not found!';
		}
		$db = JFactory::getDBO();
		$query = "SELECT * FROM #__content WHERE id = " . $cid;
		$db->setQuery($query);
		$result = $db->loadObject();
		$error = $db->getErrorMsg();

		if ($error) {
			return $error;
		}
		$group = array();
		$regex = '#::(.*?)::(.*?)::/(.*?)::#s';
		preg_match_all($regex, $result->introtext, $matches);

		if (sizeof($matches[1])) {
			foreach ($matches[1] as $key => $val) {
				$gmatch = explode('|', $val);
				if (count($gmatch) > 1) {
					$gkey = $gmatch[1];
					$iname = $gmatch[0];
					$gname = $gmatch[2];
					$group[$gkey][$iname] = $matches[2][$key];
					$res[$gname] = $group;
				} else {
					$res[$val] = $matches[2][$key];
				}
			}
		}
		$res = (object)$res;
		$res->id = $cid;
		$res->stock = $result->stock;
		$res->title = $result->title;
		return $res;
	}

}
