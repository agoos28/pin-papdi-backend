<?php
/**
 * @version    $Id: view.html.php 14401 2010-01-26 14:10:00Z louis $
 * @package    Joomla
 * @subpackage  Weblinks
 * @copyright  Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license    GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');


/**
 * HTML View class for the Users component
 *
 * @static
 * @package    Joomla
 * @subpackage  Weblinks
 * @since 1.0
 */
class UserViewUser extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$model =& $this->getModel();

		$layout = $this->getLayout();
		if ($layout == 'form') {
			$this->_displayForm($tpl);
			return;
		}

		if ($layout == 'login') {
			parent::display($tpl);
			return;
		}

		if ($layout == 'transaction') {
			if (JRequest::getVar('id')) {
				if(strlen(JRequest::getVar('id')) > 10){
					$id = $model->getOrderIdFromExt(JRequest::getVar('id'));
				}else{
					$id = JRequest::getVar('id');
				}
				$transaction = $model->getOrder($id);
				$this->assignRef('data', $transaction);
				parent::display('view');
			}else if(JRequest::getVar('order_id')){
				$transaction = $model->getOrder(JRequest::getVar('order_id'));
				$this->assignRef('data', $transaction);
				parent::display('view');
			}else{
				$transactions = $model->getOrders();
				$this->assignRef('data', $transactions->data);
				$this->assignRef('pagination', $transactions->pagination);
				parent::display($tpl);

			}
			return;
		}

		if ($layout == 'bookingviewnologin') {
			global $mainframe;
			$session = &JFactory::getSession();
			$id = $session->get('viewBookingId');
			if (!$id) {
				$mainframe->redirect(JURI::base().'event-login', 'Autentikasi dibutuhkan!');
				return;
			}

			$booking = $model->getBooking($id);
			$this->assignRef('data', $booking);
			parent::display($tpl);
			return;
		}

		if ($layout == 'bookings') {
			if (JRequest::getVar('id')) {
				$booking = $model->getBooking(JRequest::getVar('id'));
				$this->assignRef('data', $booking);
				parent::display('view');
			}else{
				$bookings = $model->getBookings();
				$this->assignRef('data', $bookings);
				// $this->assignRef('pagination', $bookings->pagination);
				parent::display($tpl);
			}
			return;
		}

		if ($layout == 'transaction_pdf') {
			if (JRequest::getVar('id')) {
				$model->viewTransaction(JRequest::getVar('id'));
				jimport('joomla.document.pdf.pdf');
				JDocument::render();
			}
			parent::display($tpl);
			return;
		}


		$user =& JFactory::getUser();
		$profile = $user->getUserProfile();
		// Get the page/component configuration
		$params = &$mainframe->getParams();

		$menus = &JSite::getMenu();
		$menu = $menus->getActive();

		// because the application sets a default page title, we need to get it
		// right from the menu item itself
		if (is_object($menu)) {
			$menu_params = new JParameter($menu->params);
			if (!$menu_params->get('page_title')) {
				$params->set('page_title', JText::_('Registered Area'));
			}
		} else {
			$params->set('page_title', JText::_('Registered Area'));
		}
		$document = &JFactory::getDocument();
		$document->setTitle($params->get('page_title'));

		// Set pathway information
		$this->assignRef('user', $user);
		$this->assignRef('profile', $profile);
		$this->assignRef('params', $params);

		parent::display($tpl);
	}

	function _displayForm($tpl = null)
	{
		global $mainframe;

		// Load the form validation behavior
		JHTML::_('behavior.formvalidation');

		$user =& JFactory::getUser();
		$profile = $user->getUserProfile();
		$params = &$mainframe->getParams();

		// check to see if Frontend User Params have been enabled
		$usersConfig = &JComponentHelper::getParams('com_users');
		$check = $usersConfig->get('frontend_userparams');

		if ($check == '1' || $check == 1 || $check == NULL) {
			if ($user->authorize('com_user', 'edit')) {
				$params = $user->getParameters(true);
			}
		}
		$params->merge($params);
		$menus = &JSite::getMenu();
		$menu = $menus->getActive();

		// because the application sets a default page title, we need to get it
		// right from the menu item itself
		if (is_object($menu)) {
			$menu_params = new JParameter($menu->params);
			if (!$menu_params->get('page_title')) {
				$params->set('page_title', JText::_('Edit Your Details'));
			}
		} else {
			$params->set('page_title', JText::_('Edit Your Details'));
		}
		$document = &JFactory::getDocument();
		$document->setTitle($params->get('page_title'));

		$this->assignRef('user', $user);
		$this->assignRef('profile', $profile);
		$this->assignRef('params', $params);

		parent::display($tpl);
	}
}
