<?php
/**
 * @version		$Id: user.php 14401 2010-01-26 14:10:00Z louis $
 * @package		Joomla
 * @subpackage	User
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant to the
 * GNU General Public License, and as distributed it includes or is derivative
 * of works licensed under the GNU General Public License or other free or open
 * source software licenses. See COPYRIGHT.php for copyright notices and
 * details.
 */

// Check to ensure this file is included in Joomla!
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.model');
jimport('joomla.html.pagination');
JModel::addIncludePath(JPATH_SITE . DS . 'components' . DS . 'com_cckjseblod' . DS . 'models');
JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_users' . DS . 'tables');


/**
 * User Component User Model
 *
 * @package		Joomla
 * @subpackage	User
 * @since 1.5
 */
class UserModelUser extends JModel
{
	/**
	 * User id
	 *
	 * @var int
	 */
	var $_id = null;

	/**
	 * User data
	 *
	 * @var array
	 */
	var $_data = null;
	var $_user = null;
	var $transaction = null;
	var $tr_total = null;
	var $pageNav = null;
	var $cart_model = null;
	/**
	 * Constructor
	 *
	 * @since 1.5
	 */
	function __construct()
	{
		parent::__construct();

		$id = JRequest::getVar('id', 0, '', 'int');
		$this->setId($id);
		$this->_user = JFactory::getUser();
		$this->cart_model = JModel::getInstance('cart', 'CCKjSeblodModel');
	}

	/**
	 * Method to set the weblink identifier
	 *
	 * @access	public
	 * @param	int Weblink identifier
	 */
	function setId($id)
	{
		// Set weblink id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	/**
	 * Method to get a user
	 *
	 * @since 1.5
	 */
	function &getData()
	{
		// Load the weblink data
		if ($this->_loadData()) {
			//do nothing
		}

		return $this->_data;
	}

	function updateUserProfile($user, $data)
	{
		$profile = JTable::getInstance('profile');

		$profileData = array(
			'user_id' => $user->id,
			'org_name' => $data->org_name,
			'org_address' => $data->org_address,
			'address' => $data->address,
			'country' => $data->country,
			'city' => $data->city,
			'province' => $data->province,
			'postal' => $data->postal,
		);

		(!$profile->bind($profileData)) && die($profile->getError());
		(!$profile->store()) && die($profile->getError());

		$this->updateUserOrganization($data->org_name, $data->org_address);

		return $profileData;
	}

	/**
	 * Method to store the user data
	 *
	 * @access	public
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function store($data)
	{
		$user		= JFactory::getUser();
		$profile = JTable::getInstance('profile');

		$username	= $user->get('username');
		$id =  $user->get('id');

		$profileData = array(
			'user_id' => $id,
			'org_name' => $data['org_name'],
			'org_address' => $data['org_address'],
			'address' => $data['address'],
			'country' => $data['country'],
			'city' => $data['city'],
			'province' => $data['province'],
			'postal' => $data['postal'],
		);

		(!$profile->bind($profileData)) && die($profile->getError());
		(!$profile->store()) && die($profile->getError());

		// Bind the form fields to the user table
		if (!$user->bind($data)) {
			$this->setError($this->_db->getErrorMsg());
			return false;
		}

		// Store the web link table to the database
		if (!$user->save()) {
			$this->setError( $user->getError() );
			return false;
		}

		$session =& JFactory::getSession();
		$session->set('user', $user);

		// check if username has been changed
		if ( $username != $user->get('username') )
		{
			$table = $this->getTable('session', 'JTable');
			$table->load($session->getId());
			$table->username = $user->get('username');
			$table->store();
		}

		return true;
	}
	
	function gLogin($access_token) {	
	
		global $mainframe;
		
		$instance =& JFactory::getUser();
		
		$url = 'https://www.googleapis.com/plus/v1/people/me';			
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));
		$data = (object) json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);		
		
		if($http_code != 200) 
			throw new Exception('Error : Failed to get user information');
			
			//echo'<pre>';print_r($data);echo'</pre>';
			$credential = array( 
				'name' => $data->displayName,
				'username' => $data->emails[0]['value'], 
				'email' => $data->emails[0]['value'],
				'gpid' => $data->id
				);
				
		
		$options = array();
		$options['remember'] = JRequest::getBool('remember', true);
		$options['return'] =  JRequest::getVar('return');
		
		//echo'<pre>';print_r($credential);echo'</pre>';die();
		
		$auth = $mainframe->login($credential, $options);
		
		$redirect = base64_decode(JRequest::getVar('return'));

		if($data->emails[0]['value']){
			if($auth){
				$mainframe->redirect($redirect,'Welcome, '.$data->displayName, 'Success');
			}else{
				$mainframe->redirect($redirect,'Error login you in', 'Danger');
			}
		}else{
			$mainframe->redirect($redirect,'Error using G+ login', 'Danger');
		}
			
		return $data;
	}
	
	function fblogin($token = null){
		
		global $mainframe;
		
		$instance =& JFactory::getUser();

		//require_once(JPATH_COMPONENT.DS.'vendor'.DS.'facebook'.DS.'autoload.php' );
		
		/*$fb = new Facebook\Facebook([
			'app_id'                => '312985772505702',
			'app_secret'            => 'a75049f496e43fc7c05a1bf772541ee5',
			'default_graph_version' => 'v2.3',
		]);*/
		
		$session = JFactory::getSession();
		//$helper = $fb->getJavaScriptHelper();
		$redirect = JRequest::getVar('redirect');
		//$helper = $fb->getRedirectLoginHelper();
		
		if(!$token){
			$token = JRequest::getVar('fbtoken');
		}
		
		if(!$token){
			$token = $session->get('fbtoken');
		}
		
		if(!$token){
			$token = $helper->getAccessToken();
		}
		
		if(!$token){
			return false;
		}
		
		$session->set('fbtoken', $token);
		//$fb->setDefaultAccessToken($token);
		
		//get fb user
		//$res = $fb->get('/me/?fields=name,email');

		
		
		$ch = curl_init('https://graph.facebook.com/v2.8/me?access_token='.$token.'&fields=id%2Cname%2Cemail&format=json&method=get&pretty=0&suppress_http_code=1');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$json = curl_exec($ch);


		$fbuser = json_decode($json);
		
		$credential = array( 
				'name' => $fbuser->name,
				'username' => $fbuser->email, 
				'email' => $fbuser->email, 
				'fbid' => $fbuser->id
				);
				
		
		$options = array();
		$options['remember'] = JRequest::getBool('remember', true);
		$options['return'] =  JRequest::getVar('return');
		
		$auth = $mainframe->login($credential, $options);
		
		$redirect = base64_decode(JRequest::getVar('return'));

		if($fbuser->email){
			if($auth){
				$mainframe->redirect($redirect,'Welcome, '.$fbuser->name, 'Success');
			}else{
				$mainframe->redirect($redirect,'Error login you in', 'Danger');
			}
		}else{
			$mainframe->redirect($redirect,'Error using fb login', 'Danger');
		}
	}

	/**
	 * Method to load user data
	 *
	 * @access	private
	 * @return	boolean	True on success
	 * @since	1.5
	 */
	function _loadData()
	{
		// Lets load the content if it doesn't already exist
		if (empty($this->_data))
		{
			$this->_data =& JFactory::getUser();
			return (boolean) $this->_data;
		}
		return true;
	}

	function getOrder($id){

		$transaction = $this->cart_model->getTransaction($id);

		return $transaction;
	}

	function getEvent($id)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT a.*, b.title AS title ' .
			' FROM #__event AS a' .
			' LEFT JOIN #__content AS b ON b.id = a.content_id' .
			' WHERE a.id = ' . (int)$id;
		$db->setQuery($query);
		return $db->loadObject();
	}

	function getBooking($id){
		$query = "SELECT a.*, b.title as package_title FROM #__event_booking AS a" .
			" LEFT JOIN #__event_package AS b ON b.id = a.package_id" .
			" WHERE a.id = ". $id;
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		if ($result){
			$result->event = $this->getEvent($result->event_id);
		}
		return $result;
	}

	function getBookingFromTransactionId($id, $email){
		$query = "SELECT * FROM #__event_booking WHERE order_id = ".$id." AND  user_email = '".$email."'";
		$this->_db->setQuery($query);
		$result = $this->_db->loadObject();
		return $result;
	}

	function getBookings(){
		return $this->cart_model->getBookings($this->_user->email);
	}

	function getActivityLinks($activity_id){
		$query = "SELECT zoomlink, pretestlink, posttestlink FROM #__event_activity " .
			" WHERE id = ". $activity_id;
		$this->_db->setQuery($query);
		return $this->_db->loadObject();
	}

	function getTimeline($id){
		$query = "SELECT a.* FROM #__event_timeline AS a" .
			" WHERE a.id = ". $id;
		$this->_db->setQuery($query);
		return $this->_db->loadObject();
	}

	function getOrderIdFromExt($id){
		$query = "SELECT order_id FROM #__transaction_payment " .
			" WHERE ext_id = '$id'";
		$this->_db->setQuery($query);
		return $this->_db->loadResult();
	}

	function statusLabel($id)
	{
		$query = "SELECT name FROM #__status_code WHERE id = " . (int)$id;
		$this->_db->setQuery($query);
		return $this->_db->loadResult($query);
	}

	function getOrders()
	{

		global $mainframe;
		$result = new stdClass();
		$user	= JFactory::getUser();
		$userId = $user->id;
		$filter = JRequest::getVar('keyword');

		$lim = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', 10, 'int');
		$lim0 = JRequest::getVar('limitstart', 0, '', 'int');

		$where = ' WHERE a.user_id = '.(int)$userId;
		if ($filter) {
			if ((int)$filter) {
				$where = " AND a.id =" . (int)$filter;
			} else {
				$where = " AND c.name LIKE '%" . $filter . "%'";
			}

		}

		$query = "SELECT a.*, b.*, a.id AS id, a.status AS status, c.name AS status_name FROM #__transaction_order AS a" .
			" LEFT JOIN #__transaction_payment AS b ON b.order_id = a.id" .
			" LEFT JOIN #__status_code AS c ON c.id = a.status" .
			$where .
			" ORDER BY a.created_date DESC";
		$this->_db->setQuery($query, $lim0, $lim);
		$result->data = $this->_db->loadObjectList();
		$result->error = $this->_db->getErrorMsg();


		$result->pagination = new JPagination($this->getTotal($query), $lim0, $lim);

		return $result;
	}

	function getTotal($query)
	{
		// Load the content if it doesn't already exist
		if (empty($this->_total)) {
			$this->_total = $this->_getListCount($query);
		}
		return $this->_total;
	}
	
	function getRentedProducts($userid = 0){
		$user	= JFactory::getUser();
		
		if(!$userid){
			$userid = $user->id;
		}
		$db = JFactory::getDBO();
		$query = 	"SELECT b.*, c.title, c.introtext FROM #__cart_order AS a".
							" INNER JOIN #__booking AS b ON b.order_id = a.id".
							" LEFT JOIN #__content AS c ON c.id = b.product_id".
							" WHERE a.user = ".$userid." AND a.status = 'paid'".
							" AND b.pickup >= CURDATE()".
							" ORDER BY b.pickup";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$error = $db->getErrorMsg();
		
		if($error){
			JFactory::getApplication()->enqueueMessage('DB Query ERROR!, '.$error, 'ERROR');
		}
		return $result;
	}

	function getRawContent($cid = null)
	{
		if ($cid == null) {
			return 'Content not found!';
		}
		$db = JFactory::getDBO();
		$query = "SELECT * FROM #__content WHERE id = " . $cid;
		$db->setQuery($query);
		$result = $db->loadObject();
		$error = $db->getErrorMsg();

		if ($error) {
			return $error;
		}
		$group = array();
		$regex = '#::(.*?)::(.*?)::/(.*?)::#s';
		preg_match_all($regex, $result->introtext, $matches);

		if (sizeof($matches[1])) {
			foreach ($matches[1] as $key => $val) {
				$gmatch = explode('|', $val);
				if (count($gmatch) > 1) {
					$gkey = $gmatch[1];
					$iname = $gmatch[0];
					$gname = $gmatch[2];
					$group[$gkey][$iname] = $matches[2][$key];
					$res[$gname] = $group;
				} else {
					$res[$val] = $matches[2][$key];
				}
			}
		}
		$res = (object)$res;
		$res->id = $cid;
		$res->title = $result->title;
		return $res;
	}
	
	function getPoints(){
	  global $mainframe;
	  $result = new stdClass();
	  $user	= JFactory::getUser();
	  
	  $db = JFactory::getDBO();
	  
	  $user_id = $user->id;
	  
	  $query = "SELECT *"
		." FROM #__user_points"
		." WHERE user_id = $user_id"
		." ORDER BY date DESC";
	  
	  $db->setQuery($query);
	  $result->earned = $db->loadObjectList();
	  
	  $query = "SELECT *"
		." FROM #__user_point_usage"
		." WHERE user_id = $user_id"
		." ORDER BY date DESC";
	  
	  $db->setQuery($query);
	  $result->used = $db->loadObjectList();
	  
	  $query = "SELECT sum(a.value) AS total, sum(b.value) AS used FROM #__user_points AS a"
		." LEFT JOIN #__user_point_usage AS b ON b.user_id = $user_id WHERE a.user_id = $user_id OR b.user_id = $user_id";
	  $db->setQuery($query);
	  $result->totals = $db->loadObject();
	  
	  $result->totals->active = $result->totals->total - $result->totals->used;
	  
	  $user->set('points', $result->totals);
		
	  $error = $db->getErrorMsg();
	  
	  //print_r($query);
	  
	  if($error){
		JFactory::getApplication()->enqueueMessage('DB Query ERROR!, '.$error, 'ERROR');
		return;
	  }
	  return $result;
	}

	function getFiles($folderPath)
	{

		$path = JPATH_BASE . DS . 'files'. DS . $folderPath;
		$folder = JFolder::files($path);
		if(!$folder){
			return array();
		}
		$files = array();

		foreach ($folder as $file){
			$uri = $path.DS.$file;
			$date = filemtime($uri);
			$size = filesize($uri);
			$thumnail = 0;
			if (preg_match('#\.(jpg|jpeg|bmp|gif|tiff|png)#i', $file)) {
				$thumnail = 1;
			}

			$files[] = (object) array(
				'filename' => $file,
				'url' => JURI::base().'files/'.$folderPath.'/'.$file,
				'size' => $size,
				'modified' => $date,
				'thumbnail' => $thumnail
			);
		}

		return $files;
	}

	function getUserOrganizations($key = null) {
		$db = JFactory::getDBO();

		$query = "SELECT name FROM #__user_organization";
		if($key){
			$query .= " WHERE name LIKE '%$key%'";
		}
		$query .= " ORDER BY name ASC";

		$db->setQuery($query);

		return $db->loadResultArray();
	}
}
?>
