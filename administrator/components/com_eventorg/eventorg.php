<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Require the base controller
require_once(JPATH_COMPONENT . DS . 'controller.php');

$controller = new EventOrgController();

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
$controller->redirect();
