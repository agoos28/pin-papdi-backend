<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JElementItem extends JElement {
  /**
   * Element name
   *
   * @access protected
   * @var string
   */
  var $_name = 'Item';
  
  function fetchElement($name, $value, &$node, $control_name)
  {
	$db = &amp;
	JFactory::getDBO();
	$query = 'SELECT a.id, CONCAT( a.title ) AS text, a.catid '
	  . ' FROM #__eventorg AS a'
	  . ' INNER JOIN #__categories AS c ON a.catid = c.id'
	  . ' WHERE a.published = 1'
	  . ' AND c.published = 1'
	  . ' ORDER BY a.catid, a.title';
	$db->setQuery($query);
	$options = $db->loadObjectList();
	return JHTML::_('select.genericlist', $options, '' . $control_name . '[' . $name . ']', 'class="inputbox"', 'id', 'text', $value, $control_name . $name);
  }
}