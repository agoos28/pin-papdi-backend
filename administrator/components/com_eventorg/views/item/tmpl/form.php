<?php 

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );


?>
<?php JHTML::_('behavior.tooltip'); ?>
<?php // Set toolbar items for the page
  $edit = JRequest :: getVar('edit', true);
  $text = !$edit ? JText :: _('New') : JText :: _('Edit');
  JToolBarHelper :: title(JText :: _('Item') . ': <small>[ ' . $text . ' ]</small>');
  JToolBarHelper :: save();
  JToolBarHelper :: apply();
  if (!$edit) {
  JToolBarHelper :: cancel();
  } else {
  JToolBarHelper :: cancel('cancel', 'Close');
  }
?>
<script language="javascript" type="text/javascript">
<!--
 function submitbutton(pressbutton) {
  var form = document.adminForm;
  if (pressbutton == 'cancel') {
  submitform( pressbutton );
  return;
  }
 // do field validation
  if (form.title.value == ""){
  alert( "<?php echo JText::_( 'Please give a title', true ); ?>" );
  } else if (form.catid.value == "0"){
  alert( "<?php echo JText::_( 'Please select a category', true ); ?>" );
  } else {
  submitform( pressbutton );
  }
  }
  -->
</script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<table cellspacing="0" cellpadding="0" border="0" width="100%">
<tr>
<td width="50%" valign="top">
<fieldset class="adminform">
<legend><?php echo JText::_( 'Details' ); ?></legend>
<table class="admintable">
<tr>
<td width="100" align="right" class="key">
<label for="title">
<?php echo JText::_( 'Title' ); ?>:
</label>
</td>
<td>
<input class="text_area" type="text" name="title" id="title" size="48" maxlength="250" value="<?php echo $this->item->title;?>" />
</td>
</tr>
<tr>
<td width="100" align="right" class="key">
<label for="alias">
<?php echo JText::_( 'Alias' ); ?>:
</label>
</td>
<td>
<input class="text_area" type="text" name="alias" id="alias" size="32" maxlength="250" value="<?php echo $this->item->alias;?>" />
</td>
</tr>
<tr>
<td valign="top" align="right" class="key">
<?php echo JText::_( 'Published' ); ?>:
</td>
<td>
<?php echo $this->lists['published']; ?>
</td>
</tr>
<tr>
<td valign="top" align="right" class="key">
<label for="catid">
<?php echo JText::_( 'Category' ); ?>:
</label>
</td>
<td>
<?php echo $this->lists['catid']; ?>
</td>
</tr>
<td valign="top" align="right" class="key">
<label for="ordering">
<?php echo JText::_( 'Ordering' ); ?>:
</label>
</td>
<td>
<?php echo $this->lists['ordering']; ?>
</td>
</tr>
</table>
</fieldset>
<fieldset class="adminform">
<legend><?php echo JText::_( 'Custom Fields' ); ?></legend>
<table class="admintable" width="100%">Array
 <tr>
<td width="100" align="right" class="key">
<label for="text">
<?php echo JText::_( 'Text' ); ?>:
</label>
</td>
<td>
<input class="text_area" type="text" name="text" id="text" size="32" maxlength="250" value="<?php echo $this->item->text;?>" />
</td>
</tr>

 <tr>
<td width="100" align="right" class="key">
<label for="picture">
<?php echo JText::_( 'Picture' ); ?>:
</label>
</td>
<td>
<input class="text_area" type="text" name="picture" id="picture" size="32" maxlength="250" value="<?php echo $this->item->picture;?>" />
</td>
</tr>

 <tr>
<td width="100" align="right" class="key">
<label for="date">
<?php echo JText::_( 'Date' ); ?>:
</label>
</td>
<td>
<input class="text_area" type="text" name="date" id="date" size="32" maxlength="250" value="<?php echo $this->item->date;?>" />
</td>
</tr>


 </table>
  
</fieldset>
</td>
<td valign="top">
<fieldset class="adminform">
<legend><?php echo JText::_( 'More Fields' ); ?></legend>

todo...
</fieldset>
</td>
</tr>
</table>
<input type="hidden" name="option" value="com_eventorg" />
<input type="hidden" name="cid[]" value="<?php echo $this->item->id; ?>" />
<input type="hidden" name="task" value="" />
</form>