<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


/**
 * @package eventorg
 */
class EventOrgHelper {
  function checkCategories()
  {
	global $mainframe;
	$db =& JFactory::getDBO();
	$db->setQuery("SELECT * FROM #__categories WHERE `section`='com_eventorg'");
	$rows = $db->loadAssocList();
	if (empty($rows))
	{
	  $mainframe->enqueueMessage(JText::_('You have no categories configured yet'), 'notice');
	  return FALSE;
	}
	return TRUE;
  }
}