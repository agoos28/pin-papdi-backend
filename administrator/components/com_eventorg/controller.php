<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');
require_once(JPATH_COMPONENT . DS . 'helpers' . DS . 'helper.php');

/**
 * EventOrg Controller
 *
 * @package EventOrg
 */
class EventOrgController extends JController {
  function __construct()
  {
	parent::__construct();
	// Register Extra tasks
	$this->registerTask('add', 'display');
	$this->registerTask('edit', 'display');
  }
  
  function display()
  {
	switch ($this->getTask())
	{
	  case 'add' :
		{
		  JRequest::setVar('hidemainmenu', 1);
		  JRequest::setVar('layout', 'form');
		  JRequest::setVar('view', 'item');
		  JRequest::setVar('edit', FALSE);
		  // Checkout the item
		  $model = $this->getModel('item');
		  $model->checkout();
		}
		break;
	  case 'edit' :
		{
		  JRequest::setVar('hidemainmenu', 1);
		  JRequest::setVar('layout', 'form');
		  JRequest::setVar('view', 'item');
		  JRequest::setVar('edit', TRUE);
		  // Checkout the item
		  $model = $this->getModel('item');
		  $model->checkout();
		}
		break;
	}
	parent::display();
  }
  
  function save()
  {
	$this->store();
	$link = 'index.php?option=com_eventorg';
	$this->setRedirect($link, $this->msg);
  }
  
  function store()
  {
	$post = JRequest::get('post');
	$cid = JRequest::getVar('cid', array(0), 'post', 'array');
	$post['id'] = (int)$cid[0];
	$this->id = $post['id'];
	//$post['text'] = JRequest::getVar('text', '', 'post', 'string', JREQUEST_ALLOWRAW);
	$model = $this->getModel('item');
	if ($model->store($post))
	{
	  $this->msg = JText::_('Item Saved');
	} else
	{
	  $this->msg = JText::_('Error Saving Item');
	}
	// Check the table in so it can be edited.... we are done with it anyway
	$model->checkin();
  }
  
  function apply()
  {
	$this->store();
	$link = 'index.php?option=com_eventorg&view=item&task=edit&cid[]=' . $this->id;
	$this->setRedirect($link, $this->msg);
  }
  
  function remove()
  {
	$cid = JRequest::getVar('cid', array(), 'post', 'array');
	JArrayHelper::toInteger($cid);
	if (count($cid) < 1)
	{
	  JError::raiseError(500, JText::_('Select an item to delete'));
	}
	$model = $this->getModel('item');
	if ( ! $model->delete($cid))
	{
	  echo "<script> alert('" . $model->getError(TRUE) . "'); window.history.go(-1); </script>
";
	}
	$this->setRedirect('index.php?option=com_eventorg');
  }
  
  function publish()
  {
	$cid = JRequest::getVar('cid', array(), 'post', 'array');
	JArrayHelper::toInteger($cid);
	if (count($cid) < 1)
	{
	  JError::raiseError(500, JText::_('Select an item to publish'));
	}
	$model = $this->getModel('item');
	if ( ! $model->publish($cid, 1))
	{
	  echo "<script> alert('" . $model->getError(TRUE) . "'); window.history.go(-1); </script>
";
	}
	$this->setRedirect('index.php?option=com_eventorg');
  }
  
  function unpublish()
  {
	$cid = JRequest::getVar('cid', array(), 'post', 'array');
	JArrayHelper::toInteger($cid);
	if (count($cid) < 1)
	{
	  JError::raiseError(500, JText::_('Select an item to unpublish'));
	}
	$model = $this->getModel('item');
	if ( ! $model->publish($cid, 0))
	{
	  echo "<script> alert('" . $model->getError(TRUE) . "'); window.history.go(-1); </script>
";
	}
	$this->setRedirect('index.php?option=com_eventorg');
  }
  
  function cancel()
  {
	// Checkin the item
	$model = $this->getModel('item');
	$model->checkin();
	$this->setRedirect('index.php?option=com_eventorg');
  }
  
  function orderup()
  {
	$model = $this->getModel('item');
	$model->move(-1);
	$this->setRedirect('index.php?option=com_eventorg');
  }
  
  function orderdown()
  {
	$model = $this->getModel('item');
	$model->move(1);
	$this->setRedirect('index.php?option=com_eventorg');
  }
  
  function saveorder()
  {
	$cid = JRequest::getVar('cid', array(), 'post', 'array');
	$order = JRequest::getVar('order', array(), 'post', 'array');
	JArrayHelper::toInteger($cid);
	JArrayHelper::toInteger($order);
	$model = $this->getModel('item');
	$model->saveorder($cid, $order);
	$msg = 'New ordering saved';
	$this->setRedirect('index.php?option=com_eventorg', $msg);
  }
}
  