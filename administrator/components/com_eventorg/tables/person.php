<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTablePerson extends JTable
{

	var $id = NULL;
	var $activity_id = NULL;
	var $name = NULL;
	var $description = NULL;
	var $user_id = NULL;


	function __construct(& $db)
	{
		parent:: __construct('#__event_activity_person', 'id', $db);
	}

	function check()
	{
		return;
	}
}
