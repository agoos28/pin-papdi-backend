<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTableEventBooking extends JTable
{

	var $id = NULL;
	var $user_id = null;
	var $user_name = null;
	var $user_email = null;
	var $user_phone = null;
	var $user_npa = null;
	var $event_id = 0;
	var $package_id = null;
	var $price = null;
	var $order_id = null;
	var $qr_code = null;
	var $approval_letter = null;
	var $workshops = null;
	var $status = null;

	function __construct(& $db)
	{
		parent:: __construct('#__event_booking', 'id', $db);
	}

	function bind($array, $ignore = '')
	{
		if (key_exists('params', $array) && is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}
		return parent:: bind($array, $ignore);
	}

	function store($updateNulls = FALSE)
	{
		$this->check();
		return parent::store($updateNulls);
	}

	function check()
	{
		$email = $this->user_email;
		$event_id = $this->event_id;
		$query = "SELECT id FROM #__event_booking WHERE user_email = '$email' AND event_id = $event_id";
		$this->_db->setQuery($query);
		$this->id = $this->_db->loadResult();
	}
}
