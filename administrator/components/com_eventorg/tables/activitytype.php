<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTableActivityType extends JTable {

  var $id = NULL;
  var $name = null;
  var $description = null;
  

  function __construct(& $db)
  {
	parent:: __construct('#__event_activity_type', 'id', $db);
  }
  
  function bind($array, $ignore = '')
  {
	if (key_exists('params', $array) && is_array($array['params']))
	{
	  $registry = new JRegistry();
	  $registry->loadArray($array['params']);
	  $array['params'] = $registry->toString();
	}
	return parent:: bind($array, $ignore);
  }

  function check()
  {
	return;
  }
}
