<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTableBatchEmail extends JTable
{

	var $id = NULL;
	var $destination = NULL;
	var $subject = NULL;
	var $content = NULL;


	function __construct(& $db)
	{
		parent:: __construct('#__batch_email', 'id', $db);
	}

	function load() {
		$query = 'SELECT id FROM #__batch_email ORDER BY id ASC';
		$this->_db->setQuery($query, 0, 1);
		$xid = intval( $this->_db->loadResult() );
		return parent::load($xid);
	}

	function check()
	{
		return;
	}
}
