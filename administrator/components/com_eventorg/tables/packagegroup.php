<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTablePackageGroup extends JTable
{

	var $id = NULL;
	var $event_id = NULL;
	var $title = NULL;
	var $start_date = NULL;
	var $end_date = NULL;
	var $description = NULL;


	function __construct(& $db)
	{
		parent:: __construct('#__event_package_group', 'id', $db);
	}

	function check()
	{
		return;
	}
}
