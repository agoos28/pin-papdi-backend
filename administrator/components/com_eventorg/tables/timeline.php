<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTableTimeline extends JTable
{

	var $id = NULL;
	var $event_id = NULL;
	var $name = NULL;
	var $activity_type = NULL;
	var $start_time = NULL;
	var $end_time = NULL;


	function __construct(& $db)
	{
		parent:: __construct('#__event_timeline', 'id', $db);
	}

	function check()
	{
		$dateNow =& JFactory::getDate();
		if (!$this->start_time) {
			$this->start_time = $dateNow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
		if (!$this->end_time) {
			$this->end_time = $dateNow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
		return;
	}
}
