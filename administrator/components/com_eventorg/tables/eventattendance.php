<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTableEventAttendance extends JTable
{

	var $id = null;
	var $booking_id = null;
	var $event_id = null;
	var $activity_id = null;
	var $checked_in = null;
	var $checked_out = null;
	var $description = null;
	var $status = null;

	function __construct(& $db)
	{
		parent:: __construct('#__event_attendance', 'id', $db);
	}

	function bind($array, $ignore = '')
	{
		if (key_exists('params', $array) && is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}
		return parent:: bind($array, $ignore);
	}

	function store($updateNulls = FALSE)
	{
		$this->check();
		return parent::store($updateNulls);
	}

	function check()
	{
		$booking_id = $this->booking_id;
		$activity_id = $this->activity_id;
		$query = "SELECT id FROM #__event_attendance WHERE booking_id = '$booking_id' AND activity_id = $activity_id";
		$this->_db->setQuery($query);
		$this->id = $this->_db->loadResult();
	}
}
