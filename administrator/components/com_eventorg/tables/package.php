<?php

/**
 * Joomla! 1.5 component eventorg
 * Agoos28
 * @package eventorg
 * @license GNU Public License (because open source matters...)
 **/

// no direct access
defined('_JEXEC') or die('Restricted access');


// Include library dependencies
jimport('joomla.filter.input');

class JTablePackage extends JTable
{

	var $id = NULL;
	var $event_id = NULL;
	var $title = NULL;
	var $price = NULL;
	var $price_member = NULL;
	var $price_finasim = NULL;
	var $price_dokter_umum = NULL;
	var $description = NULL;
	var $package_group = NULL;
	var $package_options = NULL;
	var $package_rules = NULL;
	var $start_time = NULL;
	var $end_time = NULL;


	function __construct(& $db)
	{
		parent:: __construct('#__event_package', 'id', $db);
	}

	function check()
	{
		return;
	}
}
