<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			SEBLOD 1.x (CCK for Joomla!)
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Content Types	Table Class
 **/
class JTableTransactionOrder extends JTable
{
	/**
	 * Vars
	 **/


	var $id = null;
	var $user_id = null;
	var $value = null;
	var $product_id = null;
	var $product_type = null;
	var $cart_data = null;
	var $status = null;
	var $created_date = null;
	var $updated_date = null;

	/**
	 * Constructor
	 **/
	function __construct(& $db)
	{
		parent:: __construct('#__transaction_order', 'id', $db);
	}

	function bind($array, $ignore = '')
	{
		if (key_exists('params', $array) && is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}
		return parent:: bind($array, $ignore);
	}

	function check()
	{
		$dateNow =& JFactory::getDate();
		if (!$this->start_date) {
			$this->start_date = $dateNow->toFormat("%Y-%m-%d-%H-%M-%S");
		}
		if (!$this->end_date) {
			$this->end_date = $dateNow->toFormat("%Y-%m-%d-%H-%M-%S");
		}

		return;
	}
	
}
?>