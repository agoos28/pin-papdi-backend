<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			SEBLOD 1.x (CCK for Joomla!)
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Content Types	Table Class
 **/
class JTableCart extends JTable
{
	/**
	 * Vars
	 **/
	var $id = null;					//Int
	var $user_id = null;					//Int
	var $items = null;				//Varchar (50)

	/**
	 * Constructor
	 **/
	function __construct(& $db ) {
		parent::__construct( '#__cart', 'id', $db );
	}

	function store($updateNulls = FALSE)
	{
		$query = "SELECT id FROM #__cart WHERE user_id = ".(int) $this->user_id;
		$this->_db->setQuery($query);
		$id = $this->_db->loadResult();
		if($id){
			$this->id = $id;
		}
		return parent::store($updateNulls);
	}

	function load($user_id){
		$query = "SELECT items FROM #__cart WHERE user_id = ".(int)$user_id;
		$this->_db->setQuery($query);
		return $this->_db->loadResult();
	}
	
}
?>