<?php
/**
* @version 			1.9.0
* @author       	http://www.seblod.com
* @copyright		Copyright (C) 2012 SEBLOD. All Rights Reserved.
* @license 			GNU General Public License version 2 or later; see _LICENSE.php
* @package			SEBLOD 1.x (CCK for Joomla!)
**/

// No Direct Access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Content Types	Table Class
 **/
class JTableAddress extends JTable
{
	/**
	 * Vars
	 **/


	var $id = null;
	var $user_id = null;
	var $address = null;
	var $country = null;
	var $city = null;
	var $province = null;
	var $zip = null;

	/**
	 * Constructor
	 **/
	function __construct(& $db)
	{
		parent:: __construct('#__user_address', 'id', $db);
	}

	function bind($array, $ignore = '')
	{
		if (key_exists('params', $array) && is_array($array['params'])) {
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}
		return parent:: bind($array, $ignore);
	}

	function getId($id)
	{
		$query = 'SELECT id FROM #__user_address WHERE user_id = '.(int) $id;
		$this->_db->setQuery($query);
		return $this->_db->loadResult();
	}
	
}
?>