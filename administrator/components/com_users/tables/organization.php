<?php
/**
 * @version      1.9.0
 * @author        http://www.seblod.com
 * @copyright    Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license      GNU General Public License version 2 or later; see _LICENSE.php
 * @package      SEBLOD 1.x (CCK for Joomla!)
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');

/**
 * Templates    Table Class
 **/
class JTableOrganization extends JTable
{
	/**
	 * Vars
	 **/
	var $id = null;      //Int
	var $name = null;      //Varchar (50)
	var $address = null;      //Tinyint (4)

	/**
	 * Constructor
	 **/
	function JTableOrganization(& $db)
	{
		parent::__construct('#__user_organization', 'id', $db);
	}

	function store()
	{
		$name = $this->activity_id;
		$query = "SELECT id FROM #__user_organization WHERE name = '$name'";
		$this->_db->setQuery($query);
		$this->id = $this->_db->loadResult();
		return parent::store(); // TODO: Change the autogenerated stub
	}
}

?>