<?php
/**
 * @version      1.9.0
 * @author        http://www.seblod.com
 * @copyright    Copyright (C) 2012 SEBLOD. All Rights Reserved.
 * @license      GNU General Public License version 2 or later; see _LICENSE.php
 * @package      SEBLOD 1.x (CCK for Joomla!)
 **/

// No Direct Access
defined('_JEXEC') or die('Restricted access');

/**
 * Templates    Table Class
 **/
class JTableProfile extends JTable
{
	/**
	 * Vars
	 **/
	var $id = null;
	var $user_id = null;      //Int
	var $front_title = null;      //Varchar (50)
	var $back_title = null;      //Tinyint (4)
	var $picture = null;      //Tinyint (4)
	var $org_name = null;
	var $org_address = null;
	var $address = null;
	var $country = null;
	var $city = null;
	var $province = null;
	var $postal = null;

	/**
	 * Constructor
	 **/
	function __construct(&$db)
	{
		parent::__construct('#__user_profile', 'id', $db);
	}

	function store($updateNulls = FALSE)
	{
		$user_id = $this->user_id;
		$query = "SELECT id FROM #__user_profile WHERE user_id = $user_id";
		$this->_db->setQuery($query);
		$this->id = $this->_db->loadResult();
		return parent::store($updateNulls); // TODO: Change the autogenerated stub
	}
}

?>